// To test it locally execute the assume role from gateway to Development banking account
// export AWS_PROFILE=raisin-dev
// assume_role arn:aws:iam::741788044765:role/Development
const {
  CognitoIdentityProviderClient,
  ListUserPoolsCommand,
  ListUserPoolClientsCommand,
  DescribeUserPoolClientCommand,
  UpdateUserPoolClientCommand,
} = require("@aws-sdk/client-cognito-identity-provider");

const { argv } = process;

const clientName = "customer_administrator_portal_web_client";
const poolNameDeu = "customer_administrator_portal_deu";
const poolNameGbr = "customer_administrator_portal_gbr";
const operation = argv[2];
const newUrl = argv[3];

const logoutUrl = `${newUrl}/`;
const callbackUrl = `${newUrl}/dashboard`;

const client = new CognitoIdentityProviderClient({});

const getCognitoPool = async (poolName) => {
  let data;
  do {
    data = await client.send(new ListUserPoolsCommand({NextToken: data?.NextToken, MaxResults: 60}));
    const found = data.UserPools?.find(({Name}) => Name === poolName);
    if (found) {
      return found;
    }
  } while (data.NextToken);
  return undefined;
};

const getCognitoClient = async (userPoolId, clientName) => {
  const data = await client.send(
    new ListUserPoolClientsCommand({ UserPoolId: userPoolId })
  );
  const cognitoClient = data.UserPoolClients?.find(
    ({ ClientName }) => ClientName === clientName
  );
  return cognitoClient;
};

const getCognitoClientConfig = async (clientId, userPoolId) => {
  const data = await client.send(
    new DescribeUserPoolClientCommand({
      UserPoolId: userPoolId,
      ClientId: clientId,
    })
  );
  const cognitoClientConfig = data["UserPoolClient"];
  return cognitoClientConfig;
};

const addCallBack = (callbackUrls = [], logoutUrls = []) => {
  let needUpdate = false;
  let newCallbackUrls = callbackUrls;
  let newLogoutUrls = logoutUrls;

  if (!callbackUrls.includes(callbackUrl)) {
    callbackUrls.push(callbackUrl);
    needUpdate = true;
  }
  if (!logoutUrls.includes(logoutUrl)) {
    logoutUrls.push(logoutUrl);
    needUpdate = true;
  }

  return { needUpdate, newCallbackUrls, newLogoutUrls };
};

const removeCallBack = (callbackUrls = [], logoutUrls = []) => {
  let needUpdate = false;
  let newCallbackUrls = callbackUrls;
  let newLogoutUrls = logoutUrls;

  if (callbackUrls.includes(callbackUrl)) {
    newCallbackUrls = callbackUrls.filter((c) => c !== callbackUrl);
    needUpdate = true;
  }
  if (logoutUrls.includes(logoutUrl)) {
    newLogoutUrls = logoutUrls.filter((l) => l !== logoutUrl);
    needUpdate = true;
  }

  return { needUpdate, newCallbackUrls, newLogoutUrls };
};

const updateUserPoolClient = async (
  cognitoClientConfig,
  callbackUrls,
  logoutUrls
) => {
  const data = await client.send(
    new UpdateUserPoolClientCommand({
      ...cognitoClientConfig,
      CallbackURLs: callbackUrls,
      LogoutURLs: logoutUrls,
    })
  );
  return data;
};

const run = async (poolName, clientName) => {
  const cognitoPool = await getCognitoPool(poolName);
  const cognitoClient = await getCognitoClient(cognitoPool["Id"], clientName);
  const cognitoClientConfig = await getCognitoClientConfig(
    cognitoClient["ClientId"],
    cognitoPool["Id"]
  );

  const callbackUrls = cognitoClientConfig["CallbackURLs"];
  const logoutUrls = cognitoClientConfig["LogoutURLs"];

  if (operation === "add") {
    const { needUpdate, newCallbackUrls, newLogoutUrls } = addCallBack(
        callbackUrls,
        logoutUrls
    );
    if (needUpdate) {
      await updateUserPoolClient(cognitoClientConfig, newCallbackUrls, newLogoutUrls);
    }
  }

  if (operation === "remove") {
    const { needUpdate, newCallbackUrls, newLogoutUrls } = removeCallBack(
      callbackUrls,
      logoutUrls
    );

    if (needUpdate) {
      await updateUserPoolClient(cognitoClientConfig, newCallbackUrls, newLogoutUrls);
    }
  }
};

const start = async () => {
  await run(poolNameDeu, clientName);
  await run(poolNameGbr, clientName);
};
start();