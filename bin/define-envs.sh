#!/bin/bash -e
set -o pipefail

# export amplify apps to be used as reference with remote in development
aws amplify list-apps --query 'apps[].{appId: appId, name: name}' --output json > ./amplify_apps.json
# e.g. [{"appId": "d2f0mfumz7r9eb","name": "auth"}, ...]


# Slug normalized from the branch name
app_slug="${AWS_BRANCH//\//-}"
app_slug="${app_slug,,}"
export APP_SLUG=${app_slug}
