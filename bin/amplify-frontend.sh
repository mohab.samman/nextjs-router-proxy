#!/bin/bash -e
set -o pipefail
BASE_DIR=$(realpath $(dirname $0))
. ${BASE_DIR}/define-envs.sh

rm -rf ./dist
mkdir -p ./dist

if [[ "${ENV_INFRA_MANAGEMENT}" != 'FRONTEND' ]]; then
  echo "Skipping frontend deployment in favor of backend onboarding / production"
  echo "<meta http-equiv=\"refresh\" content=\"0;url=${DEU_PUBLIC_PATH}\" />" > ./dist/index.html
  exit 0;
fi


# INSTALLING EXPECTED NODE VERSION AND AMPLIFY CLI BECAUSE THE BUILT IN FUNCTION ON AMPLIFY IS NOT WORKING AS EXPECTED
. ~/.nvm/nvm.sh
nvm install 14.15.5
nvm use 14.15.5
npm install -g @aws-amplify/cli@"${VERSION_AMPLIFY}"

yarn install
yarn build:libs
yarn build

if [[ "${IS_PORTAL}" = true ]] && [[ "${IS_DEVELOPMENT}" = true ]]; then
  cd ${BASE_DIR}
  npm install
  node --unhandled-rejections=strict ./configure-dev-cognito.js add https://${APP_SLUG}.raisin-dev.network
  node --unhandled-rejections=strict ./configure-dev-cognito.js add https://${APP_SLUG}.dev-weltsparen.de
  node --unhandled-rejections=strict ./configure-dev-cognito.js add https://${APP_SLUG}.dev-raisin.co.uk
fi

# LIGHTHOUSE REPORT
if [[ "${AWS_BRANCH}" = "staging" ]] && [[ "${IS_PORTAL}" = true ]]; then
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
  yum -y localinstall google-chrome-stable_current_x86_64.rpm 

  npm install -g @lhci/cli@0.7.x
  lhci autorun --collect.url=${DEU_PUBLIC_PATH} --collect.url=${GBR_PUBLIC_PATH} --collect.headful=false --upload.target=temporary-public-storage --collect.settings.chromeFlags="--no-sandbox" || echo "LHCI failed!"
fi
