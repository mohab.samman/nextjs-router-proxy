#!/bin/bash -e
set -o pipefail
BASE_DIR=$(realpath $(dirname $0))
. ${BASE_DIR}/define-envs.sh

# configure Artifactory
yum install jq -y
ARTIFACTORY_NPM_AUTH=$(aws secretsmanager get-secret-value --secret-id amplify/cdk --query 'SecretString' --output text | jq -r '.artifactory_token')
npm config set _auth ${ARTIFACTORY_NPM_AUTH}
npm config set registry https://raisin.jfrog.io/artifactory/api/npm/npm/
npm config set always-auth true
npm config set email cloud-automation@raisin.com
yarn config set registry https://raisin.jfrog.io/artifactory/api/npm/npm/


if [[ "${ENV_INFRA_MANAGEMENT}" != 'BACKEND' ]]; then
  echo "Skipping backend deploy since branch is not onboarding or production"
  exit 0;
fi

# some changes are cached in amplify, so sometimes we need to clean the cache (e.g. template parameters)
if [[ "${AMPLIFY_CACHE_CLEAN}" = 'true' ]]; then
  envCache --set stackInfo ''
fi

# INSTALLING EXPECTED NODE VERSION AND AMPLIFY CLI BECAUSE THE BUILT IN FUNCTION ON AMPLIFY IS NOT WORKING AS EXPECTED
. ~/.nvm/nvm.sh
nvm install 14.15.5
nvm use 14.15.5
npm install -g @aws-amplify/cli@"${VERSION_AMPLIFY}"

yarn install
yarn build:libs
amplifyPush --simple
amplify publish --yes --invalidateCloudFront
