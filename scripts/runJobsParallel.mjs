import { spawn } from "child_process";
import config from "../projectsConfigs.mjs";

//portal or lib
const project = process.argv[2];

// start or build
const command = process.argv[3];

const getProjectScopeOptions = () => {
  const options = [];
  config[project].forEach((mfName) => {
    options.push("--scope", `@raisin/${mfName}`);
  });

  return options;
};

const executeCommand = (command) => {
  if (!config[project]) {
    throw new Error(`${project} portal does not exist`);
  }

  spawn("lerna", ["run", "--parallel", command, ...getProjectScopeOptions()], {
    stdio: "inherit",
  });
};

executeCommand(command);
