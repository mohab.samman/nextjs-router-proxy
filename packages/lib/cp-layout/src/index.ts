import Layout, { ILayout } from './layout';
import { getLayoutData } from './utils/Layout';

export { Layout, getLayoutData };
export type { ILayout };
