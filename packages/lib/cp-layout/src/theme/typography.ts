const fontSans = ['Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif'];

export default {
  fontFamily: fontSans.join(', '),
  htmlFontSize: 16,
  fontSize: 16,
  fontWeightLight: 300,
  fontWeightRegular: 400,
  fontWeightMedium: 500,
  fontWeightSemiBold: 600,
  fontWeightBold: 700,
  disclaimerFontSize: '12px',
  bodyTextFontSize: '16px',
  bodyTextFontSizeSm: '14px',
  bodyTextFontSizeLg: '18px',
  button: {
    fontFamily: fontSans.join(', '),
    fontSize: 16,
    fontWeight: 500,
    letterSpacing: '0.02857em',
    lineHeight: 1.75,
  },
  body1: {
    fontFamily: fontSans.join(', '),
    fontSize: 16,
    fontWeight: 400,
    letterSpacing: 'normal',
    lineHeight: 1.5,
  },
};
