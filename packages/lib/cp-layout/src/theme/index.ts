import { createTheme, responsiveFontSizes } from '@material-ui/core';

import breakpoints from './breakpoints';
import palette from './palette';
import typography from './typography';
import shape from './shape';
import zIndex from './zIndex';
import components from './components';

const customTheme = {
  breakpoints,
  palette,
  components,
  shape,
  typography,
  zIndex,
};

const theme = createTheme(customTheme);

export default responsiveFontSizes(theme);
