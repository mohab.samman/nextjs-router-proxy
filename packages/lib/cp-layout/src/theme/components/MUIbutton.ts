import Typography from '../typography';
import Colors from '../colors';

export default {
  styleOverrides: {
    defaultProps: {
      disableRipple: true,
    },
    root: {
      textTransform: 'none',
      fontWeight: Typography.fontWeightSemiBold,
      height: 40,
      padding: '8px 16px',
      boxShadow: 'none',
      '&:hover': {
        boxShadow: 'none',
      },
    },
    contained: {
      backgroundColor: Colors.blue60,
      color: Colors.white,
      '&:hover': {
        backgroundColor: Colors.blue60,
        color: Colors.white,
      },
    },
    outlined: {
      color: Colors.blue60,
      borderColor: Colors.blue60,
      '&:hover': {
        color: Colors.blue60,
        borderColor: Colors.blue60,
      },
    },
  },
} as any;
