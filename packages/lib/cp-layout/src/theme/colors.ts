export default {
  contrastText: '#fff',
  white: '#fff',
  // Orange
  orange100: '#e6411a', // Brand Color
  orange80: '#ec5512', // CTA Color
  orange60: '#f26e0d',
  orange40: '#ffa60d',
  orange10: '#fdf4e4',
  // Blue
  blue100: '#003554', // Brand Color
  blue80: '#004b8c',
  blue60: '#156cc4',
  blue40: '#3a8dff', // Störer
  blue10: '#e8f0f9',
  // Black
  black: '#000',
  black80: '#404040',
  black60: '#929292',
  black40: '#d7d7d7', // Level-1
  black20: '#eeeeee', // Level-2
  black10: '#f3f3f3', // Level-3
  // Green
  green100: '#007300',
  green80: '#1bad2b',
  green10: '#e5f8e0',
  // Red
  red100: '#922c1c',
  red10: '#fae6e4',
  // Yellow
  yellow100: '#9c851b',
  yellow80: '#fddf00',
  yellow10: '#fffbdc',
  // JustETF colors
  justEtfBlue: '#1169a5',
  justEtfRed: '#cd3c3c',
  // Social Media colors
  twitter: '#00aced',
  facebook: '#3b5998',
  googleplus: '#dd4b39',
  linkedin: '#007bb6',
  youtube: '#bb0000',
};
