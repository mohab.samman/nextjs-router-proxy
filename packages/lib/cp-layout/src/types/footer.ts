export interface IContactInformationData {
  phone: {
    phone_number: string;
    availability: string;
    icon: {
      uid: string;
      _content_type_uid: string;
    }[];
  };
  email: {
    email_address: string;
    icon: {
      uid: string;
      _content_type_uid: string;
    }[];
  };
  display_contact_information: boolean;
}

export interface ISocialMediaIcons {
  uid: string;
  iconUrl: string;
  link: string;
  account: string;
}

export interface IFooterData {
  display_footer_links: boolean;
  display_trustpilot: boolean;
  mobile_banner: {
    icon: {
      uid: string;
      _content_type_uid: string;
    }[];
    display_mobile_banner: boolean;
  };
  contact_information: {
    uid: string;
    _content_type_uid: string;
  }[];
  footer_links: {
    heading: string;
    link: Array<{
      title: string;
      href: string;
    }>;
  }[];
  legal_links: Array<{
    title: string;
    href: string;
  }>;
  locale: string;
  social_media: Array<{
    icon: {
      uid: string;
      _content_type_uid: string;
    }[];
    link: string;
    social_media_account: string;
    _metadata: {
      uid: string;
    };
  }>;
  title: string;
  uid: string;
  copyright: string;
  credits: Array<{
    institute_logo: {
      url: string;
      title: string;
    };
    institute_link: {
      title: string;
      href: string;
    };
  }>;
  disclaimer: string;
}
