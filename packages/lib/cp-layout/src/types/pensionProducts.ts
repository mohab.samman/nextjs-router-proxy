export interface IPPData {
  products: {
    id: string;
    state: string;
    name: string;
    type: string;
    balance: {
      value: number;
      currency: string;
    };
  }[];
}
