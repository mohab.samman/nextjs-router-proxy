export interface IGlobalSettings {
  businessLogo: {
    url: string;
    title: string;
  };
  retailLogo: {
    url: string;
    title: string;
  };
}

export interface IHeaderTopBar {
  isLoggedIn: boolean;
  useSWR: Function;
}
export interface IHeaderNavBar extends IGlobalSettings {
  isLoggedIn: boolean;
  useSWR: Function;
}

export interface IBankingMenuItem {
  category: string;
  showFn?: Function;
  subItems: {
    label: string;
    href: string;
    showFn?: Function;
  }[];
}

export interface IMainNavigationData {
  main_menu: {
    menu_item: {
      title: string;
      href: string;
    };
    _metadata: { uid: string };
    sub_menu_item: {
      title: string;
      href: string;
    }[];
  }[];
}
