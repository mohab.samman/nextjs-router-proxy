export interface ICustomer {
  bac_number: string;
  academic_title: string;
  display_name: string;
  first_name: string;
  last_name: string;
  email: string;
  phone_number_anonymized: string;
  tax_id_number: string;
  is_company_customer: boolean;
  has_fiduciary_account: boolean;
  default_address: {
    street: string;
    street_no: string;
    postal_code: string;
    city: string;
    country: string;
  };
  documents: {
    identification: {
      state: string;
      upload_time: string;
      verification_time: string;
    };
    post_ident_document: {
      state: string;
      upload_time: string;
      verification_time: string;
    };
    proof_of_address: {
      state: string;
    };
  };
  signature_method: string;
  region: string;
  locale: string;
  transactional_emails_enabled: boolean;
  marketing_emails_enabled: boolean;
  subscribed_to_newsletter: boolean;
  enabled_features: string[];
  referenceAccount: {
    iban: string;
    account_holder: string;
    bic_bank_code: string;
    bank_name: string;
  };
  distributor_id: string;
  is_pending_activation: boolean;
  is_broker_originated: boolean;
  is_locked_out: boolean;
  state: string;
}
