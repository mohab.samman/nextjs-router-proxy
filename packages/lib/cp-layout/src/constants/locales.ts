export const LOCALES = {
  // USA
  'en-US': {
    code: 'en_US',
    label: 'en',
    notation: 'en-us',
  },

  // Germany
  'de-DE': {
    code: 'de_DE',
    label: 'de',
    notation: 'de-de',
  },

  // UK
  'en-GB': {
    code: 'en_GB',
    label: 'gb',
    notation: 'en-gb',
  },

  // Ireland
  'en-IE': {
    code: 'en_IE',
    label: 'ie',
    notation: 'en-ie',
  },

  // Austria
  'de-AT': {
    code: 'de_AT',
    label: 'at',
    notation: 'de-at',
  },

  // France
  'fr-FR': {
    code: 'fr_FR',
    label: 'fr',
    notation: 'fr-fr',
  },

  // Spain
  'es-ES': {
    code: 'es_ES',
    label: 'es',
    notation: 'es-es',
  },
};

export const DEFAULT_LOCALE_LABEL = LOCALES['en-US'].label;
