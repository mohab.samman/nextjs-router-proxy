export const LEGACY_OBS_BASE = 'https://banking.weltsparen.de/savingglobal/#';

export const LINKS = {
  DASHBOARD: `${LEGACY_OBS_BASE}/Dashboard`,
  MY_INVESTMENTS: {
    TERM_DEPOSIT: `${LEGACY_OBS_BASE}/MyInvestments/TermDeposit`,
    OVERNIGHT: `${LEGACY_OBS_BASE}/MyInvestments/Overnight`,
    NOTICE: `${LEGACY_OBS_BASE}/MyInvestments/Notice`,
    COCKPIT: `${LEGACY_OBS_BASE}/InvestmentProducts/cockpit`,
  },
  PRODUCTS: {
    DEFAULT: `${LEGACY_OBS_BASE}/Products`,
    EASY_ACCESS: `${LEGACY_OBS_BASE}/Products/easyAccess`,
    OVERNIGHT: `${LEGACY_OBS_BASE}/Products/overnight`,
    NOTICE: `${LEGACY_OBS_BASE}/Products/notice`,
    INVESTMENT_PRODUCTS_PARENT: `${LEGACY_OBS_BASE}/investieren`,
    INVESTMENT_PRODUCTS_ROBO: `${LEGACY_OBS_BASE}/investieren/robo-advisor`,
    INVESTMENT_PRODUCTS_CONFIGURATOR: `${LEGACY_OBS_BASE}/investieren/configurator`,
    INVESTMENT_PRODUCTS_CONFIGURATOR_FUNDS_OVERVIEW: `${LEGACY_OBS_BASE}/investieren/configurator/fondsuebersicht`,
    JUSTETF_PRODUCTS: `${LEGACY_OBS_BASE}/InvestmentProducts/justetf`,
  },
  POSTBOX: {
    INBOX: `${LEGACY_OBS_BASE}/Postbox/inbox`,
    DOCUMENTS: `${LEGACY_OBS_BASE}/Postbox/documents`,
    SENT: `${LEGACY_OBS_BASE}/Postbox/sent`,
    COMPOSE: `${LEGACY_OBS_BASE}/Postbox/Compose/new`,
  },
  SETTINGS: {
    MY_DATA: `${LEGACY_OBS_BASE}/MyData`,
    TRANSACTIONS: `${LEGACY_OBS_BASE}/Administration/Transactions`,
    REMITTANCE: `${LEGACY_OBS_BASE}/Administration/CustomerPayout/edit`,
    LOCK_ACCOUNT: `${LEGACY_OBS_BASE}/BlockAccount`,
    LOCK_MTAN: `${LEGACY_OBS_BASE}/LockMTan/edit`,
    TAX_EXEMPTION: `${LEGACY_OBS_BASE}/TaxExemption`,
    SHOW_DOCUMENTS: `${LEGACY_OBS_BASE}/Administration/FormsList`,
    SWITCH_MTAN_METHOD: `${LEGACY_OBS_BASE}/Administration/SwitchMTan`,
  },
  REFER_A_FRIEND: `${LEGACY_OBS_BASE}/ReferAFriend/leads`,
  INITIAL_REFERENCE_ACCOUNT: `${LEGACY_OBS_BASE}/InitialReferenceAccount`,
  SET_PASSWORD: `${LEGACY_OBS_BASE}/SetPassword`,
  LOGIN: `${LEGACY_OBS_BASE}/Login`,
  INVESTMENT_PRODUCTS: {
    SAVINGS_PLAN: `${LEGACY_OBS_BASE}/InvestmentProducts/savingsPlan/edit`,
  },
  ABOUT: {
    PRIVACY: `${LEGACY_OBS_BASE}/about/privacy`,
  },
  IDENTIFICATION: `${LEGACY_OBS_BASE}/Identification`,
  IDENTIFICATION_POA: `${LEGACY_OBS_BASE}/Identification/proofOfAddress`,
  LOADING_SCREEN: `${LEGACY_OBS_BASE}/Loading`,
};
