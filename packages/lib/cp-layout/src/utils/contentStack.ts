import Contentstack from 'contentstack';
import { LOCALES } from '../constants/locales';

/**
 * ContentStack
 * Contentstack contains global variables injected by Webpack,
 * the object was defined in the config file in the root directory
 * and contains the stack API Key, the delivery Token and environment
 * to which stack to connect
 */
export interface IContentStackSecrets {
  stackAPIKey: string;
  stackDeliveryToken: string;
  environment: string;
}

/**
 * ContentStack Authentication
 * ContentStack SDK function to authenticate and
 * connect the the desire stack.
 * The region will always be EU.
 */
const Stack = ({ stackAPIKey, stackDeliveryToken, environment }) =>
  Contentstack.Stack(stackAPIKey, stackDeliveryToken, environment, Contentstack.Region.EU);

/**
 * @param contentStack - This object provides the contentstack secrets
 * If no reference uid (@param entry)  was provided, will return all available entries under that contentType.
 * If a reference uid (@param entry) was provided, will return an object containing the entry.
 */
export const getCMSEntriesForContentType = async (
  contentStackSecrets: IContentStackSecrets,
  locale: string,
  contentType: string,
  entry?: string,
) => {
  const language = LOCALES[locale];

  try {
    if (entry) {
      const response = await Stack(contentStackSecrets)
        .ContentType(contentType)
        .Entry(entry)
        .language(language.notation)
        .fetch();

      return response.toJSON();
    }

    const response = await Stack(contentStackSecrets)
      .ContentType(contentType)
      .Query()
      .language(language.notation)
      .toJSON()
      .find();

    return response[0];
  } catch (e) {
    return Promise.reject(e.message);
  }
};

export const getCMSIcons = async (
  contentStackSecrets: IContentStackSecrets,
  locale: string,
  entry: string,
) => {
  const language = LOCALES[locale];
  try {
    const response = await Stack(contentStackSecrets)
      .ContentType('icons_set')
      .Entry(entry)
      .language(language.notation)
      .fetch();
    return { [entry]: response.get('file').url };
  } catch (e) {
    return Promise.reject(e.message);
  }
};
