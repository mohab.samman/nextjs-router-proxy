const getMessages = async (locale = 'default') => {
  try {
    const messages = await import(`../../__translations__/${locale}.json`);

    return messages.default;
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(`Application couldn't find ${locale}.json`);

    return {};
  }
};

export default getMessages;
