import axios from 'axios';
import fs from 'fs';
import { DEFAULT_LOCALE_LABEL, LOCALES } from '../constants/locales';

const IS_BASE_URL_BY_LOCALE = {
  de: 'https://www.weltsparen.de',
  // IS routes .at requests to .de (This is why we see .de trustpilot data on .at)
  at: 'https://www.weltsparen.at',
  gb: 'https://www.raisin.co.uk',
  en: 'https://www.raisin.com',
  fr: 'https://www.raisin.fr',
  es: 'https://www.raisin.es',
  nl: 'https://www.raisin.nl',
  ie: 'https://www.raisin.ie',
};

export const fetchTrustpilot = async (locale: string) => {
  const language = LOCALES[locale];
  const baseUrlIS =
    IS_BASE_URL_BY_LOCALE[language.label] || IS_BASE_URL_BY_LOCALE[DEFAULT_LOCALE_LABEL];

  if (fs.existsSync('/tmp/raisinTrustpilotData.json')) {
    const data = fs.readFileSync('/tmp/raisinTrustpilotData.json', 'utf8');

    return JSON.parse(data);
  }

  const req = await axios({
    method: 'GET',
    url: `${baseUrlIS}/dp/public/v1/trustpilot/rating`,
  });

  fs.writeFile('/tmp/raisinTrustpilotData.json', JSON.stringify(req.data), (err) => {
    if (err) throw err;
  });

  return req.data;
};
