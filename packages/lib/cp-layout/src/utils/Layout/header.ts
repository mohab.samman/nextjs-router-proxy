import { getCMSEntriesForContentType, getCMSIcons, IContentStackSecrets } from '../contentStack';

export const fetchHeaderData = async (
  contentStackSecrets: IContentStackSecrets,
  locale: string,
) => {
  try {
    const globalSettings = await getCMSEntriesForContentType(
      contentStackSecrets,
      locale,
      'global_settings',
    );
    const mainNavEntires = await getCMSEntriesForContentType(
      contentStackSecrets,
      locale,
      'main_navigation',
    );

    const giftIconReference = globalSettings[0].app_assets.find(
      (icon) => icon.reference === 'refer_a_friend',
    );

    const giftIconUrl = Object.values(
      await getCMSIcons(contentStackSecrets, locale, giftIconReference.icon[0].uid),
    )[0];

    return { globalSettings: globalSettings[0], mainNavData: mainNavEntires[0], giftIconUrl };
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    return {};
  }
};
