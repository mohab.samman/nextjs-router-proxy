/* eslint-disable no-underscore-dangle */
import { IFooterData, IContactInformationData } from '../../types/footer';
import { getCMSIcons, getCMSEntriesForContentType, IContentStackSecrets } from '../contentStack';
import { fetchTrustpilot } from '../fetchTrustpilot';

interface IEntryWithIconReference {
  icon: {
    uid: string;
    _content_type_uid: string;
  }[];
}

export const normalizeSocialMediaIcons = (
  iconsSet: { [key: string]: string },
  socialMedia: IFooterData['social_media'],
) =>
  socialMedia.map((item) => ({
    uid: item?._metadata?.uid ?? '',
    iconUrl: iconsSet[item?.icon?.[0].uid] ?? '',
    link: item?.link ?? '',
    account: item?.social_media_account ?? '',
  }));

const getIconsUID = (
  entryWithIconReference: IEntryWithIconReference | IEntryWithIconReference[],
) => {
  if (Array.isArray(entryWithIconReference)) {
    return entryWithIconReference?.reduce((acc: string[], curVal) => {
      acc.push(curVal?.icon?.[0].uid);
      return acc;
    }, []);
  }

  return [entryWithIconReference?.icon?.[0].uid];
};

const fetchIconURL = async (
  contentStackSecrets: IContentStackSecrets,
  locale: string,
  entry: string,
) => {
  try {
    return await getCMSIcons(contentStackSecrets, locale, entry);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    return {};
  }
};

const getIconsURL = async (
  contentStackSecrets: IContentStackSecrets,
  locale: string,
  iconUIDs: string[],
) => {
  try {
    return await Promise.all(
      iconUIDs.map((uid: string) => fetchIconURL(contentStackSecrets, locale, uid)),
    );
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
    return [];
  }
};

export const fetchFooterData = async (
  contentStackSecrets: IContentStackSecrets,
  locale: string,
  footerReference: {
    uid?: string;
    _content_type_uid: string;
  },
) => {
  const footer: IFooterData | IFooterData[] = await getCMSEntriesForContentType(
    contentStackSecrets,
    locale,
    footerReference._content_type_uid,
    footerReference.uid,
  ).catch((e: string) => {
    // eslint-disable-next-line no-console
    console.error(e);
    return {};
  });

  let footerData: IFooterData;
  // If footerData is an array, we will use the retail footer as default which has pos 0.
  // TODO: This needs to be refactored in the future. All pages should have a page entry in CMS and therefore a footer reference.
  // When that is done, these checks and providing a default footer will no longer be needed
  if (Array.isArray(footer)) {
    [footerData] = footer;
  } else {
    footerData = footer;
  }

  const contactInformationReference = footerData.contact_information[0];

  const contactInformationData: IContactInformationData = await getCMSEntriesForContentType(
    contentStackSecrets,
    locale,
    contactInformationReference._content_type_uid,
    contactInformationReference.uid,
  ).catch((e: string) => {
    // eslint-disable-next-line no-console
    console.error(e);
    return {};
  });
  const socialMediaIconsUID = getIconsUID(footerData.social_media);
  const phoneIconUID = getIconsUID(contactInformationData.phone);
  const emailIconUID = getIconsUID(contactInformationData.email);
  const mobileBannerIconUID = getIconsUID(footerData.mobile_banner);

  const footerIconsUID = [
    ...socialMediaIconsUID,
    ...phoneIconUID,
    ...emailIconUID,
    ...mobileBannerIconUID,
  ];

  const icons: any = await getIconsURL(contentStackSecrets, locale, footerIconsUID);
  const mutatedIcons = icons.reduce(
    (obj: object, item: { [key: string]: string }) => ({
      ...obj,
      [Object.keys(item)[0]]: Object.values(item)[0],
    }),
    {},
  );

  const trustpilotData = await fetchTrustpilot(locale);

  return {
    ...footerData,
    contactInformationData,
    phoneIconURL: mutatedIcons[phoneIconUID[0]],
    emailIconURL: mutatedIcons[emailIconUID[0]],
    mobileBannerIconURL: mutatedIcons[mobileBannerIconUID[0]],
    socialMediaIcons: normalizeSocialMediaIcons(mutatedIcons, footerData.social_media),
    trustpilotData,
  };
};
