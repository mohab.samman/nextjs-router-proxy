import { isDEUCustomer, isAUTCustomer, isGBRCustomer } from '../CustomerUtils';
import {
  LINKS as PENSION_PRODUCTS_LINKS,
  hasPPProducts,
  hasEtfConfiguratorProducts,
} from '../PensionProducts';
import { ICustomer } from '../../types/customer';
import { IPPData } from '../../types/pensionProducts';
import { LINKS } from '../../constants/links';
import {
  HAS_FIXED_DEPOSIT_ACCOUNT,
  HAS_FLEX_DEPOSIT_ACCOUNT,
  HAS_INVESTMENT_PRODUCT_ACCOUNT,
  HAS_NOTICE_DEPOSIT_ACCOUNT,
  HAS_OVERNIGHT_DEPOSIT_ACCOUNT,
} from '../../constants/productAccess';

export const getBankingMenu = ([customer, productAccess, pensionProductsData]: [
  customer: ICustomer,
  productAccess: string[],
  pensionProductsData: IPPData,
]) => [
  {
    category: 'myInvestments',
    subItems: [
      {
        label: 'dashboard',
        href: LINKS.DASHBOARD,
        showFn: () =>
          !customer.is_pending_activation ||
          (!isGBRCustomer(customer) && !isDEUCustomer(customer) && !isAUTCustomer(customer)),
      },
      {
        label: 'fixedDeposit',
        showFn: () =>
          productAccess.includes(HAS_FIXED_DEPOSIT_ACCOUNT) ||
          productAccess.includes(HAS_FLEX_DEPOSIT_ACCOUNT),
        href: LINKS.MY_INVESTMENTS.TERM_DEPOSIT,
      },
      {
        label: 'overnightDeposit',
        showFn: () => productAccess.includes(HAS_OVERNIGHT_DEPOSIT_ACCOUNT),
        href: LINKS.MY_INVESTMENTS.OVERNIGHT,
      },
      {
        label: 'noticeDeposit',
        showFn: () => productAccess.includes(HAS_NOTICE_DEPOSIT_ACCOUNT),
        href: LINKS.MY_INVESTMENTS.NOTICE,
      },
      {
        label: 'investmentProductsRobo',
        showFn: () => productAccess.includes(HAS_INVESTMENT_PRODUCT_ACCOUNT),
        href: LINKS.MY_INVESTMENTS.COCKPIT,
      },
      {
        label: 'investmentProductsConfigurator',
        showFn: () => hasEtfConfiguratorProducts(pensionProductsData),
        href: PENSION_PRODUCTS_LINKS.etfcCockpit,
      },
      {
        label: 'pensionProducts',
        showFn: () => hasPPProducts(pensionProductsData),
        href: PENSION_PRODUCTS_LINKS.cockpit,
      },
    ],
  },
  {
    category: 'postbox',
    subItems: [
      {
        label: 'postboxMessages',
        href: LINKS.POSTBOX.INBOX,
      },
      {
        label: 'postboxDocuments',
        href: LINKS.POSTBOX.DOCUMENTS,
      },
      {
        label: 'postboxSentMessages',
        href: LINKS.POSTBOX.SENT,
      },
    ],
  },
  {
    category: 'administration',
    showFn: () => !customer.is_pending_activation,
    subItems: [
      {
        label: 'myData',
        href: LINKS.SETTINGS.MY_DATA,
      },
      {
        label: 'transactions',
        href: LINKS.SETTINGS.TRANSACTIONS,
      },
      {
        label: 'payOut',
        href: LINKS.SETTINGS.REMITTANCE,
      },
      {
        label: 'blockAcc',
        href: LINKS.SETTINGS.LOCK_ACCOUNT,
      },
      {
        label: 'blockTan',
        href: LINKS.SETTINGS.LOCK_MTAN,
      },
      {
        label: 'taxExemption',
        href: LINKS.SETTINGS.TAX_EXEMPTION,
        showFn: () => customer.has_fiduciary_account,
      },
      {
        label: 'forms',
        href: LINKS.SETTINGS.SHOW_DOCUMENTS,
        showFn: () => isDEUCustomer(customer) || isAUTCustomer(customer) || isGBRCustomer(customer),
      },
      {
        label: 'switchTanMethod',
        href: LINKS.SETTINGS.SWITCH_MTAN_METHOD,
      },
    ],
  },
];
