import { IContentStackSecrets } from '../contentStack';
import { fetchFooterData } from './footer';
import { fetchHeaderData } from './header';
import { fetchTranslations } from './i18n';

/**
 *
 * @param contentStackSecrets contentstack secrets neede to do contentstack API calls. They should be available as a global variable in your project
 * @param locale the project language. It should also be available as a global variable
 * @param footerReference a specific footer reference (business/retail etc). If none is provided, a default value is used
 * @returns
 */

export const getLayoutData = async (
  contentStackSecrets: IContentStackSecrets,
  locale: string,
  footerReference: { uid?: string; _content_type_uid: string } = {
    _content_type_uid: 'footer',
  },
) => {
  if (!contentStackSecrets) {
    throw new Error('contentstack secrets were not provided to getLayoutData');
  }

  if (!locale) {
    throw new Error('locale was not provided to getLayoutData');
  }

  const footerData = await fetchFooterData(contentStackSecrets, locale, footerReference);
  const headerData = await fetchHeaderData(contentStackSecrets, locale);

  const { messages, language } = await fetchTranslations(locale);

  return { footerData, headerData, localizedMessages: messages, locale: language.label };
};
