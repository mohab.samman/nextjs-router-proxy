import getMessages from '../i18n/getMessages';
import { LOCALES } from '../../constants/locales';

export const fetchTranslations = async (locale: string) => {
  const language = LOCALES[locale];
  const messages = await getMessages(language.code);

  return { messages, language };
};
