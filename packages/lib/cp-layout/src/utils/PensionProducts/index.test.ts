import { HOSTS, getHost } from '.';

const windowCopy: any = window;
const { location }: { location: Location } = windowCopy;

beforeEach(() => {
  delete windowCopy.location;
  windowCopy.location = { assign: jest.fn() };
});

afterAll(() => {
  windowCopy.location = location;
});

describe('PensionProducts Utils', () => {
  describe('HOSTS', () => {
    it('should have 3 keys', () => {
      expect(Object.keys(HOSTS)).toHaveLength(3);
    });

    it('should return staging host by type and name', () => {
      expect(HOSTS.staging('cool-name', 'invest')).toBe(
        'https://obs-api-cool-name.fairr.online/.invest',
      );
      expect(HOSTS.staging('cool-name', 'pension')).toBe('https://obs-api-cool-name.fairr.online');
      expect(HOSTS.staging('cool-name', 'non-existent')).toBe(
        'https://obs-api-cool-name.fairr.online',
      );
    });
  });

  describe('getHost', () => {
    it('should return correct production host', () => {
      expect(getHost()).toBe('https://www.raisin-pension.de');
      expect(getHost()).toBe('https://www.raisin-pension.de');
      expect(getHost('pension')).toBe('https://www.raisin-pension.de');
      expect(getHost('invest')).toBe('https://invest.raisin.com');
    });

    it('should return correct staging host', () => {
      window.location.host = 'https://bk-deu-my-namespace.raisin-dev.network/';
      expect(getHost()).toBe('https://obs-api-my-namespace.fairr.online');
      expect(getHost('pension')).toBe('https://obs-api-my-namespace.fairr.online');
      expect(getHost('invest')).toBe('https://obs-api-my-namespace.fairr.online/.invest');
    });

    it('should return production host if regex does not apply', () => {
      window.location.host = 'https://bk-gbr-my-namespace.raisin-dev.network/';
      expect(getHost()).toBe('https://www.raisin-pension.de');
      expect(getHost('pension')).toBe('https://www.raisin-pension.de');
      expect(getHost('invest')).toBe('https://invest.raisin.com');
    });
  });
});
