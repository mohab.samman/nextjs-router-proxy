import { IPPData } from '../../types/pensionProducts';

export const HOSTS = {
  pension: 'https://www.raisin-pension.de',
  invest: 'https://invest.raisin.com',
  staging: (name: string, type: string) => {
    switch (type) {
      case 'invest':
        return `https://obs-api-${name}.fairr.online/.invest`;
      default:
        return `https://obs-api-${name}.fairr.online`;
    }
  },
};

export const getHost = (type = 'pension') => {
  if (global.window?.location?.host?.indexOf('raisin-dev.network') >= 0) {
    const name = /bk-deu-(.*)\.raisin-dev\.network/.exec(global.window.location.host);

    return name ? HOSTS.staging(name[1], type) : HOSTS[type];
  }

  return HOSTS[type];
};

export const LINKS = {
  general: getHost(),
  ruerup: `${getHost()}/produkte/ruerup/`,
  fairruerup: {
    product: `${getHost()}/produkte/ruerup/rechner/`,
    document: `${getHost()}/assets/media/leitfaden-ruerup.pdf`,
  },
  fairriester: {
    product: `${getHost()}/produkte/riester/rechner/`,
    document: `${getHost()}/assets/media/leitfaden-riester.pdf`,
  },
  cockpit: `${getHost()}/cockpit/`,
  postbox: `${getHost()}/cockpit/kommunikation/dokumente/`,
  customerData: `${getHost()}/cockpit/meine-daten/`,
  connectAccounts: `${getHost()}/login/connect-accounts`,
  etfcCockpit: `${getHost('invest')}/de/cockpit`,
};

export const hasEtfConfiguratorProducts = (pensionProducts: IPPData) =>
  !!pensionProducts?.products?.filter((p) => p.type === 'invest')?.length;

export const hasPPProducts = (pensionProducts: IPPData) =>
  !!pensionProducts?.products?.filter((p) => p.type !== 'invest')?.length;
