import React, { FC } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { ThemeProvider } from '@material-ui/core';

import theme from '../theme';
import getMessages from './i18n/getMessages';
import { DEFAULT_LOCALE_LABEL, LOCALES } from '../constants/locales';

const getTranslationMessages = async (locale: string) => {
  const hostMessages = await getMessages(locale);

  return hostMessages;
};

// eslint-disable-next-line react/prop-types
const Providers: FC = ({ children }) => {
  const messages: any = getTranslationMessages(LOCALES['en-US'].code);

  return (
    <IntlProvider
      locale={LOCALES['en-US'].label}
      defaultLocale={DEFAULT_LOCALE_LABEL}
      messages={messages}
    >
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </IntlProvider>
  );
};

const customRender = (ui, options = {}) => render(ui, { wrapper: Providers, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
