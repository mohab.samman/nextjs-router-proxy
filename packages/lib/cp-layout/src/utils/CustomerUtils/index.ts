import { ICustomer } from '../../types/customer';

export const countries = {
  AUT: 'AUT',
  DEU: 'DEU',
  ESP: 'ESP',
  FRA: 'FRA',
  GBR: 'GBR',
  IRL: 'IRL',
  NLD: 'NLD',
};

export const isDEUCustomer = ({
  default_address,
}: {
  default_address: ICustomer['default_address'];
}) => default_address?.country === countries.DEU;

export const isAUTCustomer = ({
  default_address,
}: {
  default_address: ICustomer['default_address'];
}) => default_address?.country === countries.AUT;

export const isGBRCustomer = ({
  default_address,
}: {
  default_address: ICustomer['default_address'];
}) => default_address?.country === countries.GBR;
