import * as CustomerUtils from '.';

describe('isDEUCustomer', () => {
  it('should return true', () => {
    const address = {
      country: CustomerUtils.countries.DEU,
      street: 'some street',
      street_no: 'some street no',
      postal_code: '12345',
      city: 'Cluk',
    };

    expect(CustomerUtils.isDEUCustomer({ default_address: address })).toBe(true);
  });

  it('should return false', () => {
    const address = {
      country: CustomerUtils.countries.GBR,
      street: 'some street',
      street_no: 'some street no',
      postal_code: '12345',
      city: 'Cluk',
    };

    expect(CustomerUtils.isDEUCustomer({ default_address: address })).toBe(false);
  });
});

describe('isAUTCustomer', () => {
  it('should return true', () => {
    const address = {
      country: CustomerUtils.countries.AUT,
      street: 'some street',
      street_no: 'some street no',
      postal_code: '12345',
      city: 'Cluk',
    };

    expect(CustomerUtils.isAUTCustomer({ default_address: address })).toBe(true);
  });

  it('should return false', () => {
    const address = {
      country: CustomerUtils.countries.GBR,
      street: 'some street',
      street_no: 'some street no',
      postal_code: '12345',
      city: 'Cluk',
    };

    expect(CustomerUtils.isAUTCustomer({ default_address: address })).toBe(false);
  });
});

describe('isGBRCustomer', () => {
  it('should return true', () => {
    const address = {
      country: CustomerUtils.countries.GBR,
      street: 'some street',
      street_no: 'some street no',
      postal_code: '12345',
      city: 'Cluk',
    };

    expect(CustomerUtils.isGBRCustomer({ default_address: address })).toBe(true);
  });

  it('should return false', () => {
    const address = {
      country: CustomerUtils.countries.NLD,
      street: 'some street',
      street_no: 'some street no',
      postal_code: '12345',
      city: 'Cluk',
    };

    expect(CustomerUtils.isGBRCustomer({ default_address: address })).toBe(false);
  });
});
