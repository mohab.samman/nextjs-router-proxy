import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';

export const DesktopContainer = withTheme(styled.div`
  display: flex;
  flex-direction: column;

  ${({ theme }) => theme.breakpoints.down('md')} {
    display: none;
  }
`);

export const MobileContainer = withTheme(styled.div`
  display: none;
  flex-direction: column;

  ${({ theme }) => theme.breakpoints.down('md')} {
    display: flex;
  }
`);
