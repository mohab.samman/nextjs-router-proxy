import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils';
import { IHeaderNavBar } from '../../../types/header';

import Logo from '.';

describe('<HeaderTopBar />', () => {
  const props: IHeaderNavBar = {
    isLoggedIn: false,
    businessLogo: { url: 'some/business/logo/url', title: 'string' },
    retailLogo: { url: 'some/retail/logo/url', title: 'string' },
    useSWR: () => jest.fn(),
  };

  it('renders a <Logo> component for unauthenticated user', async () => {
    const component = render(<Logo {...props} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });

  it('renders a <Logo> component for authenticated user', async () => {
    const newProps: IHeaderNavBar = {
      ...props,
      isLoggedIn: true,
      useSWR: () => ({ data: { distributor_id: 'Raisin', is_company_customer: false } }),
    };
    const component = render(<Logo {...newProps} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });

  it('renders a <Logo> component for business user', async () => {
    const newProps: IHeaderNavBar = {
      ...props,
      useSWR: () => ({ data: { distributor_id: 'Raisin', is_company_customer: true } }),
    };
    const component = render(<Logo {...newProps} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });

  it('renders a <Logo> component for distributor user', async () => {
    const newProps: IHeaderNavBar = {
      useSWR: () => ({ data: { distributor_id: 'Paypal', is_company_customer: false } }),

      ...props,
    };
    const component = render(<Logo {...newProps} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
