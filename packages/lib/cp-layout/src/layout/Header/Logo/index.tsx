import React from 'react';
import Link from 'next/link';
import { useIntl } from 'react-intl';
import { customerTypes } from '../../../constants/customer';
import { IHeaderNavBar } from '../../../types/header';
import messages from './messages';
import * as S from './styles';
import useFetchHeader from '../../../hooks/useFetchHeaderData';

const Logo: React.FC<IHeaderNavBar> = ({ businessLogo, retailLogo, isLoggedIn, useSWR }) => {
  /** TODO:
   * Distribution parter logo should be fetched
   * * */
  const intl = useIntl();
  const { customerData } = useFetchHeader(useSWR);
  const customerType = customerData?.is_company_customer
    ? customerTypes.business
    : customerTypes.retail;
  const distributorId =
    customerData?.distributor_id === 'Raisin' ? '' : customerData?.distributor_id || '';

  if (distributorId) {
    /** * TODO: comupte corrent URL for distribution logo link, based on page they are accessing.
     * Refer to OA-6738 for correct scenarios
     *** */

    const url = isLoggedIn ? '/dashboard ' : `/${distributorId}`;
    return (
      <Link href={url} passHref>
        <S.Anchor href="replace">
          <S.Image src={businessLogo.url} alt={businessLogo.title} />
          <S.Text variant="body2">{intl.formatMessage(messages.cooperationText)}</S.Text>
          <S.Image src={businessLogo.url} alt={businessLogo.title} />
        </S.Anchor>
      </Link>
    );
  }

  if (customerType === customerTypes.business) {
    /** * TODO: Link for business customers should be coming from CMS.
     * Refer to OA-6738 for correct scenarios
     *** */

    return (
      <Link href="/business-page" passHref>
        <S.Anchor href="replace">
          <S.Image src={businessLogo.url} alt={businessLogo.title} />
        </S.Anchor>
      </Link>
    );
  }

  /** * TODO:
   * 1 - Link for retail customers should be coming from CMS. Refer to OA-6738 for correct scenarios.
   * 2 - Logo for retail customer should be different for Germany and same for rest of the users.
   *** */
  return (
    <Link href="/" passHref>
      <S.Anchor href="replace">
        <S.Image src={retailLogo.url} alt={retailLogo.title} />
      </S.Anchor>
    </Link>
  );
};

export default Logo;
