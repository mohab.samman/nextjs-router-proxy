import { defineMessages } from 'react-intl';

const messages = defineMessages({
  cooperationText: {
    id: 'header.logo.cooperationText',
    defaultMessage: 'In co-operation with',
  },
});

export default messages;
