import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../theme/colors';

export const Anchor = styled.a`
  display: flex;
  text-decoration: none;
  color: ${Colors.black};
`;

export const Text = withTheme(styled.span`
  margin: 20px 24px;
`);

export const Image = withTheme(styled.img`
  ${({ theme }) => theme.breakpoints.down('md')} {
    max-width: 150px;
  }
`);
