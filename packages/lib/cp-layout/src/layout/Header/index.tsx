import React from 'react';
import DesktopHeader from './DesktopHeader';
import MobileHeader from './MobileHeader';
import * as S from './styles';
import { IGlobalSettings, IMainNavigationData } from '../../types/header';
export interface IHeader {
  headerData: {
    globalSettings: {
      business_logo: IGlobalSettings['businessLogo'];
      retail_logo: IGlobalSettings['retailLogo'];
    };
    mainNavData: IMainNavigationData;
    giftIconUrl: string;
  };
  isLoggedIn?: boolean;
  useSWR: Function;
}

const Header: React.FC<IHeader> = ({
  headerData: { globalSettings, mainNavData, giftIconUrl },
  isLoggedIn = false,
  useSWR,
}) => (
  <>
    <S.DesktopContainer>
      <DesktopHeader
        isLoggedIn={isLoggedIn}
        businessLogo={globalSettings.business_logo}
        retailLogo={globalSettings.retail_logo}
        mainMenu={mainNavData.main_menu}
        giftIconUrl={giftIconUrl}
        useSWR={useSWR}
      />
    </S.DesktopContainer>
    <S.MobileContainer>
      <MobileHeader
        isLoggedIn={isLoggedIn}
        businessLogo={globalSettings.business_logo}
        retailLogo={globalSettings.retail_logo}
        mainMenu={mainNavData.main_menu}
        giftIconUrl={giftIconUrl}
        useSWR={useSWR}
      />
    </S.MobileContainer>
  </>
);

export default Header;
