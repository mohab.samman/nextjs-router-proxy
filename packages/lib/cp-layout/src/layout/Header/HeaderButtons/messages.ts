import { defineMessages } from 'react-intl';

const messages = defineMessages({
  login: {
    id: 'header.buttons.login',
    defaultMessage: 'My account',
  },
  register: {
    id: 'header.buttons.register',
    defaultMessage: 'Register',
  },
});

export default messages;
