import styled from 'styled-components';
import Button from '@material-ui/core/Button';

export const CustomButton = styled(Button)`
  &:first-child {
    margin-right: 16px;
  }
`;
