import React from 'react';
import { useIntl } from 'react-intl';
import messages from './messages';
import * as S from './styles';

const HeaderButtons: React.FC = () => {
  const intl = useIntl();
  return (
    <>
      <S.CustomButton variant="outlined">{intl.formatMessage(messages.login)}</S.CustomButton>
      <S.CustomButton variant="contained">{intl.formatMessage(messages.register)}</S.CustomButton>
    </>
  );
};

export default HeaderButtons;
