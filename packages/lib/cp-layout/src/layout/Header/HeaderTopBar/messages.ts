import { defineMessages } from 'react-intl';

const messages = defineMessages({
  privateCustomer: {
    id: 'header.topBar.privateCustomer',
    defaultMessage: 'Private Customers',
  },
  businessCustomer: {
    id: 'header.topBar.businessCustomer',
    defaultMessage: 'Business Customers',
  },
  welcomeMessage: {
    id: 'header.topBar.welcomeMessage',
    defaultMessage: 'Welcome',
  },
  extendedWelcomeMessage: {
    id: 'header.topBar.extendedWelcomeMessage',
    defaultMessage: 'to online banking',
  },
});

export default messages;
