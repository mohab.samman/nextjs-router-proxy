import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Container from '@material-ui/core/Container';
import Colors from '../../../theme/colors';

export const Background = styled.div`
  background-color: ${Colors.black10};
`;

export const TopNavbarContainer = withTheme(styled(Container)`
  padding-top: 5px;
  padding-bottom: 5px;
  line-height: 1; /* because the default body line-height is 2.2 */

  ${({ theme }) => theme.breakpoints.down('md')} {
    padding-top: 13px;
    padding-bottom: 13px;
    border-bottom: 1px solid ${Colors.black40};
  }
`);

export const Anchor = withTheme(styled.a<{ active: boolean }>`
  ${({ theme, active }) => `
    color: ${theme.palette.primary.main};
    font-size: ${theme.typography.bodyTextFontSizeSm};
    font-weight: ${active ? theme.typography.fontWeightBold : theme.typography.fontWeightRegular};
`}

  text-decoration: none;
  line-height: 22px;

  &:not(:last-child) {
    margin-right: 16px;
  }
`);

export const Text = withTheme(styled.p`
  font-size: ${({ theme }) => theme.typography.bodyTextFontSizeSm};
  color: ${({ theme }) => theme.palette.primary.main};
  line-height: 22px;
  margin: 0;

  & > strong {
    font-weight: ${({ theme }) => theme.typography.fontWeightBold};
    padding-right: 4px;
  }
`);
