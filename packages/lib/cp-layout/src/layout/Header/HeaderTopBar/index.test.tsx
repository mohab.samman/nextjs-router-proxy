import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils';
import { IHeaderTopBar } from '../../../types/header';

import HeaderTopBar from '.';

describe('<HeaderTopBar />', () => {
  const props: IHeaderTopBar = {
    isLoggedIn: false,
    useSWR: () => ({
      data: { display_name: 'Dr. Dr. Dude' },
    }),
  };

  it('renders a <HeaderTopBar> component for unauthenticated user', async () => {
    const component = render(<HeaderTopBar {...props} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });

  it('renders a <HeaderTopBar> component for authenticated user', async () => {
    const newProps: IHeaderTopBar = {
      ...props,
      isLoggedIn: true,
    };
    const component = render(<HeaderTopBar {...newProps} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
