import React from 'react';
import { useIntl } from 'react-intl';
import Link from 'next/link';
import * as S from './styles';
import messages from './messages';
import { IHeaderTopBar } from '../../../types/header';
import { customerTypes } from '../../../constants/customer';
import useFetchHeader from '../../../hooks/useFetchHeaderData';

const HeaderTopBar: React.FC<IHeaderTopBar> = ({ isLoggedIn, useSWR }) => {
  const intl = useIntl();
  // TODO Only fetch if customer is logged in.
  const { customerData } = useFetchHeader(useSWR);

  const customerName = customerData?.display_name || null;
  const customerType = customerData?.is_company_customer
    ? customerTypes.business
    : customerTypes.retail;

  return (
    <S.Background>
      <S.TopNavbarContainer maxWidth="xl">
        {isLoggedIn ? (
          <S.Text variant="body2">
            <strong>{intl.formatMessage(messages.welcomeMessage)}</strong>
            {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
            {intl.formatMessage(messages.extendedWelcomeMessage)}, {customerName}
          </S.Text>
        ) : (
          <>
            {/** * TODO: this href should be dynamic and to be fetched from CMS} ** */}
            <Link href="/" passHref>
              <S.Anchor active={customerType === customerTypes.retail}>
                {intl.formatMessage(messages.privateCustomer)}
              </S.Anchor>
            </Link>
            {/** * TODO: this href should be dynamic and to be fetched from CMS} ** */}
            <Link href="/business-page" passHref>
              <S.Anchor active={customerType === customerTypes.business}>
                {intl.formatMessage(messages.businessCustomer)}
              </S.Anchor>
            </Link>
          </>
        )}
      </S.TopNavbarContainer>
    </S.Background>
  );
};

export default HeaderTopBar;
