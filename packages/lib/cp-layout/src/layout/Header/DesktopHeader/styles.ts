import styled from 'styled-components';
import Colors from '../../../theme/colors';

export const Wrapper = styled.div`
  padding: 13px 0;
  display: flex;
  justify-content: space-between;
`;

export const OuterWrapper = styled.div`
  & > hr {
    border: 0;
    margin: 0;
    border-bottom: 1px solid ${Colors.black40};
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;
