import React from 'react';
import { Container } from '@material-ui/core';
import MainNavigation from '../MainNavigation';
import { IHeaderNavBar, IMainNavigationData } from '../../../types/header';
import HeaderTopBar from '../HeaderTopBar';
import HeaderButtons from '../HeaderButtons';
import Logo from '../Logo';
import * as S from './styles';

export interface IDesktopHeader extends IHeaderNavBar {
  mainMenu: IMainNavigationData['main_menu'];
  giftIconUrl: string;
  useSWR: Function;
}

const DesktopHeader: React.FC<IDesktopHeader> = ({
  isLoggedIn,
  businessLogo,
  retailLogo,
  mainMenu,
  giftIconUrl,
  useSWR,
}) => (
  <>
    <HeaderTopBar isLoggedIn={isLoggedIn} useSWR={useSWR} />
    <S.OuterWrapper>
      <Container maxWidth="xl">
        <S.Wrapper>
          <Logo
            businessLogo={businessLogo}
            retailLogo={retailLogo}
            isLoggedIn={isLoggedIn}
            useSWR={useSWR}
          />
          <S.ButtonWrapper>
            <HeaderButtons />
          </S.ButtonWrapper>
        </S.Wrapper>
      </Container>
      <hr />
      <Container maxWidth="xl">
        <MainNavigation
          mainMenu={mainMenu}
          isLoggedIn={isLoggedIn}
          giftIconUrl={giftIconUrl}
          useSWR={useSWR}
        />
      </Container>
    </S.OuterWrapper>
  </>
);

export default DesktopHeader;
