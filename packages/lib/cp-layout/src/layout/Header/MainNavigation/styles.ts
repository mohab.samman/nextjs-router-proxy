import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../theme/colors';

export const Wrapper = withTheme(styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  display: flex;
  padding: 12px 0 0;

  ${({ theme }) => theme.breakpoints.down('md')} {
    flex: 1;
    flex-direction: column;
    align-items: flex-start;
    padding: 0;
  }

  a {
    text-decoration: none;
    font-family: inherit;
    font-size: inherit;
    cursor: pointer;
    color: ${Colors.blue60};

    &:hover {
      color: ${Colors.orange60};
    }
  }
`);
