import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../../../utils/test-utils';

import DesktopNav, { IDesktopNav } from '.';

describe('<DesktopNav />', () => {
  const props: IDesktopNav = {
    isLoggedIn: false,
    mainMenu: [
      {
        menu_item: {
          title: 'a menu item',
          href: 'a menu href',
        },
        _metadata: { uid: 'uniqueId234' },
        sub_menu_item: [
          {
            title: 'a menu subitem',
            href: 'a menu subitem href',
          },
        ],
      },
      {
        menu_item: {
          title: 'a menu item',
          href: 'a menu href',
        },
        _metadata: { uid: 'a unique uid' },
        sub_menu_item: [],
      },
    ],
    anchorEl: {},
    open: { uniqueId234: false },
    handleOpen: jest.fn(),
    setMouseOverSubmenu: jest.fn(),
    setMouseOverButton: jest.fn(),
    setCurrentEl: jest.fn(),
  };

  it('renders a <DesktopNav> component that has menu items with and without dropdowns for unauthenticated users (no dropdown opened)', async () => {
    const component = render(<DesktopNav {...props} />);
    const { queryByTestId } = component;
    const submenu = await waitFor(() => queryByTestId('submenuDesktop'));
    await waitFor(() => component);

    expect(submenu).not.toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <DesktopNav> component that has menu items with and without dropdowns for unauthenticated users (with dropdown opened)', async () => {
    const newProps: IDesktopNav = {
      ...props,
      open: { uniqueId234: true },
    };

    const component = render(<DesktopNav {...newProps} />);
    const { getByTestId } = component;
    const submenu = await waitFor(() => getByTestId('submenuDesktop'));
    await waitFor(() => component);

    expect(submenu).toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <DesktopNav> component that has menu items with and without dropdowns for authenticated users', async () => {
    const newProps: IDesktopNav = {
      ...props,
      isLoggedIn: true,
    };
    const component = render(<DesktopNav {...newProps} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
