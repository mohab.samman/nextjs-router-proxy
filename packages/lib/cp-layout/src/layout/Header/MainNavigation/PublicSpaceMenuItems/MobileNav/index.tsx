import React from 'react';
import MenuList from '@material-ui/core/MenuList';
import Link from 'next/link';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { IMainNavigationData } from '../../../../../types/header';
import * as S from './styles';

export interface IMobileNav {
  mainMenu: IMainNavigationData['main_menu'];
  mobileItemState: { [key: string]: boolean };
  setMobileItemState: Function;
}

const renderMenuWithoutDropdown = (menu_item: { title: string; href: string }) => (
  <S.MobileMenuItem key={menu_item.href}>
    <Link href={menu_item.href}>
      <S.Anchor>{menu_item.title}</S.Anchor>
    </Link>
  </S.MobileMenuItem>
);

const renderMenuwithDropdown = ({
  menu_item,
  uid,
  sub_menu_item,
  mobileItemState,
  setMobileItemState,
}: {
  uid: string;
  menu_item: {
    title: string;
    href: string;
  };
  sub_menu_item: {
    title: string;
    href: string;
  }[];
  mobileItemState: { [key: string]: boolean };
  setMobileItemState: Function;
}) => (
  <S.List key={uid}>
    <S.MenuItem
      onClick={() => {
        setMobileItemState({ ...mobileItemState, [uid]: !mobileItemState[uid] });
      }}
    >
      {menu_item.title}
      <KeyboardArrowDownIcon />
    </S.MenuItem>
    {mobileItemState[uid] && (
      <S.Submenu data-testid="submenuMobile">
        <MenuList>
          {sub_menu_item.map((subMenuItem) => (
            <S.SubmenuItem key={subMenuItem.title}>
              <Link href={subMenuItem.href}>{subMenuItem.title}</Link>
            </S.SubmenuItem>
          ))}
        </MenuList>
      </S.Submenu>
    )}
  </S.List>
);

const MobileNav: React.FC<IMobileNav> = ({ mainMenu, ...rest }) => (
  <>
    {mainMenu.map(({ menu_item, _metadata: { uid }, sub_menu_item }) => {
      if (sub_menu_item.length) {
        return renderMenuwithDropdown({
          menu_item,
          uid,
          sub_menu_item,
          ...rest,
        });
      }

      return renderMenuWithoutDropdown(menu_item);
    })}
  </>
);

export default MobileNav;
