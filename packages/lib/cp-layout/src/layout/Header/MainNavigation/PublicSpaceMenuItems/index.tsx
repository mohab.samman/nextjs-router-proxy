/* eslint-disable no-underscore-dangle */
import React from 'react';
import usePublicNavigation from '../../../../hooks/usePublicNavigation';

import { IMainNavigationData } from '../../../../types/header';
import DesktopNav from './DesktopNav';
import MobileNav from './MobileNav';

interface IPublicSpaceMenuItems {
  mainMenu: IMainNavigationData['main_menu'];
  isLoggedIn: boolean;
  isMobile?: boolean;
}

const PublicSpaceMenuItems: React.FC<IPublicSpaceMenuItems> = ({
  mainMenu,
  isLoggedIn,
  isMobile,
}) => {
  const props = usePublicNavigation();

  return isMobile ? (
    <MobileNav mainMenu={mainMenu} {...props} />
  ) : (
    <DesktopNav mainMenu={mainMenu} isLoggedIn={isLoggedIn} {...props} />
  );
};

export default PublicSpaceMenuItems;
