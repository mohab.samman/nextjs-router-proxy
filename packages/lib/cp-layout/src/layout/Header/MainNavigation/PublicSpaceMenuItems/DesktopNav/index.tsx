/* eslint-disable no-underscore-dangle */
import React, { Fragment } from 'react';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import { PopperProps } from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import Link from 'next/link';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

import { IMainNavigationData } from '../../../../../types/header';
import * as S from './styles';

export interface IDesktopNav {
  mainMenu: IMainNavigationData['main_menu'];
  isLoggedIn: boolean;
  anchorEl: { [key: string]: PopperProps['anchorEl'] };
  open: { [key: string]: boolean };
  handleOpen: (event: React.MouseEvent, id: string) => void;
  setMouseOverSubmenu: Function;
  setMouseOverButton: Function;
  setCurrentEl: Function;
}

const renderMenuWithoutDropdown = (
  menu_item: {
    title: string;
    href: string;
  },
  isLoggedIn: boolean,
) => (
  <S.MenuItemWithoutDropdown isLoggedIn={isLoggedIn} key={menu_item.title}>
    <Link href={menu_item.href} passHref>
      {menu_item.title}
    </Link>
  </S.MenuItemWithoutDropdown>
);

const renderMenuwithDropdown = ({
  menu_item,
  uid,
  sub_menu_item,
  anchorEl,
  open,
  isLoggedIn,
  handleOpen,
  setMouseOverSubmenu,
  setMouseOverButton,
  setCurrentEl,
}: {
  uid: string;
  menu_item: {
    title: string;
    href: string;
  };
  sub_menu_item: {
    title: string;
    href: string;
  }[];
  anchorEl: { [key: string]: PopperProps['anchorEl'] };
  open: { [key: string]: boolean };
  isLoggedIn: boolean;
  handleOpen: (event: React.MouseEvent, id: string) => void;
  setMouseOverSubmenu: Function;
  setMouseOverButton: Function;
  setCurrentEl: Function;
}) => (
  <Fragment key={uid}>
    <S.MenuItem
      isLoggedIn={isLoggedIn}
      aria-controls={open ? uid : undefined}
      isOpen={open[uid] || false}
      aria-haspopup="true"
      onMouseLeave={() => setMouseOverButton({ [uid]: false })}
      onMouseOver={(e: React.MouseEvent) => {
        setMouseOverButton({ [uid]: true });
        handleOpen(e, uid);
        setCurrentEl(uid);
      }}
      onClick={(e: React.MouseEvent) => {
        setMouseOverButton({ [uid]: true });
        handleOpen(e, uid);
        setCurrentEl(uid);
      }}
    >
      {menu_item.title}
      <S.ChevronDownWrapper>
        <KeyboardArrowDownIcon />
      </S.ChevronDownWrapper>
    </S.MenuItem>
    <S.Submenu
      open={open[uid] || false}
      anchorEl={anchorEl[uid]}
      role={undefined}
      placement="bottom-start"
      transition
      disablePortal
    >
      {({ TransitionProps }) => (
        <Grow {...TransitionProps} style={{ transformOrigin: 'center top' }}>
          <Paper>
            <MenuList
              onMouseEnter={() => setMouseOverSubmenu({ [uid]: true })}
              onMouseLeave={() => setMouseOverSubmenu({ [uid]: false })}
              id={uid}
              data-testid="submenuDesktop"
            >
              {sub_menu_item.map((subMenuItem) => (
                <S.SubmenuItem key={subMenuItem.title}>
                  <Link href={subMenuItem.href}>{subMenuItem.title}</Link>
                </S.SubmenuItem>
              ))}
            </MenuList>
          </Paper>
        </Grow>
      )}
    </S.Submenu>
  </Fragment>
);

const DesktopNav: React.FC<IDesktopNav> = ({ mainMenu, isLoggedIn, ...rest }) => (
  <>
    {mainMenu.map(({ menu_item, _metadata: { uid }, sub_menu_item }) => {
      if (sub_menu_item.length) {
        return renderMenuwithDropdown({
          menu_item,
          uid,
          sub_menu_item,
          isLoggedIn,
          ...rest,
        });
      }

      return renderMenuWithoutDropdown(menu_item, isLoggedIn);
    })}
  </>
);

export default DesktopNav;
