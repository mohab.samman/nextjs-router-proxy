import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../../../theme/colors';

export const MenuItem = styled.span`
  display: flex;
  padding: 16px;
  align-items: center;
  color: ${Colors.blue60};
  cursor: pointer;

  &:hover {
    color: ${Colors.orange60};
  }
`;

export const List = styled.li`
  width: 100%;
  border-bottom: 1px solid ${Colors.black40};
`;

export const SubmenuItem = styled.li`
  color: ${Colors.blue60};
  display: block;
  hyphens: auto;

  a {
    display: block;
    padding: 8px 0;
    width: 100%;
  }

  &:hover {
    color: ${Colors.orange60};
  }

  &:first-child a {
    padding-top: 0;
  }
  &:last-child a {
    padding-bottom: 0;
  }
`;

export const Anchor = styled.a`
  padding: 16px;
  color: ${Colors.blue60};
  display: block;
  hyphens: auto;

  &:hover {
    color: ${Colors.orange60};
  }
`;

export const MobileMenuItem = styled.li`
  border-bottom: 1px solid ${Colors.black40};
  width: 100%;
`;

export const Submenu = styled.div`
  display: flex;
  padding: 0px 36px 8px;

  ul {
    width: 100%;
  }
`;

export const CategoryHeader = withTheme(styled.div`
  font-size: ${({ theme }) => theme.typography.bodyTextFontSizeLg};
  font-weight: ${({ theme }) => theme.typography.fontWeightSemiBold};
  margin-bottom: 16px;
`);

export const CategoryWrapper = styled.nav`
  flex: 1;
  &:not(:first-child) {
    margin-left: 12px;
  }
  min-width: 189px;
`;

export const ReferAFriendPromo = styled.div`
  padding: 12px 24px;
  background-color: ${Colors.black20};
`;
