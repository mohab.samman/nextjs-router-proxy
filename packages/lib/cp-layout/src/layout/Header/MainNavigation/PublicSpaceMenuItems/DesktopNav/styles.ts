import styled from 'styled-components';
import Popper from '@material-ui/core/Popper';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../../../theme/colors';

export const MenuItemWithoutDropdown = withTheme(styled.li<{ isLoggedIn: boolean }>`
  padding: 0 12px 12px;

  &:first-child {
    padding: ${({ isLoggedIn }) => (isLoggedIn ? '0 12px 12px;' : '0 12px 12px 0;')};
  }
`);

export const MenuItem = styled.li<{ isLoggedIn: boolean; isOpen: boolean }>`
  display: flex;
  align-items: center;
  padding: 0 12px 12px;
  color: ${Colors.blue60};
  cursor: pointer;

  &:first-child {
    padding: ${({ isLoggedIn }) => (isLoggedIn ? '0 12px 12px;' : '0 12px 12px 0;')};
  }

  &:hover {
    color: ${Colors.orange60};
  }

  ${({ isOpen }) =>
    isOpen &&
    `
    & > div:before {
      content: '';
      z-index: 1;
      bottom: -13px;
      right: 4px;
      border: 8px solid transparent;
      position: absolute;
      border-bottom-color: white;
    }
    `}
`;

export const SubmenuItem = styled.li`
  padding: 8px 24px;
  color: ${Colors.blue60};
  cursor: pointer;

  &:hover {
    color: ${Colors.orange60};
  }

  &:first-child {
    padding: 20px 24px 8px;
  }

  &:last-child {
    padding: 8px 24px 20px;
  }
`;

export const Submenu = styled(Popper)`
  & > div {
    border-radius: 4px;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.18);
  }
`;

export const ChevronDownWrapper = withTheme(styled.div`
  position: relative;
  display: flex;
`);
