import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../../../utils/test-utils';

import MobileNav, { IMobileNav } from '.';

describe('<MobileNav />', () => {
  const props: IMobileNav = {
    isLoggedIn: false,
    mainMenu: [
      {
        menu_item: {
          title: 'a menu item',
          href: 'a menu href',
        },
        _metadata: { uid: 'someUniqueItemId' },
        sub_menu_item: [
          {
            title: 'a menu subitem',
            href: 'a menu subitem href',
          },
        ],
      },
      {
        menu_item: {
          title: 'a menu item',
          href: 'a menu href',
        },
        _metadata: { uid: 'someOtherUniqueItemId' },
        sub_menu_item: [
          {
            title: 'a menu subitem',
            href: 'a menu subitem href',
          },
        ],
      },
      {
        menu_item: {
          title: 'a menu item',
          href: 'a menu href',
        },
        _metadata: { uid: 'a unique uid' },
        sub_menu_item: [],
      },
    ],
    mobileItemState: { someUniqueItemId: false },
    setMobileItemState: jest.fn(),
  };

  it('renders a <MobileNav> component that has menu items with and without dropdowns for unauthenticated users (no dropdowns opened)', async () => {
    const component = render(<MobileNav {...props} />);
    const { queryByTestId } = component;
    const submenu = await waitFor(() => queryByTestId('submenuMobile'));
    await waitFor(() => component);

    expect(submenu).not.toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <DesktopNav> component that has menu items with and without dropdowns for unauthenticated users (with dropdown opened)', async () => {
    const newProps: IMobileNav = {
      ...props,
      mobileItemState: { someUniqueItemId: true },
    };

    const component = render(<MobileNav {...newProps} />);
    const { getByTestId } = component;
    const submenu = await waitFor(() => getByTestId('submenuMobile'));
    await waitFor(() => component);

    expect(submenu).toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <MobileNav> component that has menu items with and without dropdowns for authenticated users', async () => {
    const newProps: IMobileNav = {
      ...props,
      isLoggedIn: true,
    };
    const component = render(<MobileNav {...newProps} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
