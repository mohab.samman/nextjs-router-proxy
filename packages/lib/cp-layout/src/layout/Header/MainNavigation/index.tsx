import React from 'react';
import { IMainNavigationData } from '../../../types/header';
import BankingSpaceMenuItems from './BankingSpaceMenuItems';
import PublicSpaceMenuItems from './PublicSpaceMenuItems';
import * as S from './styles';

interface IMainNavigation {
  mainMenu?: IMainNavigationData['main_menu'];
  isLoggedIn: boolean;
  isMobile?: boolean;
  giftIconUrl: string;
  useSWR: Function;
}

const MainNavigation: React.FC<IMainNavigation> = ({
  mainMenu,
  isLoggedIn,
  isMobile,
  giftIconUrl,
  useSWR,
}) => (
  <S.Wrapper>
    {isLoggedIn ? (
      <BankingSpaceMenuItems isMobile={isMobile} giftIconUrl={giftIconUrl} useSWR={useSWR} />
    ) : null}
    {mainMenu && (
      <PublicSpaceMenuItems mainMenu={mainMenu} isLoggedIn={isLoggedIn} isMobile={isMobile} />
    )}
  </S.Wrapper>
);

export default MainNavigation;
