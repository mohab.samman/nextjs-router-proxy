import React from 'react';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import { useIntl } from 'react-intl';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Link from 'next/link';
import * as S from './styles';
import messages from '../mesages';
import { LINKS } from '../../../../../constants/links';
import { IBankingMenuItem } from '../../../../../types/header';

export interface IDesktopNav {
  anchorRef: React.RefObject<HTMLLIElement>;
  open: boolean;
  setMouseOverButton: Function;
  setMouseOverSubmenu: Function;
  giftIconUrl: string;
  bankingMenu: IBankingMenuItem[];
  shouldShowRAF: boolean;
}

const DesktopNav: React.FC<IDesktopNav> = ({
  anchorRef,
  open,
  setMouseOverButton,
  setMouseOverSubmenu,
  giftIconUrl,
  bankingMenu,
  shouldShowRAF,
}) => {
  const intl = useIntl();

  return (
    <>
      <S.MenuItem
        aria-controls={open ? 'menu-online-banking' : undefined}
        aria-haspopup="true"
        isOpen={open}
        ref={anchorRef}
        onMouseOver={() => setMouseOverButton(true)}
        onMouseLeave={() => setMouseOverButton(false)}
        onClick={() => setMouseOverButton(true)}
      >
        {intl.formatMessage(messages.bankingDropdown)}
        <S.ChevronDownWrapper>
          <KeyboardArrowDownIcon />
        </S.ChevronDownWrapper>
      </S.MenuItem>

      <S.SubmenuWrapper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        placement="bottom-start"
        transition
        disablePortal
      >
        {({ TransitionProps }) => (
          <Grow {...TransitionProps} style={{ transformOrigin: 'left top' }}>
            <Paper
              onMouseLeave={() => setMouseOverSubmenu(false)}
              onMouseEnter={() => setMouseOverSubmenu(true)}
              id="menu-online-banking"
            >
              <S.Submenu data-testid="submenuDesktop">
                {bankingMenu
                  .filter((item) => (item.showFn ? item.showFn() : true))
                  .map(({ category, subItems }) => (
                    <S.CategoryWrapper key={category}>
                      <S.CategoryHeader>{intl.formatMessage(messages[category])}</S.CategoryHeader>
                      {subItems
                        .filter((subItem) => (subItem.showFn ? subItem.showFn() : true))
                        .map(({ label, href }) => (
                          <Link key={label} href={href} passHref>
                            <S.Anchor>{intl.formatMessage(messages[label])}</S.Anchor>
                          </Link>
                        ))}
                    </S.CategoryWrapper>
                  ))}
              </S.Submenu>
              {shouldShowRAF && (
                <S.ReferAFriendPromo>
                  <Link href={LINKS.REFER_A_FRIEND} passHref>
                    <S.ReferAFriendAnchor>
                      <img src={giftIconUrl} alt="gift" loading="lazy" />
                      {intl.formatMessage(messages.referAFriendDesktop)}
                    </S.ReferAFriendAnchor>
                  </Link>
                </S.ReferAFriendPromo>
              )}
            </Paper>
          </Grow>
        )}
      </S.SubmenuWrapper>
    </>
  );
};

export default DesktopNav;
