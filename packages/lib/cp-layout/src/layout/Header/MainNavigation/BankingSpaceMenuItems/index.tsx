import React from 'react';
import useBankingNavigation from '../../../../hooks/useBankingNavigation';
import DesktopNav from './DesktopNav';
import MobileNav from './MobileNav';
interface IBankingSpaceMenuItems {
  isMobile?: boolean;
  giftIconUrl: string;
  useSWR: Function;
}

const BankingSpaceMenuItems: React.FC<IBankingSpaceMenuItems> = ({
  isMobile,
  giftIconUrl,
  useSWR,
}) => {
  const props = useBankingNavigation(useSWR);

  return isMobile ? (
    <MobileNav {...props} giftIconUrl={giftIconUrl} />
  ) : (
    <DesktopNav {...props} giftIconUrl={giftIconUrl} />
  );
};

export default BankingSpaceMenuItems;
