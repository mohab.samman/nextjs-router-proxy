import { defineMessages } from 'react-intl';

const messages = defineMessages({
  referAFriendDesktop: {
    id: 'header.desktop.referAFriend',
    defaultMessage: 'Recommend Raisin and Secure Your Bonus of €50 + €50',
  },
  referAFriendMobile: {
    id: 'header.mobile.referAFriend',
    defaultMessage: 'Refer a Friend',
  },
  bankingDropdown: {
    id: 'header.mainNav.banking',
    defaultMessage: 'Your Raisin Account',
  },
  myInvestments: {
    id: 'header.mainNav.myInvestments',
    defaultMessage: 'My Investments',
  },
  postbox: {
    id: 'header.mainNav.postbox',
    defaultMessage: 'Postbox',
  },
  administration: {
    id: 'header.mainNav.administration',
    defaultMessage: 'Settings',
  },

  dashboard: {
    id: 'header.mainNav.dashboard',
    defaultMessage: 'Dashboard',
  },
  fixedDeposit: {
    id: 'header.mainNav.fixedDeposit',
    defaultMessage: 'Fixed Deposit',
  },
  overnightDeposit: {
    id: 'header.mainNav.overnightDeposit',
    defaultMessage: 'Overnight Deposit',
  },
  noticeDeposit: {
    id: 'header.mainNav.noticeDeposit',
    defaultMessage: 'Notice Account',
  },
  investmentProductsRobo: {
    id: 'header.mainNav.investmentProductsRobo',
    defaultMessage: 'ETF Robo',
  },
  investmentProductsConfigurator: {
    id: 'header.mainNav.investmentProductsConfigurator',
    defaultMessage: 'ETF Configurator',
  },
  pensionProducts: {
    id: 'header.mainNav.pensionProducts',
    defaultMessage: 'Pension Products',
  },

  postboxMessages: {
    id: 'header.mainNav.postboxMessages',
    defaultMessage: 'Messages',
  },
  postboxDocuments: {
    id: 'header.mainNav.postboxDocuments',
    defaultMessage: 'Documents',
  },
  postboxSentMessages: {
    id: 'header.mainNav.postboxSentMessages',
    defaultMessage: 'Sent',
  },

  myData: {
    id: 'header.mainNav.myData',
    defaultMessage: 'My Data',
  },
  transactions: {
    id: 'header.mainNav.transactions',
    defaultMessage: 'Transactions',
  },
  payOut: {
    id: 'header.mainNav.payOut',
    defaultMessage: 'Payout',
  },
  blockAcc: {
    id: 'header.mainNav.blockAcc',
    defaultMessage: 'Deactivate Account',
  },
  blockTan: {
    id: 'header.mainNav.blockTan',
    defaultMessage: 'Deactivate mTAN',
  },
  taxExemption: {
    id: 'header.mainNav.taxExemption',
    defaultMessage: 'Tax exemption',
  },
  forms: {
    id: 'header.mainNav.forms',
    defaultMessage: 'Show Forms',
  },
  switchTanMethod: {
    id: 'header.mainNav.switchTanMethod',
    defaultMessage: 'Switch Tan Method',
  },
});

export default messages;
