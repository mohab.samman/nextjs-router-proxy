import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../../../theme/colors';

export const MenuItem = styled.span`
  display: flex;
  padding: 16px;
  align-items: center;
  color: ${Colors.blue60};
  cursor: pointer;

  &:hover {
    color: ${Colors.orange60};
  }
`;

export const List = styled.li`
  width: 100%;
  border-bottom: 1px solid ${Colors.black40};
`;

export const Anchor = styled.a`
  padding: 8px 0;
  color: ${Colors.blue60};
  display: block;
  hyphens: auto;

  &:hover {
    color: ${Colors.orange60};
  }

  &:first-child {
    padding-top: 0;
  }
  &:last-child {
    padding-bottom: 0;
  }
`;

export const Submenu = styled.div`
  display: flex;
  padding: 0px 36px 16px;
  flex-direction: column;

  nav:first-child p {
    margin-top: 0;
  }
`;

export const CategoryHeader = withTheme(styled.p`
  font-size: ${({ theme }) => theme.typography.bodyTextFontSizeLg};
  font-weight: ${({ theme }) => theme.typography.fontWeightSemiBold};
  margin: 24px 0 8px;
`);

export const CategoryWrapper = styled.nav`
  flex: 1;
  min-width: 189px;
`;

export const ReferAFriendPromo = styled.div`
  padding: 16px;
  background-color: ${Colors.black20};
`;

export const ReferAFriendAnchor = styled.a`
  display: flex;
  align-items: center;

  img {
    padding-right: 8px;
  }
`;
