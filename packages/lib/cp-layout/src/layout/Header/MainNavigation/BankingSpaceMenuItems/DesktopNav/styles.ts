import styled from 'styled-components';
import Popper from '@material-ui/core/Popper';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../../../theme/colors';

export const MenuItem = styled.li<{ isOpen: boolean }>`
  display: flex;
  align-items: center;
  padding: 0 12px 12px 0;
  color: ${Colors.blue60};
  cursor: pointer;

  &:hover {
    color: ${Colors.orange60};
  }

  ${({ isOpen }) =>
    isOpen &&
    `
    & > div:before {
      content: '';
      z-index: 1;
      bottom: -13px;
      right: 4px;
      border: 8px solid transparent;
      position: absolute;
      border-bottom-color: white;
    }
  `}
`;

export const Anchor = styled.a`
  padding: 8px 0;
  color: ${Colors.blue60};
  display: block;
  hyphens: auto;

  &:hover {
    color: ${Colors.orange60};
  }

  &:first-child {
    padding-top: 0;
  }
  &:last-child {
    padding-bottom: 0;
  }
`;

export const SubmenuWrapper = styled(Popper)`
  & > div {
    border-radius: 4px;
    box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.18);
  }
`;

export const Submenu = styled.div`
  display: flex;
  padding: 24px 24px 16px;
`;

export const CategoryHeader = withTheme(styled.p`
  font-size: ${({ theme }) => theme.typography.bodyTextFontSizeLg};
  font-weight: ${({ theme }) => theme.typography.fontWeightSemiBold};
  margin-bottom: 16px;
`);

export const CategoryWrapper = styled.nav`
  flex: 1;
  &:not(:first-child) {
    margin-left: 12px;
  }
  min-width: 189px;
`;

export const ReferAFriendPromo = styled.div`
  padding: 12px 24px;
  background-color: ${Colors.black20};
`;

export const ChevronDownWrapper = styled.div`
  position: relative;
  display: flex;
`;

export const ReferAFriendAnchor = styled.a`
  display: flex;
  align-items: center;

  img {
    padding-right: 8px;
  }
`;
