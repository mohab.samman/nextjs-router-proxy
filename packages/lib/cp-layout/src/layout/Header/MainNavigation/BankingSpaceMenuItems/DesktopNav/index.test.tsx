import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../../../utils/test-utils';

import DesktopNav, { IDesktopNav } from '.';

describe('<DesktopNav />', () => {
  const props: IDesktopNav = {
    anchorRef: {
      current: {
        getBoundingClientRect: jest.fn(),
      },
    },
    open: false,
    setMouseOverButton: jest.fn(),
    setMouseOverSubmenu: jest.fn(),
    giftIconUrl: 'some/icon/url',
    bankingMenu: [
      {
        category: 'postbox',
        showFn: () => true,
        subItems: [
          {
            label: 'postboxMessages',
            href: 'href to postbox messages',
            showFn: () => true,
          },
        ],
      },
    ],
    shouldShowRAF: true,
    useSWR: jest.fn(),
  };

  it('renders a <DesktopNav> with the banking submenu closed', async () => {
    const component = render(<DesktopNav {...props} />);
    const { queryByTestId } = component;
    const submenu = await waitFor(() => queryByTestId('submenuDesktop'));
    await waitFor(() => component);

    expect(submenu).not.toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <DesktopNav> with the banking submenu opened', async () => {
    const localProps = {
      ...props,
      open: true,
    };

    const component = render(<DesktopNav {...localProps} />);
    const { getByTestId } = component;
    const submenu = await waitFor(() => getByTestId('submenuDesktop'));
    await waitFor(() => component);

    expect(submenu).toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });
});
