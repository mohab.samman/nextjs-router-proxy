import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../../../utils/test-utils';

import MobileNav, { IMobileNav } from '.';

describe('<MobileNav />', () => {
  const props: IMobileNav = {
    open: true,
    setMouseOverButton: jest.fn(),
    giftIconUrl: 'some/icon/url',
    bankingMenu: [
      {
        category: 'postbox',
        showFn: () => true,
        subItems: [
          {
            label: 'postboxMessages',
            href: 'href to postbox Messages',
            showFn: () => true,
          },
        ],
      },
    ],
    shouldShowRAF: true,
  };

  it('renders a <MobileNav> component with open menu items', async () => {
    const component = render(<MobileNav {...props} />);
    const { getByTestId } = component;
    const submenuCategory = await waitFor(() => getByTestId('mobileSubmenuCategory'));
    const rafSection = await waitFor(() => getByTestId('RAFSection'));
    await waitFor(() => component);

    expect(rafSection).toBeInTheDocument();
    expect(submenuCategory).toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <MobileNav> component with closed menu items', async () => {
    const newProps: IMobileNav = {
      ...props,
      open: false,
    };

    const component = render(<MobileNav {...newProps} />);
    const { queryByTestId } = component;
    const submenuCategory = await waitFor(() => queryByTestId('mobileSubmenuCategory'));
    await waitFor(() => component);

    expect(submenuCategory).not.toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <MobileNav> component without RAF section', async () => {
    const newProps: IMobileNav = {
      ...props,
      open: true,
      shouldShowRAF: false,
    };

    const component = render(<MobileNav {...newProps} />);
    const { queryByTestId } = component;
    const rafSection = await waitFor(() => queryByTestId('RAFSection'));
    await waitFor(() => component);

    expect(rafSection).not.toBeInTheDocument();
    expect(component).toMatchSnapshot();
  });

  it('renders a <MobileNav> component without RAF section', async () => {
    const newProps: IMobileNav = {
      ...props,
      open: true,
      bankingMenu: [
        {
          category: 'postbox',
          showFn: () => true,
          subItems: [
            {
              label: 'postboxMessages',
              href: 'href to postbox Messages',
              showFn: () => true,
            },
            {
              label: 'postboxDocuments',
              href: 'href to postbox documents',
            },
            {
              label: 'pensionProducts',
              href: 'href to pension products',
              showFn: () => false,
            },
          ],
        },
        {
          category: 'dashboard',
          showFn: () => true,
          subItems: [],
        },
        {
          category: 'myInvestments',
          showFn: () => false,
          subItems: [],
        },
      ],
    };

    const component = render(<MobileNav {...newProps} />);
    const { getAllByTestId } = component;
    const submenuCategory = await waitFor(() => getAllByTestId('mobileSubmenuCategory'));
    const submenuItem = await waitFor(() => getAllByTestId('mobileSubmenuLink'));
    await waitFor(() => component);

    expect(submenuCategory.length).toBe(1);
    expect(submenuItem.length).toBe(2);
    expect(component).toMatchSnapshot();
  });
});
