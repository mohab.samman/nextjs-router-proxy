import React from 'react';
import { useIntl } from 'react-intl';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Link from 'next/link';
import * as S from './styles';
import messages from '../mesages';
import { LINKS } from '../../../../../constants/links';
import { IBankingMenuItem } from '../../../../../types/header';

export interface IMobileNav {
  open: boolean;
  setMouseOverButton: Function;
  giftIconUrl: string;
  bankingMenu: IBankingMenuItem[];
  shouldShowRAF: boolean;
}

const MobileNav: React.FC<IMobileNav> = ({
  open,
  setMouseOverButton,
  giftIconUrl,
  bankingMenu,
  shouldShowRAF,
}) => {
  const intl = useIntl();

  return (
    <S.List>
      <S.MenuItem onClick={() => setMouseOverButton(!open)}>
        {intl.formatMessage(messages.bankingDropdown)}
        <KeyboardArrowDownIcon />
      </S.MenuItem>
      {open && (
        <>
          <S.Submenu data-testid="mobileSubmenuCategory">
            {bankingMenu
              .filter((item) => (item.showFn ? item.showFn() : true))
              .map(({ category, subItems }) => (
                <S.CategoryWrapper key={category}>
                  <S.CategoryHeader>{intl.formatMessage(messages[category])}</S.CategoryHeader>
                  {subItems
                    .filter((subItem) => (subItem.showFn ? subItem.showFn() : true))
                    .map(({ label, href }) => (
                      <Link key={label} href={href} passHref>
                        <S.Anchor data-testid="mobileSubmenuLink">
                          {intl.formatMessage(messages[label])}
                        </S.Anchor>
                      </Link>
                    ))}
                </S.CategoryWrapper>
              ))}
          </S.Submenu>
          {shouldShowRAF && (
            <S.ReferAFriendPromo data-testid="RAFSection">
              <Link href={LINKS.REFER_A_FRIEND} passHref>
                <S.ReferAFriendAnchor>
                  <img src={giftIconUrl} alt="gift" loading="lazy" />
                  {intl.formatMessage(messages.referAFriendMobile)}
                </S.ReferAFriendAnchor>
              </Link>
            </S.ReferAFriendPromo>
          )}
        </>
      )}
    </S.List>
  );
};

export default MobileNav;
