import { defineMessages } from 'react-intl';

const messages = defineMessages({
  menuText: {
    id: 'header.mobile.menuText',
    defaultMessage: 'Menu',
  },
});

export default messages;
