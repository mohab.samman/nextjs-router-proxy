import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { IHeaderNavBar, IMainNavigationData } from '../../../types/header';
import HeaderTopBar from '../HeaderTopBar';
import messages from './messages';

import * as S from './styles';
import Logo from '../Logo';
import { CloseIcon, OpenIcon } from './mobileMenuIcons';
import MainNavigation from '../MainNavigation';
import HeaderButtons from '../HeaderButtons';

export interface IMobileHeader extends IHeaderNavBar {
  mainMenu: IMainNavigationData['main_menu'];
  giftIconUrl: string;
  useSWR: Function;
}

const MobileHeader: React.FC<IMobileHeader> = ({
  isLoggedIn,
  businessLogo,
  retailLogo,
  mainMenu,
  giftIconUrl,
  useSWR,
}) => {
  const [isOpen, setOpenState] = useState(false);
  const intl = useIntl();

  const toggleDrawer = () => {
    setOpenState(!isOpen);
  };

  return (
    <>
      <S.StyledContainer maxWidth="xl">
        <S.Wrapper>
          <Logo
            businessLogo={businessLogo}
            retailLogo={retailLogo}
            isLoggedIn={isLoggedIn}
            useSWR={useSWR}
          />
          <S.IconWrapper onClick={toggleDrawer}>
            <S.Button type="button">{intl.formatMessage(messages.menuText)}</S.Button>
            {isOpen ? <CloseIcon /> : <OpenIcon />}
          </S.IconWrapper>
        </S.Wrapper>
      </S.StyledContainer>

      <S.StyledDrawer
        ModalProps={{
          disablePortal: true,
        }}
        anchor="top"
        open={isOpen}
        onClose={toggleDrawer}
      >
        <HeaderTopBar isLoggedIn={isLoggedIn} useSWR={useSWR} />
        <MainNavigation
          mainMenu={mainMenu}
          isLoggedIn={isLoggedIn}
          isMobile
          giftIconUrl={giftIconUrl}
          useSWR={useSWR}
        />
        <S.ButtonWrapper>
          <HeaderButtons />
        </S.ButtonWrapper>
      </S.StyledDrawer>
    </>
  );
};

export default MobileHeader;
