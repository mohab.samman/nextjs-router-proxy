import React from 'react';

export const CloseIcon = () => (
  <svg
    width="24"
    height="24"
    xmlns="http://www.w3.org/2000/svg"
    aria-labelledby="closeTitle"
    role="img"
  >
    <title id="closeTitle">mobile close menu button</title>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0L24 0 24 24 0 24z" />
      <path
        fill="#404040"
        fillRule="nonzero"
        d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"
      />
    </g>
  </svg>
);

export const OpenIcon = () => (
  <svg
    width="24"
    height="24"
    xmlns="http://www.w3.org/2000/svg"
    aria-labelledby="openTitle"
    role="img"
  >
    <title id="openTitle">mobile open menu button</title>
    <g fill="none" fillRule="evenodd">
      <path d="M0 0L24 0 24 24 0 24z" />
      <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z" fill="#404040" fillRule="nonzero" />
    </g>
  </svg>
);
