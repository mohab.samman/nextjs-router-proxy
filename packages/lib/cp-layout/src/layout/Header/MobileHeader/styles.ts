import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import { Container } from '@material-ui/core';
import Drawer from '@material-ui/core/Drawer';
import Colors from '../../../theme/colors';

export const Wrapper = withTheme(styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 56px;
`);

export const StyledContainer = styled(Container)`
  position: relative;
  background: white;
  z-index: 1400;
  border-bottom: 1px solid ${Colors.black40};
`;

export const StyledDrawer = withTheme(styled(Drawer)`
  > .MuiDrawer-paper {
    margin-top: 57px;
  }
`);

export const IconWrapper = withTheme(styled.div`
  display: flex;
`);

export const Button = withTheme(styled.button`
  background: none;
  border: none;
  color: ${Colors.black80};
  font-size: ${({ theme }) => theme.typography.bodyTextFontSize};
  > img {
    padding-left: 5px;
  }
`);

export const ButtonWrapper = withTheme(styled.div`
  display: flex;
  justify-content: space-between;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    padding: 9px 24px;
    border-bottom: 1px solid ${Colors.black40};
  }
`);
