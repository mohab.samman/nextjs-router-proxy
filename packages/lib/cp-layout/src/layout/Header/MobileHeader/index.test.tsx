import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils';

import MobileHeader, { IMobileHeader } from '.';

describe('<MobileHeader />', () => {
  const props: IMobileHeader = {
    isLoggedIn: false,
    businessLogo: {
      url:
        'https://eu-images.contentstack.com/v3/assets/bltaad01d1a2ba34de9/blt70e3ad2071adbecf/609bed21996bf2507755271e/weltsparen-business-logo.svg',
      title: 'weltsparen-business-logo.svg',
    },
    retailLogo: {
      url:
        'https://eu-images.contentstack.com/v3/assets/bltaad01d1a2ba34de9/blt0b57f76ef82e1fd6/5f451f4c8c65956b03a80130/weltsparen-logo.svg',
      title: 'weltsparen-logo.svg',
    },
    mainMenu: [],
    giftIconUrl: 'some/string',
    useSWR: () => ({ data: null }),
  };

  it('renders a <MobileHeader>', async () => {
    const component = render(<MobileHeader {...props} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
