import React from 'react';
import { useIntl } from 'react-intl';
import { Container, Link, Typography } from '@material-ui/core';
import { LOCALES } from '../../../constants/locales';
import { PLAY_STORE_BADGES, APP_STORE_BADGES } from './images';

import messages from './messages';
import * as S from './styles';
interface IMobileBanner {
  mobileBannerIconURL: string;
}

const getIOSLocale = (iosLocale: string) => {
  if (iosLocale === LOCALES['en-US'].label) {
    return '';
  }

  return `${iosLocale}/`;
};

const MobileBanner: React.FC<IMobileBanner> = ({ mobileBannerIconURL }) => {
  const intl = useIntl();

  const AppStoreBadge =
    APP_STORE_BADGES[intl.locale] ?? APP_STORE_BADGES[LOCALES[intl.defaultLocale].label];

  const PlayStoreBadge =
    PLAY_STORE_BADGES[intl.locale] ?? PLAY_STORE_BADGES[LOCALES[intl.defaultLocale].label];

  return (
    <S.Background>
      <Container maxWidth="xl">
        <S.Wrapper>
          {mobileBannerIconURL && intl.locale !== LOCALES['en-GB'].label && (
            <S.Icon src={mobileBannerIconURL} alt="" loading="lazy" />
          )}
          <S.Content>
            <S.TextWrapper>
              <S.Heading>{intl.formatMessage(messages.title)}</S.Heading>
              <Typography variant="body1">{intl.formatMessage(messages.text)}</Typography>
            </S.TextWrapper>
            <S.ImagesWrapper>
              {/* TODO: add tracking on this link */}
              <Link
                rel="noreferrer noopener"
                target="_blank"
                href="https://play.google.com/store/apps/details?id=com.raisin.app"
              >
                <PlayStoreBadge />
              </Link>
              {/* TODO: add tracking on this link */}
              <Link
                rel="noreferrer noopener"
                target="_blank"
                href={`https://apps.apple.com/${getIOSLocale(intl.locale)}app/id1515520813`}
              >
                <AppStoreBadge />
              </Link>
            </S.ImagesWrapper>
          </S.Content>
        </S.Wrapper>
      </Container>
    </S.Background>
  );
};

export default MobileBanner;
