import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../theme/colors';

export const Background = styled.div`
  background-color: ${Colors.black10};
`;

export const Wrapper = withTheme(styled.div`
  padding: 40px 0;
  display: flex;

  ${({ theme }) => theme.breakpoints.down('sm')} {
    padding: 30px 0;
  }
`);

export const Content = withTheme(styled.div`
  display: flex;

  ${({ theme }) => theme.breakpoints.down('lg')} {
    flex-direction: column;
  }
`);

export const Icon = withTheme(styled.img`
  max-width: 80px;
  max-height: 80px;
  margin-right: 16px;

  ${({ theme }) => theme.breakpoints.down('sm')} {
    display: none;
  }
`);

export const TextWrapper = withTheme(styled.div`
  display: flex;
  flex: 0.8;
  flex-direction: column;

  ${({ theme }) => theme.breakpoints.down('lg')} {
    flex: 1;
  }

  ${({ theme }) => theme.breakpoints.down('sm')} {
    text-align: center;
  }
`);

export const Heading = withTheme(styled.span`
  font-weight: 700;
  margin-bottom: 10px;
`);

export const ImagesWrapper = withTheme(styled.div`
  display: flex;
  align-items: center;

  & > a:first-child {
    margin-right: 20px;
  }

  & > a {
    font-size: 0;
  }

  ${({ theme }) => theme.breakpoints.up('lg')} {
    margin-left: auto;
  }

  ${({ theme }) => theme.breakpoints.down('lg')} {
    margin-top: 20px;
  }

  ${({ theme }) => theme.breakpoints.down('sm')} {
    flex-direction: column;

    & > a:first-child {
      margin-right: 0;
      margin-bottom: 10px;
    }
  }
`);
