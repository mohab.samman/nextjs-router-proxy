import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'footer.mobileBanner.title',
    defaultMessage: 'See how your wealth is growing on the go',
  },
  text: {
    id: 'footer.mobileBanner.text',
    defaultMessage:
      'With the Raisin app, you always have your Raisin account quickly at hand and can start new investments with just a few clicks.',
  },
  playStoreImage: {
    id: 'footer.mobileBanner.playStore.alt',
    defaultMessage: 'PlayStore logo',
    description: 'alternate text for image',
  },
  appStoreImage: {
    id: 'footer.mobileBanner.appStore.alt',
    defaultMessage: 'AppStore logo',
    description: 'alternate text for image',
  },
});

export default messages;
