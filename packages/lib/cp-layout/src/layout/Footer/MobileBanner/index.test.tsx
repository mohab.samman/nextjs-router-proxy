import '@testing-library/jest-dom';
import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils'; // from our RTL's custom render util

import MobileBanner from './index';

describe('<MobileBanner />', () => {
  it('should match snapshot', async () => {
    const component = render(<MobileBanner mobileBannerIconURL="/some/path/to/icon" />);
    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
