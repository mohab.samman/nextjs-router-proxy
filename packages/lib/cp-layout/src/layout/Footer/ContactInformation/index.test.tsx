import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils';

import ContactInformation from '.';

describe('<ContactInformation />', () => {
  const props = {
    isTrustPilotAvailable: true,
    contactInformation: {
      phone: {
        phone_number: '+4930770191295',
        availability: '(Mo – Fr 8:30 AM to 4:30 PM (CET))',
        icon: [
          {
            uid: 'string',
            _content_type_uid: 'string',
          },
        ],
      },
      email: {
        email_address: 'service@raisin.com',
        icon: [
          {
            uid: 'string',
            _content_type_uid: 'string',
          },
        ],
      },
      display_contact_information: true,
    },
    socialMediaIcons: [
      {
        uid: 'cs0e2c8fcd78493cb6',
        iconUrl: 'https://facebook.svg',
        link: 'https://facebook.com/weltsparen',
        account: 'Facebook',
      },
      {
        uid: 'csebb9de37ecb3f947',
        iconUrl: 'https://linkedin.svg',
        link: 'https://linkedin.com/weltsparen',
        account: 'LinkedIn',
      },
      {
        uid: 'csfdc4b4c40af404df',
        iconUrl: 'https://twitter.svg',
        link: 'https://twitter.com/weltsparen',
        account: 'Twitter',
      },
      {
        uid: 'cse20f7212f9b239bb',
        iconUrl: 'https://youtube.svg',
        link: 'https://youtube.com/weltsparen',
        account: 'YouTube',
      },
      {
        uid: 'csa574e2dccc9cf21c',
        iconUrl: 'https://instagram.svg',
        link: 'https://instagram.com/weltsparen',
        account: 'Instagram',
      },
      {
        uid: 'cs238368e1c893ff51',
        iconUrl: 'https://xing.svg',
        link: 'https://xing.com/weltsparen',
        account: 'Xing',
      },
    ],
  };

  it('renders a <ContactInformation> component', async () => {
    const component = render(<ContactInformation {...props} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
