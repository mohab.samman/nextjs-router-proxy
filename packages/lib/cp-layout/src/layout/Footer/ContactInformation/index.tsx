import React from 'react';
import Container from '@material-ui/core/Container';
import { IFooterData, IContactInformationData, ISocialMediaIcons } from '../../../types/footer';
import ContactDetails from '../ContactDetails';
import SocialMedia from '../SocialMedia';
import * as S from './styles';

interface IContactInformation {
  contactInformation?: IContactInformationData;
  socialMediaIcons?: ISocialMediaIcons[];
  isTrustPilotAvailable?: IFooterData['display_trustpilot'];
  phoneIconURL?: string;
  emailIconURL?: string;
}

const ContactInformation: React.FC<IContactInformation> = React.memo(
  ({ contactInformation, socialMediaIcons, isTrustPilotAvailable, phoneIconURL, emailIconURL }) => (
    <S.Background isTrustPilotAvailable={isTrustPilotAvailable}>
      <Container maxWidth="xl">
        <S.Wrapper>
          {contactInformation?.display_contact_information ? (
            <ContactDetails
              phoneIconURL={phoneIconURL}
              emailIconURL={emailIconURL}
              contactInformation={contactInformation}
            />
          ) : null}
          {socialMediaIcons?.length ? <SocialMedia icons={socialMediaIcons} /> : null}
        </S.Wrapper>
      </Container>
    </S.Background>
  ),
);

export default ContactInformation;
