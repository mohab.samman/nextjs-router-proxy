import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../theme/colors';

export const Background = withTheme(styled('div')<{ isTrustPilotAvailable: boolean }>`
  background-color: ${({ isTrustPilotAvailable }) => (isTrustPilotAvailable ? '' : Colors.black10)};
`);

export const Wrapper = withTheme(styled.div`
  display: flex;
  justify-content: space-between;
  box-sizing: border-box;
  padding: 20px 0;

  & > div:first-child {
    align-items: flex-start;
  }

  ${({ theme }) => theme.breakpoints.down('sm')} {
    flex-direction: column;
    align-items: center;

    & > div:first-child {
      align-items: center;
    }
  }
`);
