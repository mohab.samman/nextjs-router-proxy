import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../theme/colors';

export const Disclaimer = withTheme(styled.div`
  padding: 20px 0;
  border-top: 1px solid ${Colors.black40};
  font-size: ${({ theme }) => theme.typography.disclaimerFontSize};
  line-height: 1.67;
  color: ${Colors.black60};

  p {
    margin: 0;
  }
`);
