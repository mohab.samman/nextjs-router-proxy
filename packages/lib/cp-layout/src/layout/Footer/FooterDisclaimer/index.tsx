import React from 'react';
import DOMPurify from 'isomorphic-dompurify';
import { IFooterData } from '../../../types/footer';
import * as S from './styles';
interface IFooterDisclaimer {
  disclaimer: IFooterData['disclaimer'];
}

const FooterDisclaimer: React.FC<IFooterDisclaimer> = React.memo(({ disclaimer }) => (
  <S.Disclaimer
    dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(disclaimer) }}
    data-testid="footer-disclaimer"
  />
));

export default FooterDisclaimer;
