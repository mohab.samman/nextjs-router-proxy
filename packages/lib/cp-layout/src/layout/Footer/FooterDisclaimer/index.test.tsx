import React from 'react';
import { render } from '../../../utils/test-utils';
import FooterDisclaimer from './index';

describe('<FooterDisclaimer />', () => {
  it('should render the <FooterDisclaimer /> component', () => {
    const { getByTestId } = render(<FooterDisclaimer disclaimer="mock disclaimer text" />);

    expect(getByTestId('footer-disclaimer')).toBeInTheDocument();
  });

  it('should match the snapshot for footer disclaimer', () => {
    const component = render(<FooterDisclaimer disclaimer="mock disclaimer text" />);

    expect(component).toMatchSnapshot();
  });
});
