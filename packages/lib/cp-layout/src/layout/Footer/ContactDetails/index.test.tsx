import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils';

import ContactDetails from '.';

describe('<ContactDetails />', () => {
  const props = {
    contactInformation: {
      phone: {
        phone_number: '+4930770191295',
        availability: '(Mo – Fr 8:30 AM to 4:30 PM (CET))',
        icon: [
          {
            uid: 'string',
            _content_type_uid: 'string',
          },
        ],
      },
      email: {
        email_address: 'service@raisin.com',
        icon: [
          {
            uid: 'string',
            _content_type_uid: 'string',
          },
        ],
      },
      display_contact_information: true,
    },
  };

  it('renders a <ContactDetails> component', async () => {
    const component = render(<ContactDetails {...props} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
