import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { IContactInformationData } from '../../../types/footer';
import messages from './messages';
import * as S from './styles';

interface IContactDetails extends WrappedComponentProps {
  contactInformation: IContactInformationData;
  phoneIconURL?: string;
  emailIconURL?: string;
}

const ContactDetails: React.FC<IContactDetails> = ({
  contactInformation,
  intl,
  phoneIconURL,
  emailIconURL,
}) => (
  <S.Wrapper>
    <S.Heading>{intl.formatMessage(messages.title)}</S.Heading>
    <S.Container>
      <S.DetailsContainer>
        <S.ScheduleContainer>
          <S.Anchor href={`tel:${contactInformation?.phone?.phone_number}`}>
            <S.Icon src={phoneIconURL} alt="" loading="lazy" />
            {contactInformation?.phone?.phone_number}
          </S.Anchor>
          <S.ScheduleItem>{contactInformation?.phone?.availability}</S.ScheduleItem>
        </S.ScheduleContainer>
      </S.DetailsContainer>
      <S.DetailsContainer>
        <S.Icon src={emailIconURL} alt="" loading="lazy" />
        <S.Anchor href={`mailto:${contactInformation?.email?.email_address}`}>
          {contactInformation?.email?.email_address}
        </S.Anchor>
      </S.DetailsContainer>
    </S.Container>
  </S.Wrapper>
);

export default injectIntl(ContactDetails);
