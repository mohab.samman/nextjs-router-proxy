import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'footer.contactDetails.title',
    defaultMessage: 'Do you have questions?',
    description: 'Title for the phone and email address of the company',
  },
});

export default messages;
