import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

export const Wrapper = withTheme(styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 0;

  ${(props) => props.theme.breakpoints.down('sm')} {
    align-items: center;
    margin-bottom: 30px;
  }
`);

export const Heading = withTheme(styled.h5`
  font-size: ${({ theme }) => theme.typography.bodyTextFontSizeLg};
  line-height: 1.44;
  margin: 0;
  user-select: none;
`);

export const Container = styled.div`
  margin: 10px 0 0;
`;

export const DetailsContainer = withTheme(styled.div`
  display: flex;

  &:first-child {
    margin-bottom: 6px;
  }

  ${(props) => props.theme.breakpoints.down('sm')} {
    justify-content: center;
  }
`);

export const Icon = styled.img`
  margin: 0 8px 0 0;
  height: fit-content;
  filter: invert(27%) sepia(18%) saturate(0%) hue-rotate(186deg) brightness(84%) contrast(93%);
`;

export const ScheduleContainer = withTheme(styled.div`
  display: flex;
  flex-direction: ${(props) =>
    useMediaQuery(props.theme.breakpoints.up('lg')) ? 'row' : 'column'};
`);

export const ScheduleItem = withTheme(styled.div`
  margin-left: 30px;

  ${(props) => props.theme.breakpoints.up('lg')} {
    margin-left: 5px;
  }

  ${(props) => props.theme.breakpoints.down('sm')} {
    margin-left: 0px;
  }
`);

export const Anchor = withTheme(styled.a`
  color: ${(props) => props.theme.palette.primary.main};
  text-decoration: none;
  display: flex;

  ${(props) => props.theme.breakpoints.down('sm')} {
    justify-content: center;
  }
`);
