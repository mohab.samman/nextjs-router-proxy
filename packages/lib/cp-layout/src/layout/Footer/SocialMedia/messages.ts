import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'footer.socialMedia.title',
    defaultMessage: 'Follow us on',
    description: 'Social media title',
  },
});

export default messages;
