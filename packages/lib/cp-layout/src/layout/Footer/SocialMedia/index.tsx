import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { ISocialMediaIcons } from '../../../types/footer';
import messages from './messages';
import * as S from './styles';

interface ISocialMedia extends WrappedComponentProps {
  icons: ISocialMediaIcons[];
}

const SocialMedia: React.FC<ISocialMedia> = React.memo(({ intl, icons }) => (
  <S.Wrapper>
    <S.Heading>{intl.formatMessage(messages.title)}</S.Heading>
    <S.Container>
      {icons.length &&
        icons.map((icon) =>
          icon.iconUrl ? (
            <S.Icon key={icon.uid} href={icon?.link} target="_blank" rel="noreferrer noopener">
              <img src={icon.iconUrl} alt={icon.account} loading="lazy" />
            </S.Icon>
          ) : null,
        )}
    </S.Container>
  </S.Wrapper>
));

export default injectIntl(SocialMedia);
