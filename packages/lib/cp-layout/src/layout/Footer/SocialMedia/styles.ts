import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';

export const Wrapper = withTheme(styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;

  ${({ theme }) => theme.breakpoints.down('sm')} {
    align-items: center;
  }
`);

export const Heading = styled.h6`
  font-size: 16px;
  line-height: 1.53;
  margin: 0;
  user-select: none;
`;

export const Container = styled.div`
  display: flex;
  margin: 10px 0;
`;

export const Icon = styled.a`
  font-size: 0;

  &:not(:last-child) {
    margin-right: 8px;
  }
`;
