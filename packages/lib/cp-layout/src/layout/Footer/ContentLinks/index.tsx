import React from 'react';
import Link from 'next/link';

import Grid from '@material-ui/core/Grid';
import { IFooterData } from '../../../types/footer';
import * as S from './styles';

interface IContentLinks {
  footerLinks: IFooterData['footer_links'];
}

const ContentLinks: React.FC<IContentLinks> = React.memo(({ footerLinks }) =>
  footerLinks.length ? (
    <S.GridContainer maxWidth="xl">
      <Grid container spacing={3}>
        {footerLinks.map((section) => (
          <Grid item key={section?.heading} xs={12} sm={6} md={3}>
            <S.Heading>{section?.heading}</S.Heading>
            {section.link?.map((link) => (
              <Link href={link?.href} key={link?.title} passHref>
                <S.Anchor>{link?.title}</S.Anchor>
              </Link>
            ))}
          </Grid>
        ))}
      </Grid>
    </S.GridContainer>
  ) : null,
);

export default ContentLinks;
