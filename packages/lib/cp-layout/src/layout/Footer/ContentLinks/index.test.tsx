import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils';

import ContentLinks from '.';

describe('<ContentLinks />', () => {
  const props = {
    shouldDisplayContent: true,
    footerLinks: [
      {
        heading: 'Company & Team',
        link: [
          {
            title: 'About Raisin',
            href: '/about',
          },
          {
            title: 'Press',
            href: '',
          },
        ],
      },
      {
        heading: 'Our Partner Banks',
        link: [
          {
            title: 'Alior Bank',
            href: '/banks',
          },
          {
            title: 'Euram Bank',
            href: '',
          },
        ],
      },
      {
        heading: 'Investment Information',
        link: [
          {
            title: 'Best Rates',
            href: '',
          },
          {
            title: 'Best Savings Accounts',
            href: '',
          },
        ],
      },
      {
        heading: 'Help & Contact',
        link: [
          {
            title: 'Contact',
            href: '',
          },
          {
            title: 'FAQ',
            href: '',
          },
        ],
      },
    ],
  };

  it('renders a <ContentLinks> component', async () => {
    const component = render(<ContentLinks {...props} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });

  it('renders a <ContentLinks> component with NO links', async () => {
    const newProps = { ...props, footerLinks: [] };
    const component = render(<ContentLinks {...newProps} />);

    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
