import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Container from '@material-ui/core/Container';

export const GridContainer = withTheme(styled(Container)`
  margin-top: 40px;
  margin-bottom: 40px;

  ${({ theme }) => theme.breakpoints.down('md')} {
    margin-top: 30px;
    margin-bottom: 30px;
  }
`);

export const Heading = withTheme(styled.h5`
  font-size: ${({ theme }) => theme.typography.bodyTextFontSizeLg};
  line-height: 1.44;
  user-select: none;
  margin: 0;
`);

export const Anchor = withTheme(styled.a`
  display: flex;
  text-decoration: none;
  color: ${({ theme }) => theme.palette.primary.main};
  margin-top: 10px;
`);
