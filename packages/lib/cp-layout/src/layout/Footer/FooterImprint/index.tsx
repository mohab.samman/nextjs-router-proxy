import React from 'react';
// missing from package
import Link from 'next/link';
import { IFooterData } from '../../../types/footer';
import * as S from './styles';

interface IFooterImprint {
  copyright?: IFooterData['copyright'];
  legalLinks?: IFooterData['legal_links'];
  credits?: IFooterData['credits'];
  shouldDisplayMobileBanner?: boolean;
  shouldDisplayFooterLinks?: boolean;
}

const getCurrentYearCopyright = () => `© ${new Date().getFullYear()} `;

const FooterImprint: React.FC<IFooterImprint> = React.memo(
  ({ copyright, legalLinks, credits, shouldDisplayMobileBanner, shouldDisplayFooterLinks }) => (
    <S.Wrapper
      borderTop={shouldDisplayMobileBanner || !shouldDisplayFooterLinks}
      data-border-top={shouldDisplayMobileBanner || !shouldDisplayFooterLinks}
      data-testid="footerImprint-wrapper"
    >
      <S.FlexContainer>
        <S.List>
          {legalLinks &&
            legalLinks.map((link) => (
              <S.ListItem key={link?.title}>
                <Link href={link?.href} passHref>
                  <S.Link data-testid={`legalLinks-link-${link?.title}`}>{link?.title}</S.Link>
                </Link>
              </S.ListItem>
            ))}
        </S.List>
        <S.Copyright data-testid="footerImprint-copyright">
          {getCurrentYearCopyright()}
          {copyright}
        </S.Copyright>
      </S.FlexContainer>
      {credits && (
        <S.Credits>
          {credits.map(({ institute_link, institute_logo }, index) =>
            institute_link.href ? (
              <a
                href={institute_link.href}
                target="_blank"
                rel="noreferrer noopener"
                key={institute_link.href}
                title={institute_link?.title || institute_logo.title}
                className="institute-logo"
                data-testid={`credit-link-${index}`}
              >
                <img src={institute_logo.url} alt={institute_logo.title} />
              </a>
            ) : (
              <img
                src={institute_logo.url}
                alt={institute_logo.title}
                key={institute_logo.url}
                className="institute-logo"
                data-testid={`credit-logo-${index}`}
              />
            ),
          )}
        </S.Credits>
      )}
    </S.Wrapper>
  ),
);

export default FooterImprint;
