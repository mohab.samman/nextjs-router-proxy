import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../theme/colors';

export const Wrapper = withTheme(styled('div')<{ borderTop: boolean }>`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: ${({ theme }) => theme.typography.bodyTextFontSizeSm};
  padding: 15px 0;
  border-top: ${({ borderTop }) => (borderTop ? 'none' : `1px solid ${Colors.black40}`)};

  ${({ theme }) => theme.breakpoints.down('lg')} {
    flex-direction: column;
    padding: 20px 0;
  }
`);

export const FlexContainer = withTheme(styled.div`
  display: flex;

  ${({ theme }) => theme.breakpoints.down('lg')} {
    flex-direction: column;
  }

  ${({ theme }) => theme.breakpoints.down('lg')} {
    align-items: center;
  }
`);

export const List = withTheme(styled.ul`
  display: flex;
  margin: 0;
  padding: 0;
  list-style-type: none;

  ${({ theme }) => theme.breakpoints.down('lg')} {
    flex-wrap: wrap;
    justify-content: center;
  }
`);

export const ListItem = withTheme(styled.li`
  &:first-child a {
    padding-left: 0;
  }

  &:last-child a {
    padding-right: 0;
  }

  ${({ theme }) => theme.breakpoints.down('lg')} {
    margin-bottom: 10px;
  }
`);

export const Link = withTheme(styled.a`
  text-decoration: none;
  color: ${({ theme }) => theme.palette.primary.main};
  line-height: 1.63;
  padding: 0 10px;

  &:hover {
    text-decoration: underline;
    color: ${Colors.orange60};
  }
`);

export const Copyright = withTheme(styled.p`
  color: ${Colors.black60};
  line-height: 1.63;
  margin: 0 0 0 20px;

  ${({ theme }) => theme.breakpoints.down('lg')} {
    margin: 0;
  }
`);

export const Credits = withTheme(styled.div`
  display: flex;
  align-items: center;

  .institute-logo:not(:first-child) {
    margin-left: 10px;
  }

  img {
    max-height: 34px;
  }

  ${({ theme }) => theme.breakpoints.down('lg')} {
    margin-top: 20px;
  }
`);
