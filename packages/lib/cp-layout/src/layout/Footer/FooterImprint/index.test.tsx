import React from 'react';
import { render } from '../../../utils/test-utils';
import FooterImprint from './index';

describe('<FooterImprint />', () => {
  const props = {
    legalLinks: [
      {
        title: 'Privacy page',
        href: '/privacy',
      },
      {
        title: 'Cookie Policy',
        href: '/cookie',
      },
      {
        title: 'Terms',
        href: '/terms',
      },
      {
        title: 'Security',
        href: '/security',
      },
    ],
    creditsWithoutInstituteLinks: [
      {
        institute_logo: {
          url: 'image1.png',
          title: 'image 1',
        },
        institute_link: {
          title: '',
          href: '',
        },
      },
      {
        institute_logo: {
          url: 'image2.png',
          title: 'image 2',
        },
        institute_link: {
          title: '',
          href: '',
        },
      },
    ],
    creditsWithInstituteLinks: [
      {
        institute_logo: {
          url: 'image1.png',
          title: 'image 1',
        },
        institute_link: {
          title: 'image 1 href',
          href: '/image1-link',
        },
      },
    ],
    isMobilBannerDisplayed: true,
    copyright: 'mock copyright text',
  };

  it('should render the copyright text', () => {
    const { getByTestId } = render(<FooterImprint {...props} legalLinks={[]} credits={[]} />);

    expect(getByTestId('footerImprint-copyright')).toBeInTheDocument();
  });

  it('should render the legal links', () => {
    const { getByTestId } = render(<FooterImprint {...props} credits={[]} />);

    props.legalLinks.forEach((link) => {
      expect(getByTestId(`legalLinks-link-${link.title}`)).toBeInTheDocument();
    });
  });

  it('should match the snapshot for the legal links', () => {
    const component = render(<FooterImprint {...props} credits={[]} />);

    expect(component).toMatchSnapshot();
  });

  it('should render the credits without links', () => {
    const { getByTestId, queryByTestId } = render(
      <FooterImprint {...props} legalLinks={[]} credits={props.creditsWithoutInstituteLinks} />,
    );

    props.creditsWithoutInstituteLinks.forEach((_, index) => {
      expect(getByTestId(`credit-logo-${index}`)).toBeInTheDocument();
      expect(queryByTestId(`credit-link-${index}`)).not.toBeInTheDocument();
    });
  });

  it('should match the snapshot for the credits without links', () => {
    const component = render(
      <FooterImprint {...props} legalLinks={[]} credits={props.creditsWithoutInstituteLinks} />,
    );

    expect(component).toMatchSnapshot();
  });

  it('should render the credits with links', () => {
    const { getByTestId, queryByTestId } = render(
      <FooterImprint {...props} legalLinks={[]} credits={props.creditsWithInstituteLinks} />,
    );

    props.creditsWithInstituteLinks.forEach((_, index) => {
      expect(queryByTestId(`credit-logo-${index}`)).not.toBeInTheDocument();
      expect(getByTestId(`credit-link-${index}`)).toBeInTheDocument();
    });
  });

  it('should match the snapshot for the credits with links', () => {
    const component = render(
      <FooterImprint {...props} legalLinks={[]} credits={props.creditsWithInstituteLinks} />,
    );

    expect(component).toMatchSnapshot();
  });
});
