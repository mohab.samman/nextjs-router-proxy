import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import { Container, Typography } from '@material-ui/core';
import * as S from './styles';
import messages from './messages';
import { STARS } from './images';
import { TRUSTPILOT_PROFILE_URL, TrustPilotIcon } from './utils';
interface ITrustpilotBar extends WrappedComponentProps {
  trustpilotData: {
    count_of_reviews?: number;
    score_value?: number;
  };
}

const TrustpilotBar: React.FC<ITrustpilotBar> = ({ intl, trustpilotData }) => {
  let stars = 0;

  if (trustpilotData?.score_value) {
    stars = Math.round(trustpilotData.score_value * 2) / 2;
  }

  const Image = STARS[stars];

  return (
    <S.Background>
      <Container maxWidth="xl">
        <S.Wrapper>
          <S.InnerWrapper>
            <Typography variant="body1">{intl.formatMessage(messages.title)}</Typography>
            <S.StarsWrapper>
              <S.StarsLink
                href={TRUSTPILOT_PROFILE_URL[intl.locale]}
                target="_blank"
                rel="noreferrer"
              >
                <S.Stars>
                  <Image />
                </S.Stars>
                <S.StarsText data-testid="trustpilot-stars" fontWeight={700}>
                  {intl.formatMessage(messages.starsText)}
                </S.StarsText>
              </S.StarsLink>
            </S.StarsWrapper>
            <S.ReviewsText>
              {intl.formatMessage(messages.reviewsCount, {
                countOfReviews: trustpilotData?.count_of_reviews
                  ? trustpilotData.count_of_reviews
                  : 0,
              })}
            </S.ReviewsText>
          </S.InnerWrapper>
          <S.LogoLink href={TRUSTPILOT_PROFILE_URL[intl.locale]} target="_blank" rel="noreferrer">
            <TrustPilotIcon />
          </S.LogoLink>
        </S.Wrapper>
      </Container>
    </S.Background>
  );
};

export default injectIntl(TrustpilotBar);
