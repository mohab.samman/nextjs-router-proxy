import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'footer.trustpilot.title',
    defaultMessage: 'How our customers rate us:',
  },
  starsText: {
    id: 'footer.trustpilot.stars.text',
    defaultMessage: 'Outstanding',
    description: 'Trustpilot rating',
  },
  reviewsCount: {
    id: 'footer.trustpilot.reviews.text',
    defaultMessage: 'Based on {countOfReviews} reviews',
    description: 'amount of reviews on Trustpilot',
  },
});

export default messages;
