import '@testing-library/jest-dom';
import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../../utils/test-utils'; // from our RTL's custom render util

import TrustPilotBar from '.';

describe('<TrustPilotBar />', () => {
  const props = {
    trustpilotData: {
      count_of_reviews: 3200,
      score_value: 4.5,
    },
    intl: {
      formatMessage: jest.fn(),
    },
  };

  it('should match snapshot when data is loaded', async () => {
    const component = render(<TrustPilotBar {...props} />);
    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });

  it('should match snapshot when trustpilotData is not yet loaded', async () => {
    const localProps = {
      trustpilotData: {},
      intl: {
        formatMessage: jest.fn(),
      },
    };

    const component = render(<TrustPilotBar {...localProps} />);
    await waitFor(() => component);
    expect(component).toMatchSnapshot();
  });
});
