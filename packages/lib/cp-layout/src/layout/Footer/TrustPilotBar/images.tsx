import React from 'react';

export const STARS = {
  0: () => (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 512 96"
    >
      <style type="text/css">
        {'\
    .st0{\
      fill:#E5E5E5;\
    }\
    .st1{\
      fill:#FFFFFF;\
    }\
      '}
      </style>
      <rect y="0" className="st0" width="96" height="96" />
      <rect x="104" y="0" className="st0" width="96" height="96" />
      <rect x="208" y="0" className="st0" width="96" height="96" />
      <rect x="312" y="0" className="st0" width="96" height="96" />
      <rect x="416" y="0" className="st0" width="96" height="96" />
      <g>
        <path
          className="st1"
          d="M48,64.7L62.6,61l6.1,18.8L48,64.7z M81.6,40.4H55.9L48,16.2l-7.9,24.2H14.4l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L81.6,40.4L81.6,40.4L81.6,40.4L81.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M152,64.7l14.6-3.7l6.1,18.8L152,64.7z M185.6,40.4h-25.7L152,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L185.6,40.4L185.6,40.4L185.6,40.4L185.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M256,64.7l14.6-3.7l6.1,18.8L256,64.7z M289.6,40.4h-25.7L256,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L289.6,40.4L289.6,40.4L289.6,40.4L289.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M360,64.7l14.6-3.7l6.1,18.8L360,64.7z M393.6,40.4h-25.7L360,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L393.6,40.4L393.6,40.4L393.6,40.4L393.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M464,64.7l14.6-3.7l6.1,18.8L464,64.7z M497.6,40.4h-25.7L464,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L497.6,40.4L497.6,40.4L497.6,40.4L497.6,40.4z"
        />
      </g>
    </svg>
  ),
  1: () => (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 512 96"
    >
      <style type="text/css">
        {'\
 .st0{fill:#FF3722;}\
 .st1{fill:#E5E5E5;}\
 .st2{fill:#FFFFFF;}\
 '}
      </style>
      <rect y="0" className="st0" width="96" height="96" />
      <rect x="104" y="0" className="st1" width="96" height="96" />
      <rect x="208" y="0" className="st1" width="96" height="96" />
      <rect x="312" y="0" className="st1" width="96" height="96" />
      <rect x="416" y="0" className="st1" width="96" height="96" />
      <g>
        <path
          className="st2"
          d="M48,64.7L62.6,61l6.1,18.8L48,64.7z M81.6,40.4H55.9L48,16.2l-7.9,24.2H14.4l20.8,15l-7.9,24.2l20.8-15
   l12.8-9.2L81.6,40.4L81.6,40.4L81.6,40.4L81.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M152,64.7l14.6-3.7l6.1,18.8L152,64.7z M185.6,40.4h-25.7L152,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
   l12.8-9.2L185.6,40.4L185.6,40.4L185.6,40.4L185.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M256,64.7l14.6-3.7l6.1,18.8L256,64.7z M289.6,40.4h-25.7L256,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
   l12.8-9.2L289.6,40.4L289.6,40.4L289.6,40.4L289.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M360,64.7l14.6-3.7l6.1,18.8L360,64.7z M393.6,40.4h-25.7L360,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
   l12.8-9.2L393.6,40.4L393.6,40.4L393.6,40.4L393.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M464,64.7l14.6-3.7l6.1,18.8L464,64.7z M497.6,40.4h-25.7L464,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
   l12.8-9.2L497.6,40.4L497.6,40.4L497.6,40.4L497.6,40.4z"
        />
      </g>
    </svg>
  ),
  1.5: () => (
    <svg
      width="512px"
      height="96px"
      viewBox="0 0 512 96"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g
        id="Trustpilot_ratings_1halfstar-RGB"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="Trustpilot_ratings_1star-RGB-Copy-3" fillRule="nonzero">
          <rect id="Rectangle-path" fill="#FF3722" x="0" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="104" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#FF3722" x="104" y="0" width="48" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="208" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="312" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="416" y="0" width="96" height="96"></rect>
          <path
            d="M48,64.7 L62.6,61 L68.7,79.8 L48,64.7 Z M81.6,40.4 L55.9,40.4 L48,16.2 L40.1,40.4 L14.4,40.4 L35.2,55.4 L27.3,79.6 L48.1,64.6 L60.9,55.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M152,64.7 L166.6,61 L172.7,79.8 L152,64.7 Z M185.6,40.4 L159.9,40.4 L152,16.2 L144.1,40.4 L118.4,40.4 L139.2,55.4 L131.3,79.6 L152.1,64.6 L164.9,55.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M256,64.7 L270.6,61 L276.7,79.8 L256,64.7 Z M289.6,40.4 L263.9,40.4 L256,16.2 L248.1,40.4 L222.4,40.4 L243.2,55.4 L235.3,79.6 L256.1,64.6 L268.9,55.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M360,64.7 L374.6,61 L380.7,79.8 L360,64.7 Z M393.6,40.4 L367.9,40.4 L360,16.2 L352.1,40.4 L326.4,40.4 L347.2,55.4 L339.3,79.6 L360.1,64.6 L372.9,55.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M464,64.7 L478.6,61 L484.7,79.8 L464,64.7 Z M497.6,40.4 L471.9,40.4 L464,16.2 L456.1,40.4 L430.4,40.4 L451.2,55.4 L443.3,79.6 L464.1,64.6 L476.9,55.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
        </g>
      </g>
    </svg>
  ),
  2: () => (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 512 96"
    >
      <style type="text/css">
        {'\
    .st0{fill:#FF8622;}\
    .st1{fill:#E5E5E5;}\
    .st2{fill:#FFFFFF;}\
    '}
      </style>
      <rect y="0" className="st0" width="96" height="96" />
      <rect x="104" y="0" className="st0" width="96" height="96" />
      <rect x="208" y="0" className="st1" width="96" height="96" />
      <rect x="312" y="0" className="st1" width="96" height="96" />
      <rect x="416" y="0" className="st1" width="96" height="96" />
      <g>
        <path
          className="st2"
          d="M48,64.7L62.6,61l6.1,18.8L48,64.7z M81.6,40.4H55.9L48,16.2l-7.9,24.2H14.4l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L81.6,40.4L81.6,40.4L81.6,40.4L81.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M152,64.7l14.6-3.7l6.1,18.8L152,64.7z M185.6,40.4h-25.7L152,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L185.6,40.4L185.6,40.4L185.6,40.4L185.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M256,64.7l14.6-3.7l6.1,18.8L256,64.7z M289.6,40.4h-25.7L256,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L289.6,40.4L289.6,40.4L289.6,40.4L289.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M360,64.7l14.6-3.7l6.1,18.8L360,64.7z M393.6,40.4h-25.7L360,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L393.6,40.4L393.6,40.4L393.6,40.4L393.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M464,64.7l14.6-3.7l6.1,18.8L464,64.7z M497.6,40.4h-25.7L464,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L497.6,40.4L497.6,40.4L497.6,40.4L497.6,40.4z"
        />
      </g>
    </svg>
  ),
  2.5: () => (
    <svg
      width="512px"
      height="96px"
      viewBox="0 0 512 96"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g
        id="Trustpilot_ratings_2halfstar-RGB"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="Trustpilot_ratings_2star-RGB-Copy-3" fillRule="nonzero">
          <rect id="Rectangle-path" fill="#FF8622" x="0" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#FF8622" x="104" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="208" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#FF8622" x="208" y="0" width="48" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="312" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="416" y="0" width="96" height="96"></rect>
          <path
            d="M48,64.7 L62.6,61 L68.7,79.8 L48,64.7 Z M81.6,40.4 L55.9,40.4 L48,16.2 L40.1,40.4 L14.4,40.4 L35.2,55.4 L27.3,79.6 L48.1,64.6 L60.9,55.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M152,64.7 L166.6,61 L172.7,79.8 L152,64.7 Z M185.6,40.4 L159.9,40.4 L152,16.2 L144.1,40.4 L118.4,40.4 L139.2,55.4 L131.3,79.6 L152.1,64.6 L164.9,55.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M256,64.7 L270.6,61 L276.7,79.8 L256,64.7 Z M289.6,40.4 L263.9,40.4 L256,16.2 L248.1,40.4 L222.4,40.4 L243.2,55.4 L235.3,79.6 L256.1,64.6 L268.9,55.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M360,64.7 L374.6,61 L380.7,79.8 L360,64.7 Z M393.6,40.4 L367.9,40.4 L360,16.2 L352.1,40.4 L326.4,40.4 L347.2,55.4 L339.3,79.6 L360.1,64.6 L372.9,55.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M464,64.7 L478.6,61 L484.7,79.8 L464,64.7 Z M497.6,40.4 L471.9,40.4 L464,16.2 L456.1,40.4 L430.4,40.4 L451.2,55.4 L443.3,79.6 L464.1,64.6 L476.9,55.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
        </g>
      </g>
    </svg>
  ),
  3: () => (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 512 96"
    >
      <style type="text/css">
        {'\
    .st0{fill:#FFCE00;}\
    .st1{fill:#E5E5E5;}\
    .st2{fill:#FFFFFF;}\
    '}
      </style>
      <rect y="0" className="st0" width="96" height="96" />
      <rect x="104" y="0" className="st0" width="96" height="96" />
      <rect x="208" y="0" className="st0" width="96" height="96" />
      <rect x="312" y="0" className="st1" width="96" height="96" />
      <rect x="416" y="0" className="st1" width="96" height="96" />
      <g>
        <path
          className="st2"
          d="M48,64.7L62.6,61l6.1,18.8L48,64.7z M81.6,40.4H55.9L48,16.2l-7.9,24.2H14.4l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L81.6,40.4L81.6,40.4L81.6,40.4L81.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M152,64.7l14.6-3.7l6.1,18.8L152,64.7z M185.6,40.4h-25.7L152,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L185.6,40.4L185.6,40.4L185.6,40.4L185.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M256,64.7l14.6-3.7l6.1,18.8L256,64.7z M289.6,40.4h-25.7L256,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L289.6,40.4L289.6,40.4L289.6,40.4L289.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M360,64.7l14.6-3.7l6.1,18.8L360,64.7z M393.6,40.4h-25.7L360,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L393.6,40.4L393.6,40.4L393.6,40.4L393.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M464,64.7l14.6-3.7l6.1,18.8L464,64.7z M497.6,40.4h-25.7L464,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L497.6,40.4L497.6,40.4L497.6,40.4L497.6,40.4z"
        />
      </g>
    </svg>
  ),
  3.5: () => (
    <svg
      width="512px"
      height="96px"
      viewBox="0 0 512 96"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g
        id="Trustpilot_ratings_3halfstar-RGB"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g id="Trustpilot_ratings_3star-RGB-Copy-3" fillRule="nonzero">
          <rect id="Rectangle-path" fill="#FFCE00" x="0" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#FFCE00" x="104" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#FFCE00" x="208" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="312" y="0" width="96" height="96"></rect>
          <rect id="Rectangle-path" fill="#FFCE00" x="312" y="0" width="48" height="96"></rect>
          <rect id="Rectangle-path" fill="#DCDCE6" x="416" y="0" width="96" height="96"></rect>
          <path
            d="M48,64.7 L62.6,61 L68.7,79.8 L48,64.7 Z M81.6,40.4 L55.9,40.4 L48,16.2 L40.1,40.4 L14.4,40.4 L35.2,55.4 L27.3,79.6 L48.1,64.6 L60.9,55.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M152,64.7 L166.6,61 L172.7,79.8 L152,64.7 Z M185.6,40.4 L159.9,40.4 L152,16.2 L144.1,40.4 L118.4,40.4 L139.2,55.4 L131.3,79.6 L152.1,64.6 L164.9,55.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M256,64.7 L270.6,61 L276.7,79.8 L256,64.7 Z M289.6,40.4 L263.9,40.4 L256,16.2 L248.1,40.4 L222.4,40.4 L243.2,55.4 L235.3,79.6 L256.1,64.6 L268.9,55.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M360,64.7 L374.6,61 L380.7,79.8 L360,64.7 Z M393.6,40.4 L367.9,40.4 L360,16.2 L352.1,40.4 L326.4,40.4 L347.2,55.4 L339.3,79.6 L360.1,64.6 L372.9,55.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
          <path
            d="M464,64.7 L478.6,61 L484.7,79.8 L464,64.7 Z M497.6,40.4 L471.9,40.4 L464,16.2 L456.1,40.4 L430.4,40.4 L451.2,55.4 L443.3,79.6 L464.1,64.6 L476.9,55.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 Z"
            id="Shape"
            fill="#FFFFFF"
          ></path>
        </g>
      </g>
    </svg>
  ),
  4: () => (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 512 96"
    >
      <style type="text/css">
        {'\
    .st0{fill:#73CF11;}\
    .st1{fill:#E5E5E5;}\
    .st2{fill:#FFFFFF;}\
    '}
      </style>
      <rect y="0" className="st0" width="96" height="96" />
      <rect x="104" y="0" className="st0" width="96" height="96" />
      <rect x="208" y="0" className="st0" width="96" height="96" />
      <rect x="312" y="0" className="st0" width="96" height="96" />
      <rect x="416" y="0" className="st1" width="96" height="96" />
      <g>
        <path
          className="st2"
          d="M48,64.7L62.6,61l6.1,18.8L48,64.7z M81.6,40.4H55.9L48,16.2l-7.9,24.2H14.4l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L81.6,40.4L81.6,40.4L81.6,40.4L81.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M152,64.7l14.6-3.7l6.1,18.8L152,64.7z M185.6,40.4h-25.7L152,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L185.6,40.4L185.6,40.4L185.6,40.4L185.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M256,64.7l14.6-3.7l6.1,18.8L256,64.7z M289.6,40.4h-25.7L256,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L289.6,40.4L289.6,40.4L289.6,40.4L289.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M360,64.7l14.6-3.7l6.1,18.8L360,64.7z M393.6,40.4h-25.7L360,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L393.6,40.4L393.6,40.4L393.6,40.4L393.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st2"
          d="M464,64.7l14.6-3.7l6.1,18.8L464,64.7z M497.6,40.4h-25.7L464,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L497.6,40.4L497.6,40.4L497.6,40.4L497.6,40.4z"
        />
      </g>
    </svg>
  ),
  4.5: () => (
    <svg
      width="512px"
      height="96px"
      viewBox="0 0 512 96"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g
        id="Trustpilot_ratings_4halfstar-RGB"
        stroke="none"
        strokeWidth="1"
        fill="none"
        fillRule="evenodd"
      >
        <g>
          <g>
            <rect
              id="Rectangle-path"
              fill="#00B67A"
              fillRule="nonzero"
              x="0"
              y="0"
              width="96"
              height="96"
            ></rect>
            <rect
              id="Rectangle-path"
              fill="#00B67A"
              fillRule="nonzero"
              x="104"
              y="0"
              width="96"
              height="96"
            ></rect>
            <rect
              id="Rectangle-path"
              fill="#00B67A"
              fillRule="nonzero"
              x="208"
              y="0"
              width="96"
              height="96"
            ></rect>
            <rect
              id="Rectangle-path"
              fill="#00B67A"
              fillRule="nonzero"
              x="312"
              y="0"
              width="96"
              height="96"
            ></rect>
            <g id="Half" transform="translate(416.000000, 0.000000)" fillRule="nonzero">
              <rect id="Rectangle-path" fill="#DCDCE6" x="48" y="0" width="48" height="96"></rect>
              <rect id="Rectangle-path" fill="#00B67A" x="0" y="0" width="48" height="96"></rect>
            </g>
            <path
              d="M48,64.7 L62.6,61 L68.7,79.8 L48,64.7 Z M81.6,40.4 L55.9,40.4 L48,16.2 L40.1,40.4 L14.4,40.4 L35.2,55.4 L27.3,79.6 L48.1,64.6 L60.9,55.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 L81.6,40.4 Z"
              id="Shape"
              fill="#FFFFFF"
              fillRule="nonzero"
            ></path>
            <path
              d="M152,64.7 L166.6,61 L172.7,79.8 L152,64.7 Z M185.6,40.4 L159.9,40.4 L152,16.2 L144.1,40.4 L118.4,40.4 L139.2,55.4 L131.3,79.6 L152.1,64.6 L164.9,55.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 L185.6,40.4 Z"
              id="Shape"
              fill="#FFFFFF"
              fillRule="nonzero"
            ></path>
            <path
              d="M256,64.7 L270.6,61 L276.7,79.8 L256,64.7 Z M289.6,40.4 L263.9,40.4 L256,16.2 L248.1,40.4 L222.4,40.4 L243.2,55.4 L235.3,79.6 L256.1,64.6 L268.9,55.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 L289.6,40.4 Z"
              id="Shape"
              fill="#FFFFFF"
              fillRule="nonzero"
            ></path>
            <path
              d="M360,64.7 L374.6,61 L380.7,79.8 L360,64.7 Z M393.6,40.4 L367.9,40.4 L360,16.2 L352.1,40.4 L326.4,40.4 L347.2,55.4 L339.3,79.6 L360.1,64.6 L372.9,55.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 L393.6,40.4 Z"
              id="Shape"
              fill="#FFFFFF"
              fillRule="nonzero"
            ></path>
            <path
              d="M464,64.7 L478.6,61 L484.7,79.8 L464,64.7 Z M497.6,40.4 L471.9,40.4 L464,16.2 L456.1,40.4 L430.4,40.4 L451.2,55.4 L443.3,79.6 L464.1,64.6 L476.9,55.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 L497.6,40.4 Z"
              id="Shape"
              fill="#FFFFFF"
              fillRule="nonzero"
            ></path>
          </g>
        </g>
      </g>
    </svg>
  ),
  5: () => (
    <svg
      version="1.1"
      id="Layer_1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 512 96"
    >
      <style type="text/css">
        {'\
    .st0{\
      fill:#00B67A;\
    }\
    .st1{\
      fill:#FFFFFF;\
    }\
    '}
      </style>
      <rect y="0" className="st0" width="96" height="96" />
      <rect x="104" y="0" className="st0" width="96" height="96" />
      <rect x="208" y="0" className="st0" width="96" height="96" />
      <rect x="312" y="0" className="st0" width="96" height="96" />
      <rect x="416" y="0" className="st0" width="96" height="96" />
      <g>
        <path
          className="st1"
          d="M48,64.7L62.6,61l6.1,18.8L48,64.7z M81.6,40.4H55.9L48,16.2l-7.9,24.2H14.4l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L81.6,40.4L81.6,40.4L81.6,40.4L81.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M152,64.7l14.6-3.7l6.1,18.8L152,64.7z M185.6,40.4h-25.7L152,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L185.6,40.4L185.6,40.4L185.6,40.4L185.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M256,64.7l14.6-3.7l6.1,18.8L256,64.7z M289.6,40.4h-25.7L256,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L289.6,40.4L289.6,40.4L289.6,40.4L289.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M360,64.7l14.6-3.7l6.1,18.8L360,64.7z M393.6,40.4h-25.7L360,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L393.6,40.4L393.6,40.4L393.6,40.4L393.6,40.4z"
        />
      </g>
      <g>
        <path
          className="st1"
          d="M464,64.7l14.6-3.7l6.1,18.8L464,64.7z M497.6,40.4h-25.7L464,16.2l-7.9,24.2h-25.7l20.8,15l-7.9,24.2l20.8-15
      l12.8-9.2L497.6,40.4L497.6,40.4L497.6,40.4L497.6,40.4z"
        />
      </g>
    </svg>
  ),
};
