import styled from 'styled-components';
import { Link } from '@material-ui/core';
import { withTheme } from '@material-ui/styles';
import Colors from '../../../theme/colors';

export const Background = styled.div`
  background-color: ${Colors.black10};
  margin-bottom: 20px;
`;

export const Wrapper = withTheme(styled.div`
  padding: 28px 0;
  display: flex;
  justify-content: space-between;

  ${({ theme }) => theme.breakpoints.down('md')} {
    padding: 30px 0;
  }

  ${({ theme }) => theme.breakpoints.down('sm')} {
    flex-direction: column;
    padding: 20px 0;
  }
`);

export const InnerWrapper = withTheme(styled.div`
  display: flex;
  flex-direction: row;

  ${({ theme }) => theme.breakpoints.down('md')} {
    flex-direction: column;
  }

  ${({ theme }) => theme.breakpoints.down('sm')} {
    margin-bottom: 10px;
  }
`);

export const StarsWrapper = withTheme(styled.div`
  margin: 0 30px;

  ${({ theme }) => theme.breakpoints.down('md')} {
    margin: 12px 0 0;
  }
`);

export const StarsLink = styled(Link)`
  display: flex;
  margin: 0 10px 10px 0;

  &:hover {
    color: inherit;
    text-decoration: none;
  }
`;

export const Stars = styled.div`
  margin-right: 10px;
  display: flex;
  align-items: center;

  & > svg {
    width: 100px;
    height: auto;
  }
`;

export const StarsText = withTheme(styled.span`
  font-weight: 700;
`);

export const ReviewsText = withTheme(styled.div`
  ${({ theme }) => theme.breakpoints.down('lg')} {
    display: none;
  }
`);

export const LogoLink = styled.a`
  display: flex;
  align-items: center;
`;
