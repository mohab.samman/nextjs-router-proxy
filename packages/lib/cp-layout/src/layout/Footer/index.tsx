import React from 'react';
import Container from '@material-ui/core/Container';
import { IContactInformationData, IFooterData, ISocialMediaIcons } from '../../types/footer';
import ContactInformation from './ContactInformation/index';
import ContentLinks from './ContentLinks';
import FooterDisclaimer from './FooterDisclaimer';
import FooterImprint from './FooterImprint';
import TrustPilotBar from './TrustPilotBar';
import MobileBanner from './MobileBanner';

export interface IFooter {
  footerData: {
    trustpilotData: {
      count_of_reviews: number;
      score_value: number;
    };
    phoneIconURL: string;
    emailIconURL: string;
    socialMediaIcons: ISocialMediaIcons[];
    contactInformationData: IContactInformationData;
    mobileBannerIconURL: string;
    display_trustpilot: IFooterData['display_trustpilot'];
    display_footer_links: IFooterData['display_footer_links'];
    footer_links: IFooterData['footer_links'];
    legal_links: IFooterData['legal_links'];
    copyright: IFooterData['copyright'];
    credits: IFooterData['credits'];
    disclaimer: IFooterData['disclaimer'];
    mobile_banner: IFooterData['mobile_banner'];
  };
}

const Footer: React.FC<IFooter> = ({
  footerData: {
    trustpilotData,
    phoneIconURL,
    emailIconURL,
    socialMediaIcons,
    contactInformationData,
    mobileBannerIconURL,
    mobile_banner: { display_mobile_banner },
    display_trustpilot,
    display_footer_links,
    footer_links,
    legal_links,
    copyright,
    credits,
    disclaimer,
  },
}) => (
  <Container disableGutters maxWidth={false}>
    {display_trustpilot && <TrustPilotBar trustpilotData={trustpilotData} />}
    <ContactInformation
      phoneIconURL={phoneIconURL}
      emailIconURL={emailIconURL}
      socialMediaIcons={socialMediaIcons}
      contactInformation={contactInformationData}
      isTrustPilotAvailable={display_trustpilot}
    />
    {display_footer_links && <ContentLinks footerLinks={footer_links} />}
    {display_mobile_banner && <MobileBanner mobileBannerIconURL={mobileBannerIconURL} />}
    <Container maxWidth="xl">
      <FooterImprint
        legalLinks={legal_links}
        copyright={copyright}
        credits={credits}
        shouldDisplayMobileBanner={display_mobile_banner}
        shouldDisplayFooterLinks={display_footer_links}
      />

      {disclaimer && <FooterDisclaimer disclaimer={disclaimer} />}
    </Container>
  </Container>
);

export default Footer;
