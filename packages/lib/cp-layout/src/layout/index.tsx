import React from 'react';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import { DEFAULT_LOCALE_LABEL } from '../constants/locales';
import theme from '../theme';
import Header, { IHeader } from './Header';
import Footer, { IFooter } from './Footer';

export interface ILayout {
  layout: {
    localizedMessages: Record<string, string> | Record<string, MessageFormatElement[]>;
    locale: string;
    footerData: IFooter['footerData'];
    headerData: IHeader['headerData'];
  };
  isLoggedIn?: boolean;
  useSWR: Function;
}

const Layout: React.FC<ILayout> = ({ children, layout, isLoggedIn, useSWR }) => (
  <IntlProvider
    messages={layout.localizedMessages}
    locale={layout.locale}
    defaultLocale={DEFAULT_LOCALE_LABEL}
  >
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Header headerData={layout.headerData} isLoggedIn={isLoggedIn} useSWR={useSWR} />
      {children}
      <Footer footerData={layout.footerData} />
    </ThemeProvider>
  </IntlProvider>
);
export default Layout;
