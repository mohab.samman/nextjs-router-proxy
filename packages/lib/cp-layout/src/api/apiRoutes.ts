const config = {
  production: {
    customer: {
      data: '/customer',
      productAccess: '/customer/product/access',
    },
    dashboard: {
      pensionProducts: '/pension/customer/products',
    },
  },
  local: {},
};
if (
  process.env.NODE_ENV === 'development' ||
  (typeof window !== 'undefined' && window.location.hostname === 'localhost')
) {
  config.local = {
    customer: {
      data: '/customerData.json',
      productAccess: '/productAccess.json',
    },
    dashboard: {
      pensionProducts: '/pensionProductsData.json',
    },
  };
}

export default config;
