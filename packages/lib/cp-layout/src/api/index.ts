import createClient from './interceptors';
import config from './apiRoutes';
const BASE_URL = '/savingglobal/rest/open_api/v2';

// similar to Lodash.get, allows us to access object properties by strings
const resolveObjPath = (path: string, obj): string =>
  path.split('.').reduce((prev, curr) => (prev ? prev[curr] : null), obj);

const obsProductionClient = createClient(BASE_URL);

export const createAsyncAction = async (path: string) => {
  // TODO: implement a proper mock server for dev env? maybe https://www.npmjs.com/package/nock
  if (
    process.env.NODE_ENV === 'development' ||
    (typeof window !== 'undefined' && window.location.hostname === 'localhost')
  ) {
    return import(`../__mocks__${resolveObjPath(path, config.local)}`).then((res) => res.default);
  }

  return obsProductionClient.get(resolveObjPath(path, config.production));
};
