import { createAsyncAction } from '../api';
import { ICustomer } from '../types/customer';
import { IPPData } from '../types/pensionProducts';

const FIVE_MINS_IN_MILISECONDS = 300000;

const useFetchHeader = (useSWR: Function) => {
  const { data: customerData = null }: { data: ICustomer | null } = useSWR(
    'customer.data',
    createAsyncAction,
    {
      dedupingInterval: FIVE_MINS_IN_MILISECONDS,
    },
  );
  const { data: ppDashboardData = null }: { data: IPPData | null } = useSWR(
    'dashboard.pensionProducts',
    createAsyncAction,
    {
      dedupingInterval: FIVE_MINS_IN_MILISECONDS,
    },
  );
  const { data: productAccess = [] }: { data: string[] } = useSWR(
    'customer.productAccess',
    createAsyncAction,
    {
      dedupingInterval: FIVE_MINS_IN_MILISECONDS,
    },
  );

  return { customerData, ppDashboardData, productAccess };
};

export default useFetchHeader;
