import { useState, useEffect } from 'react';

const usePublicNavigation = () => {
  // keep track of opened submenus for each menu items
  const [open, setOpen] = useState({});
  const [mobileItemState, setMobileItemState] = useState({});
  // keep track of currently selected menu item
  const [currentEl, setCurrentEl] = useState<string | null>(null);
  // keep track of mouse position over menu/submenu items to handle submenu closing
  const [mouseOverButton, setMouseOverButton] = useState({});
  const [mouseOverSubmenu, setMouseOverSubmenu] = useState({});
  const [anchorEl, setAnchorEl] = useState({});

  useEffect(() => {
    if (currentEl) {
      if (!mouseOverButton[currentEl] && !mouseOverSubmenu[currentEl]) {
        setOpen({ [currentEl]: false });
      } else {
        setOpen({ [currentEl]: true });
      }
    }
  }, [mouseOverButton, mouseOverSubmenu]);

  const handleOpen = (event: React.MouseEvent, id: string) => {
    if (!anchorEl[id] || !anchorEl[id].isSameNode(event.currentTarget)) {
      setAnchorEl({ ...anchorEl, [id]: event.currentTarget });
    }
  };

  return {
    anchorEl,
    open,
    handleOpen,
    setMouseOverButton,
    setMouseOverSubmenu,
    setCurrentEl,
    mobileItemState,
    setMobileItemState,
  };
};

export default usePublicNavigation;
