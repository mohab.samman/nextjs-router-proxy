import { useState, useEffect, useRef } from 'react';
import { getBankingMenu } from '../utils/Layout/bankingMenu';
import { IBankingMenuItem } from '../types/header';
import useFetchHeader from './useFetchHeaderData';

const useBankingNavigation = (useSWR: Function) => {
  const [open, setOpen] = useState(false);
  const [mouseOverButton, setMouseOverButton] = useState(false);
  const [mouseOverSubmenu, setMouseOverSubmenu] = useState(false);
  const [bankingMenu, setBankingMenu] = useState<IBankingMenuItem[]>([]);
  const [shouldShowRAF, setShouldShowRAF] = useState(true);
  const [isBankingMenuLoaded, setIsBankingMenuLoaded] = useState(false);

  const { customerData, ppDashboardData, productAccess } = useFetchHeader(useSWR);

  useEffect(() => {
    if (!!customerData && !!productAccess?.length && !!ppDashboardData) {
      setBankingMenu(getBankingMenu([customerData, productAccess, ppDashboardData]));
      setShouldShowRAF(!customerData.is_company_customer && !customerData.is_pending_activation);
      setIsBankingMenuLoaded(true);
    }
  }, [customerData, productAccess, ppDashboardData]);

  const anchorRef = useRef(null);

  useEffect(() => {
    if (!mouseOverButton && !mouseOverSubmenu) {
      setOpen(false);
    } else {
      setOpen(true);
    }
  }, [mouseOverButton, mouseOverSubmenu]);

  return {
    anchorRef,
    open,
    shouldShowRAF,
    bankingMenu,
    setMouseOverButton,
    setMouseOverSubmenu,
    mouseOverButton,
    isBankingMenuLoaded,
  };
};

export default useBankingNavigation;
