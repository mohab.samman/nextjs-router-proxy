# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.11.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.10.1...@raisin/cp-layout@1.11.0) (2021-07-27)


### Bug Fixes

* **cp-layout:** logo and header buttons styling ([0605667](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/060566777393b23cbbc7ee4517cd418e55ae9d3c))
* **Cp-layout:** fixing TS issue ([19dd75e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/19dd75e5bd53036184a8056771ec525567c87579))
* **CustomerPortal:** adding Header login/registration button ([c91948d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c91948dbd9497ef37ae6a58ab4d92f53a4c7d59d))
* **header:** add api mocks and api utils ([dace844](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/dace8440e1dd3c73144356822ced8da99108a8fa))
* **header:** add proper base url to obs links ([5129950](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5129950462396c2f20b99bbf4bf811ac334777c6))
* **header:** adjust border bottom for desktop header ([7f7699d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7f7699dedf5ee0456060e87dab4b535f9df12ade))
* **header:** adjust media queries for new breakpoints ([9c6487c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9c6487c7a7251437bae7b401d8d3bf769d73ba89))
* **header:** adjust paddings and gutters in main nav ([0dd1469](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0dd146909a4f0acf65f082cb51c0239c0cb0b38e))
* **header:** adjust spacing between navigation items according to design ([cd2cbba](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cd2cbbae30471904bd87e2b25d128150269c5bf1))
* **header:** adjust spacing between submenu elements ([8e0410e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8e0410ea18529f87c66f86a8e42e0d0c16d2451f))
* **header:** adjust submenu opening/closing to make it more robust ([d07ab74](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d07ab74541a0c7dcbc11290672807431f092602c))
* **header:** better markdown ([977f980](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/977f9806b58b15b9a0069558e6e3c73a907b29ad))
* **header:** border between menu and logo ([69481f4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/69481f43cdab2afaf771bae76c1238f9af2fd25a))
* **header:** missing key in map ([6dc36ea](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/6dc36ea920488377a8055b95c04c20bb906884e5))
* **header:** missing keys in maps and other console warnings ([953ffcf](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/953ffcf55cdc4892d8e9df11d5b5838049489365))
* **header:** padding of menu items for logged out state ([2eb157e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2eb157e49f28b1ee333f5132aff6de464a1c891d))
* **header:** remove some obsolete css styling ([b601971](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b6019713add77557512a0974e47351cecfee1130))
* **header:** remove useless import of jest-styled-components ([f5585b2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f5585b2ba5ce0ee92bf1f912868ec11bce4f03f8))
* **header:** split customerType and distributor into different props ([b03f0f7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b03f0f70bea29fc0d2987e03e9a2a85e450a2501))
* **header:** style menu hover effect and color ([fed3302](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/fed3302017336fcbc5545a28680e1d678c9a4ff7))
* **header:** various markdown and style improvements ([92d538d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/92d538d99507c272218ab8ccb251f682d87c8921))
* **mobileHeader:** Add mobile icons inline ([4b64601](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4b646013bdc9d745cd65a05c808ac1ebe9699034))
* **package.json:** add missing cross-env dep and fix test:watch script ([5cbb664](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5cbb66499aa6d6a344a74371ba1d2cd0637fbba9))
* **package.json:** revert the build script to default ([70aee8a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/70aee8a2d4d328772d4a696d0fb0f3c4f81131ff))


### Features

* **cp-layout:** add mocks to the lib folder ([af0cf72](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/af0cf7259ae57e3ec3eaf0aa3c5384522d9e96f6))
* **cp-layout:** wire redux with header data fetching ([d3578be](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d3578beb837400040d4c813e7172ce69aee1a1e4))
* **Cp-layout:** Adding header component ([699ae11](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/699ae11c46add48ece69d26cd7e3abd7238ca4c6))
* **Cp-layout:** adding translations ([e1aa7bd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e1aa7bdaeb4785022f2c27e2c0d117370984e2a6))
* **Cp-layout:** Adding translations and images ([37be883](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/37be8832a37f84fd490a16c537f2ed802c3d1005))
* **Cp-layout:** Modifying Header paths ([1415eff](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1415eff2d91bb9bd09317c926bdae80df6fbbf0b))
* **Cp-layout:** removing body2 style from typography ([931a4df](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/931a4dfd55ce86fffa9e2234ed5df24d908f20a4))
* **Cp-layout:** removing images ([5c3b7e7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5c3b7e7bebf7be8e2706512b2d7d986e18afec8e))
* **Cp-layout:** Updating test cases ([9aa8d15](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9aa8d1588c0b6ba66aeed05b3e8eaf5b741b4b5e))
* **Cp-layout:** Updating test cases ([d1c92a8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d1c92a8c6f3769b3e90f9d209812ffadbadc4d86))
* **CustomerPortal:** Adding button component to themes ([8cab8f3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8cab8f301ab23cb0ba0a6289a0f4be850588a49f))
* **CustomerPortal:** Adding constants file for customer type ([63ec624](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/63ec62419d1f60191941f0e03ed0712288c8439f))
* **CustomerPortal:** adding Header login/registration buttons ([b5f1176](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b5f117682961f0a940d7cbba53253b9e1d997850))
* **CustomerPortal:** Adding review comments ([6c1b3b1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/6c1b3b17d2e39f2bfb39227a397aa01114d2bf16))
* **CustomerPortal:** Adding review comments ([c320691](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c32069162628e2758328fd1f4b44076c78f842c6))
* **CustomerPortal:** Adding review comments ([e9af8a4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e9af8a41ce6dc0f5f36c3defa499480a856b2fef))
* **CustomerPortal:** Adding translations ([b83a72b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b83a72bcef2c7d6d1e3c2589b8398c085ebb6652))
* **CustomerPortal:** cleaning Withthemes method ([08a2298](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/08a2298304edd040ad5ad851b25a57969b0a3c44))
* **CustomerPortal:** cleaning Withthemes method ([d91343c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d91343c0a987644fa25a35554a15aa28f0ed8f7c))
* **CustomerPortal:** cleaning Withthemes method ([810e66c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/810e66c378a963e87efe93234073785649c29531))
* **CustomerPortal:** Modifying icons ([3831c28](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3831c2893d2d516582d16aacf8f5f498e5f61634))
* **CustomerPortal:** Modifying styles ([3966c54](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3966c54d343e535a0860cfb125daa661447edc5e))
* **CustomerPortal:** updating snapshot tests ([e7a99fd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e7a99fd4f0874d0a554eecb42d25eb82bc6d6ee0))
* **CustomerPortal:** Updating snapshots ([daf43e5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/daf43e53b1b5969dcaeedd086d27e3220a9800a8))
* **footer:** Add basic main header navigation ([86a7829](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/86a78295e0870d4363e8452ecf17c3fe8e311f2d))
* **header:** add arrow pointing to chevron from submenu popovers ([ee0caef](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ee0caeff81e6b78c5ed12dfbee8f93b6cf8edab7))
* **header:** add banking menu translations ([3863bf1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3863bf19494c5c5fbcc2b87c1744e550d82cf11d))
* **header:** add banking pages dropdown ([dc31eeb](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/dc31eebfa62588033920c05260d8781ea9435808))
* **header:** add better spacing between submenu items ([c1984a6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c1984a6b3fe8dea0335e809274a44bdd59ba096a))
* **header:** Add dropdowns to main navigation ([2c5809c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2c5809cf30099b7cbad59998ece1b14c93121195))
* **header:** Add logic to banking items displayed in header main navigation ([c61bbf1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c61bbf1e1c14a74eca5492021a512637b5688bbf))
* **header:** add main navigation for public pages ([7f2f5dd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7f2f5dd241bb6b3623e28f08da23e4e19afd63a7))
* **header:** Add redux and refactor header data fetching logic ([7975454](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7975454f3f336f9e8de0e6c11f74d862fd17af4a))
* **header:** add refer a friend icon ([f06e801](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f06e80192c662f72be404aa812b0ada44a86a197))
* **header:** add responsiveness to banking menu items ([eea5eff](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/eea5eff3878ad969d7d5afeb7020a92f38f0c70f))
* **header:** add responsiveness to mobile screen for public menu items ([d6a59cc](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d6a59cc23fdfe2c37023fd198472a4efb5e86f8a))
* **header:** add translations for banking main navigation items ([60df93f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/60df93f41bf99e1cdd9e3682006c7bfeb800d64e))
* **header:** Add translations for other languages as well ([09b2c5f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/09b2c5f6fc8e9837c398ba62f6e44ed9a4334142))
* **header:** first iteration of styling mobile nav ([671cda0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/671cda06d19798fff2e9b7b29b15206f38a05c43))
* **header:** make it responsive ([47ade11](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/47ade1134c0118e252969e77e5930ddbb6e72fdc))
* **header:** move hardcoded text to translations ([8ee0df1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8ee0df1bbcff922dec415c3e661114f9d59a0299))
* **header:** move translations to transifex ([4142696](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/41426963fd6b2c282848769f41e0376311b45eb4))
* **header:** use Link from next js to transition between pages ([987a7e0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/987a7e064ba327d89d03a202068c654abde3b4ca))
* **Header:** Add option for simple menu link without submenu ([5b126af](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5b126afca748ab3bc5fbf50cd151ac89c26f42d7))
* **Header:** store customer data in local storage ([cd804dc](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cd804dcd4d1b10e02fa480d9c68fba78447c5eca))





## [1.10.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.10.0...@raisin/cp-layout@1.10.1) (2021-07-27)

**Note:** Version bump only for package @raisin/cp-layout





# [1.10.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.9.0...@raisin/cp-layout@1.10.0) (2021-07-23)


### Features

* **Styles:** cleaned commented code and fixed theme ([0f92fef](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0f92fefd9fb066f36a4d68f9eacb381265394906))
* **Styles:** fixed type for theme ([1b2c56b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1b2c56ba6dd17b60ad0ea874e9a3b575522e3e23))
* **Tests:** fixed broken unit tests and test setup ([b73411c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b73411ce5c38c1295756c59ce4bc1296c3a7a450))
* **TSErros:** fixed type errors under cp-layout which failing cap build ([5f0edbe](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5f0edbed3fb87769a9e782a07d7db443b0a84aeb))





# [1.9.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.8.0...@raisin/cp-layout@1.9.0) (2021-07-22)


### Features

* **WrappedFooter:** Export footer wrapped with i18n ([98da0f1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/98da0f176b9694f92ac1ff38425cbca1e3185ecf))





# [1.8.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.7.0...@raisin/cp-layout@1.8.0) (2021-07-09)


### Bug Fixes

* **cp:** Fix FOUC bug ([23f6c96](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/23f6c964f0adf5dd9577dc50ca55edeb8b5d41b1))
* **packages:** various improvements ([45b8a6e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/45b8a6ea6b2f7d80c8b021e73713b3dde9017e18))


### Features

* **cp-layout:** use new breakpoints ([97f2444](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/97f244438a43edeadf231205647b22e6b6be4ebf))





# [1.7.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.6.6...@raisin/cp-layout@1.7.0) (2021-06-03)


### Features

* **Gitlab and Amplify:** Add dynamic import to the libraries build scripts ([9e4417d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9e4417df5dc283cc916460e05000723197c41fb8))





## [1.6.6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.6.5...@raisin/cp-layout@1.6.6) (2021-05-28)


### Bug Fixes

* locale used in trustpilotData fetching and save the data under /tmp ([f373dc4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f373dc45658215a36ec364c4c22b3958a8c4ec8d))
* mobile banner images ([0e69f94](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0e69f94b9d4cc49639396388fe8c5bce7d1672df))
* **cp-layout:** transpile to commonjs ([7f25233](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7f2523387a9af35ff4d7ef523c98185f85a8d280))
* **layout:** Provide locale from consumer project ([49a0b96](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/49a0b96800d337e6d5fbee190f4db545ab2d4c06))





## [1.6.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.6.0...@raisin/cp-layout@1.6.1) (2021-05-26)


### Bug Fixes

* **cp-layout:** add aliases in ts-config ([a31b7a5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/a31b7a5615f6e9bc22df75a441b34495b22d0ee2))
* **libs:** eslint ignore dist folders and add test files to tsc ([378637c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/378637c9d51b27f9b2867fddd8e7e0082560c491))





# [1.6.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.4.1...@raisin/cp-layout@1.6.0) (2021-05-25)


### Features

* **gitlab-ci:** Add build script ([60a1954](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/60a195482ea71c4a9fdce3fa91b7f4fbcc09392d))





## [1.4.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.4.0...@raisin/cp-layout@1.4.1) (2021-05-25)

**Note:** Version bump only for package @raisin/cp-layout





# [1.4.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.3.0...@raisin/cp-layout@1.4.0) (2021-05-20)


### Bug Fixes

* **cp-layout:** ignores junit.xml file ([b93472c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b93472cc87c6ea5ef68b242d30695538d33d4614))
* **cp-layout:** introduces fileMock so that import files in tests work again ([4df27f1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4df27f19f2c94ff04a37e8545a9e2647f9d81299))
* **cp-layout:** Introduces global locale for jest setup ([d8a5cd9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d8a5cd92e00ec7e0f52340f2860449461692f8d7))
* **cp-layout:** Makes styles in snapshots readable again ([9f1af75](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9f1af753839431847c0ee7adcf62ff56ad0b4e5b))


### Features

* **cp-layout:** Introduces jest config files to allow jest to run on cp-layout ([6cd1250](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/6cd1250f3154c45ad4855b90d006f00f493da447))





# [1.3.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.2.0...@raisin/cp-layout@1.3.0) (2021-05-19)


### Bug Fixes

* align defaultLocale to en-US in cp-layout as well ([97bc928](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/97bc928eeb9c1acf60ebf844411ae8eac0a6f246))
* **footer:** Various fixes and improvements ([75e38b6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/75e38b66a2cb33c7ef756490264e66294221b5f6))
* **palette:** add required main theme color ([d7267c8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d7267c865384301b09e23a7601a0e862bc7c61c3))
* **theme:** Remove theme and obsolete componentfrom CP and update the theme palette with all missing colors ([78099b4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/78099b43f670cb80a6945597e3ac2afd78f15885))


### Features

* **fonts:** Add self hosted open sans font to CP ([9278774](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/927877480ddc4aa902c0e73da13134d895ed8678))





# [1.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-layout@1.1.0...@raisin/cp-layout@1.2.0) (2021-05-19)


### Bug Fixes

* remove some commented out code ([09b1026](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/09b10260b95c172cfbfb83c655225f69e3c172c4))


### Features

* **footer:** Update cp-layout to fetch footer by page footer reference ([b06e6b0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b06e6b0f26a08ee220193e38706e3a677816d0ab))





# 1.1.0 (2021-05-18)


### Features

* **cp-layout:** Fix intl in cp-layout ([3e8fbaa](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3e8fbaab6a5fcec447926c24ea286b21393d0858))
