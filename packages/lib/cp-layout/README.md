# Customer Portal Layout

This package provides a Layout component to be used inside Customer Portal(CP) NextJS pages.
The idea is to share the header and footer between each Zone used in CP.
The methods used to fetch cms data for the components should also be found in here
The `lib` folder contains the transpiled code that is production ready.

## Installation

1. install it in consumer project: `yarn add @raisin/cp-layout`
2. run `yarn build` in this project to build the `lib` folder.
3. In `package.json`, make sure the version is prefixed with ^ so it gets updated automatically for minor/patches. e.g.: `"@raisin/cp-layout": "^1.0.0",`

## Usage/Examples

1. Import the Layout component in the pages you want to have header/footer aligned:

```ts
import { Layout, ILayout, getLayoutData } from '@raisin/cp-layout';
```

2. Use the `getLayoutData` method to pre-fetch all the data required for the `Layout` component in NextJS projects:

```ts
export const getStaticProps: GetStaticProps = async () => {
  const layout = await getLayoutData();
  return {
    props: { layout },
  };
};
export default Page;
```

3. The page will look like this:

```ts
import React from 'react';
import { NextPage, GetStaticProps } from 'next';
import { Layout, ILayout, getLayoutData } from '@raisin/cp-layout';

interface IPage {
  layout: ILayout['layout'];
}

const Page: NextPage<IPage> = ({ layout }) => (
  <Layout layout={layout}>
  {* page content *}
  </Layout>
);

export const getStaticProps: GetStaticProps = async () => {
  const layout = await getLayoutData();
  return {
    props: { layout },
  };
};

export default Page;
```

## How to create a resource on Transifex for MFE

- make sure you are into the app directory
- run `yarn tx:extract` to extract translation keys to `default.json`
- run `tx push -s` to push the source file for the first time. (in case you don't have transifex cli locally installed yet, see how to install it [here](https://docs.transifex.com/client/installing-the-client))
  PS: in the console, you'll see a message saying the resource doesn't exist and it's being created.

## How to Update translations for MFE

- make sure you are into the app directory
- add translations into code with `id` and `defaultMessage`. Optionally, a `description` can be added as well to provide context.
- run `yarn tx:extract` to extract translation keys to `default.json`
- run `yarn tx:push` to push extracted keys to trasifex
- update translations for required languages on transifex
- run `yarn tx:pull` to pull updated translations from transifex.
- run `yarn tx:compile` to compile pulled translations.
- please make sure updated/pulled translations are commited in source control.

## Development

Since the layout is an external package that does not run as a standalone, hot reload is not available during development. However, we do have some advantages due to the monorepo setup which allows us to develop and test external packages without needing to publish them each time. The FE Monorepo uses Lerna, which links our packages to the local version. Here is the development workflow after installing the `cp-layout` following the instructions above:

1. run `npx lerna bootstrap` to link the consumer project to the local version of the `cp-layout` project
1. do some changes in the `cp-layout` project
1. run yarn build
1. run(/re-run) your consumer project and see the changes in `cp-layout` without having to publish them to yarn
