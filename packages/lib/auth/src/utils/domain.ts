const { config } = require('../../config');

const configurations = {
  deu: {
    domain: config.globals.domainDEU,
    userPoolId: config.globals.cognitoUserPoolIdDEU,
    userPoolWebClientId: config.globals.cognitoUserPoolWebClientIdDEU,
  },
  gbr: {
    domain: config.globals.domainGBR,
    userPoolId: config.globals.cognitoUserPoolIdGBR,
    userPoolWebClientId: config.globals.cognitoUserPoolWebClientIdGBR,
  },
};

export const getConfigValuesBaseOnRegion = () => {
  const { hostname } = window.location;
  // As the url domains have the same structure this line will always work
  const region = hostname.includes('co.uk') ? 'gbr' : 'deu';
  return configurations[region] ?? configurations.deu;
};

export default {
  getConfigValuesBaseOnRegion,
};
