import React, { FC } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import getMessages from './getMessages';
import { Locale, getShortLocale } from './localeContext';

const getTranslationMessages = async (locale: string) => {
  const hostMessages = await getMessages(locale);

  return hostMessages;
};

// eslint-disable-next-line react/prop-types
const Providers: FC = ({ children }) => {
  const messages: any = getTranslationMessages(Locale.lang);

  return (
    <IntlProvider
      locale={getShortLocale(Locale.lang)}
      defaultLocale={getShortLocale(Locale.lang)}
      messages={messages}
    >
      {children}
    </IntlProvider>
  );
};

const customRender = (ui, options = {}) => render(ui, { wrapper: Providers, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
