import { createContext } from 'react';

export const Locale = {
  lang: 'de_DE',
};

export const LocaleContext = createContext(Locale);

export const getShortLocale = (locale) => locale.substr(0, 2);
