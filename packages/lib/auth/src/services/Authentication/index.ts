import Amplify, { Auth } from 'aws-amplify';

import awsConfig from './awsConfig';

Amplify.configure(awsConfig);

export const signOut = () => Auth.signOut();

export const signIn = () => Auth.federatedSignIn({ customProvider: 'OktaRaisin' });

export const currentSession = () => Auth.currentSession();

export const currentAuthenticatedUser = () => Auth.currentAuthenticatedUser();
