import { getConfigValuesBaseOnRegion } from './domain';

const { domain, userPoolId, userPoolWebClientId } = getConfigValuesBaseOnRegion();

const oauth = {
  domain,
  scope: ['email', 'openid', 'profile'],
  redirectSignIn: typeof window !== 'undefined' ? `${document.location.origin}/dashboard` : '',
  redirectSignOut: typeof window !== 'undefined' ? `${document.location.origin}/` : '',
  responseType: 'code',
};

const awsConfig = {
  Auth: {
    region: 'eu-central-1',
    oauth,
    userPoolId,
    userPoolWebClientId,
  },
};

export default awsConfig;
