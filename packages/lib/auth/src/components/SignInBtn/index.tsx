import React from 'react';
import Button from '@material-ui/core/Button';
import { signIn } from '../../services/Authentication';

const SignInBtn = ({ label = 'Sign In' }) => (
  <Button variant="contained" data-testid="signInButton" onClick={signIn}>
    {label}
  </Button>
);
export default SignInBtn;
