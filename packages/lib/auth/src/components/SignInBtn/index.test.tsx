import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../utils/test-utils';
import SignInBtn from '.';

describe('<SignInBtn />', () => {
  it('renders <SignInBtn> button', async () => {
    const { getByTestId } = render(<SignInBtn />);
    const text = await waitFor(() => getByTestId('signInButton'));

    expect(text).toBeInTheDocument();
  });
});
