import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from '../../utils/test-utils';

import SignOutBtn from '.';

describe('<SignOutBtn />', () => {
  it('renders <SignOutBtn> text', async () => {
    const { getByTestId } = render(<SignOutBtn />);
    const text = await waitFor(() => getByTestId('signOutButton'));

    expect(text).toBeInTheDocument();
  });
});
