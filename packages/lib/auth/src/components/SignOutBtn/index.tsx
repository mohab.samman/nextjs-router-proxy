import React from 'react';
import Button from '@material-ui/core/Button';

import { signOut } from '../../services/Authentication';

const SignOutBtn = ({ label = 'Sign out', className = '' }) => {
  const onSignOut = async () => {
    await signOut();
  };

  return (
    <Button size="small" data-testid="signOutButton" onClick={onSignOut} className={className}>
      {label}
    </Button>
  );
};

export default SignOutBtn;
