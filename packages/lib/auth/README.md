# Auth

This package provides Auth components to be used inside Customer Administrator portal(CAP) NextJS pages.
The idea is to share the SignIn, SignOut button components and Auth service for logout.
The `lib` folder contains the transpiled code that is production ready.

## Installation

1. install it in consumer project: `yarn add @raisin/auth`
2. run `yarn build` in this project to build the `lib` folder.
3. In `package.json`, make sure the version is prefixed with ^ so it gets updated automatically for minor/patches. e.g.: `"@raisin/auth": "^1.0.0",`

## Usage/Examples

1. Import the Button component in the pages you want:

```ts
import { SignInBtn } from '@raisin/auth';
```

2. Use the `SignInBtn` component to render button.

```ts
<SignInBtn label={'Sign In'} />
```

## Development

Since the auth is an external package that does not run as a standalone, hot reload is not available during development. However, we do have some advantages due to the monorepo setup which allows us to develop and test external packages without needing to publish them each time. The FE Monorepo uses Lerna, which links our packages to the local version. Here is the development workflow after installing the `auth` following the instructions above:

1. run `npx lerna bootstrap` to link the consumer project to the local version of the `auth` project
1. do some changes in the `auth` project
1. run yarn build
1. run(/re-run) your consumer project and see the changes in `auth` without havign to publish them to yarn
