# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/auth@0.1.0...@raisin/auth@0.2.0) (2021-07-23)


### Features

* **SearchCustomer:** search customer by bac number ([7019dea](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7019dea276c8da90bf3d8626e0553b918b2136c6))





# [0.1.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/auth@0.0.2...@raisin/auth@0.1.0) (2021-07-09)


### Features

* **Auth:** bumped auth version ([f5c81e8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f5c81e8c142f124176e9b978e1aedbf2b2829f45))
