/**
 * Application Variables
 * These variables are used to run the project locally and in the GitLab pipeline.
 */

const NAME = 'auth';
const MFE_NAME = 'auth';
const APP_PORT = '3001';
const APP_HOST = 'localhost';
const ENVIRONMENT = 'local';
const LOCALE = process.env.APP_LOCALE || 'en-US';

/**
 * Environments
 * These are the environments where the application runs:
 * For each environment there are different values, urls, endpoints and domains.
 * Staging, Onboarding and Production env configs are necessary to deploy the project with the GitLab pipeline.
 * Development env config is used only to run the project locally.
 */
const environments = {
  local: {
    publicPath: '/',

    domain: {
      DEU: 'cap-raisin-deu-dev.auth.eu-central-1.amazoncognito.com',
      GBR: 'cap-raisin-gbr-dev.auth.eu-central-1.amazoncognito.com',
    },

    cognito: {
      userPoolId: {
        DEU: 'eu-central-1_bwcaWCBD9',
        GBR: 'eu-central-1_EvKIupqm0',
      },

      userPoolWebClientId: {
        DEU: '1gomh1srrfce9jqdetkpueo5tp',
        GBR: '2fris4j1221j137ej9k0ev29r3',
      },
    },
  },
  development: {
    publicPath: `https://mfe-${MFE_NAME}.amplifyapp.com/`,

    domain: {
      DEU: 'cap-raisin-deu-dev.auth.eu-central-1.amazoncognito.com',
      GBR: 'cap-raisin-gbr-dev.auth.eu-central-1.amazoncognito.com',
    },

    cognito: {
      userPoolId: {
        DEU: 'eu-central-1_bwcaWCBD9',
        GBR: 'eu-central-1_EvKIupqm0',
      },

      userPoolWebClientId: {
        DEU: '1gomh1srrfce9jqdetkpueo5tp',
        GBR: '2fris4j1221j137ej9k0ev29r3',
      },
    },
  },
  staging: {
    publicPath: `https://mfe-${MFE_NAME}.testraisin.com/`,

    domain: {
      DEU: 'cap-raisin-deu-staging.auth.eu-central-1.amazoncognito.com',
      GBR: 'cap-raisin-gbr-staging.auth.eu-central-1.amazoncognito.com',
    },

    cognito: {
      userPoolId: {
        DEU: 'eu-central-1_cnITADB9Q',
        GBR: 'eu-central-1_n4iHynIPX',
      },

      userPoolWebClientId: {
        DEU: 'lgdbcl9frqe1l0dc81e8f5b4j',
        GBR: '5m7akp6qk64e68ptfr35easvus',
      },
    },
  },
  onboarding: {
    publicPath: `https://mfe-${MFE_NAME}.onboarding-raisin.com/`,

    domain: {
      DEU: 'cap-raisin-deu-onboarding.auth.eu-central-1.amazoncognito.com',
      GBR: 'cap-raisin-gbr-onboarding.auth.eu-central-1.amazoncognito.com',
    },

    cognito: {
      userPoolId: {
        DEU: 'eu-central-1_YYyguoLr1',
        GBR: 'eu-central-1_Qhza8NOh9',
      },

      userPoolWebClientId: {
        DEU: '6kuoaas8epgh0htndk8vaij7jv',
        GBR: 'rl063cs04lc0obb8h6647thp',
      },
    },
  },
  production: {
    publicPath: `https://mfe-${MFE_NAME}.raisin.com/`,
    domain: {
      DEU: 'cap-raisin-deu.auth.eu-central-1.amazoncognito.com',
      GBR: 'cap-raisin-gbr.auth.eu-central-1.amazoncognito.com',
    },

    cognito: {
      userPoolId: {
        DEU: 'eu-central-1_EmeStX1Zp',
        GBR: 'eu-central-1_CMMRivS15',
      },

      userPoolWebClientId: {
        DEU: '2per09u06qhgm49o2m50l7hu87',
        GBR: '6478qbhv89bkl8m098jdq2h7oj',
      },
    },
  },
};

/**
 * Configuration
 * The config object is used internally for the project.
 * The object has the  following section:
 * name: µFE name
 * port: On what port should the project run locally
 * host: URL for the application, localhost for development,
 * globals: Object used for injecting global values into the application.
 */
const config = {
  name: NAME,
  port: APP_PORT,
  host: APP_HOST,

  globals: {
    locale: LOCALE,

    redirectSignIn: '/dashboard',
    redirectSignout: '/',
    domainDEU: environments[ENVIRONMENT].domain.DEU,
    domainGBR: environments[ENVIRONMENT].domain.GBR,
    cognitoUserPoolIdDEU: environments[ENVIRONMENT].cognito.userPoolId.DEU,
    cognitoUserPoolIdGBR: environments[ENVIRONMENT].cognito.userPoolId.GBR,
    cognitoUserPoolWebClientIdDEU: environments[ENVIRONMENT].cognito.userPoolWebClientId.DEU,
    cognitoUserPoolWebClientIdGBR: environments[ENVIRONMENT].cognito.userPoolWebClientId.GBR,
  },

  publicPath: environments[ENVIRONMENT].publicPath,
};

module.exports.config = config;
