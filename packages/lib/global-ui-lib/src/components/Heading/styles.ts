import { makeStyles } from '@material-ui/styles';

export const useStyles = makeStyles({
  h1: {
    fontSize: '40px',
    lineHeight: 1.1,
    fontWeight: 300,
  },
  h2: {
    fontSize: '35px',
    lineHeight: 1.2,
    fontWeight: 300,
  },
  h3: {
    fontSize: '30px',
    lineHeight: 1.2,
    fontWeight: 300,
  },
  h4: {
    fontSize: '20px',
    lineHeight: 1.3,
    fontWeight: 600,
  },
  h5: {
    fontSize: '17px',
    lineHeight: 1.4,
    fontWeight: 600,
  },
  h1Large: {
    fontSize: '50px',
  },
});
