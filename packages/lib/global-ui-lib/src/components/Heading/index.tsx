import React from 'react';
import MUITypography, { TypographyProps } from '@material-ui/core/Typography';
import { useStyles } from './styles';

export interface IHeading {
  variant: 'h1' | 'h2' | 'h3' | 'h4' | 'h5';
  isLarge?: boolean;
  align?: TypographyProps['align'];
  className?: string;
}

const Heading: React.FC<IHeading> = ({ children, variant, isLarge, ...props }) => {
  const classes = useStyles();

  const largeClass = isLarge && variant === 'h1' && classes.h1Large;

  return (
    <MUITypography
      className={`${classes[variant]} ${largeClass} ${props.className}`}
      data-testid="heading"
      {...props}
    >
      {children}
    </MUITypography>
  );
};

export default Heading;
