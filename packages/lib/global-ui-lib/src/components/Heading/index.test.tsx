import React from 'react';
import { waitFor, render } from '@testing-library/react';

import Heading from './index';

describe('<Home />', () => {
  it('renders a <Heading> component', async () => {
    const { getByTestId } = render(<Heading variant="h1" />);
    const text = await waitFor(() => getByTestId('heading'));

    expect(text).toBeInTheDocument();
  });
});
