import React from 'react';
import { Story, Meta } from '@storybook/react';
import Heading, { IHeading } from './index';

export default {
  title: 'Heading',
  component: Heading,
  argTypes: {},
} as Meta;

const Template: Story<IHeading> = (args) => (
  <Heading {...args}>
    {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
    Heading {args.variant} {args.isLarge ? 'large' : ''}
  </Heading>
);

export const Heading1 = Template.bind({});
Heading1.args = {
  variant: 'h1',
};

export const Heading2 = Template.bind({});
Heading2.args = {
  variant: 'h2',
};

export const Heading3 = Template.bind({});
Heading3.args = {
  variant: 'h3',
};

export const Heading4 = Template.bind({});
Heading4.args = {
  variant: 'h4',
};

export const Heading5 = Template.bind({});
Heading5.args = {
  variant: 'h5',
};
