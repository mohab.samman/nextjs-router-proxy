import React from 'react';
import { waitFor, render } from '@testing-library/react';

import Grid from './index';

describe('<Home />', () => {
  it('renders <Home> text', async () => {
    const { getByTestId } = render(<Grid />);
    const text = await waitFor(() => getByTestId('grid'));

    expect(text).toBeInTheDocument();
  });
});
