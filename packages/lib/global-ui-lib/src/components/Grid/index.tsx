import React from 'react';
import MUIGrid, { GridProps } from '@material-ui/core/Grid';

export interface IGrid extends GridProps {}

const Grid: React.FC<IGrid> = ({ children, ...props }) => (
  <MUIGrid data-testid="grid" {...props}>
    {children}
  </MUIGrid>
);

export default Grid;
