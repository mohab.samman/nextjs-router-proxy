import React from 'react';
import MUIButton, { ButtonProps } from '@material-ui/core/Button';

export interface IButton extends ButtonProps {
  /**
   * Button contents
   */
  label?: string;
  /**
   * Optional click handler
   */
  onClick?: () => void;
}

const Button: React.FC<IButton> = ({ label, children, ...props }) => (
  <MUIButton data-testid="button" {...props}>
    {label || children}
  </MUIButton>
);

export default Button;
