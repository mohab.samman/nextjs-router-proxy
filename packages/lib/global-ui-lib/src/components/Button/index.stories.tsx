import React from 'react';
import { Story, Meta } from '@storybook/react';
import MailIcon from '@material-ui/icons/Mail';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import Button, { IButton } from './index';

export default {
  title: 'Button',
  component: Button,
  argTypes: {
    color: {
      control: {
        type: 'radio',
        options: ['primary', 'secondary', 'inherit'],
      },
    },
  },
} as Meta;

const Template: Story<IButton> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  variant: 'contained',
  color: 'primary',
  label: 'Primary',
};

export const Secondary = Template.bind({});
Secondary.args = {
  variant: 'contained',
  color: 'secondary',
  label: 'Secondary',
};

export const Disabled = Template.bind({});
Disabled.args = {
  variant: 'contained',
  disabled: true,
  label: 'Disabled',
};

export const Large = Template.bind({});
Large.args = {
  variant: 'outlined',
  color: 'primary',
  size: 'large',
  label: 'Large',
};

export const Small = Template.bind({});
Small.args = {
  variant: 'outlined',
  color: 'primary',
  size: 'small',
  label: 'Small',
};

export const WithStartIcon = Template.bind({});
WithStartIcon.args = {
  variant: 'contained',
  color: 'primary',
  startIcon: <MailIcon />,
  label: 'Mail',
};

export const WithEndIcon = Template.bind({});
WithEndIcon.args = {
  variant: 'contained',
  color: 'primary',
  endIcon: <MonetizationOnIcon />,
  label: 'DogeCoin',
};
