import React from 'react';
import { waitFor, render } from '@testing-library/react';

import Button from './index';

describe('<Home />', () => {
  it('renders a <Button> component', async () => {
    const { getByTestId } = render(<Button />);
    const text = await waitFor(() => getByTestId('button'));

    expect(text).toBeInTheDocument();
  });
});
