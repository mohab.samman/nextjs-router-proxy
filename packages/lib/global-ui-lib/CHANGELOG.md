# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.6.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.5.1...@raisin/global-ui-lib@1.6.0) (2021-07-23)


### Features

* **Global:** fixed broken build for Mui version ([16d9ae1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/16d9ae17609f5e8411d23b679b73427997101981))





## [1.5.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.5.0...@raisin/global-ui-lib@1.5.1) (2021-07-09)


### Bug Fixes

* **packages:** various improvements ([45b8a6e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/45b8a6ea6b2f7d80c8b021e73713b3dde9017e18))





# [1.5.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.4.1...@raisin/global-ui-lib@1.5.0) (2021-06-03)


### Features

* **Gitlab and Amplify:** Add dynamic import to the libraries build scripts ([9e4417d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9e4417df5dc283cc916460e05000723197c41fb8))





## [1.4.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.4.0...@raisin/global-ui-lib@1.4.1) (2021-05-26)


### Bug Fixes

* **libs:** disable eslint import rule for libs because tsconfig already convers that ([1f2b612](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1f2b612095f5fd0e8015c11e7d4ee55ab8f2ffe6))
* **libs:** eslint ignore dist folders and add test files to tsc ([378637c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/378637c9d51b27f9b2867fddd8e7e0082560c491))
* **libs:** eslint ignore lib folder in cp-layout ([0348110](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0348110f69ce124786cd5064b36720b4a9b19e02))





# [1.4.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.2.0...@raisin/global-ui-lib@1.4.0) (2021-05-25)


### Features

* **gitlab-ci:** Add build script ([60a1954](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/60a195482ea71c4a9fdce3fa91b7f4fbcc09392d))





# [1.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.1.1...@raisin/global-ui-lib@1.2.0) (2021-05-12)


### Bug Fixes

* ui-lib deps ([cf804c8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cf804c85d10431a313702c6e08c81664a5f46366))


### Features

* add heading component to global-ui-lib ([b41bd94](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b41bd94842a3decb833f464249e0606218ad975a))





## [1.1.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.1.0...@raisin/global-ui-lib@1.1.1) (2021-04-26)


### Bug Fixes

* autoformat was applied to this file by precise-commit ([39fa284](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/39fa284f209da3a7abc20c941dd1dae7d68c8c8e))





# [1.1.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/global-ui-lib@1.0.3...@raisin/global-ui-lib@1.1.0) (2021-04-26)


### Bug Fixes

* Git ignore junit.xml ([e8d3263](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e8d3263b7edf1e4edf565d01b5dfbecaa35b00ff))
* remove comments during tsc build ([56bdc27](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/56bdc272c72901a5b53f1d24cc7de2d4a90c2c14))


### Features

* update readme.md ([439bfa8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/439bfa851d769c3b8b8c86bba586b3d0cf4d34e8))
