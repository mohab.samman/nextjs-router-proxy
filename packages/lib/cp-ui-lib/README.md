# Raisin's Customer Portal UI-lib

This is a shared components library, hosting and exporting a variety of components built on top of MaterialUI and used only in the Customer Portal. The components are customised to fit Raisin's look and feel. The components in this library are _organisms_ or _molecules_. _atomic_ level components should live in the global-ui-lib

You can read more on atomic web design [here](https://bradfrost.com/blog/post/atomic-web-design/)

## Pre-requisites

- Node v12.18.3 or higher.
- Yarn v1.22.4 or higher.

## Using the Customer Portal UI-lib in another project

install it as a dependency

> **Info:** Lerna bootstrap will link the `cp-ui-lib` from the consumer projects in this monorepo, to the local version present in the `fe-monorepo/packages/lib/` for convenience during development. This will happen only if the version requested in `package.json` for `cp-ui-lib` matches the version you have locally under `package/lib/cp-ui-lib`). For this to happen successfully make sure you run `yarn build` in the `cp-ui-lib` before installing it to a consumer project.

```
yarn add @raisin/cp-ui-lib
```

And thats it :). Now you can start using the contents of this lib in your project

```
import { Button } from '@raisin/cp-ui-lib';
```

_If you encounter any issues, have a look at the Troubleshooting section below_

## Usage

> **Info:** The project is located inside the `fe-monorepo`, meaning that dependencies are already installed due to using `yarn workspaces`. If you are using the monorepo for the first time, make sure you run `yarn`

> **Info:** If you want to symlink the local version of this lib in a µFE from the `fe-monorepo`, make sure you already compiled the lib by running `yarn build` (in the `cp-ui-lib` folder) and then just run `lerna bootstrap`. Lerna will take care of the rest. After doing this, you won't have to publish & install a new version of the lib every time you want to test something. You will only have to run `yarn build` in the `cp-ui-lib` directory and your µFE hot-reload will take care of the rest.

To run/start the project (storybook):

> **Info:** Since this project is a lib, we run it with storybook to view our components

```
$ yarn start
```

To build the project as a lib before publishing (output in `./lib` folder):

```
$ yarn build
```

To build as static storybook for StoryBook deployment (output in `./dist` folder):

```
$ yarn build:storybook
```

To run the tests:

```
$ yarn test
```

To run the test on watch mode:

```
$ yarn test:watch
```

To run eslint on the project.

```
$ yarn lint
```

To see the list the rest of available scripts, check `package.json`

## A note on creating components in this library

The components are presented in a storybook. All relevant states of a component should be showcased by creating stories for the said component. This project uses Typescript, therefore types should be well documented for each component with comments above describing the context of each prop. See examples in the `WIP` component. The types, comments and even default values are picked up by Storybook creating a wonderful documentation for anyone using the Storybook. Moreover, since the components are based on MUI, when we customise a component we should extend the custom component interface with the types provided by MUI. These will then also be picked up by Storybook.

## A note on dependencies

Each team is free to use whatever dependencies versions they wish, with one caveat. All µFEs living inside the `fe-monorepo` need to be aligned on the same `webpack` version. This is due to the fact that we use `yarn workspaces` and different versions of `webpack` cause hard to debug issues. To see the current version of `webpack` synced across all µFEs/libs, check the fe-monorepo [README.md](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/-/blob/master/README.md)

## Troubleshooting

1. exports is not defined

```
Typescript ReferenceError: exports is not defined
```

We transpile this library to ES5 before publishing it to npm. The code is then picked up by babel in the consumer project when you run/build the project. The issue above is very likely to be caused by the `@babel/plugin-transform-modules-commonjs`. Try removing it and run your project again. If you do need this plugin, exclude the cp-ui-lib from babels scope.
