# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.3.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-ui-lib@1.3.0...@raisin/cp-ui-lib@1.3.1) (2021-07-09)


### Bug Fixes

* **packages:** various improvements ([45b8a6e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/45b8a6ea6b2f7d80c8b021e73713b3dde9017e18))





# [1.3.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-ui-lib@1.2.1...@raisin/cp-ui-lib@1.3.0) (2021-06-03)


### Features

* **Gitlab and Amplify:** Add dynamic import to the libraries build scripts ([9e4417d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9e4417df5dc283cc916460e05000723197c41fb8))





## [1.2.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-ui-lib@1.2.0...@raisin/cp-ui-lib@1.2.1) (2021-05-26)


### Bug Fixes

* **libs:** disable eslint import rule for libs because tsconfig already convers that ([1f2b612](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1f2b612095f5fd0e8015c11e7d4ee55ab8f2ffe6))
* **libs:** eslint ignore dist folders and add test files to tsc ([378637c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/378637c9d51b27f9b2867fddd8e7e0082560c491))





# [1.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cp-ui-lib@1.0.1...@raisin/cp-ui-lib@1.2.0) (2021-05-25)


### Features

* **gitlab-ci:** Add build script ([60a1954](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/60a195482ea71c4a9fdce3fa91b7f4fbcc09392d))





## 1.0.1 (2021-05-18)


### Bug Fixes

* **package.json:** fix minify svg script and remove unwanted dep ([a133574](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/a1335741c3812ed5e4cc237b4eac577ffed761f0))
