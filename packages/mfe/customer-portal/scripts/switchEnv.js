const fs = require('fs');
const { green, yellow } = require('chalk');
const { locale } = require('../config').config.globals;

const data = fs.readFileSync('./.env', 'utf-8');
const arg = process.argv[2];

if (!arg) {
  const msg =
    'Run the script followed by the locale you want your workspace to be on. \ne.g. yarn switch-env de';
  // eslint-disable-next-line no-console
  console.log(`${yellow(msg)}`);
  return;
}

let lang;

switch (arg) {
  case 'en':
  case 'com':
  case 'us':
    lang = 'en-US';
    break;
  case 'ie':
    lang = 'en-IE';
    break;
  case 'uk':
  case 'gbr':
  case 'gb':
    lang = 'en-GB';
    break;
  case 'de':
  case 'deu':
    lang = 'de-DE';
    break;
  case 'at':
    lang = 'de-AT';
    break;
  case 'es':
    lang = 'es-ES';
    break;
  case 'fr':
    lang = 'fr-FR';
    break;
  case 'nl':
    lang = 'nl-NL';
    break;
  default:
    lang = arg;
    break;
}

fs.writeFileSync('./.env', data.replace(locale, lang), 'utf-8');

const msg = `Your workspace is now on ${lang} environment`;
// eslint-disable-next-line no-console
console.log(`${green(msg)}`);
