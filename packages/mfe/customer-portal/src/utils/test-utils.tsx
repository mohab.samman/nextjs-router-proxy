import React, { FC } from 'react';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import getMessages from './getMessages';
import { Locale, DEFAULT_LOCALE_LABEL } from './localeContext';

const getTranslationMessages = async (locale: string) => {
  const hostMessages = await getMessages(locale);

  return hostMessages;
};

// eslint-disable-next-line react/prop-types
const Providers: FC = ({ children }) => {
  const messages: any = getTranslationMessages(Locale.language.code);

  return (
    <IntlProvider
      locale={Locale.language.label}
      defaultLocale={DEFAULT_LOCALE_LABEL}
      messages={messages}
    >
      {children}
    </IntlProvider>
  );
};

const customRender = (ui, options = {}) => render(ui, { wrapper: Providers, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
