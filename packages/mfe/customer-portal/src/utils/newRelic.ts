/**
 * Interface ICustomWindow
 * Use to define the New Relic object NREUM
 * this object was added to the GlobalThis (window)
 * using the NewRelic snippet code in the index.html
 */
interface ICustomWindow extends Window {
  NREUM: any;
}

declare const window: ICustomWindow;

/**
 * newRelicConfig
 * Is an global variables injected by Webpack
 * the object was defined in the config file in the root directory
 */
declare const newRelicConfig: {
  accountID: string;
  agentID: string;
  applicationID: string;
  domains: [];
  licenseKey: string;
  trustKey: string;
};

/**
 * setNewRelicConfig
 * Set the New Relic script values based on the environment
 * Based on which environment we are running the application
 * this will add to the NREUM object the values depending on which
 * environment is running on (Development, Onboarding, Staging and Production).
 */
const setNewRelicConfig = (({
  domains,
  accountID,
  trustKey,
  agentID,
  licenseKey,
  applicationID,
}) => {
  const isValidDomain = !!domains.find((domain) => window.location.hostname.includes(domain));

  if (isValidDomain) {
    window.NREUM.loader_config = {
      accountID,
      trustKey,
      agentID,
      licenseKey,
      applicationID,
    };

    window.NREUM.info = {
      beacon: 'bam.eu01.nr-data.net',
      errorBeacon: 'bam.eu01.nr-data.net',
      licenseKey,
      applicationID,
      sa: 1,
    };
  }
})(newRelicConfig);

export default setNewRelicConfig;
