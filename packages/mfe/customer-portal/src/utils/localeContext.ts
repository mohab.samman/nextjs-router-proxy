// global variable defined in config/index.js provided by amplify via env vars
declare const locale: string;

export const LOCALES = {
  // USA
  'en-US': {
    code: 'en_US',
    label: 'en',
    notation: 'en-us',
  },

  // Germany
  'de-DE': {
    code: 'de_DE',
    label: 'de',
    notation: 'de-de',
  },

  // UK
  'en-GB': {
    code: 'en_GB',
    label: 'gb',
    notation: 'en-gb',
  },

  // Ireland
  'en-IE': {
    code: 'en_IE',
    label: 'ie',
    notation: 'en-ie',
  },

  // Austria
  'de-AT': {
    code: 'de_AT',
    label: 'at',
    notation: 'de-at',
  },

  // France
  'fr-FR': {
    code: 'fr_FR',
    label: 'fr',
    notation: 'fr-fr',
  },

  // Spain
  'es-ES': {
    code: 'es_ES',
    label: 'es',
    notation: 'es-es',
  },

  // Netherlands
  'nl-NL': {
    code: 'nl_NL',
    label: 'nl',
    notation: 'nl-nl',
  },
};

// TODO: rename this to APP_LOCALE_OBJ
export const Locale = {
  language: LOCALES[locale],
};

export const defaultLanguage = LOCALES['en-US'];

export const DEFAULT_LOCALE_LABEL = LOCALES['en-US'].label;
