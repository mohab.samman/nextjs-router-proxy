import getMessages from 'utils/getMessages';
import { Locale } from 'utils/localeContext';

export const fetchTranslations = async () => {
  const { language } = Locale;
  const messages = await getMessages(language.code);

  return { messages, language };
};
