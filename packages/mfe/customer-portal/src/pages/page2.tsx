import React from 'react';
import { NextPage, GetStaticProps } from 'next';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import UseCase2 from 'components/UseCase2';
import { Layout, ILayout, getLayoutData } from '@raisin/cp-layout';
import { DEFAULT_LOCALE_LABEL } from 'utils/localeContext';
import { fetchTranslations } from 'utils/i18n';
import { IContentStackSecrets } from 'src/api/contentStack';

declare const contentStack: IContentStackSecrets;
declare const locale: string;
interface IPage2 {
  layout: ILayout['layout'];
  messages: Record<string, string> | Record<string, MessageFormatElement[]>;
  language: { label: string };
  useSWR: Function;
}

const Page2: NextPage<IPage2> = ({ layout, messages, language, useSWR }) => (
  <Layout layout={layout} isLoggedIn useSWR={useSWR}>
    <IntlProvider messages={messages} locale={language.label} defaultLocale={DEFAULT_LOCALE_LABEL}>
      <UseCase2 />
    </IntlProvider>
  </Layout>
);

export const getStaticProps: GetStaticProps = async () => {
  const layout = await getLayoutData(contentStack, locale);
  const { messages, language } = await fetchTranslations();

  return {
    props: { layout, messages, language },
  };
};

export default Page2;
