import React from 'react';
import ErrorPage from 'next/error';
import { NextPage, GetStaticProps } from 'next';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import { Layout, ILayout, getLayoutData } from '@raisin/cp-layout';
import UseCase1 from 'components/UseCase1';
import { getCMSEntriesForContentType, IContentStackSecrets } from 'src/api/contentStack';
import { DEFAULT_LOCALE_LABEL } from 'utils/localeContext';
import { fetchTranslations } from 'utils/i18n';

declare const contentStack: IContentStackSecrets;
declare const locale: string;

interface IRetailTemplate {
  layout: ILayout['layout'];
  retailPageData?: {
    title: string;
  };
  page: {
    title: string;
  };
  messages: Record<string, string> | Record<string, MessageFormatElement[]>;
  language: { label: string };
  useSWR: Function;
}

const RetailTemplate: NextPage<IRetailTemplate> = ({
  layout,
  page,
  messages,
  language,
  useSWR,
}) => {
  if (!page) {
    return <ErrorPage statusCode={404} />;
  }

  if (!layout) {
    return null;
  }

  return (
    <Layout layout={layout} useSWR={useSWR}>
      <IntlProvider
        messages={messages}
        locale={language.label}
        defaultLocale={DEFAULT_LOCALE_LABEL}
      >
        <UseCase1 subHeading={page?.title} />
      </IntlProvider>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async ({ params, preview = false }) => {
  const allPages = await getCMSEntriesForContentType('pages');

  const currentPage = allPages.find((page: { url: string }) => {
    if (Array.isArray(params?.page)) {
      return `/${params?.page?.join('/')}` === page.url;
    }

    return page.url === params?.page;
  });

  const layout = await getLayoutData(contentStack, locale, currentPage?.footer[0]);
  const { messages, language } = await fetchTranslations();
  return {
    props: {
      preview,
      layout,
      page: currentPage,
      messages,
      language,
    },
  };
};

export const getStaticPaths = async () => {
  const allPages = await getCMSEntriesForContentType('pages');

  return {
    /**
     * split route received from CMS into an array of strings and remove the first entry since it will be an empty string
     * e.g '/some/route' -> ['some', 'route']
     * This is how Next.js expects us to provide the path to pages in a catch-all scenario
     *
     * Info: This implementation does not support url params
     */
    paths: allPages?.map(({ url }) => ({ params: { page: url.split('/').slice(1) } })) ?? [],
    fallback: false,
  };
};

export default RetailTemplate;
