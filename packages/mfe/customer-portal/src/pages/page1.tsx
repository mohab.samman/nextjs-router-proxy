import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { NextPage, GetStaticProps } from 'next';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import UseCase1 from 'components/UseCase1';
import { Layout, ILayout, getLayoutData } from '@raisin/cp-layout';
import { DEFAULT_LOCALE_LABEL } from 'utils/localeContext';
import { fetchTranslations } from 'utils/i18n';
import { IContentStackSecrets } from 'src/api/contentStack';

declare const contentStack: IContentStackSecrets;
declare const locale: string;

interface IPage1 {
  userData: {
    id: number;
    email: string;
  }[];
  layout: ILayout['layout'];
  messages: Record<string, string> | Record<string, MessageFormatElement[]>;
  language: { label: string };
  useSWR: Function;
}

const Page1: NextPage<IPage1> = ({ layout, messages, language, useSWR }) => {
  const [IsLoggedIn, setIsLoggedIn] = useState(false);

  // TODO: refactor this when logged in state is received from OBS
  useEffect(() => {
    axios('https://reqres.in/api/users?page=2').then(() => {
      setIsLoggedIn(true);
    });
  }, []);
  // TODO: remove loggedIn when testing is done
  return (
    <Layout layout={layout} isLoggedIn={IsLoggedIn} useSWR={useSWR}>
      <IntlProvider
        messages={messages}
        locale={language.label}
        defaultLocale={DEFAULT_LOCALE_LABEL}
      >
        <UseCase1 />
      </IntlProvider>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const req = await axios('https://reqres.in/api/users?page=2');
  const layout = await getLayoutData(contentStack, locale);
  const { messages, language } = await fetchTranslations();

  return {
    props: { userData: req.data.data, layout, messages, language },
  };
};

export default Page1;
