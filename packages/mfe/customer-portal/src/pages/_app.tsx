import React from 'react';
import Head from 'next/head';
import StyledTheme from 'utils/StyledTheme';
import useSWR from 'swr';
import '@fontsource/open-sans';
import('utils/newRelic');

function App({ Component, pageProps }) {
  return (
    <StyledTheme>
      <Head>
        <title>Welcome to Raisin</title>
        <meta name="description" content="header of customer portal" />
        <script type="text/javascript" src="/static/vendor/newrelic.js"></script>
      </Head>
      <Component {...pageProps} useSWR={useSWR} />
    </StyledTheme>
  );
}

export default App;
