import React from 'react';
import { NextPage, GetStaticProps } from 'next';
import Link from 'next/link';
import { ILayout, getLayoutData, Layout } from '@raisin/cp-layout';
import { getCMSEntriesForContentType, IContentStackSecrets } from 'src/api/contentStack';
import { Container } from '@material-ui/core';

declare const contentStack: IContentStackSecrets;
declare const locale: string;

interface IHome {
  layout: ILayout['layout'];
  urls: string[];
  useSWR: Function;
}

const Home: NextPage<IHome> = ({ urls, layout, useSWR }) => (
  <Layout layout={layout} useSWR={useSWR}>
    <Container maxWidth="xl">
      <h2>Welcome to your new Customer Portal</h2>
      <div>
        <Link href="/page1" passHref>
          <a href="replace">Link to SSG page</a>
        </Link>
      </div>
      <div>
        <Link href="/page2" passHref>
          <a href="replace">Link to Page that requests data at render time on client side</a>
        </Link>
      </div>
      <div>
        <h3>Auto generated routes from CMS Pages</h3>
        {urls.map((url) => (
          <div key={url}>
            <Link href={url} passHref>
              {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
              <a href="replace">Link to {url}</a>
            </Link>
          </div>
        ))}
      </div>
    </Container>
  </Layout>
);

export const getStaticProps: GetStaticProps = async () => {
  const allPages = await getCMSEntriesForContentType('pages');
  const layout = await getLayoutData(contentStack, locale);
  const urls = allPages?.map(({ url }) => url) ?? [];

  return {
    props: {
      layout,
      urls,
    },
  };
};

export default Home;
