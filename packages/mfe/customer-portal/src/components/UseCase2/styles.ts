import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: center;
`;

export const H1 = styled.h1`
  font-size: 1.5em;
  color: #000;
`;

export const InlineText = styled.span`
  font-size: 1.5em;
  color: #c1c1c1;
  text-decoration: underline;
`;

export const Button = styled.button`
  width: 200px;
  height: 40px;
  background-color: #ec5512;
  border: 1px solid #ec5512;
  padding: 12px 20px;
  color: #ffffff;
`;
