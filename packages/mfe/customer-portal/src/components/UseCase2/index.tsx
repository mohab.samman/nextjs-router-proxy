import React, { useState, useEffect } from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import axios from 'axios';
import * as S from './styles';
import messages from './messages';

/**
 *
 * Hybrid SSG page sample
 */
const UseCase2: React.FC<WrappedComponentProps> = ({ intl }) => {
  const [userData, setUserData] = useState([]);
  useEffect(() => {
    axios('https://reqres.in/api/users?page=2').then((json) => {
      setUserData(json.data.data);
    });
  }, []);

  type User = {
    id: number;
    email: string;
  };

  const RemoteDataComponent = () => (
    <>
      {userData.map((item: User, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={i}>
          <h4>
            <span>{item.id}</span>
            {item.email}
          </h4>
        </div>
      ))}
    </>
  );

  return (
    <S.Wrapper>
      <S.H1 data-cy="app-title">{intl.formatMessage(messages.AppTitleExample)}</S.H1>
      {userData && <RemoteDataComponent />}
    </S.Wrapper>
  );
};

export default injectIntl(UseCase2);
