import { defineMessages } from 'react-intl';

const messages = defineMessages({
  AppTitleExample: {
    id: 'UseCase2.title.example',
    defaultMessage: 'I am a hybrid page',
  },
  CTALabel: {
    id: 'UseCase2.cta.label',
    defaultMessage: 'Click me!',
  },
});

export default messages;
