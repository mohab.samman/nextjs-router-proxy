import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';

export const Wrapper = styled.div`
  width: 100%;
  height: 1200px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  box-sizing: border-box;
  text-align: center;
  background: url(https://banking.weltsparen.de/savingglobal/assets/img/plp_gradient_bg.svg)
    no-repeat 50% / cover;
  font-family: Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
`;

export const Logo = withTheme(styled.div`
  position: absolute;
  right: 120px;
  bottom: 0px;

  ${({ theme }) => theme.breakpoints.down('sm')} {
    right: 10px;
  }
`);

export const H1 = withTheme(styled.h1`
  font-size: 5em;
  font-weight: 600;
  color: #ffffff;

  ${({ theme }) => theme.breakpoints.down('sm')} {
    font-size: 2.8em;
  }
`);

export const InlineText = withTheme(styled.span`
  font-size: 2.5em;
  color: #ffffff;

  ${({ theme }) => theme.breakpoints.down('sm')} {
    font-size: 1.8em;
  }
`);
