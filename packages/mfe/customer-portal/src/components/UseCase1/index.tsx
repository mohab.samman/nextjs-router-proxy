import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';

import Logo from './Logo';
import * as S from './styles';
import messages from './messages';
/**
 *
 * SSG without data fetching
 *
 */

interface IUseCase1 extends WrappedComponentProps {
  subHeading?: string;
}

const UseCase1: React.FC<IUseCase1> = ({ intl, subHeading }) => (
  <>
    <S.Wrapper>
      <S.Logo>
        <Logo width={300} height={300} />
      </S.Logo>
      <S.H1 data-cy="app-title">{intl.formatMessage(messages.PageTitle)}</S.H1>
      <S.InlineText data-testid="home-text">{subHeading}</S.InlineText>
    </S.Wrapper>
  </>
);

export default injectIntl(UseCase1);
