import { defineMessages } from 'react-intl';

const messages = defineMessages({
  PageTitle: {
    id: 'Home.title',
    defaultMessage: 'Customer Portal',
    description: 'name of the µFE on home page',
  },
  PageDescription: {
    id: 'Home.desc',
    defaultMessage: 'Coming Soon',
    description: 'description on home page',
  },
});

export default messages;
