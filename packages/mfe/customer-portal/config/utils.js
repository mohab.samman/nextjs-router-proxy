const { AWS_APP_ID } = process.env;

const getEnvironment = (awsBranch) => {
  const environment = {
    staging: 'staging',
    onboarding: 'onboarding',
    production: 'production',
  };

  if (environment[awsBranch]) {
    return awsBranch;
  }

  return AWS_APP_ID ? 'development' : 'local';
};

module.exports = {
  getEnvironment,
};
