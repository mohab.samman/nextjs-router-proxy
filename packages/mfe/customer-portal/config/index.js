// Please read more about how to use this file [here](https://raisin-jira.atlassian.net/wiki/spaces/OA/pages/1096286943/Technology+Framework+web+portals)
const utils = require('./utils');
require('dotenv').config();

/**
 * ENV Variables
 * These variables are comming from the deployment pipeline
 * They provide different values and based on this, different environments are deployed.
 * These variables have no values when running the project locally
 */
const {
  APP_SLUG,
  AWS_BRANCH,
  AWS_APP_ID,
  APP_NAME,
  APP_LOCALE,
  PORT,
  HOST,
  MF_NAME,
  STACK_API_KEY,
  STACK_DELIVERY_TOKEN,
  STACK_ENVIRONMENT,
} = process.env;

/**
 * Application Variables
 * These variables are used to run the project locally and in the GitLab pipeline.
 * The right hand side values are required and only used when running the project locally
 */
const NAME = APP_NAME || 'customer-portal';
const WMF_NAME = MF_NAME || 'customer-portal';
const APP_PORT = PORT || 3000;
const APP_HOST = HOST || 'localhost';
const ENVIRONMENT = utils.getEnvironment(AWS_BRANCH);
const LOCALE = APP_LOCALE || 'en-US';

/**
 * Environments
 * There are 5 environments where the application could run:
 * Staging, Onboarding, Production, Development and local.
 * For each environment there are different values, urls, endpoints and domains.
 */
const environments = {
  local: {
    publicPath: '/',

    newRelic: {
      domains: [],
      accountID: '2240158',
      agentID: '179111133',
      applicationID: '179111133',
      licenseKey: '2d067173e5',
      trustKey: '2239786',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },

  development: {
    publicPath: `https://${APP_SLUG}.${AWS_APP_ID}.amplifyapp.com/`,

    newRelic: {
      domains: [],
      accountID: '2240158',
      agentID: '179111133',
      applicationID: '179111133',
      licenseKey: '2d067173e5',
      trustKey: '2239786',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },

  staging: {
    publicPath: `https://mfe-${WMF_NAME}.testraisin.com/`,

    newRelic: {
      domains: [],
      accountID: '2240165',
      agentID: '179103405',
      applicationID: '179103405',
      licenseKey: 'cb9df53742',
      trustKey: '2239786',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },

  onboarding: {
    publicPath: `https://mfe-${WMF_NAME}.onboarding-raisin.com/`,

    newRelic: {
      domains: [],
      accountID: '2726188',
      agentID: '179113543',
      applicationID: '179113543',
      licenseKey: 'NRJS-223266643c1db92f198',
      trustKey: '2231222',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },

  production: {
    publicPath: `https://mfe-${WMF_NAME}.raisin.com/`,
    locale: LOCALE,

    newRelic: {
      domains: [],
      accountID: '2231222',
      agentID: '179112700',
      applicationID: '179112700',
      licenseKey: '42874e9287',
      trustKey: '2231222',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
};

/**
 * Configuration
 * The config object is used internally for the project.
 * The object has the  following section:
 * name: µFE name
 * port: On what port should the project run locally
 * host: URL for the application, localhost for development,
 * moduleFederation: Object for the Webpack module federation plugin,
 * globals: Object used for injecting global values into the application.
 * publicPath: Path for the Webpack output file.
 */
const config = {
  name: NAME,
  port: APP_PORT,
  host: APP_HOST,

  moduleFederation: {
    name: WMF_NAME,
    exposes: {},
    remotes: {},
  },

  globals: {
    newRelicConfig: environments[ENVIRONMENT].newRelic,
    contentStack: environments[ENVIRONMENT].contentStack,
    locale: LOCALE,
  },

  publicPath: environments[ENVIRONMENT].publicPath,
};

module.exports.config = config;
