const { resolve } = require('path');
const customConfig = require('./config').config;

module.exports = {
  future: {
    webpack5: true,
  },
  rewrites: async () => ({
    fallback: [
      {
        source: '/:path*',
        destination: 'https://staging.weltsparen.de/:path*',
      },
    ],
  }),
  webpack: (config, { isServer, webpack }) => {
    // Fixes npm packages that depend on `fs` module
    if (!isServer) {
      config.resolve = {
        ...config.resolve,
        fallback: {
          ...config.resolve.fallback,
          fs: false,
        },
      };
    }
    config.plugins.push(
      new webpack.DefinePlugin(
        Object.keys(customConfig.globals).reduce(
          (globals, key) => ({ ...globals, [key]: JSON.stringify(customConfig.globals[key]) }),
          {},
        ),
      ),
    );
    config.module.rules.push({
      test: /\.(png|gif|jp(e*)g|svg)$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 8 * 1024,
            name: 'assets/images/[name]-[contenthash].[ext]',
          },
        },
      ],
      include: [resolve(process.cwd(), 'src/assets/images')],
      exclude: [resolve(process.cwd(), 'src/assets/fonts')],
    });

    return config;
  },
};
