# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.18.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.17.4...@raisin/customer-portal@1.18.0) (2021-07-27)


### Bug Fixes

* **header:** various markdown and style improvements ([92d538d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/92d538d99507c272218ab8ccb251f682d87c8921))
* **mobileHeader:** Add mobile icons inline ([4b64601](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4b646013bdc9d745cd65a05c808ac1ebe9699034))


### Features

* **Cp-layout:** Adding header component ([699ae11](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/699ae11c46add48ece69d26cd7e3abd7238ca4c6))
* **Cp-layout:** Adding translations and images ([37be883](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/37be8832a37f84fd490a16c537f2ed802c3d1005))
* **CustomerPortal:** Adding Header component ([12366b0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/12366b09b2126ebf291724bfd6a12b840d6eb34c))
* **CustomerPortal:** Adding menu icons and links to logos ([9289462](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/92894627741ebdf3457c80cee0abc55ac03fee09))
* **CustomerPortal:** Adding Todos comment blocks to component ([706221b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/706221bb0a7adab54100c24fbea57a1583942aa9))
* **CustomerPortal:** Changing mobile header menu component ([851af47](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/851af47a27f6fa9b08929186563891aae536fe96))
* **CustomerPortal:** Deleting irrelevent files ([745ee7a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/745ee7ab773b851b9b29f92d204d498a7a0685af))
* **CustomerPortal:** Handling logo on mobile device and adding snapshot tests ([b19f145](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b19f1455cb5faf5ecd3330c551811b2abadc348a))
* **CustomerPortal:** Modifying icons ([3831c28](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3831c2893d2d516582d16aacf8f5f498e5f61634))
* **CustomerPortal:** Modifying icons ([689fb5a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/689fb5ace6d0be4717426f49ff457f2efe89a242))
* **CustomerPortal:** Removing unnecessary translations ([c8bc0b3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c8bc0b3bee6be203e3c50efd79b0277e754f0a68))
* **CustomerPortal:** Renaming desktopHeader => DesktopHeader ([ac17faa](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ac17faa564c78286250cd20da56186be045304d1))
* **CustomerPortal:** Replacing injectIntl with useIntl hook, fixing typos, adding more test cases and correcting some UI elements ([2ea98a2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2ea98a2e6b0e8d9c1ba56ae1f93e4798b1ce7454))
* **footer:** Add basic main header navigation ([86a7829](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/86a78295e0870d4363e8452ecf17c3fe8e311f2d))
* **header:** make it responsive ([47ade11](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/47ade1134c0118e252969e77e5930ddbb6e72fdc))
* **Header:** store customer data in local storage ([cd804dc](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cd804dcd4d1b10e02fa480d9c68fba78447c5eca))





## [1.17.4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.17.3...@raisin/customer-portal@1.17.4) (2021-07-27)

**Note:** Version bump only for package @raisin/customer-portal





## [1.17.3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.17.2...@raisin/customer-portal@1.17.3) (2021-07-23)

**Note:** Version bump only for package @raisin/customer-portal





## [1.17.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.17.1...@raisin/customer-portal@1.17.2) (2021-07-22)

**Note:** Version bump only for package @raisin/customer-portal





## [1.17.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.17.0...@raisin/customer-portal@1.17.1) (2021-07-09)


### Bug Fixes

* **cp:** Fix FOUC bug ([23f6c96](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/23f6c964f0adf5dd9577dc50ca55edeb8b5d41b1))
* **CustomerPortal:** Fixing flickering issue ([055d9f3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/055d9f32747a9ebc639a390f543789b516a0e52f))
* **packages:** various improvements ([45b8a6e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/45b8a6ea6b2f7d80c8b021e73713b3dde9017e18))





# [1.17.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.16.0...@raisin/customer-portal@1.17.0) (2021-06-29)


### Features

* CA-1952 clean up CP amplify ([db4cf4d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/db4cf4d43bc44fb0e61c657e8f2c88659e1a1fdd))





# [1.16.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.15.2...@raisin/customer-portal@1.16.0) (2021-06-03)


### Features

* **Gitlab and Amplify:** Add dynamic import to the libraries build scripts ([9e4417d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9e4417df5dc283cc916460e05000723197c41fb8))





## [1.15.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.15.1...@raisin/customer-portal@1.15.2) (2021-05-28)


### Bug Fixes

* locale used in trustpilotData fetching and save the data under /tmp ([f373dc4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f373dc45658215a36ec364c4c22b3958a8c4ec8d))
* **customer-portal:** remove double imports ([ea327e9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ea327e9f7affd9462106bf1095dbfe91f8d1bd29))
* **layout:** provide CMS secrets to cp-layout ([841b3c1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/841b3c1123f6f12977f16c3f1232e03c16bfcd66))
* **layout:** Provide locale from consumer project ([49a0b96](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/49a0b96800d337e6d5fbee190f4db545ab2d4c06))
* **layout:** remove assets in CP and fix tests and lint issues ([9dab9c6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9dab9c617eec189555e2b3e2bf398a6dfc1f4a39))
* **layout:** revert aliases from ts-config since they are not transpiler correctly ([5433090](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/543309004ac4250003335ef092e382af2f0964fb))





## [1.15.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.15.0...@raisin/customer-portal@1.15.1) (2021-05-26)

**Note:** Version bump only for package @raisin/customer-portal





# [1.15.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.14.0...@raisin/customer-portal@1.15.0) (2021-05-25)


### Features

* **cp:**  Adding aliases to CloudFront ([1231245](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/123124590d6847c8fcd409202b5ff4323bde1788))





# [1.14.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.13.2...@raisin/customer-portal@1.14.0) (2021-05-25)


### Features

* **gitlab-ci:** Add build script ([60a1954](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/60a195482ea71c4a9fdce3fa91b7f4fbcc09392d))





## [1.13.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.13.1...@raisin/customer-portal@1.13.2) (2021-05-25)

**Note:** Version bump only for package @raisin/customer-portal





## [1.13.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.13.0...@raisin/customer-portal@1.13.1) (2021-05-20)

**Note:** Version bump only for package @raisin/customer-portal





# [1.13.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.12.0...@raisin/customer-portal@1.13.0) (2021-05-19)


### Bug Fixes

* **footer:** Various fixes and improvements ([75e38b6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/75e38b66a2cb33c7ef756490264e66294221b5f6))
* **theme:** Remove theme and obsolete componentfrom CP and update the theme palette with all missing colors ([78099b4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/78099b43f670cb80a6945597e3ac2afd78f15885))


### Features

* **fonts:** Add self hosted open sans font to CP ([9278774](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/927877480ddc4aa902c0e73da13134d895ed8678))





# [1.12.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.11.0...@raisin/customer-portal@1.12.0) (2021-05-19)


### Bug Fixes

* **pages:** support catch-all routes and fix page route match ([283783b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/283783b0a8cab11c19211badf80296c56ab54b53))


### Features

* **footer:** fetch footer by page reference ([d4589df](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d4589df3330958e296e15e61e33cfc71a48597e1))





# [1.11.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.10.0...@raisin/customer-portal@1.11.0) (2021-05-18)


### Bug Fixes

* **Customer Portal:** Removes next build from dev command ([3e90dc2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3e90dc2e98a2a4574b84ebd61e968c53c6b1a3dd))


### Features

* **cp-layout:** Fix intl in cp-layout ([3e8fbaa](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3e8fbaab6a5fcec447926c24ea286b21393d0858))
* **customer portal:** Includes caret symbol to keep cp-layout up to date with minor versions and forcing to always build the application before running the start command ([300555a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/300555ae9fd0c8a2f0a780c1b71e2b16a31ada79))





# [1.10.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.9.0...@raisin/customer-portal@1.10.0) (2021-05-12)


### Bug Fixes

* remove gitignored trustpilotData.json from remote ([37ddd75](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/37ddd75ba6b2c95634699445cc71750a56bb8e9a))
* replace dompurify with isomorphic-dompurify to support purifying on server side ([f3576fb](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f3576fbb6f68c61ac21f537321f120de81f32de5))
* set default locale to de-DE ([a35f0a5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/a35f0a524519ffb8b9550fa91349db49c2ccd98b))
* **JestFileMock:** make it work with next/image component ([9fa4f01](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9fa4f01a67f35cc0f650453650ab4265fa2d3c44))
* **MobileBanner:** add translations in transifex ([4d93def](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4d93defefd29973d1b7e9c52ead96e1f1c4d4a6a))
* **MobileBanner:** remove and replace custom global styles with CSSBaseLink ([c102178](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c10217838ed781a65de6bc4c8b0f481ee38e9610))
* **TrustPilotBar:** add translations in transifex ([05c0b5c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/05c0b5c9bfae03e10a1c7e7a12370b1d66d866ad))
* **TrustPilotBar:** make file import async and test the function ([43af743](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/43af743dc53c9f518d072958be3c451c7f59396f))
* add trustpilot since it was removed during merge conflict ([72ac077](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/72ac07708006daf930259f617a430a157bbee13c))
* address code review discussions ([880c2b2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/880c2b254286f9065080c52d38628d30a54d3e9b))
* css adjustments ([fa48986](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/fa48986837ce7a18d5a4b628b21ae0a3d6706d6a))
* fetch translations inside entry file, and not each file independently ([662035f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/662035f0c68997336f538da60290b6f13a959124))
* issue with filename capitalisation ([eac376b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/eac376b10aea3b145024379660dc87b5b8394682))
* remove a console.log ([834dbef](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/834dbef70ab776eb06157f2be8cb9bd8e0554801))
* remove and replace custom global styles with CSSBaseLink ([7490be8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7490be81959d466f307d58c16260e8d1d6476484))
* remove and replace custom global styles with CSSBaseLink ([bcc837c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/bcc837cb528e8bf784051a9ec3668a57b4a78db0))
* some bugs due to refactorings ([76d4123](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/76d4123a02720c4c14f4310fa909c80729535f60))
* styles and IOS link as well as tests ([1fabf86](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1fabf86e8c87352aa19e201393ec99f46c2ed5b5))
* ui-lib deps ([cf804c8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cf804c85d10431a313702c6e08c81664a5f46366))
* update NL translations ([abe3ce6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/abe3ce67ccad1845257dd4c1ef0a242600990ca9))
* update snapshots ([525a811](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/525a8118ca9ef4a2c1f6076451215cfdb7183403))
* update snapshots ([31de228](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/31de22832c59f29f00eb118584410d1d5ac81346))
* use alias for path to footer ([3f62ad2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3f62ad2ff82c5ccc808de6419158bd9531a92126))


### Features

* **MobileBanner:** Use image from next.js ([3546c30](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3546c309e0d46f0b0cba5d39c6e2383cd3ab9cdb))
* **MobileBanner:** Use image from next.js ([f0a5146](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f0a51465c679ce111666dc8b0acb393a0aaf4b12))
* Add translations for various langauges ([678a1c6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/678a1c60c6c658b24e84de5149c707078d17d938))
* display trustpilot section based on CMS flag ([4fc9c7b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4fc9c7b30e7eefef406ba4a863c3d4a161501a9e))
* **customer-portal/footer:** Add and use ContentStack Delivery SDK ([c044e87](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c044e87f2b8302853432f8f072f2ab11e7b1fed8))
* **customer-portal/footer:** Add transifex translations ([c74392c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c74392c2d564a8a0ebb7f7e790f84b4f6d10f823))
* **footer:** Create new component footer with contact details, social media and content links components ([d75ca82](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d75ca8260f334551804fb0603cb35a35efcf4622))
* **Footer:** added the FooterImprint and the FooterDisclaimer to the Footer ([83a6d06](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/83a6d067ddd55524ca352d7fa151a65d6752871c))
* **FooterDisclaimer:** properly outputting the disclaimer text ([25fad41](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/25fad41561f15cd16101f42d62044ce2c8ea344e))
* **FooterImprint:** added a new FooterImprint component for the footer ([f6816eb](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f6816ebe63ae123150bde6bb6ab5bd068768faa1))
* **FooterImprint:** adjusted styling for tablet/mobile ([4dc813d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4dc813d187d0c5486c3f4537580e46fff173f9be))
* **GlobalStyle:** added global styling for the whole customer portal ([0f11e94](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0f11e94a4b59ef1cfd3b9409b4a70a44190f4de4))
* **package.json:** installed a new library - DOMpurify to be able to safely use dangerouslySetHTML ([eb345ef](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/eb345efcf854d70d5049b8d6fdfe06393ecc88d6))
* Add trustpilot section to the footer ([e16d1ce](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e16d1ce73b3a9b6750c89bfced05a4e74e394cb9))





# [1.9.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.8.1...@raisin/customer-portal@1.9.0) (2021-05-07)


### Features

* **Amplify:** Add backend parameters ([7528f12](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7528f127841a3e726c0201bbac76053eccb29ea2))
* **Amplify:** Add template.json ([7c59623](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7c59623ddbeb25a498e2f56fb927c6d0dc316e89))





## [1.8.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.8.0...@raisin/customer-portal@1.8.1) (2021-05-04)

**Note:** Version bump only for package @raisin/customer-portal





# [1.8.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.7.0...@raisin/customer-portal@1.8.0) (2021-04-26)


### Bug Fixes

* **customer portal:** adjusts page type ([2225f67](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2225f6742a86a9f458ff02c4e193eab0edec7b22))
* **customer portal:** Fixes newrelic/webpack/nextjs settings ([f869121](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f8691217800b0234c3b48b7ebffb0faf1296a050))
* **customer portal:** Replaces _document config to get rid of ts issue ([15fb99e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/15fb99ed1fa966d03285d0f2c444a99f2633c83b))
* **Customer Portal:** Adjusts typescript issue ([b70c446](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b70c446ac942b3f2305481054bcd9673cb5f4be8))
* **Customer Portal:** converts file extension to tsx ([34349b8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/34349b8997c7f49acb928588d2e3eb08a61c871b))
* **Customer Portal:** reverts to getInitialProps to get styles injected at build time ([9273e40](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9273e402b5815c757cda147bc77f1f984bdfe932))
* **Customer Portal:** typescript error ([4f24b11](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4f24b114c8d669fcc72c38777f68842a44756f87))
* adjusts eslint issues ([356da97](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/356da97d1eec703016e9155b7863cca69ed5d4c4))
* hybrid page with SSG fetching data onclick ([a2a76c0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/a2a76c015d442ce0d628ea4fba7d2c3eb41d9380))
* ignores dist files for nextjs setup ([fd30dde](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/fd30dde258bef56a910bd2d5121f6475d278efc4))
* jest config for nextjs ([e0c545e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e0c545e435235e70bb67806a3ab5db43aeba22c3))
* lint issues and removes test page - WIP ([6ca61cf](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/6ca61cfbf6ddeccbe41eba6c759f5db2cea26bd5))
* next complains about default anonymous functions causing unnecessary re-rendering ([888d904](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/888d904017a4e65058b96fd0db6a8dec5df719ac))
* Nextjs i18n setup when not running in browser ([96e598b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/96e598bd4e251d34b61d81829f406ee2e2e2397e))
* Nextjs setup for customer portal ([ccf0e67](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ccf0e67196f00816a1d3be749796b1c437580f26))
* removes empty index file ([8cd8ec6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8cd8ec6b60c41de76db984d9056065e9657e12b2))
* Removes test from pages as nextjs considers it route of application and throws error ([f02f375](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f02f375019dd35e4dd58f74ff45f91df3b89c795))
* runs eslint only in src folder ([5166700](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/516670042573ffa1781a6e86e84a1ff3a6eb8cb6))


### Features

* **Customer Portal:** Adds new relic script to all pages ([29625a6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/29625a6831220baf0a249a4320ec697b94749fde))
* **Customer Portal:** fetches translations from transifex to test i18n with SSG ([abd12c7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/abd12c71cbabd99d8f44377afd4f2bea562b5b35))
* **Customer Portal:** prefetching translations for page1 sample ([47be7ed](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/47be7edfddadb7ea040c2e365f5da231cf59b75a))
* **Customer Portal:** WIP - Injects html lang attr from custom document ([534e8ba](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/534e8ba339fdae0aa3c1d0d03c7fa2367bb56f82))
* **customerPortal:** Adding demo test.tsx ([9f18738](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9f18738e637ed0ae388a7b32f57084e682fb4e1e))
* **customerPortal:** Adding eslint rule for Link component ([9644690](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/96446902056c887f4363f4b7221a2ebe7218a3be))
* **customerPortal:** Adding next.js ([621a5b5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/621a5b512316b2d0b1935489817ed9124b96bb30))
* **customerPortal:** Adding next.js to customer portal ([699f3e4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/699f3e42e0c96cd137b958a7973e8adb288a7213))
* **customerPortal:** Adding SWR to next.js ([a6935fe](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/a6935fed5ed5071e605c34c99749ecd2a3346184))
* **customerPortal:** Adding types to index.tsx ([0c258b3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0c258b367915dedfb369a859f3f06b8b46561a77))
* **customerPortal:** fetching remote data in newapp page ([ff601e3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ff601e3e7831ab53e66b42cab56b54b28d31a7b6))
* **customerPortal:** fetching remote data in newapp page ([ae70bab](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ae70babdb9243821f135db80ddc7e7c3e892746d))
* **customerPortal:** fixing eslint ([4f2a05d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4f2a05d8c0b4cb0352d95148b8f42a7105b84a26))
* **customerPortal:** fixing styled components ([75a253e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/75a253e91f600c90f6a6310b4bf120baa5cf8723))
* **customerPortal:** fixing styled components ([0080748](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0080748ccc804c9e9d3f8f1844b8ba155211cf29))
* **customerPortal:** fixing test cases ([365a462](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/365a46294e712edc18185469606955b0a20f127c))
* **customerPortal:** Modifying output folder ([1129773](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/11297739fea92f88aafdff9b35a3a59919c7f040))
* **customerPortal:** removing eslint rule for Link component ([11d9688](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/11d9688bf67185626ddcbd9b0a6bdb48505e6255))
* **customerPortal:** removing routes folder ([e479702](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e47970250fd9956dc401efa7123cd9643fb981de))
* **CustomerPortal:** Fixing cors and translations issues for customer portal ([3de9e64](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3de9e64c8f2e769c67aedad4cd2bcb564ab87343))
* **CustomerPortal:** Fixing cors and translations issues for customer portal ([1a74141](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1a74141bff9dd8088c7a43180b4b286f84528558))
* **CustomerPortal:** Fixing translations ([754ea2a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/754ea2aa4306c6389de6fa1e73764ee7b6fa1d37))
* Adds a page to simulate ssg without data fetching ([0b8ebd5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0b8ebd536c40e71ddec695a4ab3470812bd1e482))
* usecase two WIP ([7c2e0df](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7c2e0df59652bde9e761aa1736a8a492ad05b537))





# [1.7.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.6.0...@raisin/customer-portal@1.7.0) (2021-04-26)


### Bug Fixes

* Global-ui-lib cann be consumed in other porjects ([fa534a3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/fa534a3480ff968096eed5b67a994bdf88cad3d4))
* remove global-ui-lib code used for testing ([786b8e5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/786b8e5a407e7f3f707960cc59d60c7eb5e3df47))
* remove obsolete packages ([ffdc00e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ffdc00e5318b189c806e434d1c2b8c748e4b9189))
* test global-ui-lib in customer portal ([9e99c47](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9e99c471ce15b44514ad13fcf95aabc758d8cd48))


### Features

* test global-ui-lib in customer portal ([d721888](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d7218880e418bf84ba96de1a5c894bc0ab293942))





# [1.6.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.5.0...@raisin/customer-portal@1.6.0) (2021-04-22)


### Features

* make home page a bit nicer ([c9d91a0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c9d91a06fc47bc45912dee18398d4f2d51cd25ab))





# [1.5.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.4.0...@raisin/customer-portal@1.5.0) (2021-04-21)


### Features

* add MFEPorts file so that the µFE generator can dynamically create local ports for new µFE ([54e12aa](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/54e12aa4a205e813aa81f486f3c2ff08cb1db5b0))





# [1.4.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.3.2...@raisin/customer-portal@1.4.0) (2021-04-15)


### Bug Fixes

* **customer-portal:** Fix ESLint error in config/utils.js file ([c365853](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c36585381079d09d8b803a0088135ee60e62afde))
* **customer-portal:** Update config NewRelic values ([6a9defd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/6a9defde4447e4d69934216d5c347f1aadeba606))


### Features

* **customer-portal:** Add newRelic config to project ([b3a69c2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b3a69c2b48c1c2b64cabac19acc6f316af440a3b))





## [1.3.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.3.1...@raisin/customer-portal@1.3.2) (2021-04-13)


### Bug Fixes

* add some babel presets  for testingn ([3ee30da](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3ee30da6b66cb4ba2048dd1a0d401d7e1a71e807))
* replace core-js and regenerator-runtime with react-app-polyfill and add polyfill to prooduction build as well ([ed6a06f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/ed6a06fb8fd4ac7ec12ad8c82b3034c683b3809e))





## [1.3.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.3.0...@raisin/customer-portal@1.3.1) (2021-04-13)


### Bug Fixes

* Use correct port in lighthouse config and remove TODO ([008d209](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/008d209ff1650dced54b6e6355d14abcdfda3606))





# [1.3.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.2.0...@raisin/customer-portal@1.3.0) (2021-04-12)


### Features

* adds react-intl basic setup to customer portal ([dd21f67](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/dd21f67358d8855da2dff0213e038b6f19b0b764))





# [1.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/customer-portal@1.1.0...@raisin/customer-portal@1.2.0) (2021-04-12)


### Features

* **CP:** added cypress configuration ([4aacec0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4aacec01f5d8914926527eb476b47c5a894cad7d))
* **CP:** added yarn scripts for cypress - open and run ([1f175c7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1f175c7f35eeaacbe19be84db7deae0952d12eda))
* **CP:** generated all necessary cypress related files and folders after running cypress open ([498b2b3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/498b2b3f41334db363c01f33ccf0872a72622a11))
* **CP:** install mochawesome reporter ([b757a90](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b757a90e6f1240f6f3a3afaad8f2a2b6b7657b05))
* **CP:** installing cypress ([33a5c35](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/33a5c35de739468e152af3f97cb09b7d9380bec8))





# 1.1.0 (2021-04-09)


### Bug Fixes

* **Ci Pipeline:** Attempt to fix the pipeline ([6910576](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/691057613762d58b14d3ba1bd33c406dde9a834a))
* **customerPortal:** fixing eslint ([db8707d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/db8707d1fca0ee9e96d24a6ac3c23da8ff9eeef2))
* **customerPortal:** fixing eslint ([3cb0c72](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3cb0c721608d052060973ca091982b63ed94b91f))
* **customerPortal:** fixing eslint ([d3294d1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d3294d1d3559c92715f84499d010eb4c96824ff2))
* **customerPortal:** fixing eslint ([6ef40bf](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/6ef40bfd18ccc721eb997e639cbe047d2fe40999))
* **customerPortal:** fixing eslint ([0fa5714](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0fa571444006a72b81e3f4125fe511e9745ac156))
* **customerPortal:** fixing eslint ([33d07b3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/33d07b3beaa9e6d49bfcf00f9450a0cd159f50fb))
* **customerPortal:** fixing eslint ([46bf25f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/46bf25f68cf669231b468fd96c60576198cb95f3))
* **customerPortal:** fixing eslint ([0103f54](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0103f5421ef63b8bda6c1f9bfbd15e9df109322d))
* **customerPortal:** fixing eslint ([32bcaa3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/32bcaa3c8b849331f3fc29a8d4685b0e42eac91c))
* **customerPortal:** fixing eslint ([821a337](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/821a337508e2decba16870422534776302a4d032))
* **customerPortal:** fixing pipeline issue ([8b8ca61](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8b8ca61d5142f606f60a6caee8e4dbfe34105839))
* **customerPortal:** fixing pipeline issue ([c9fa495](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/c9fa495366ca14465f75bce33e3c16ffc7b564e4))
* **customerPortal:** Modifying .gitlab-ci.yml file ([214c22b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/214c22b25d807e0b512366eef5e7439830983ceb))
* import jest-dom in the environment setup file instead of adding it in each test file ([b72c2b9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b72c2b9856757f237bce4fc4bcb8854bb0ddc4b5))
* remove dotennv ([8ba1c94](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8ba1c941d9b7b1ff8bf0fe776ecbd73354c3562a))
* remove dotennv ([b692006](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b69200601fcfe7c07283c71837b02ab92c3da793))
* remove raisin eslint ([1ba5453](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1ba54530763a284c3142e707222fa3ec610d141b))
* revert eslint from running with npx ([d705f39](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d705f3981dcf590b1abfa936fb2057c1a4eaff00))
* run eslint with npx ([a231904](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/a2319042033dd6a41d3101d9b1b05ba486e23813))
* unit test issues ([43a944d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/43a944d4c247ee34a0b1b4e48d37de4d0efbaa4d))


### Features

* Generate customer portal and add start script ([4dee154](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4dee154ac6e9133ff61dbf9b46a828658a21a090))
