const { config } = require('./config');

module.exports = {
  verbose: true,

  globals: config.globals,

  transform: {
    '^.+\\.js$': 'babel-jest',
    '^.+\\.(css|scss|less)$': 'jest-css-modules',
  },

  moduleFileExtensions: ['ts', 'tsx', 'js'],

  testURL: 'http://localhost',

  testMatch: ['**/__tests__/**/*.js?(x)', '**/?(*.)(spec|test).js?(x)'],

  testPathIgnorePatterns: ['/node_modules/', '/cypress/', '.cache/Cypress'],

  setupFiles: ['raf/polyfill'],

  setupFilesAfterEnv: ['./jest/setupJest.js'],

  modulePaths: ['<rootDir>/src', '<rootDir>/node_modules'],

  transformIgnorePatterns: ['/node_modules/(?!lodash|@raisin|react-intl)'],

  moduleDirectories: ['node_modules', 'src', 'utils'],

  moduleNameMapper: {
    '^.+\\.(jpg|jpeg|png|gif|eot|webp|svg|ttf|woff|woff2)$': '<rootDir>/jest/fileMock.js',
    '^.+\\.(css|scss)$': 'identity-obj-proxy',
    '^@ui-lib/src/(.*)$': '<rootDir>/node_modules/@raisin/ui-lib/src/$1',
    '^@ui-lib/__mocks__/(.*)$': '<rootDir>/node_modules/@raisin/ui-lib/__mocks__/$1',
    '^jest/(.*)$': '<rootDir>/jest/$1',
    '^auth/(.*)': '<rootDir>/__mocks__/Auth/$1',
    '^cap/(.*)': '<rootDir>/__mocks__/Cap/$1',
  },

  coverageReporters: ['html', 'text-summary', 'lcov'],

  collectCoverage: true,

  collectCoverageFrom: ['<rootDir>/src/**/*.js', '!<rootDir>/src/styles/**/*.js'],

  watchPathIgnorePatterns: ['node_modules'],
};
