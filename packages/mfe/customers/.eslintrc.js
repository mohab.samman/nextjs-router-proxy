const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(fs.readFileSync(path.resolve(__dirname, '.prettierrc'), 'utf8'));

module.exports = {
  // parser: 'babel-eslint', // commented for now due to https://github.com/babel/babel-eslint/issues/530#issuecomment-385774262
  root: true,
  env: {
    jest: true,
    browser: true,
    node: true,
    es6: true,
  },

  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'airbnb',
    'prettier',
    'plugin:package-json/recommended',
  ],

  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },

  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2020,
    sourceType: 'module',
  },

  plugins: ['react', 'prettier', 'react-hooks', 'jsx-a11y', 'package-json'],

  rules: {
    curly: ['error', 'all'],
    eqeqeq: ['error', 'always'],
    indent: ['error', 2],
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    'dot-location': ['error', 'property'],
    'import/no-extraneous-dependencies': 0,
    'linebreak-style': ['error', 'unix'],
    'no-console': ['error', { allow: ['warn', 'error'] }],
    'no-underscore-dangle': 0,
    'import/no-unresolved': [0, { caseSensitive: false }],
    'react/jsx-filename-extension': 0,
    'react/jsx-indent': [2, 2, { checkAttributes: true, indentLogicalExpressions: true }],
    'react/jsx-props-no-spreading': 0,
    'react/jsx-uses-react': 0,
    'react/jsx-wrap-multilines': 0,
    'react/react-in-jsx-scope': 0,
    'prettier/prettier': ['error', prettierOptions],
    'import/order': [
      'error',
      {
        groups: [['builtin', 'external']],
        'newlines-between': 'always',
      },
    ],
  },
};
