# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.20.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.19.0...@raisin/cap-customers@1.20.0) (2021-06-18)


### Features

* **Cypress:** Customer details tests ([7e0727d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/7e0727d7090c622fb1ec082d6b70e9239d39519c))





# [1.19.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.18.0...@raisin/cap-customers@1.19.0) (2021-06-09)


### Features

* **Lint:** fixed lint issue ([f16ab23](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f16ab2321b686b815a6ebf76dc323c72e57d5110))
* **Test:** fixed broken unit tests ([05cc72c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/05cc72ccae19f8122e5b5613055fda6a027005ff))
* **UnderConstruction:** added CAP under construction view ([aedf826](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/aedf8269d8f1eb417b8d92c1280968046b13c002))





# [1.18.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.17.0...@raisin/cap-customers@1.18.0) (2021-06-07)


### Features

* **Scroll:** added comment for top height ([431c493](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/431c4936189dd7d44c17eedb4d025e8d93aa3fec))
* **Tests:** fixed scroll test ([4a534b1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4a534b169e54ad81be0ffa30b507eb1670590fb8))
* **VerticalTabs:** scroll for vertical tabs container ([fc5c301](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fc5c30128ddafd4c117eb206ec6c8bae8cdd8934))





# [1.17.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.16.0...@raisin/cap-customers@1.17.0) (2021-06-03)


### Features

* **Gitlab and Amplify:** Add dynamic import to the libraries build scripts ([9e4417d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9e4417df5dc283cc916460e05000723197c41fb8))





# [1.16.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.15.0...@raisin/cap-customers@1.16.0) (2021-06-02)


### Features

* **UX:** ux enhancements ([2845b82](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/2845b82be2dad063f5a8777d77e48953a6bad9b7))





# [1.15.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.14.0...@raisin/cap-customers@1.15.0) (2021-06-01)


### Features

* **Lint:** fixed lint errors ([0704104](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/0704104e9c708630ac9b5c8fc80e6d1d0a8a70d8))
* **RightPanel:** fixed right panel collapse style issue ([c90d855](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c90d8552fb93769aa8d5472d5209b699516cbf96))





# [1.14.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.13.0...@raisin/cap-customers@1.14.0) (2021-05-26)


### Features

* **CSS:** fixed input text field layout ([3fde427](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3fde4273bfef17763f5a43d24f0265bb47f50d7e))
* **CustomerData:** customer address details and login details ([d84b15d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d84b15d47f9577ac30a3e0268d763481df5f7b22))
* **customerMasterData:** master data view for customer details and address data ([85ef49d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/85ef49daba26900515b3b1f5c641474c52cba8e9))
* **Customers:** added test cases for customers view ([088d3cd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/088d3cdb515ca42c4968c4478167f30d6a6fff80))
* **Customers:** fixed customers to persons term ([792d13c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/792d13c254e7d4753b46b40079c07dc200324071))
* **Masterdata:** cleaned code ([81d2418](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/81d241846c3cb16e80f88ddda70a6bc83166c79a))
* **ScrollableTabs:** fixed lint error ([056f041](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/056f041e0909cc6f998fb980ed3388131576f7ed))
* **ScrollableTabs:** implemented clean solution for scrollable behaviour ([e4321ea](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e4321ead66f26a01cfe2efcf46d9be8de9596ffd))
* **ScrollableTabs:** poc for scrollable tabs in cap customers ([263db4a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/263db4acee793f5fc3a666ea9724f0fe820cc76f))
* **ScrollTab:** code cleanup ([26df32d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/26df32dfb5a9ae3f881e1ebb3ec2a29e081379bf))
* **Translations:** Added translations for customers details and fixed langugae switch issue ([d116dd5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d116dd53f99f7c3ddeada8678b8a8056960b3884))





# [1.13.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.12.0...@raisin/cap-customers@1.13.0) (2021-05-17)


### Features

* **Refactor:** cleaned up unused styled-components for cap ([f592c8c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f592c8c1420d1bb6825074e162deaab9a3f9fee6))





# [1.12.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.11.0...@raisin/cap-customers@1.12.0) (2021-05-12)


### Features

* **Breadcrumbs:** implemented breadcrumbs with typography ([e1445e1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e1445e1142fe709fdaaaad61ef0a34615379d2c2))
* **Cleanup:** cleaned unused variables and styles ([fa4d47b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fa4d47b4ff7361501597d1140aa48322aadeb991))
* **Customers:** fixed customer MFE with tabs and vertical navigation ([61ce532](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/61ce53282957b26f3b62b929e75ab25b0ba78803))
* **Customers:** fixed styles for edit icons and container shadow ([a7d2983](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a7d29837ad01a558e5df44594111fab61d698126))
* **Enhancements:** addressed code review comments ([e77d55e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e77d55e4e23273bbae9cc042a9288742345fb927))
* **Fonts:** added Inter as a google font and fixed style issues on list items drawer ([b71829c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b71829c1ed53dc7f005d7db13dc585bf549205d1))
* **Navigation:** fixed nested routes and theme pallete ([3b70470](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3b7047069f9785935f39e1f7449a2e474558abb5))
* **PropTypes:** fixed customer data proptypes ([0cb063b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/0cb063b0e952e5ec95061a3e60dab6962d268dc0))
* **Styles:** fixed container full width for customers ([8ba4733](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/8ba4733a17ec8646f3944e4d66cfa27b26d02dfb))
* **Tests:** skipped customer details tests for now ([1e4daf2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1e4daf21033236f4345b2a26ad3f85034b3488e8))
* **Translations:** DE translations fixed for cap and customers ([fa39ade](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fa39adeee8ee7eebc846d2f91e1e0cb2a35ee2fe))
* **Translations:** migrated customers mfe translations to messages file ([bd62d2c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/bd62d2c0915ef377f83d3db470805303291dceee))
* **Translations:** shared translations from customers mfe to cap portal ([79257f0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/79257f0cdce555e78b4556b012cb1a4f25aeb66d))
* **Translations:** updated translations with transifex ([7949797](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/79497976ee7462e160b0c273867ce1db6b1c73ea))
* **UX:** fixed ux review comments for search customers ([ce2530b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/ce2530bf0b185dd6a7e0263ad0fbe7577c7614c9))
* **UX:** more fixes related to elavation and border radius ([21b2767](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/21b27675a77e9dad955fc386c1a679cc9e677648))





# [1.11.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.10.6...@raisin/cap-customers@1.11.0) (2021-04-21)


### Features

* **Transifex:** updated parent project to transidex resources ([fba6679](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fba66792f013dfd50f0971a3ff95b644204b7a4b))
* **Transifex:** updated translations for cap mfe ([1d86552](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1d865521af50ff1e418e11d0ee1632d9935f855e))





## [1.10.6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.10.5...@raisin/cap-customers@1.10.6) (2021-03-31)

**Note:** Version bump only for package @raisin/cap-customers





## [1.10.5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.10.4...@raisin/cap-customers@1.10.5) (2021-03-31)

**Note:** Version bump only for package @raisin/cap-customers





## [1.10.4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.10.3...@raisin/cap-customers@1.10.4) (2021-03-31)

**Note:** Version bump only for package @raisin/cap-customers





## [1.10.3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.10.2...@raisin/cap-customers@1.10.3) (2021-03-30)


### Bug Fixes

* **utils:** Fix logic about how to know if it is GBR or DEU based on the domain ([4ac627d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4ac627d84fa52c7955c2de7aa0e698f895c3beca))





## [1.10.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.10.1...@raisin/cap-customers@1.10.2) (2021-03-29)

**Note:** Version bump only for package @raisin/cap-customers





## [1.10.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.10.0...@raisin/cap-customers@1.10.1) (2021-03-26)

**Note:** Version bump only for package @raisin/cap-customers





# [1.10.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.9.0...@raisin/cap-customers@1.10.0) (2021-03-26)


### Features

* **CAS:** Change endpoints for production envs ([f67ce8f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f67ce8fc802209c48cd501e3bf5e748bf64f9011))





# [1.9.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.8.4...@raisin/cap-customers@1.9.0) (2021-03-26)


### Features

* **CAP:** Modifying staging pool id and dev namespace ([c6080ae](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c6080aef3846b3861717eda8ea9cd84347326350))
* **CAP:** Removing JSON.Stringify() and updating webpack accordingly ([e697931](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e6979318cf329ab1d23e9320b4a2daaca360236c))





## [1.8.4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.8.3...@raisin/cap-customers@1.8.4) (2021-03-25)

**Note:** Version bump only for package @raisin/cap-customers





## [1.8.3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.8.2...@raisin/cap-customers@1.8.3) (2021-03-25)

**Note:** Version bump only for package @raisin/cap-customers





## [1.8.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.8.1...@raisin/cap-customers@1.8.2) (2021-03-24)

**Note:** Version bump only for package @raisin/cap-customers





## [1.8.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.8.0...@raisin/cap-customers@1.8.1) (2021-03-18)

**Note:** Version bump only for package @raisin/cap-customers





# [1.8.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.7.3...@raisin/cap-customers@1.8.0) (2021-03-05)


### Features

* **customers/interceptors:** Add staging token for api test on CORS issue ([b0f1804](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b0f180442615260e82bae6ffdc085f4e69e13cbd))





## [1.7.3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.7.2...@raisin/cap-customers@1.7.3) (2021-03-03)

**Note:** Version bump only for package @raisin/cap-customers





## [1.7.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.7.1...@raisin/cap-customers@1.7.2) (2021-03-02)

**Note:** Version bump only for package @raisin/cap-customers





## [1.7.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.7.0...@raisin/cap-customers@1.7.1) (2021-03-02)

**Note:** Version bump only for package @raisin/cap-customers





# [1.7.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.6.0...@raisin/cap-customers@1.7.0) (2021-03-01)


### Features

* **auth:** Add config file for mf, update webpack to work with this config file and removing .env file and references ([4c38561](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4c38561eff9360421cf56928e1e94cad1f46c59f))





# 1.6.0 (2021-02-25)


### Features

* **CAP:** Adding Cap project ([6a3aee7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/6a3aee778fbd3b4e9babe73487a47feea87d6638))
* **Webpack:** fixed broken webpack with latest version upgrade ([a8aa19c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a8aa19c7ad5e2d2dd49fc852574470c6a646b44d))





# [1.5.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.4.2...@raisin/cap-customers@1.5.0) (2021-02-18)

### Features

- **Customers:** added unit test setup ([ff1edb2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/ff1edb2e43a775d1970c26e2b7537e244122ba00))
- **Customers:** customer components test cases ([788623f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/788623fd4c8787a2ed1996dce5136076e147e355))
- **Customers:** optimized test cases for repeated code ([5619ff2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/5619ff2b76984f1b659f072d6c9af4349ed2d7d0))
- **Tests:** changed relative imports with aliase ([bb45f1e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/bb45f1ef1cdb85015080de8ad0af3400442ccfc5))
- **Tests:** Customers test case ([bd05dd0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/bd05dd04cc6cd8ca6abec5fa2df1a03ad021888b))
- **Tests:** merged with master ([96c8e5b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/96c8e5bfc7c5cb70ccd7d27a897c57787fe51ace))
- **Tests:** refactored to use screen api ([1843b41](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1843b419fd34b18cba8a5ffe42c328a7b8b01d47))
- **Tests:** using userEvent instead of fireEvent ([52df65c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/52df65c2c9ce37e9a5e9484ea1d4d3ea1d41eb54))

## [1.4.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.4.1...@raisin/cap-customers@1.4.2) (2021-02-18)

**Note:** Version bump only for package @raisin/cap-customers

## [1.4.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.4.0...@raisin/cap-customers@1.4.1) (2021-02-16)

**Note:** Version bump only for package @raisin/cap-customers

# [1.4.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.3.0...@raisin/cap-customers@1.4.0) (2021-02-15)

### Features

- **customers:** Inject customers reducer ([bf20d5e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/bf20d5ef5bd9a4bb857b20fb80ebc907c3a498c0))

# [1.3.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.2.1...@raisin/cap-customers@1.3.0) (2021-02-12)

### Features

- **Colors:** fixed icons labels alignments ([3e890b2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3e890b2d1e0844f6e2812a8d6b04a541c0bfafe8))
- **Colors:** merged latest master and fixed colors ([dbe365d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/dbe365da20c6b30255c29d6c3912cb03a217dddc))
- **MaterialUI:** fixed input component ([1db6f93](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1db6f935a4bb40f5c1dba03589b6a624c64f678a))
- **Theme:** fixed theme configuration for Button and removed cap-ui-lib usage from MF ([8b1f0d5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/8b1f0d53ae3c6cc8eeca3c2ef9ba327824cdfbd2))

## [1.2.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.2.0...@raisin/cap-customers@1.2.1) (2021-02-09)

**Note:** Version bump only for package @raisin/cap-customers

# [1.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap-customers@1.1.0...@raisin/cap-customers@1.2.0) (2021-02-09)

### Features

- **customer/Status:** Add status component into the customerMasterData MF ([00786b2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/00786b2436c6c58372b238f9f0a69c15dd64eb73))
- **materialui:** fixed lint issue ([385e383](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/385e38315620802bc4029602535e56f4463bb980))
- **materialUI:** rebased with master ([6162334](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/6162334db7435feb181c16d02651ac51d92c7424))
- **MaterialUi:** fixed color themes ([bfb0d37](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/bfb0d378cdc9acf66ca67a6e9dc4a6a3641d759d))
- **MaterialUI:** fixed lint issues ([abf31c0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/abf31c05d36945362ea1080f68b5b6e4b23232e0))
- **MaterialUI:** fixed shared context theme issue ([99f4dca](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/99f4dca1dccb58073c272f3feb196aeee10763ed))
- **MaterialUI:** temp tabs introduced ([077e25b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/077e25b74a64fc30ff6f104b05806b1767ab3832))

# 1.1.0 (2021-02-08)

### Bug Fixes

- **App:** reverting dotenv-webpack ([0adcef7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/0adcef7af75dc00584c0bd4661d19af8f6bebe99))
- **Ci Pipeline:** Add url to the repository in package.json ([f0f7ac5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f0f7ac5f3e7177f467017f36f864f2cad8e94a25))
- **Ci Pipeline:** Create NPM packages for all submodules ([512a802](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/512a802203a221e1ee5a418a42bbd4e9363bf31f))

### Features

- **CD:** Integrate CAP with Amplify for Staging, Onboarding and Production ([171d0f3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/171d0f3463853be52ba6cbfdbc37d45131df83f3))
