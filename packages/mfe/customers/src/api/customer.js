import { api } from './interceptors';

export const searchByBacNumber = async (bacnumber) => {
  const url = `customers/${bacnumber}?embed=all-persons&embed=reference-accounts`;
  return api.get(url);
};

export default {
  searchByBacNumber,
};
