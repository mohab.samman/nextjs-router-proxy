import axios from 'axios';
import { setAuthorizationTokenInHeaders } from 'auth/Auth';

import { getConfigValuesBaseOnRegion } from '../utils/domain';

const HTTP_REQUEST_TIMEOUT = 3000;

const { baseURL } = getConfigValuesBaseOnRegion();

export const api = axios.create({
  baseURL,
  timeout: HTTP_REQUEST_TIMEOUT,
  headers: {
    'content-type': 'application/json',
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Origin': '*',
  },
});

// Set the JWT Access Token into the axios Headers (in memory for security)
setAuthorizationTokenInHeaders((jwtToken) => {
  api.defaults.headers.Authorization = `Bearer ${jwtToken}`;
});

api.interceptors.request.use(
  (req) => req,
  () => {},
);

api.interceptors.response.use(
  (res) => res.data,

  (error) => {
    let customError = {};

    if (error.message === 'Network Error') {
      customError = {
        errorCode: 404,
        error: { message: error.message },
      };
    }

    if (error?.response?.status === 404) {
      customError = {
        errorCode: 404,
        error: error.response.data,
      };
    }

    return Promise.reject(customError);
  },
);

export default api;
