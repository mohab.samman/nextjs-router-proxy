export const findCustomerByRole = (persons, role) =>
  persons?.find((person) => person?.roles[0]?.name === role);

export const formatCustomerDetails = (customer) => {
  const customerByAccountHolderRole = findCustomerByRole(
    customer?._embedded?.persons,
    'ACCOUNT_HOLDER',
  );

  const customerDetails = {
    id: customerByAccountHolderRole?.id ?? '-',
    firstName: customerByAccountHolderRole?.firstName ?? '-',
    lastName: customerByAccountHolderRole?.lastName ?? '-',
    salutation: customerByAccountHolderRole?.personalDetails?.courtesyTitleCode ?? '-',
    academicTitle: customerByAccountHolderRole?.personalDetails?.academicTitleCode ?? '-',
    birthDate: customerByAccountHolderRole?.personalDetails?.birthDate ?? '-',
    phoneNumber: customerByAccountHolderRole?.contactDetails?.phoneNumber ?? '-',
    email: customerByAccountHolderRole?.contactDetails?.email ?? '-',
    birthPlace: customerByAccountHolderRole?.personalDetails?.birthPlace ?? '-',
    maritalStatus: customerByAccountHolderRole?.personalDetails?.maritalStatus ?? '-',
    nationalityPrimary: customerByAccountHolderRole?.personalDetails?.nationalityPrimary ?? '-',
    nationalitySecondary: customerByAccountHolderRole?.personalDetails?.nationalitySecondary ?? '-',
    profession: customerByAccountHolderRole?.personalDetails?.profession ?? '-',
    industryCode: customerByAccountHolderRole?.personalDetails?.industry ?? '-',
    status: customerByAccountHolderRole?.roles[0].status ?? '-',
    taxDetails:
      customerByAccountHolderRole?.taxDetails?.map((tax, index) => ({ ...tax, id: index })) ?? [],
  };

  return customerDetails;
};

export const formatAddressDetails = (customer) => {
  const customerByAccountHolderRole = findCustomerByRole(
    customer?._embedded?.persons,
    'ACCOUNT_HOLDER',
  );

  const addressDetails = {
    street: customerByAccountHolderRole?.address?.street ?? '-',
    streetNumber: customerByAccountHolderRole?.address?.streetNumber ?? '-',
    city: customerByAccountHolderRole?.address?.city ?? '-',
    postalCode: customerByAccountHolderRole?.address?.postalCode ?? '-',
    countryCode: customerByAccountHolderRole?.address?.countryCode ?? '-',
  };

  return addressDetails;
};

export const formatLoginDetails = (customer) => {
  const customerByAccountHolderRole = findCustomerByRole(
    customer?._embedded?.persons,
    'ACCOUNT_HOLDER',
  );

  return {
    username: customerByAccountHolderRole?.username,
    password: customerByAccountHolderRole?.password,
  };
};
