import { ThemeProvider } from '@material-ui/core/styles';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import theme from 'styles/themes';

import getMessages from './getMessages';
import { Locale, getShortLocale } from './localeContext';
import store from '../redux/store';

const getTranslationMessages = async (locale) => {
  const hostMessages = await getMessages(locale);

  return hostMessages;
};

// eslint-disable-next-line react/prop-types
const Providers = ({ children }) => {
  const messages = getTranslationMessages(Locale.lang);
  return (
    <ThemeProvider theme={theme}>
      <IntlProvider
        locale={getShortLocale(Locale.lang)}
        defaultLocale={getShortLocale(Locale.lang)}
        messages={messages}
      >
        <Provider store={store}>{children}</Provider>
      </IntlProvider>
    </ThemeProvider>
  );
};

const customRender = (ui, options = {}) => {
  return render(ui, { wrapper: Providers, ...options });
};

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
