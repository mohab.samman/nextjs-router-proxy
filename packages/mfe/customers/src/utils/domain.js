/* global apiBaseUrlGBR apiBaseUrlDEU */

const configurations = {
  deu: {
    baseURL: apiBaseUrlDEU,
  },
  gbr: {
    baseURL: apiBaseUrlGBR,
  },
};

export const getConfigValuesBaseOnRegion = () => {
  const { hostname } = window.location;
  // As the url domains have the same structure this line will always work
  const region = hostname.includes('co.uk') ? 'gbr' : 'deu';
  return configurations[region] ?? configurations.deu;
};

export default {
  getConfigValuesBaseOnRegion,
};
