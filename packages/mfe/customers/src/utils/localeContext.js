import { createContext } from 'react';

const supportedLanguage = ['de_DE', 'en_US'];

const getLanguage = () => {
  if (localStorage.getItem('locale')) {
    return localStorage.getItem('locale');
  }

  const browserLangugage = navigator.language.replace('-', '_'); // en-GB => en_GB,

  if (supportedLanguage.some((language) => language === browserLangugage)) {
    return browserLangugage;
  }

  return browserLangugage === 'en' ? supportedLanguage[1] : supportedLanguage[0];
};

export const Locale = {
  lang: getLanguage(),
  setLanguage: () => {},
};

export const LocaleContext = createContext(Locale);

export const getShortLocale = (locale) => locale.substr(0, 2);
