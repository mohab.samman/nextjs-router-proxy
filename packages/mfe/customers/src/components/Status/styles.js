import { makeStyles } from '@material-ui/core/styles';

import Colors from '../../styles/themes/colors';

const useStyles = makeStyles(() => ({
  container: {
    width: 20,
    height: 20,
    borderRadius: 50,
    marginRight: 5,
  },
  active: {
    backgroundColor: Colors.green,
  },
  inactive: {
    backgroundColor: Colors.yellow,
  },
}));

export default useStyles;
