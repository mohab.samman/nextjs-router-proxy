import { render } from 'utils/test-utils';

import Status from '.';

describe('Status', () => {
  test('renders Status with ACTIVE', () => {
    const { queryByTestId } = render(<Status status="ACTIVE" />);

    const status = queryByTestId('status');

    expect(status).toBeInTheDocument();
    expect(status).toHaveClass('makeStyles-active-2');
  });

  test('renders Status with INACTIVE', () => {
    const { queryByTestId } = render(<Status status="INACTIVE" />);

    const status = queryByTestId('status');

    expect(status).toBeInTheDocument();
    expect(status).toHaveClass('makeStyles-inactive-6');
  });
});
