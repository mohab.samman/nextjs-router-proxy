import PropTypes from 'prop-types';

import useStyles from './styles';

const Status = ({ status }) => {
  const classes = useStyles();

  return (
    <span
      data-testid="status"
      className={`${classes.container} ${classes[status.toLowerCase()]}`}
    />
  );
};

Status.propTypes = {
  status: PropTypes.string.isRequired,
};

export default Status;
