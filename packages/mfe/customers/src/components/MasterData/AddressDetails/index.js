import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Typography } from '@material-ui/core';

import TextField from '../../TextField';
import messages from './messages';
import addressDetailsFields from './fields';
import useStyles from './styles';
import sharedStyles from '../styles';

const AddressDetails = ({ intl, addressDetails }) => {
  const classes = useStyles();
  const sharedClasses = sharedStyles();
  const [fields, setFields] = useState([]);

  useEffect(() => {
    const fieldset = addressDetailsFields(intl, addressDetails);
    setFields(fieldset);
  }, [addressDetails, intl]);

  const handleChange = () => {};

  return (
    <>
      <Typography variant="h6" className={classes.sectionHeading}>
        {intl.formatMessage(messages.AddressData)}
      </Typography>

      <div className={sharedClasses.fieldWrapper}>
        {fields.map((field) => {
          return (
            <TextField
              key={field.id}
              type={field.type}
              name={field.name}
              id={field.id}
              label={field.label}
              placeholder={field.placeholder}
              value={field.value}
              onChange={handleChange}
            />
          );
        })}
      </div>
    </>
  );
};

AddressDetails.propTypes = {
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  addressDetails: PropTypes.shape({
    street: PropTypes.string,
    streetNumber: PropTypes.string,
    postalCode: PropTypes.string,
    city: PropTypes.string,
    countryCode: PropTypes.string,
  }),
};
AddressDetails.defaultProps = {
  addressDetails: {
    street: '',
    streetNumber: '',
    postalCode: '',
    city: '',
    countryCode: '',
  },
};

export default injectIntl(AddressDetails);
