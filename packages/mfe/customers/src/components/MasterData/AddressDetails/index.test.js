import { screen } from '@testing-library/react';
import { render } from 'utils/test-utils';

import customer from '../../../../__mocks__/customer';
import AddressDetails from '.';

const {
  street,
  streetNumber,
  city,
  postalCode,
  countryCode,
} = customer._embedded.persons[0].address;

describe('AddressDetails', () => {
  test('renders AddressDetails including street, streetNumber, postalCode etc for given customer', () => {
    render(
      <AddressDetails addressDetails={{ street, streetNumber, city, postalCode, countryCode }} />,
    );

    expect(screen.getByText('Street')).toBeInTheDocument();
    expect(screen.getByText('Street number')).toBeInTheDocument();
    expect(screen.getByText('City')).toBeInTheDocument();
    expect(screen.getByText('Postal code')).toBeInTheDocument();
    expect(screen.getByText('Country code')).toBeInTheDocument();
  });
});
