import { defineMessages } from 'react-intl';

const messages = defineMessages({
  AddressData: {
    id: 'AddressDetails.section.heading',
    defaultMessage: 'Address data',
  },
  Street: {
    id: 'AddressDetails.street.label',
    defaultMessage: 'Street',
  },
  StreetNumber: {
    id: 'AddressDetails.streetNumber.label',
    defaultMessage: 'Street number',
  },
  City: {
    id: 'AddressDetails.city.label',
    defaultMessage: 'City',
  },
  PostalCode: {
    id: 'AddressDetails.postalCode.label',
    defaultMessage: 'Postal code',
  },
  CountryCode: {
    id: 'AddressDetails.countryCode.label',
    defaultMessage: 'Country code',
  },
});

export default messages;
