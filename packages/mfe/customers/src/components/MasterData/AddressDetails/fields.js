import messages from './messages';

const addressDetailsFields = (intl, data) => [
  {
    type: 'text',
    name: 'street',
    id: 'street',
    label: intl.formatMessage(messages.Street),
    value: data.street,
  },
  {
    type: 'text',
    name: 'streetNumber',
    id: 'streetNumber',
    label: intl.formatMessage(messages.StreetNumber),
    value: data.streetNumber,
  },
  {
    type: 'text',
    name: 'city',
    id: 'city',
    label: intl.formatMessage(messages.City),
    value: data.city,
  },
  {
    type: 'text',
    name: 'postalCode',
    id: 'postalCode',
    label: intl.formatMessage(messages.PostalCode),
    value: data.postalCode,
  },
  {
    type: 'text',
    name: 'countryCode',
    id: 'countryCode',
    label: intl.formatMessage(messages.CountryCode),
    value: data.countryCode,
  },
];

export default addressDetailsFields;
