import { makeStyles } from '@material-ui/core/styles';

import Colors from '../../styles/themes/colors';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
  },
  tabsWrapper: {
    display: 'flex',
  },
  appBarRoot: {
    backgroundColor: Colors.white,
    color: Colors.black,
    borderBottom: `1px solid ${Colors.black40}`,
    boxShadow: 'none',
  },
  tabsContainer: {
    display: 'flex',
  },
  toolbarHeading: {
    color: Colors.black80,
  },
  tabContent: {
    minHeight: 400,
  },
  tabPanelContainer: {
    width: '100%',
    height: '100vh',
    overflowY: 'auto',
  },
  fieldWrapper: {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    gridGap: 24,
  },
}));

export default useStyles;
