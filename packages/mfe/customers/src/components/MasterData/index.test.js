import { screen, fireEvent } from '@testing-library/react';
import { render } from 'utils/test-utils';

import customer from '../../../__mocks__/customer';
import MasterData from '.';

describe('MasterData', () => {
  test('renders MasterData container with all 6 tab panels', () => {
    render(<MasterData customer={customer} />);

    expect(screen.getByTestId('toolbar_heading')).toBeInTheDocument();
    expect(screen.getByTestId('customer_details')).toBeInTheDocument();
    expect(screen.getByTestId('login')).toBeInTheDocument();
    expect(screen.getByTestId('address')).toBeInTheDocument();
    expect(screen.getByTestId('nominated_account')).toBeInTheDocument();
    expect(screen.getByTestId('documents')).toBeInTheDocument();
    expect(screen.getByTestId('activitylog')).toBeInTheDocument();
  });

  test('handles Login tab click and validates tab selection with scroll', async () => {
    render(<MasterData customer={customer} />);

    const container = screen.getByTestId('tabPanelContainerId');
    container.scroll = jest.fn();

    const loginElement = screen.getByText('Login / Password').closest('button');
    expect(loginElement).toHaveAttribute('aria-selected', 'false');

    fireEvent.click(loginElement);
    expect(container.scroll).toBeCalledTimes(1);
    expect(loginElement).toHaveAttribute('aria-selected', 'true');
  });
});
