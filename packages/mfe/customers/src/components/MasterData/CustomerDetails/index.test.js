import { screen } from '@testing-library/react';
import { render } from 'utils/test-utils';

import customer from '../../../../__mocks__/customer';
import CustomerDetails from '.';

const {
  personalDetails: {
    academicTitleCode,
    courtesyTitleCode,
    firstName,
    lastName,
    birthName,
    birthDate,
    birthPlace,
    nationalityPrimary,
    maritalStatus,
  },
  taxDetails: [{ taxIdentificationNumber, countryCode }],
} = customer._embedded.persons[0];

describe('CustomerDetails', () => {
  test('renders CustomerManagement for given customer with all fields', () => {
    render(
      <CustomerDetails
        customerDetails={{
          academicTitleCode,
          courtesyTitleCode,
          firstName,
          lastName,
          birthName,
          birthDate,
          birthPlace,
          nationalityPrimary,
          maritalStatus,
          taxDetails: [{ taxIdentificationNumber, countryCode }],
        }}
      />,
    );

    expect(screen.getByText('First name')).toBeInTheDocument();
    expect(screen.getByText('Last name')).toBeInTheDocument();
    expect(screen.getByText('Marital status')).toBeInTheDocument();
    expect(screen.getByText('Nationality')).toBeInTheDocument();
    expect(screen.getByText('Date of birth')).toBeInTheDocument();
  });
});
