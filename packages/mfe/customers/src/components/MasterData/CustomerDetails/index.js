import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Typography } from '@material-ui/core';

import TextField from '../../TextField';
import messages from './messages';
import customerDetailsFields from './fields';
import useStyles from './styles';
import sharedStyles from '../styles';

const CustomerDetails = ({ intl, customerDetails }) => {
  const classes = useStyles();
  const sharedClasses = sharedStyles();

  const [fields, setFields] = useState([]);

  useEffect(() => {
    const fieldset = customerDetailsFields(intl, customerDetails);
    setFields(fieldset);
  }, [customerDetails, intl]);

  const handleChange = () => {};

  return (
    <>
      <Typography variant="h6" className={classes.sectionHeading}>
        {intl.formatMessage(messages.CustomerDetails)}
      </Typography>
      <div className={sharedClasses.fieldWrapper}>
        {fields.map((field) => {
          return (
            <TextField
              key={field.id}
              type={field.type}
              name={field.name}
              id={field.id}
              label={field.label}
              placeholder={field.placeholder}
              value={field.value}
              onChange={handleChange}
            />
          );
        })}
      </div>
    </>
  );
};

CustomerDetails.propTypes = {
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  customerDetails: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
  }),
};

CustomerDetails.defaultProps = {
  customerDetails: {
    firstName: '',
    lastName: '',
  },
};

export default injectIntl(CustomerDetails);
