import messages from './messages';

const customerDetailsFields = (intl, data) => [
  {
    type: 'text',
    name: 'firstName',
    id: 'firstName',
    label: intl.formatMessage(messages.FirstName),
    value: data.firstName,
  },
  {
    type: 'text',
    name: 'lastName',
    id: 'lastName',
    label: intl.formatMessage(messages.LastName),
    value: data.lastName,
  },
  {
    type: 'text',
    name: 'salutation',
    id: 'salutation',
    label: intl.formatMessage(messages.Salutation),
    value: data.salutation,
    isSmall: true,
  },
  {
    type: 'text',
    name: 'title',
    id: 'title',
    label: intl.formatMessage(messages.Title),
    value: data.academicTitle,
    isSmall: true,
  },
  {
    type: 'text',
    name: 'dateofbirth',
    id: 'dateofbirth',
    label: intl.formatMessage(messages.DateOfBirth),
    value: data.birthDate,
  },
  {
    type: 'text',
    name: 'phonenumber',
    id: 'phonenumber',
    label: intl.formatMessage(messages.PhoneNumber),
    value: data.phoneNumber,
  },
  {
    type: 'email',
    name: 'email',
    id: 'email',
    label: intl.formatMessage(messages.Email),
    value: data.email,
  },
  {
    type: 'text',
    name: 'placeofbirth',
    id: 'placeofbirth',
    label: intl.formatMessage(messages.PlaceOfBirth),
    value: data.birthPlace,
  },
  {
    type: 'text',
    name: 'marital_status',
    id: 'marital_status',
    label: intl.formatMessage(messages.MaritalStatus),
    value: data.maritalStatus,
  },
  {
    type: 'text',
    name: 'nationality',
    id: 'nationality',
    label: intl.formatMessage(messages.Nationality),
    value: data.nationalityPrimary,
  },
  {
    type: 'text',
    name: 'second_nationality',
    id: 'second_nationality',
    label: intl.formatMessage(messages.SecondNationality),
    value: data.nationalitySecondary,
  },
  {
    type: 'text',
    name: 'profession',
    id: 'profession',
    label: intl.formatMessage(messages.Profession),
    value: data.profession,
  },
  {
    type: 'text',
    name: 'industry',
    id: 'industry',
    label: intl.formatMessage(messages.Industry),
    value: data.industryCode,
  },
  {
    type: 'text',
    name: 'tax_residency',
    id: 'tax_residency',
    label: intl.formatMessage(messages.TaxResidency),
    value: data?.taxDetails[0]?.countryCode,
  },
  {
    type: 'text',
    name: 'tax_number',
    id: 'tax_number',
    label: intl.formatMessage(messages.TaxNumber),
    value: data?.taxDetails[0]?.taxIdentificationNumber,
  },
];

export default customerDetailsFields;
