import { defineMessages } from 'react-intl';

const messages = defineMessages({
  CustomerDetails: {
    id: 'CustomerDetails.section.heading',
    defaultMessage: 'Customer details',
  },
  FirstName: {
    id: 'CustomerDetails.firstName.label',
    defaultMessage: 'First name',
  },
  LastName: {
    id: 'CustomerDetails.lastName.label',
    defaultMessage: 'Last name',
  },
  Salutation: {
    id: 'CustomerDetails.salutation.label',
    defaultMessage: 'Salutation',
  },
  Title: {
    id: 'CustomerDetails.title.label',
    defaultMessage: 'Title',
  },
  DateOfBirth: {
    id: 'CustomerDetails.dateOfBirth.label',
    defaultMessage: 'Date of birth',
  },
  PhoneNumber: {
    id: 'CustomerDetails.phoneNumber.label',
    defaultMessage: 'Phone number',
  },
  Email: {
    id: 'CustomerDetails.email.label',
    defaultMessage: 'E-mail address',
  },
  PlaceOfBirth: {
    id: 'CustomerDetails.placeOfBirth.label',
    defaultMessage: 'Place of birth',
  },
  MaritalStatus: {
    id: 'CustomerDetails.maritalStatus.label',
    defaultMessage: 'Marital status',
  },
  Nationality: {
    id: 'CustomerDetails.nationality.label',
    defaultMessage: 'Nationality',
  },
  SecondNationality: {
    id: 'CustomerDetails.secondNationality.label',
    defaultMessage: 'Second nationality',
  },
  Profession: {
    id: 'CustomerDetails.profession.label',
    defaultMessage: 'Profession',
  },
  Industry: {
    id: 'CustomerDetails.industry.label',
    defaultMessage: 'Industry',
  },
  TaxResidency: {
    id: 'CustomerDetails.taxRecidency.label',
    defaultMessage: 'Tax recidency',
  },
  TaxNumber: {
    id: 'CustomerDetails.taxNumber.label',
    defaultMessage: 'Tax number',
  },
});

export default messages;
