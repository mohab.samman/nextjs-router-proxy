import { defineMessages } from 'react-intl';

const messages = defineMessages({
  LoginDetails: {
    id: 'LoginDetails.section.heading',
    defaultMessage: 'Login details',
  },
  Username: {
    id: 'LoginDetails.username.label',
    defaultMessage: 'Username',
  },
  Password: {
    id: 'LoginDetails.password.label',
    defaultMessage: 'Password',
  },
});

export default messages;
