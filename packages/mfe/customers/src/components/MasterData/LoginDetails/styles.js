import { makeStyles } from '@material-ui/core/styles';

import { fontWeight } from '../../../styles/themes/typography';

const useStyles = makeStyles(() => ({
  sectionHeading: {
    marginBottom: 16,
    fontWeight: fontWeight.semi_bold,
  },
}));

export default useStyles;
