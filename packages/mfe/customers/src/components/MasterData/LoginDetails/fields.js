import messages from './messages';

const loginDetailsFields = (intl, data) => [
  {
    type: 'text',
    name: 'username',
    id: 'username',
    label: intl.formatMessage(messages.Username),
    value: data.username,
  },
  {
    type: 'text',
    name: 'password',
    id: 'password',
    label: intl.formatMessage(messages.Password),
    value: data.password,
  },
];

export default loginDetailsFields;
