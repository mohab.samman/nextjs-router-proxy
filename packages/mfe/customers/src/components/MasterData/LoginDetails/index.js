import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Typography } from '@material-ui/core';

import TextField from '../../TextField';
import messages from './messages';
import loginDetailsFields from './fields';
import useStyles from './styles';
import sharedStyles from '../styles';

const LoginDetails = ({ intl, loginDetails }) => {
  const classes = useStyles();
  const sharedClasses = sharedStyles();
  const [fields, setFields] = useState([]);

  useEffect(() => {
    const fieldset = loginDetailsFields(intl, loginDetails);
    setFields(fieldset);
  }, [loginDetails, intl]);

  const handleChange = () => {};

  return (
    <>
      <Typography variant="h6" className={classes.sectionHeading}>
        {intl.formatMessage(messages.LoginDetails)}
      </Typography>
      <div className={sharedClasses.fieldWrapper}>
        {fields.map((field) => {
          return (
            <TextField
              key={field.id}
              type={field.type}
              name={field.name}
              id={field.id}
              label={field.label}
              placeholder={field.placeholder}
              value={field.value}
              onChange={handleChange}
            />
          );
        })}
      </div>
    </>
  );
};

LoginDetails.propTypes = {
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
  loginDetails: PropTypes.shape({
    username: PropTypes.string,
    password: PropTypes.string,
  }),
};
LoginDetails.defaultProps = {
  loginDetails: {
    username: '',
    password: '',
  },
};

export default injectIntl(LoginDetails);
