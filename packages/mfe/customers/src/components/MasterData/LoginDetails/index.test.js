import { screen } from '@testing-library/react';
import { render } from 'utils/test-utils';

import customer from '../../../../__mocks__/customer';
import LoginDetails from '.';

const { username } = customer._embedded.persons[0].username;

describe('LoginDetails', () => {
  test('renders LoginDetails for given customer with username', () => {
    render(<LoginDetails loginDetails={{ username }} />);

    expect(screen.getByText('Username')).toBeInTheDocument();
  });
});
