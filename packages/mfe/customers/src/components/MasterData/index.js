import { useState, useEffect, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import { Box, Typography, Toolbar, AppBar, Container } from '@material-ui/core';
import isEmpty from 'lodash/isEmpty';

import { getTabs } from './tabsConfig';
import ScrollSpyTabs from '../ScrollSpyTabs';
import CustomerDetails from './CustomerDetails';
import AddressDetails from './AddressDetails';
import LoginDetails from './LoginDetails';
import {
  formatCustomerDetails,
  formatAddressDetails,
  formatLoginDetails,
} from '../../utils/customer';
import messages from './messages';
import useStyles from './styles';

const UnderConstruction = lazy(() => import('cap/UnderConstruction'));

const UnderConstructionWrapper = () => {
  return (
    <Suspense fallback="Loading ...">
      <UnderConstruction />
    </Suspense>
  );
};

const TabPanel = ({ children, value, index, ...other }) => (
  <div
    role="tabpanel"
    id={`wrapped-tabpanel-${index}`}
    aria-labelledby={`wrapped-tab-${index}`}
    {...other}
  >
    <Box sx={{ p: 3 }}>{children}</Box>
  </div>
);

TabPanel.propTypes = {
  children: PropTypes.node.isRequired,
  index: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

const MasterData = ({ customer, intl }) => {
  const classes = useStyles();

  const tabs = getTabs(intl);
  const [activeTab, setActiveTab] = useState(tabs[0].id);
  const [customerDetails, setCustomerDetails] = useState({});
  const [addressDetails, setAddressDetails] = useState({});
  const [loginDetails, setLoginDetails] = useState({});

  useEffect(() => {
    setCustomerDetails(formatCustomerDetails(customer));
    setAddressDetails(formatAddressDetails(customer));
    setLoginDetails(formatLoginDetails(customer));
  }, [customer]);

  const handleTabChange = (value) => {
    setActiveTab(value);
  };

  return (
    <div>
      <AppBar position="static" classes={{ root: classes.appBarRoot }}>
        <Toolbar>
          <Typography variant="h5" className={classes.toolbarHeading} data-testid="toolbar_heading">
            {intl.formatMessage(messages.CustomerData)}
          </Typography>
        </Toolbar>
      </AppBar>

      <Container disableGutters classes={{ root: classes.tabsContainer }} maxWidth={false}>
        <ScrollSpyTabs
          orientation="vertical"
          onChange={handleTabChange}
          aria-label="customer master data"
          tabsInScroll={getTabs(intl)}
        />
        <div
          className={classes.tabPanelContainer}
          id="tabPanelContainer"
          data-testid="tabPanelContainerId"
        >
          <TabPanel
            value={activeTab}
            index="customer_details"
            className={classes.tabContent}
            id="customer_details"
            data-testid="customer_details"
          >
            {isEmpty(customerDetails) ? (
              <></>
            ) : (
              <CustomerDetails customerDetails={customerDetails} />
            )}
          </TabPanel>
          <TabPanel
            value={activeTab}
            index="login"
            className={classes.tabContent}
            id="login"
            data-testid="login"
          >
            {isEmpty(loginDetails) ? <></> : <LoginDetails loginDetails={loginDetails} />}
          </TabPanel>
          <TabPanel
            value={activeTab}
            index="address"
            className={classes.tabContent}
            id="address"
            data-testid="address"
          >
            {isEmpty(addressDetails) ? <></> : <AddressDetails addressDetails={addressDetails} />}
          </TabPanel>
          <TabPanel
            value={activeTab}
            index="nominated_account"
            className={classes.tabContent}
            id="nominated_account"
            data-testid="nominated_account"
          >
            <UnderConstructionWrapper />
          </TabPanel>
          <TabPanel
            value={activeTab}
            index="documents"
            className={classes.tabContent}
            id="documents"
            data-testid="documents"
          >
            <UnderConstructionWrapper />
          </TabPanel>
          <TabPanel
            value={activeTab}
            index="activitylog"
            className={classes.tabContent}
            id="activitylog"
            data-testid="activitylog"
          >
            <UnderConstructionWrapper />
          </TabPanel>
        </div>
      </Container>
    </div>
  );
};

MasterData.propTypes = {
  customer: PropTypes.shape({}).isRequired,
  intl: PropTypes.shape({
    formatMessage: PropTypes.func,
  }).isRequired,
};

export default injectIntl(MasterData);
