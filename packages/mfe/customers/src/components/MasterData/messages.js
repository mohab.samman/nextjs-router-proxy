import { defineMessages } from 'react-intl';

const messages = defineMessages({
  CustomerDetails: {
    id: 'CustomerMasterData.tab.customerDetails',
    defaultMessage: 'Customer details',
  },
  LoginPassword: { id: 'CustomerMasterData.tab.login', defaultMessage: 'Login / Password' },
  AddressData: { id: 'CustomerMasterData.tab.address', defaultMessage: 'Address data' },
  NominatedAccount: {
    id: 'CustomerMasterData.tab.nominatedAccount',
    defaultMessage: 'Nominated account',
  },
  Documents: {
    id: 'CustomerMasterData.tab.documents',
    defaultMessage: 'Documents',
  },
  ActivityLog: {
    id: 'CustomerMasterData.tab.activitylog',
    defaultMessage: 'Activity log (History)',
  },
  CustomerData: {
    id: 'CustomerData.toolbar.heading',
    defaultMessage: 'Customer data',
  },
  AccountHolder: { id: 'CustomerData.AccountHolder.heading', defaultMessage: 'Account holder' },
});

export default messages;
