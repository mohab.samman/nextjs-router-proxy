import messages from './messages';

export const getTabs = (intl) => {
  return [
    {
      label: intl.formatMessage(messages.CustomerDetails),
      id: 'customer_details',
    },
    {
      label: intl.formatMessage(messages.LoginPassword),
      id: 'login',
    },
    {
      label: intl.formatMessage(messages.AddressData),
      id: 'address',
    },
    {
      label: intl.formatMessage(messages.NominatedAccount),
      id: 'nominated_account',
    },
    {
      label: intl.formatMessage(messages.Documents),
      id: 'documents',
    },
    {
      label: intl.formatMessage(messages.ActivityLog),
      id: 'activitylog',
    },
  ];
};

export default getTabs;
