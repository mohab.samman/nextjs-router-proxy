import { makeStyles } from '@material-ui/core/styles';

import Colors from '../../styles/themes/colors';

const useStyles = makeStyles(() => ({
  inputPropRoot: {
    height: 58,
    backgroundColor: Colors.black20,
    boxShadow: 'none',
  },
}));

export default useStyles;
