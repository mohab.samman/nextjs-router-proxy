import PropTypes from 'prop-types';
import { TextField as MuiTextField } from '@material-ui/core';

import useStyles from './styles';

const TextField = ({
  type,
  name,
  label,
  id,
  placeholder,
  value,
  helperText,
  error,
  disabled,
  required,
  onChange,
}) => {
  const classes = useStyles();

  return (
    <MuiTextField
      type={type}
      name={name}
      id={id}
      label={label}
      placeholder={placeholder}
      helperText={helperText}
      error={error}
      disabled={disabled}
      required={required}
      variant="filled"
      InputLabelProps={{
        shrink: true,
      }}
      value={value}
      onChange={onChange}
      InputProps={{
        disableUnderline: true,
        classes: {
          root: classes.inputPropRoot,
        },
      }}
    />
  );
};

TextField.propTypes = {
  type: PropTypes.string,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  id: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
  helperText: PropTypes.string,
  error: PropTypes.bool,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
};

TextField.defaultProps = {
  type: 'text',
  label: '',
  id: '',
  placeholder: '',
  value: '',
  onChange: () => {
    return '';
  },
  helperText: '',
  error: false,
  disabled: false,
  required: false,
};

export default TextField;
