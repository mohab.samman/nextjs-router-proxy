/** Our own version of https://stackoverflow.com/questions/63982305/material-ui-scrollspy-react-use-tabs-to-move-through-list-and-auto-scroll
 * we can replace it with official MUI component in future when its available in stable release
 */

import { useEffect, useMemo, useState, useRef, useCallback } from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash/throttle';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import Colors from '../../styles/themes/colors';

const tabHeight = 54;
const topHeight = 240; // 240px is approximately adjusted height (total height from top)

const StyledTabs = withStyles({
  root: {},
  indicator: {
    left: 0,
    width: 3,
  },
})((props) => <Tabs {...props} />);

const StyledTab = withStyles(() => ({
  root: {
    fontSize: '1rem',
    whiteSpace: 'inherit',
    opacity: 1,
    height: tabHeight,
    overflow: 'inherit',
    backgroundColor: Colors.black10,
    padding: '8px 16px',
  },
  wrapper: {
    alignItems: 'flex-start',
  },
}))((props) => <Tab disableRipple {...props} />);

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },
  navWrapper: {
    position: 'sticky',
    alignSelf: 'flex-start',
    top: 0,
    left: 0,
    right: 0,
  },

  mainWrapper: {
    width: 234,
    display: 'flex',
    backgroundColor: Colors.black10,
    borderRight: `1px solid ${Colors.black40}`,
  },
}));

const noop = () => {};

function useThrottledOnScroll(callback, delay) {
  const container = document.getElementById('tabPanelContainer');
  const throttledCallback = useMemo(() => (callback ? throttle(callback, delay) : noop), [
    callback,
    delay,
  ]);

  useEffect(() => {
    if (throttledCallback === noop) {
      return undefined;
    }

    if (container) {
      container.addEventListener('scroll', throttledCallback);
    }

    return () => {
      if (container) {
        container.removeEventListener('scroll', throttledCallback);
        throttledCallback.cancel();
      }
    };
  }, [throttledCallback]);
}

const ScrollSpyTabs = ({ tabsInScroll, orientation, onChange }) => {
  const [activeState, setActiveState] = useState(null);

  const tabsConfig = tabsInScroll.map((tab) => {
    return {
      id: tab.id,
      label: tab.label,
      hash: tab.id,
      node: document.getElementById(tab.id),
    };
  });

  const itemsClientRef = useRef([]);
  useEffect(() => {
    itemsClientRef.current = tabsConfig;
  }, [tabsConfig]);

  const clickedRef = useRef(false);
  const unsetClickedRef = useRef(null);

  const findActiveIndex = useCallback(() => {
    const container = document.getElementById('tabPanelContainer');
    // set default if activeState is null
    if (activeState === null) {
      return setActiveState(tabsConfig[0].hash);
    }

    // Don't set the active index based on scroll if a link was just clicked
    if (clickedRef.current) {
      return null;
    }

    let active;
    for (let i = itemsClientRef.current.length - 1; i >= 0; i -= 1) {
      // No hash if we're near the top of the page
      if (container.scrollTop < 0) {
        active = { hash: null };
        break;
      }

      const item = itemsClientRef.current[i];

      if (
        item.node &&
        item.node.offsetTop < container.scrollTop + container.clientHeight / 8 + topHeight
      ) {
        active = item;
        break;
      }
    }

    if (active && activeState !== active.hash) {
      setActiveState(active.hash);
      onChange(active.id);
    }
    return null;
  }, [activeState, tabsConfig]);

  // Corresponds to 10 frames at 60 Hz
  useThrottledOnScroll(tabsConfig.length > 0 ? findActiveIndex : null, 166);

  const handleClick = ({ hash, id }) => {
    // Used to disable findActiveIndex if the page scrolls due to a click
    clickedRef.current = true;
    unsetClickedRef.current = setTimeout(() => {
      clickedRef.current = false;
    }, 1000);

    if (activeState !== hash) {
      setActiveState(hash);
      onChange(id);
      const container = document.getElementById('tabPanelContainer');
      if (container) {
        return container.scroll({
          top:
            document.getElementById(hash).getBoundingClientRect().top +
            container.scrollTop -
            topHeight,
          behavior: 'smooth',
        });
      }
    }

    return null;
  };

  useEffect(
    () => () => {
      clearTimeout(unsetClickedRef.current);
    },
    [],
  );

  const classes = useStyles();

  return (
    <div className={classes.mainWrapper}>
      <nav className={classes.navWrapper}>
        <StyledTabs value={activeState || tabsConfig[0].hash} orientation={orientation}>
          {tabsConfig.map((tab) => (
            <StyledTab
              key={tab.hash}
              label={tab.label}
              onClick={() => handleClick(tab)}
              value={tab.hash}
            />
          ))}
        </StyledTabs>
      </nav>
    </div>
  );
};

ScrollSpyTabs.propTypes = {
  orientation: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  tabsInScroll: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      tabel: PropTypes.string,
    }),
  ).isRequired,
};

ScrollSpyTabs.defaultProps = {
  orientation: 'vertical',
};

export default ScrollSpyTabs;
