import { screen } from '@testing-library/react';
import { render } from 'utils/test-utils';

import ScrollSpyTabs from './index';

const props = {
  tabsInScroll: [
    {
      label: 'Customer details',
      id: 'customer_details',
    },
    {
      label: 'Login',
      id: 'login',
    },
  ],
  orientation: 'vertical',
  onChange: jest.fn(),
};

describe('ScrollSpyTabs', () => {
  test('renders ScrollSpyTabs component with tabs length of 2', () => {
    render(<ScrollSpyTabs {...props} />);

    expect(screen.getByText(/Customer details/i)).toBeInTheDocument();
    expect(screen.getByText(/Login/i)).toBeInTheDocument();
  });
});
