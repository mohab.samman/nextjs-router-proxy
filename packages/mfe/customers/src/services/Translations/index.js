import getMessages from '../../utils/getMessages';

const fetchTranslations = (locale) => {
  return getMessages(locale);
};

export default fetchTranslations;
