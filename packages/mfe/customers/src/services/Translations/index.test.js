import * as fetchTranslations from '.';

beforeEach(() => {
  jest.resetAllMocks();
});

describe('Translations', () => {
  test('translations', () => {
    const spy = jest.spyOn(fetchTranslations, 'default');

    fetchTranslations.default('de_DE');
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
