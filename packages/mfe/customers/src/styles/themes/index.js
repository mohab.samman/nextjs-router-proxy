import { createMuiTheme } from '@material-ui/core';

import breakpoints from './breakpoints';
import palette from './palette';
import typography from './typography';
import components from './components';
import overrides from './overrides';

const baseTheme = {
  breakpoints,
  direction: 'ltr',
  palette,
  components,
  typography,
  overrides,
};

export default createMuiTheme(baseTheme);
