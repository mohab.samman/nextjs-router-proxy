export default {
  primary: {
    main: '#4454d3',
    light: '#8498ff',
    dark: '#1c2a4c;',
    contrastText: '#f5f5f5',
  },
  secondary: {
    main: '#e3e6f9',
    contrastText: '#fff',
  },
  error: {
    main: '#d24141',
  },
  success: {
    main: '#008a19',
  },
  info: {
    main: '#1c2a4c',
  },
  text: {
    primary: '#404040',
    secondary: '#797979',
    disabled: '#f3f3f3',
    default: '#000000',
  },
  action: {
    hover: '#8498ff',
  },
  background: {
    drawer: '#30363d',
    appBar: '#ffffff',
    toolbar: '#f5f6fb',
  },
  status: {
    active: '#58b64b',
    inactive: '#dc6363',
  },
  alert: {
    bg: '#fff',
    error: '#dc6363',
    info: '#677be3',
  },
};
