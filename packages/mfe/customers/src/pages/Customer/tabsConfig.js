export const getTabs = (intl) => {
  return [
    {
      label: intl.formatMessage({
        id: 'CustomerRetailData.tab.customerData',
        defaultMessage: 'Customer Data',
      }),
      isActive: true,
      id: 'customer_data',
    },
    {
      label: intl.formatMessage({
        id: 'CustomerRetailData.tab.depositProducts',
        defaultMessage: 'Deposit Products',
      }),
      isActive: false,
      id: 'deposit_products',
    },
    {
      label: intl.formatMessage({
        id: 'CustomerRetailData.tab.investmentProducts',
        defaultMessage: 'Investment Products',
      }),
      isActive: false,
      id: 'investment_products',
    },
    {
      label: intl.formatMessage({
        id: 'CustomerRetailData.tab.pensionsProducts',
        defaultMessage: 'Pension Products',
      }),
      isActive: false,
      id: 'pension_products',
    },
    {
      label: intl.formatMessage({
        id: 'CustomerRetailData.tab.transactionalAccount',
        defaultMessage: 'Transactional Account',
      }),
      isActive: false,
      id: 'transactional_account',
    },
    {
      label: intl.formatMessage({
        id: 'CustomerRetailData.tab.postbox',
        defaultMessage: 'Postbox',
      }),
      isActive: false,
      id: 'postbox',
    },
  ];
};

export default getTabs;
