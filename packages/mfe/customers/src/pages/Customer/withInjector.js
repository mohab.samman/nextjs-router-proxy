import { useEffect } from 'react';
import { useStore } from 'react-redux';
import PropTypes from 'prop-types';

import Customer from '.';
import CustomerSlice from '../../redux/CustomerSlice';

const CustomerWithInjector = ({ bacNumber }) => {
  const store = useStore();

  useEffect(() => store.injectReducer('customer', CustomerSlice), []);

  return <Customer bacNumber={bacNumber} />;
};

CustomerWithInjector.propTypes = {
  bacNumber: PropTypes.string,
};

CustomerWithInjector.defaultProps = {
  bacNumber: '',
};

export default CustomerWithInjector;
