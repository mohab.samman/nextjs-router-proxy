import { render, screen } from 'utils/test-utils';

import Customer from '.';

describe('Customer', () => {
  test('renders Customer container', () => {
    render(<Customer bacNumber="BAC_111_112_427_645" />);

    expect(screen.getByTestId('container')).toBeInTheDocument();
  });
});
