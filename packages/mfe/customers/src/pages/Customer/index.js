import { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from '@material-ui/core';

import MasterData from '../../components/MasterData';
import { getCustomerByBACNumber } from '../../redux/CustomerSlice';

const Customer = ({ bacNumber }) => {
  const customer = useSelector((state) => state.customer);
  const dispatch = useDispatch();

  const isDataEmpty = !Object.keys(customer?.data ?? {}).length;

  useEffect(() => {
    if (bacNumber) {
      dispatch(getCustomerByBACNumber(bacNumber));
    }
  }, [bacNumber]);

  return (
    <Container disableGutters maxWidth={false} data-testid="container">
      {isDataEmpty ? null : <MasterData customer={customer?.data} data-testid="retail" />}
    </Container>
  );
};

Customer.propTypes = {
  bacNumber: PropTypes.string,
};

Customer.defaultProps = {
  bacNumber: '',
};

export default Customer;
