import { useState } from 'react';
import { IntlProvider } from 'react-intl';
import { ThemeProvider, StylesProvider, jssPreset } from '@material-ui/core/styles';
import { create } from 'jss';
import CssBaseline from '@material-ui/core/CssBaseline';
import StyledEngineProvider from '@material-ui/core/StyledEngineProvider';
import { Provider } from 'react-redux';
import theme from 'styles/themes';

import Customer from './Customer';
import { LocaleContext, Locale, getShortLocale } from '../utils/localeContext';
import store from '../redux/store';

const jss = create({ plugins: [...jssPreset().plugins] });

const App = () => {
  const [lang, setLanguage] = useState(Locale.lang);
  const [messages] = useState({});

  return (
    <LocaleContext.Provider value={{ lang, setLanguage }}>
      <IntlProvider
        messages={messages}
        locale={getShortLocale(lang)}
        defaultLocale={getShortLocale(Locale.lang)}
      >
        <StyledEngineProvider injectFirst>
          <StylesProvider jss={jss}>
            <ThemeProvider theme={theme}>
              <Provider store={store}>
                <CssBaseline />
                <Customer />
              </Provider>
            </ThemeProvider>
          </StylesProvider>
        </StyledEngineProvider>
      </IntlProvider>
    </LocaleContext.Provider>
  );
};

export default App;
