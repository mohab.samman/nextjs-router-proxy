import { createSlice } from '@reduxjs/toolkit';

import { searchByBacNumber } from '../api/customer';

const initialState = {
  bac_number: '',
  error: {},
  isLoading: false,
  data: {},
};

const handleLoadingStart = (state) => ({
  ...state,
  isLoading: true,
});

const handleCustomerSuccess = (state, action) => ({
  ...state,
  error: initialState.error,
  isLoading: initialState.isLoading,
  data: action.payload,
});

const handleCustomerFailed = (state, action) => ({
  ...state,
  isLoading: initialState.isLoading,
  data: initialState.data,
  error: {
    errorCode: action.payload.errorCode,
    errorMessage: action.payload.error.message,
  },
});

const handleBacNumber = (state, action) => ({
  ...state,
  bac_number: action.payload,
});

const CustomerSlice = createSlice({
  name: 'customer',
  initialState,
  reducers: {
    setCustomerFailed: handleCustomerFailed,
    setCustomerSuccess: handleCustomerSuccess,
    setBacNumber: handleBacNumber,
    setLoadingStart: handleLoadingStart,
  },
});

export const {
  setCustomerFailed,
  setCustomerSuccess,
  setBacNumber,
  setLoadingStart,
} = CustomerSlice.actions;

export default CustomerSlice.reducer;

export const getCustomerByBACNumber = (bacNumber) => async (dispatch) => {
  try {
    dispatch(setLoadingStart());
    dispatch(setBacNumber(bacNumber));
    const customer = await searchByBacNumber(bacNumber);
    dispatch(setCustomerSuccess(customer));
  } catch (err) {
    dispatch(setCustomerFailed(err));
  }
};
