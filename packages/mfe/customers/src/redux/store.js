import { combineReducers, configureStore } from '@reduxjs/toolkit';

import CustomerSlice from './CustomerSlice';

const reducers = {
  customer: CustomerSlice,
};
const rootReducer = combineReducers(reducers);

const store = configureStore({
  reducer: rootReducer,
});

export default store;
