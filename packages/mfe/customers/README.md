# Microfrontend-Application

Micro-Frontend basic template.

Please read the Wiki documentation for more information about the basic concepts:
[Raisin Wiki Page: Frontend Composition Strategy](https://raisin-jira.atlassian.net/wiki/x/TABOGQ)

# Pre-requesites

- Node v12.18.3 or higher.
- NPM v6.14.6 or higher.
- Yarn v1.22.4 or higher.

# Usage

- To run/start the project run.

  ```
  $ yarn start
  ```

- To build the project run.

  ```
  $ yarn build
  ```

- To run the tests.

  ```
  $ yarn test
  ```

- To run the test on watch mode.

  ```
  $ yarn test:watch
  ```

- To run eslint on the project.
  ```
  $ yarn lint
  ```

These are the available scripts:

```
"start": "cross-env NODE_ENV=development webpack-dev-server --config ./webpack/webpack.development.js",
"build": "cross-env NODE_ENV=production webpack --config ./webpack/webpack.production.js",
"lint": "eslint --ext .ts,.tsx,.js,.jsx --no-error-on-unmatched-pattern src/",
"lint:fix": "eslint --ext .ts,.tsx,.js,.jsx --no-error-on-unmatched-pattern --quiet --fix",
"pretest": "yarn test:clean && yarn lint",
"test:clean": "rimraf ./coverage",
"test": "cross-env NODE_ENV=test jest --config jest.config.js --coverage",
"test:watch": "cross-env NODE_ENV=test jest --watchAll",
"prettify": "prettier . --write"
```

# Getting Started

1. Install project dependencies:

   ```
   $ yarn install
   ```

2. Update the config file as follow. `<root>/config/index.js`

   ```
    const { includePathFromSrc } = require('../webpack/paths');

    // Variables from process.env when application is running in Amplify pipeline
    const { APP_SLUG, AWS_BRANCH, AWS_APP_ID, APP_NAME, PORT, HOST, MF_NAME } = process.env;

    // Application constants
    const WMF_NAME = MF_NAME || 'customers';
    const NAME = APP_NAME || 'customers';
    const APP_PORT = PORT || 3003;
    const APP_HOST = HOST || 'localhost';

    // Amplify environements
    const environments = {
      staging: {
        publicPath: `https://mfe-${WMF_NAME}.testraisin.com/`,
        apiBaseUrl: 'https://cas.testweltsparen.de/v1/',
      },
      onboarding: {
        publicPath: `https://mfe-${WMF_NAME}.onboarding-raisin.com/`,
        apiBaseUrl: 'https://customer-administration-service-onboarding.raisin-onboarding.network/v1/',
      },
      production: {
        publicPath: `https://mfe-${WMF_NAME}.raisin.com/`,
        apiBaseUrl: 'https://banking.weltsparen.de/v1/',
      },
    };

    const config = {
      name: NAME,
      port: APP_PORT,
      host: APP_HOST,

      moduleFederation: {
        name: WMF_NAME,
        exposes: {
          './Customer': includePathFromSrc('pages/Customer/withInjector.js'),
        },
        remotes: {},
      },

      globals: {
        apiBaseUrl: JSON.stringify(
          environments[AWS_BRANCH]?.apiBaseUrl ??
            'https://customer-administration-service-development.raisin-dev.network/v1/',
        ),
      },
      publicPath:
        environments[AWS_BRANCH]?.publicPath ?? `https://${APP_SLUG}.${AWS_APP_ID}.amplifyapp.com/`,
    };

    module.exports.config = config;
   ```

3. Start the project running the command.
   ```
   $ yarn start
   ```

# Expose/Export assets through Webpack Module Federation (Plugin).

To expose any type of assets or element using the MF plugin, first check that your local project runs properly locally. After this, you can share/expose any asset or element in the following way.

1. Open the `<root>/config/index.js` file, should look like this:

   ```
   ...
   const config = {
     ...
     moduleFederation: {
       exposes: {},
       remotes: {},
     }
   };
   ...
   ```

2. Edit the file and add the list or assets inside the `exposes` object:

   ```
   ...
   const config = {
     ...
     moduleFederation: {
       exposes: {
           './<asset name>': includePathFromSrc('path/of/your/element'),
           './<asset name>': includePathFromSrc('path/of/your/element'),
         },
         remotes: {},
     },
   };
   ...
   ```

> NOTE: Please be sure to add always a dot and slash before the asset name. e.g `./MyButton` and the elements to share will be always inside `src/` directory.

3. Restart the application.

4. To confirm that the assets are properly exposed through the MF plugin, the project should run properly locally and see if the module was created properly run the URL.

   ```
   http://localhost:<port>/js/remoteEntry.js
   ```

   > NOTE: The port is the same value that you set in the `.env` file and the remote entry file should load without any problem. For more details please check the [How to ensure that the remote application is exposing the module?](https://raisin-jira.atlassian.net/wiki/spaces/ENGPEOP/pages/424542284/Frontend+Composition+Strategy#How-to-ensure-that-the-remote-application-is-exposing-the-module%3F)

# Consume/Import assets through Webpack Module Federation (Plugin).

1. Confirm that the project is running properly locally.
2. Open the `<root>/config/index.js` file, should look like this:

   ```
   ...
   const config = {
     ...
     moduleFederation: {
       exposes: {},
       remotes: {},
     }
   };
   ...
   ```

3. Edit the file and add inside the `remotes` object the reference to the assets you need to import.

   ```
   ...
   const config = {
     ...
     moduleFederation: {
       exposes: {},
       remotes: {
         <module federation name>: '<module federation name>@<URL>:<port>/remoteEntry.js'
       },
     },
   };
   ...
   ```

> NOTE: The module federation name, is the name used for the other micro-frontend application inside the `.env` file, the variable name is `MF_NAME`.
> In case you are pointing to the production URL, you need to use the `module federation name`, domain and the remoteEntry.js, like this: `mfApp1: 'mfApp1@<production URL>/remoteEntry.js'`

4. Restart the application and confirm the application is still running properly.
5. To start using the remote entry in your application go to the file that you need to import the assets and import it in the following way

   ```
   const <asset name> = React.lazy(() => import('mfApp1/<asset name>'));
   ```

   > Remember: `mfApp1` is the value that you assign to the remote entry in your application inside the `webpack/moduleFederation.js` file in the remotes section.
   > For more details please check [How to know that the host application is consuming the remote module?](https://raisin-jira.atlassian.net/wiki/spaces/ENGPEOP/pages/424542284/Frontend+Composition+Strategy#How-to-know-that-the-host-application-is-consuming-the-remote-module%3F)

# Deployment

Currently, we are using AWS Amplify for deployments and setting it up as a monorepo. For monorepo setup, Amplify expects a single amplify.yml file at root folder of the repository in which we can mention all project which needs deployment. But due to some reason AWS Amplify is not behaving as expected.

So for now, we are putting amplify.yml file inside each project (packages/\*\*') and you need to specify these files manually if you are setting up Amplify yourself.
