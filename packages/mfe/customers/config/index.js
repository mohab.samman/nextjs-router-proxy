const { includePathFromSrc } = require('../webpack/paths');
const utils = require('./utils');

/**
 * ENV Variables
 * The variables values are comming from the deployment pipeline not local development.
 * vairables provide different values and based on this the different environments are deployed.
 * The variables have now values when run locally
 */
const { APP_SLUG, AWS_BRANCH, AWS_APP_ID, APP_NAME, PORT, HOST, MF_NAME } = process.env;

/**
 * Application Variables
 * These variables are constant values that the application use to be able to run locally and in the GitLab pipeline.
 */
const WMF_NAME = MF_NAME || 'customers';
const NAME = APP_NAME || 'customers';
const APP_PORT = PORT || 3003;
const APP_HOST = HOST || 'localhost';
const REMOTE_AUTH = 'auth';
const REMOTE_AUTH_PORT = 3001;
const REMOTE_CAP = 'cap';
const REMOTE_CAP_PORT = 3002;

const ENVIRONMENT = utils.getEnvironment(AWS_BRANCH);

/**
 * Environments
 * There are 5 environments where the application runs:
 * Staging, Onboarding, Production and Development (locally).
 * For each environment there are different, values, urls, endpoints and domains.
 * The Staging, Onboarding, Production and Development are the necessary configuration to deploy the project with AWS Amplofy.
 * the Local environment is use only to run the project locally.
 */
const environments = {
  // LOCAL
  local: {
    authRemote: `${REMOTE_AUTH}@http://localhost:${REMOTE_AUTH_PORT}/${REMOTE_AUTH}/remoteEntry.js`,
    capRemote: `${REMOTE_CAP}@http://localhost:${REMOTE_CAP_PORT}/${REMOTE_CAP}/remoteEntry.js`,

    apiBaseUrl: {
      DEU: 'https://internal-api.dev-weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.dev-raisin.co.uk/cas/v1/',
    },

    publicPath: '/',
  },

  // DEVELOPMENT
  development: {
    authRemote: `${REMOTE_AUTH}@https://${APP_SLUG}.${utils.getApplicationId(
      REMOTE_AUTH,
    )}.amplifyapp.com/${REMOTE_AUTH}/remoteEntry.js`,
    capRemote: `${REMOTE_CAP}@https://${APP_SLUG}.${utils.getApplicationId(
      REMOTE_CAP,
    )}.amplifyapp.com/${REMOTE_CAP}/remoteEntry.js`,

    apiBaseUrl: {
      DEU: 'https://internal-api.dev-weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.dev-raisin.co.uk/cas/v1/',
    },

    publicPath: `https://${APP_SLUG}.${AWS_APP_ID}.amplifyapp.com/`,
  },

  // STAGING
  staging: {
    authRemote: `${REMOTE_AUTH}@https://mfe-${REMOTE_AUTH}.testraisin.com/${REMOTE_AUTH}/remoteEntry.js`,
    capRemote: `${REMOTE_CAP}@https://${APP_SLUG}.${utils.getApplicationId(
      REMOTE_CAP,
    )}.amplifyapp.com/${REMOTE_CAP}/remoteEntry.js`,

    apiBaseUrl: {
      DEU: 'https://internal-api.testweltsparen.de/cas/v1/',
      GBR: 'https://internal-api.testraisin.co.uk/cas/v1/',
    },

    publicPath: `https://mfe-${WMF_NAME}.testraisin.com/`,
  },

  // ONBOARDING
  onboarding: {
    authRemote: `${REMOTE_AUTH}@https://mfe-${REMOTE_AUTH}.onboarding-raisin.com/${REMOTE_AUTH}/remoteEntry.js`,
    capRemote: `${REMOTE_CAP}@https://${APP_SLUG}.${utils.getApplicationId(
      REMOTE_CAP,
    )}.amplifyapp.com/${REMOTE_CAP}/remoteEntry.js`,

    apiBaseUrl: {
      DEU: 'https://internal-api.onboarding-weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.onboarding-raisin.co.uk/cas/v1/',
    },

    publicPath: `https://mfe-${WMF_NAME}.onboarding-raisin.com/`,
  },

  // PRODUCTION
  production: {
    authRemote: `${REMOTE_AUTH}@https://mfe-${REMOTE_AUTH}.raisin.com/${REMOTE_AUTH}/remoteEntry.js`,
    capRemote: `${REMOTE_CAP}@https://${APP_SLUG}.${utils.getApplicationId(
      REMOTE_CAP,
    )}.amplifyapp.com/${REMOTE_CAP}/remoteEntry.js`,

    apiBaseUrl: {
      DEU: 'https://internal-api.weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.raisin.co.uk/cas/v1/',
    },

    publicPath: `https://mfe-${WMF_NAME}.raisin.com/`,
  },
};

/**
 * Config
 * The config object is the one use for the project. This is use to run
 * the project locally and is the same configuration for run the pipeline.
 * The object has the  following section:
 * name: Name of the application,
 * port: Port where the application will run (locally),
 * host: URL for the application, localhost for development,
 * moduleFederation: Object for the Webpack module federation plugin,
 * globals: Object use for inject global values to the application.
 * publicPath: Path for the Webpack output file.
 */
const config = {
  name: NAME,
  port: APP_PORT,
  host: APP_HOST,

  moduleFederation: {
    name: WMF_NAME,
    exposes: {
      './Customer': includePathFromSrc('pages/Customer/withInjector.js'),
      './CustomerTranslations': includePathFromSrc('services/Translations/index.js'),
    },
    remotes: {
      auth: utils.getModuleFederationRemote(REMOTE_AUTH, environments, ENVIRONMENT),
      cap: utils.getModuleFederationRemote(REMOTE_CAP, environments, ENVIRONMENT),
    },
  },

  globals: {
    apiBaseUrlGBR: environments[ENVIRONMENT].apiBaseUrl.GBR,
    apiBaseUrlDEU: environments[ENVIRONMENT].apiBaseUrl.DEU,
  },

  publicPath: environments[ENVIRONMENT].publicPath,
};

module.exports.config = config;
