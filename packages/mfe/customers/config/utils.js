const fs = require('fs');

const { AWS_APP_ID } = process.env;

const getApplicationId = (applicationName) => {
  const amplifyAppsJson = './amplify_apps.json';

  try {
    const data = fs.readFileSync(amplifyAppsJson, 'utf8');
    const applications = JSON.parse(data);
    return applications.find((app) => app.name === applicationName)?.appId;
  } catch (e) {
    return console.error(`${amplifyAppsJson} not found`);
  }
};

const getModuleFederationRemote = (mfName, environments, environment) => {
  const applicationId = getApplicationId(mfName);

  // If not application Id but the AWS_APP_ID has a value that means is running in Amplify but failed to get apps.
  if (AWS_APP_ID && !applicationId) {
    throw new Error(`Amplify app '${mfName}' not found.`);
  }

  // If not application Id and env variable AWS_APP_ID is undefined, then is running locally
  if (!applicationId && !AWS_APP_ID) {
    return environments.local[`${mfName}Remote`];
  }

  // Return the remote from the following environemts stating/onboarding/production/development
  return environments[environment][`${mfName}Remote`];
};

const getEnvironment = (awsBranch) => {
  const environment = {
    staging: 'staging',
    onboarding: 'onboarding',
    production: 'production',
  };

  if (environment[awsBranch]) {
    return awsBranch;
  }

  return AWS_APP_ID ? 'development' : 'local';
};

module.exports = {
  getApplicationId,
  getModuleFederationRemote,
  getEnvironment,
};
