export default {
  id: 'BAC_111_113_879_888',
  legacyId: {
    partnerNo: '41008',
  },
  status: 'ACTIVE',
  type: 'PERSONAL',
  distributorId: 'Raisin',
  registrationDate: '2021-04-26',
  company: null,
  persons: [],
  _embedded: {
    persons: [
      {
        personId: '2768783',
        firstName: 'Brigitte',
        lastName: 'Schönenberger',
        birthDate: '2001-07-05',
        username: '2041008000',
        personalDetails: {
          academicTitleCode: 'academic_title_prof_dr_dr',
          courtesyTitleCode: 'courtesy_title_mr',
          firstName: 'Brigitte',
          lastName: 'Schönenberger',
          birthName: null,
          birthDate: '2001-07-05',
          birthPlace: 'Thomasburg',
          birthCountryCode: 'DEU',
          nationalityPrimary: 'DEU',
          nationalitySecondary: null,
          gender: 'gender_male',
          maritalStatus: 'marital_status_widowed',
          profession: 'profession_houseman',
          industry: null,
        },
        address: {
          street: 'Schlehdornstr. Crossing',
          streetNumber: '2',
          additionalInformation: null,
          city: 'Sagafeburg',
          postalCode: '05558',
          countryCode: 'DEU',
        },
        contactDetails: {
          email: 'mark.wilder-1619421450290@test.com',
          phoneNumber: '+491616281650',
        },
        taxDetails: [
          {
            taxIdentificationNumber: '88136250741',
            countryCode: 'DEU',
            precedence: 0,
          },
        ],
        roles: [
          {
            id: '1',
            name: 'ACCOUNT_HOLDER',
            status: 'ACTIVE',
          },
        ],
      },
    ],
    referenceAccounts: [
      {
        accountId: '2768796',
        accountHolder: 'Brigitte Schönenberger',
        bankName: 'Landesbank Berlin - Berliner Sparkasse',
        bic: 'BELADEBEXXX',
        iban: 'DE08100500001234528804',
        accountNumber: '1234528804',
        branchCode: null,
        ccy: 'EUR',
      },
    ],
  },
};
