const { basename } = require('path');
const execa = require('execa');
const { red, magentaBright, whiteBright, yellowBright } = require('chalk');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CopyPlugin = require('copy-webpack-plugin');

const { dependencies, devDependencies } = require('../package.json');
const { includePathFromAssets, includePathFromNodeModules } = require('./paths');

/**
 * Check the dependencies and devDependencies
 * in the package.json and validate if the @raisin/ui-lib exists
 */
const isUILibInstalled = () =>
  Object.keys({ ...dependencies, ...devDependencies }).includes('@raisin/ui-lib');

/**
 *  Update the weboackBaseConfig loaders in case
 *  the @raisin/ui-lib package is installed
 * @param {Object} webpackBaseConfig
 */
const updateWebpackLoaders = (webpackConfig) => {
  const baseConfig = { ...webpackConfig };

  // Update babel-loader
  baseConfig.module.rules[0].include.push(includePathFromNodeModules('@raisin/ui-lib/src'));

  // Update css-loader
  baseConfig.module.rules[1].include.push(includePathFromNodeModules('@raisin'));

  // Update SASS and SCSS Loader
  baseConfig.module.rules[2].use.splice(2, 0, {
    loader: 'resolve-url-loader',
    options: {
      root: includePathFromNodeModules('@raisin/ui-lib/src/styles/fonts'),
    },
  });

  // Update Fonts Loader
  baseConfig.module.rules[3].include.push(
    includePathFromNodeModules('@raisin/ui-lib/src/styles/fonts/fonts'),
  );

  return baseConfig;
};

/**
 * Add BundleAnalyzerPlugin to the webpack plugin array
 * in case this is set to true
 * @param {Object} webpackConfig
 */
const addBundleAnalyzerPlugin = (webpackConfig) => {
  webpackConfig.plugins.push(new BundleAnalyzerPlugin());
  return webpackConfig;
};

const updateWebpackPlugin = (webpackConfig) => {
  webpackConfig.plugins.push(
    new CopyPlugin({
      patterns: [
        {
          from: includePathFromNodeModules('@raisin/ui-lib/src/styles/fonts/fonts/'),
          to: includePathFromAssets('fonts/'),
        },
      ],
    }),
  );
  return webpackConfig;
};

const copyFontsFromUILib = async () => {
  const files = [
    includePathFromNodeModules('@raisin/ui-lib/src/styles/fonts/fonts/ws-iconset.svg'),
    includePathFromNodeModules('@raisin/ui-lib/src/styles/fonts/fonts/ws-iconset.ttf'),
    includePathFromNodeModules('@raisin/ui-lib/src/styles/fonts/fonts/ws-iconset.woff'),
  ];

  files.forEach(async (file, index) => {
    try {
      if (index === 0) {
        await execa('cp', [file, includePathFromAssets('images')]);
      } else {
        await execa('cp', [file, includePathFromAssets('fonts')]);
      }
    } catch (e) {
      console.error(red.bold('\n  ERROR:'));
      console.error(
        whiteBright(
          `  Failed to copy ${magentaBright(basename(file))} file from ${yellowBright(
            '@raisin/ui-lib/src/styles/fonts/fonts',
          )} to ${yellowBright('./src/assets/fonts')}\n`,
        ),
      );
      process.exit(1);
    }
  });
};

/**
 * Update the webpack initial configuration in case the
 * BundleAnalyzerPlugin is set to true or @raisin/ui-lib is installed
 * @param {Object} webpackBaseConfig
 */
const updateWebpackConfig = (webpackBaseConfig) => {
  let webpackConfig = webpackBaseConfig;

  // Is package.json config analyze enabled
  if (process.env.npm_package_config_analyze === 'true') {
    webpackConfig = addBundleAnalyzerPlugin(webpackConfig);
  }

  if (isUILibInstalled()) {
    copyFontsFromUILib();
    webpackConfig = updateWebpackLoaders(webpackConfig);
    webpackConfig = updateWebpackPlugin(webpackConfig);
  }

  return webpackConfig;
};

module.exports = {
  updateWebpackConfig,
};
