const { join, resolve } = require('path');

// Set paths
const APP_DIR = resolve(process.cwd(), 'src');
const ASSETS_PATH = resolve(process.cwd(), 'src/assets');
const BUILD_DIR = resolve(process.cwd(), 'dist');
const NODE_MODULES = resolve(process.cwd(), 'node_modules');
const PUBLIC_PATH = resolve(process.cwd(), 'public');

// Set functions
const includePathFromAssets = (relativePath) => join(ASSETS_PATH, relativePath);
const includePathFromDist = (relativePath) => join(BUILD_DIR, relativePath);
const includePathFromNodeModules = (relativePath) => join(NODE_MODULES, relativePath);
const includePathFromPublic = (relativePath) => join(PUBLIC_PATH, relativePath);
const includePathFromSrc = (relativePath) => join(APP_DIR, relativePath);

module.exports = {
  APP_DIR,
  ASSETS_PATH,
  BUILD_DIR,
  includePathFromAssets,
  includePathFromDist,
  includePathFromNodeModules,
  includePathFromPublic,
  includePathFromSrc,
  PUBLIC_PATH,
};
