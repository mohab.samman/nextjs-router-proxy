const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');

const webpackBaseConfig = require('./webpack.config');
const { includePathFromSrc } = require('./paths');
const { updateWebpackConfig } = require('./utils');
const { config } = require('../config');

const webpackProduction = merge(updateWebpackConfig(webpackBaseConfig), {
  entry: {
    app: includePathFromSrc('index.js'),
  },

  output: {
    publicPath: config.publicPath,
  },

  mode: 'production',

  devtool: 'source-map',

  performance: {
    hints: 'warning',
    maxAssetSize: 200 * 1024, // 150 KiB
    maxEntrypointSize: 200 * 1024, // 150 KiB
  },

  plugins: [
    new CompressionPlugin({
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
  ],

  optimization: {
    minimize: true,

    sideEffects: true,

    concatenateModules: true,

    minimizer: [
      new TerserPlugin({
        terserOptions: {
          compress: {
            comparisons: false,
          },
          mangle: true,
        },
      }),
    ],
  },
});

module.exports = webpackProduction;
