const webpack = require('webpack');
const { merge } = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');

const webpackBaseConfig = require('./webpack.config');
const { BUILD_DIR, includePathFromSrc } = require('./paths');
const { updateWebpackConfig } = require('./utils');
const { config } = require('../config');

const webpackDevelopment = merge(updateWebpackConfig(webpackBaseConfig), {
  entry: {
    app: ['core-js/stable', 'regenerator-runtime/runtime', includePathFromSrc('index.js')],
  },

  output: {
    publicPath: `http://${config.host}:${config.port}/`,
  },

  mode: 'development',

  devtool: 'source-map',

  cache: {
    type: 'memory',
  },

  performance: {
    hints: false,
  },

  devServer: {
    historyApiFallback: true,
    contentBase: BUILD_DIR,
    port: config.port,
    host: config.host,
    compress: true,
    open: true,
  },

  plugins: [
    new CleanWebpackPlugin({ cleanStaleWebpackAssets: false }),

    new webpack.HotModuleReplacementPlugin(),

    new CircularDependencyPlugin({
      exclude: /a\.js|node_modules/,
      failOnError: false,
    }),
  ],
});

module.exports = webpackDevelopment;
