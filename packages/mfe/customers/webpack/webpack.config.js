const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const dotenv = require('dotenv');

const { dependencies } = require('../package.json');
const { config } = require('../config');
const {
  BUILD_DIR,
  includePathFromPublic,
  includePathFromAssets,
  includePathFromSrc,
} = require('./paths');

const { ModuleFederationPlugin } = webpack.container;
dotenv.config();

module.exports = {
  name: config.name,

  output: {
    uniqueName: config.name,
    filename: `${config.name}/[name].[contenthash].js`,
    chunkFilename: `${config.name}/[name].[contenthash].chunk.js`,
    path: BUILD_DIR,
  },

  resolve: {
    mainFields: ['browser', 'main', 'module'],
    extensions: ['.js', '.jsx', '.json', '.scss'],
    alias: {
      components: includePathFromSrc('components'),
      fonts: includePathFromAssets('fonts'),
      images: includePathFromAssets('images'),
      pages: includePathFromSrc('pages'),
      routes: includePathFromSrc('routes'),
      styles: includePathFromSrc('styles'),
    },
  },

  stats: {
    chunks: true,
    modules: false,
    chunkModules: false,
    chunkOrigins: true,
    children: false,
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        options: {
          rootMode: 'upward',
        },
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          'style-loader',
          // {
          //   loader: MiniCssExtractPlugin.loader, // There is current issue with this plugin  Webpack 5 - https://github.com/webpack-contrib/mini-css-extract-plugin/issues/487
          //   // enable HMR only in dev
          //   options: {
          //     hmr: NODE_ENV === 'development',
          //   },
          // },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: { localIdentName: '[name]_[local]___[contenthash:base64:5]' },
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      {
        test: /\.(eot?.+|svg?.+|ttf?.+|otf?.+|woff?.+|woff2?.+)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'assets/fonts/[name]-[contenthash].[ext]',
            },
          },
        ],
        include: [includePathFromAssets('fonts')],
        exclude: [includePathFromAssets('images')],
      },
      {
        test: /\.(png|gif|jp(e*)g|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8 * 1024,
              name: 'assets/images/[name]-[contenthash].[ext]',
            },
          },
        ],
        include: [includePathFromAssets('images')],
        exclude: [includePathFromAssets('fonts')],
      },
    ],
  },

  plugins: [
    new webpack.ProgressPlugin(),
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin(
      Object.keys(config.globals).reduce(
        (globals, key) => ({ ...globals, [key]: JSON.stringify(config.globals[key]) }),
        {},
      ),
    ),

    new MiniCssExtractPlugin({
      filename: 'styles/[name].[contenthash].css',
      chunkFilename: 'styles/[id].[contenthash].css',
    }),

    new OptimizeCSSAssetsPlugin(),

    new HtmlWebpackPlugin({
      template: includePathFromPublic('index.html'),
      scriptLoading: 'defer',
      title: config.name,
      favicon: includePathFromPublic('favicon.ico'),
      manifest: includePathFromPublic('manifest.json'),
    }),

    new ModuleFederationPlugin({
      name: config.moduleFederation.name,
      filename: `${config.moduleFederation.name}/remoteEntry.js`,
      exposes: config.moduleFederation.exposes,
      remotes: config.moduleFederation.remotes,
      shared: {
        ...dependencies,
        '@material-ui/styles': {
          singleton: true,
        },
        '@emotion/core': {
          singleton: true,
        },
        '@emotion/styled': {
          singleton: true,
        },
        '@material-ui/core': {
          singleton: true,
        },
      },
    }),
  ],

  optimization: {
    moduleIds: 'deterministic',

    splitChunks: {
      chunks: 'async',
      minSize: 20 * 1024,
      minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      enforceSizeThreshold: 50 * 1024,

      cacheGroups: {
        default: false,

        vendor: {
          name: 'vendor',
          test: /[\\/]node_modules[\\/]/,
          chunks: 'async',
          reuseExistingChunk: true,
          enforce: true,
        },

        common: {
          name: 'common',
          test: /[\\/]src[\\/]/,
          chunks: 'async',
          minSize: 0,
          enforce: true,
        },

        styles: {
          name: 'styles',
          test: /\.css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },
};
