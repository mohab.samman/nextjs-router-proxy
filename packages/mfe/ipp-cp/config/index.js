/**
 * Application Variables
 * These variables are used to run the project locally and in the GitLab pipeline.
 */

const { APP_LOCALE, STACK_API_KEY, STACK_DELIVERY_TOKEN, STACK_ENVIRONMENT } = process.env;

const NAME = 'ipp-cp';
const MFE_NAME = 'ipp-cp';
const APP_PORT = '3004';
const APP_HOST = 'localhost';
const ENVIRONMENT = 'local';
const LOCALE = APP_LOCALE || 'de-DE';

/**
 * Environments
 * These are the environments where the application runs:
 * For each environment there are different values, urls, endpoints and domains.
 * Staging, Onboarding and Production env configs are necessary to deploy the project with the GitLab pipeline.
 * Development env config is used only to run the project locally.
 */
const environments = {
  local: {
    publicPath: '/',

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  development: {
    publicPath: `https://mfe-${MFE_NAME}.amplifyapp.com/`,

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  staging: {
    publicPath: `https://mfe-${MFE_NAME}.testraisin.com/`,

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  onboarding: {
    publicPath: `https://mfe-${MFE_NAME}.onboarding-raisin.com/`,

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  production: {
    publicPath: `https://mfe-${MFE_NAME}.raisin.com/`,

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
};

/**
 * Configuration
 * The config object is used internally for the project.
 * The object has the  following section:
 * name: µFE name
 * port: On what port should the project run locally
 * host: URL for the application, localhost for development,
 * globals: Object used for injecting global values into the application.
 */
const config = {
  name: NAME,
  port: APP_PORT,
  host: APP_HOST,

  globals: {
    contentStack: environments[ENVIRONMENT].contentStack,
    locale: LOCALE,
  },
  publicPath: environments[ENVIRONMENT].publicPath,
};

module.exports.config = config;
