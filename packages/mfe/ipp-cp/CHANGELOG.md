# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.3.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/ipp-cp@0.3.1...@raisin/ipp-cp@0.3.2) (2021-07-27)

**Note:** Version bump only for package @raisin/ipp-cp





## 0.3.1 (2021-07-13)

**Note:** Version bump only for package @raisin/ipp-cp





# [0.3.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/ipp@0.2.0...@raisin/ipp@0.3.0) (2021-07-09)


### Features

* **IPP:** downgraded node to 14.x for cypress tests ([7d36138](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7d3613835d56d01bc912bf7aa2ebffd2d324866b))





# 0.2.0 (2021-07-06)


### Features

* **IPP:** Create initial boilerplate ([11df539](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/11df53944cf9237ea0416bf1944bf20d0e563c13))
