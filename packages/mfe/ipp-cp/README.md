# App name placeholder\_

_add App description_

## Pre-requisites

_add App pre-requisites_

## Usage

To run/start the project:

```
$ yarn start
```

To build the project:

```
$ yarn build
```

To run the tests:

```
$ yarn test
```

To run the test on watch mode:

```
$ yarn test:watch
```

To run eslint on the project.

```
$ yarn lint
```

To see the list the rest of available scripts, check `package.json`

# Getting Started

1. Install project dependencies (if not installed already):

   ```
   $ yarn install
   ```

2. Create a new `.env` file in the root of the project and add the values to make possible the Content Stack request.

   ```
   // .env

    STACK_API_KEY = <Content Stack Key>
    STACK_DELIVERY_TOKEN = <Content Stack Token>
    STACK_ENVIRONMENT = <Content Stack Environment could be "staging | production">
    APP_LOCALE = <de_DE | en_US | en_IE | en_GB | fr_FR | de_AT | es_ES | nl_NL>
   ```

3. Start the project running the command.

   ```
   $ yarn start
   ```

4. If applicable, add the name of the micro-frontend to corresponding web portal in `portalsConfig.mjs` file located in [`frontend-monorepo` repository](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo). This step will enable you to automatically start the micro-frontend when running the web portal locally. For more info check `fe-monorepo` [README.md](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/-/blob/master/README.md)
## How to create a resource on Transifex for MFE

- make sure you are into the app directory
- run `yarn tx:extract` to extract translation keys to `default.json`
- run `tx push -s` to push the source file for the first time. (in case you don't have transifex cli locally installed yet, see how to install it [here](https://docs.transifex.com/client/installing-the-client))
  PS: in the console, you'll see a message saying the resource doesn't exist and it's being created.

## How to Update translations for MFE

- make sure you are into the app directory
- add translations into code with `id` and `defaultMessage`. Optionally, a `description` can be added as well to provide context.
- run `yarn tx:extract` to extract translation keys to `default.json`
- run `yarn tx:push` to push extracted keys to trasifex
- update translations for required languages on transifex
- run `yarn tx:pull` to pull updated translations from transifex.
- run `yarn tx:compile` to compile pulled translations.
- please make sure updated/pulled translations are commited in source control.
## Setup / Run Lighthouse locally.

To run Lighthouse locally, you will first need to install the Lighthouse CI tool globally, by running `npm install -g @lhci/cli@0.7.x`.

Now, you can run `lhci` commands. Run `lhci autorun` command to run Lighthouse on your Micro-Frontend.

This will either output all warnings and errors (if there are any) or a success message in your terminal. It will also generate a report in `.lighthouseci` folder.
