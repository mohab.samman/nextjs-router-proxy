import React, { FC } from 'react';
import 'jest-styled-components';
import { render } from '@testing-library/react';
import { IntlProvider } from 'react-intl';

import getMessages from './getMessages';
import { DEFAULT_LOCALE_LABEL, LOCALES } from './localeContext';

const getTranslationMessages = async (locale: string) => {
  const hostMessages = await getMessages(locale);

  return hostMessages;
};

const Providers: FC = ({ children }) => {
  const messages: any = getTranslationMessages(LOCALES['de-DE'].code);

  return (
    <IntlProvider
      locale={LOCALES['de-DE'].label}
      defaultLocale={DEFAULT_LOCALE_LABEL}
      messages={messages}
    >
      {children}
    </IntlProvider>
  );
};

const customRender = (ui, options = {}) => render(ui, { wrapper: Providers, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
