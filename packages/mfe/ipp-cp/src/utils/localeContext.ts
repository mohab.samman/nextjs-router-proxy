import { createContext } from 'react';

// global variable defined in config/index.js provided by amplify via env vars
declare const locale: string;

export const LOCALES = {
  // Germany
  'de-DE': {
    code: 'de_DE',
    label: 'de',
    notation: 'de-de',
  },
};

export const Locale = {
  language: LOCALES[locale],
};

export const DEFAULT_LOCALE_LABEL = LOCALES['de-DE'].label;

export const LocaleContext = createContext(Locale);
