import React from 'react';
import { StylesProvider, jssPreset } from '@material-ui/styles';
import StyledEngineProvider from '@material-ui/core/StyledEngineProvider';
import { create } from 'jss';

const jss = create({ plugins: [...jssPreset().plugins] });

interface IStyledTheme {
  children?: React.ReactNode;
}

const StyledTheme: React.FC<IStyledTheme> = ({ children }) => (
  <StyledEngineProvider injectFirst>
    <StylesProvider jss={jss}>{children}</StylesProvider>
  </StyledEngineProvider>
);

export default StyledTheme;
