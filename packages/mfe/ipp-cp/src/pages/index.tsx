import React from 'react';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import { NextPage, GetStaticProps } from 'next';
import MainComponent from '../components/MainPageComponent';
import { DEFAULT_LOCALE_LABEL } from '../utils/localeContext';
import { fetchTranslations } from '../utils/i18n';

interface IHome {
  messages: Record<string, string> | Record<string, MessageFormatElement[]>;
  language: { label: string };
}

const Home: NextPage<IHome> = ({ messages, language }) => (
  <IntlProvider messages={messages} locale={language.label} defaultLocale={DEFAULT_LOCALE_LABEL}>
    <MainComponent />
  </IntlProvider>
);

export const getStaticProps: GetStaticProps = async () => {
  const { messages, language } = await fetchTranslations();

  return {
    props: { messages, language },
  };
};

export default Home;
