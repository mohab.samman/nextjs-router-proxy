import React from 'react';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import { NextPage, GetStaticProps } from 'next';
import UseCase1 from '../components/UseCase1';
import { DEFAULT_LOCALE_LABEL } from '../utils/localeContext';
import { fetchTranslations } from '../utils/i18n';

interface IPage1 {
  messages: Record<string, string> | Record<string, MessageFormatElement[]>;
  language: { label: string };
}

const Page1: NextPage<IPage1> = ({ messages, language }) => (
  <IntlProvider messages={messages} locale={language.label} defaultLocale={DEFAULT_LOCALE_LABEL}>
    <UseCase1 />
  </IntlProvider>
);

export const getStaticProps: GetStaticProps = async () => {
  const { messages, language } = await fetchTranslations();

  return {
    props: { messages, language },
  };
};

export default Page1;
