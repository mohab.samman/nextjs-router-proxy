import React from 'react';
import Head from 'next/head';
import StyledTheme from 'utils/styledTheme';
import '@fontsource/open-sans';

function App({ Component, pageProps }) {
  return (
    <StyledTheme>
      <Head>
        <title>Welcome to Raisin</title>
        <meta name="description" content="add description" />
      </Head>
      <Component {...pageProps} />
    </StyledTheme>
  );
}

export default App;
