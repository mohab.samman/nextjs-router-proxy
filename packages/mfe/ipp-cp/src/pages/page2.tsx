import React from 'react';
import axios from 'axios';
import { NextPage, GetStaticProps } from 'next';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import UseCase2 from '../components/UseCase2';
import { DEFAULT_LOCALE_LABEL } from '../utils/localeContext';
import { fetchTranslations } from '../utils/i18n';

interface IPage2 {
  userData: {
    id: number;
    email: string;
  }[];
  messages: Record<string, string> | Record<string, MessageFormatElement[]>;
  language: { label: string };
}

const Page2: NextPage<IPage2> = ({ messages, language, userData }) => (
  <IntlProvider messages={messages} locale={language.label} defaultLocale={DEFAULT_LOCALE_LABEL}>
    <UseCase2 userData={userData} />
  </IntlProvider>
);

export const getStaticProps: GetStaticProps = async () => {
  const { messages, language } = await fetchTranslations();
  const req = await axios('https://reqres.in/api/users?page=2');

  return {
    props: { messages, language, userData: req.data.data },
  };
};

export default Page2;
