import createClient from './interceptors';

const BASE_URL = '/';

const exampleClient = createClient(BASE_URL);

export const getExample = (userId) => exampleClient.get('/example', { params: userId });

export const postExample = (username, password) =>
  exampleClient.post('/user', { username, password });
