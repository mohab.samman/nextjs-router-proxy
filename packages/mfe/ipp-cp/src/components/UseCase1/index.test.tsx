import React from 'react';
import { waitFor } from '@testing-library/react';
import { render } from 'utils/test-utils'; // from our RTL's custom render util

import UseCase1 from '.';

describe('<UseCase1 />', () => {
  it('renders <UseCase1> text', async () => {
    const { getByTestId } = render(<UseCase1 />);
    const text = await waitFor(() => getByTestId('home-text'));

    expect(text).toBeInTheDocument();
  });
});
