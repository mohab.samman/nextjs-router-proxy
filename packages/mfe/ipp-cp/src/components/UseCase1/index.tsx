import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import Image from 'next/image';

import * as S from './styles';
import messages from './messages';
/**
 *
 * SSG with local data fetching (translations)
 */

const UseCase1: React.FC<WrappedComponentProps> = ({ intl }) => (
  <S.OuterContainer>
    <S.Logo>
      <Image src="/assets/images/logo.svg" alt="logo" width={100} height={100} />
    </S.Logo>
    <S.H1 data-cy="app-title">{intl.formatMessage(messages.PageTitle)}</S.H1>
    <S.InlineText data-testid="home-text">
      {intl.formatMessage(messages.PageDescription)}
    </S.InlineText>
  </S.OuterContainer>
);

export default injectIntl(UseCase1);
