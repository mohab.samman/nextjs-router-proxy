import React, { useState, useEffect } from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import Image from 'next/image';
import axios from 'axios';
import * as S from './styles';
import messages from './messages';

/**
 *
 * Hybrid SSG page sample
 */

type User = {
  avatar: string;
  email: string;
};

const UseCase3: React.FC<WrappedComponentProps> = ({ intl }) => {
  const [userData, setUserData] = useState([]);
  useEffect(() => {
    axios('https://reqres.in/api/users?page=2').then((json) => {
      setUserData(json.data.data);
    });
  }, []);

  const RemoteDataComponent = () => (
    <S.List>
      {userData.map((item: User, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <S.ListItem key={i}>
          <S.ImageContainer>
            <Image src={item.avatar} alt="user" width={100} height={100} />
          </S.ImageContainer>
          {item.email}
        </S.ListItem>
      ))}
    </S.List>
  );

  return (
    <S.OuterContainer>
      <S.Logo>
        <Image src="/assets/images/logo.svg" alt="logo" width={100} height={100} />
      </S.Logo>
      <S.H1 data-cy="app-title">{intl.formatMessage(messages.title)}</S.H1>
      <S.InlineText data-testid="home-text">
        {intl.formatMessage(messages.description)}
      </S.InlineText>
      {userData && <RemoteDataComponent />}
    </S.OuterContainer>
  );
};

export default injectIntl(UseCase3);
