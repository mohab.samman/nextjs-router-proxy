import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'UseCase3.title',
    defaultMessage: 'I am a hybrid page',
  },
  description: {
    id: 'UseCase3.description',
    defaultMessage: 'Data is fetched during render time',
  },
});

export default messages;
