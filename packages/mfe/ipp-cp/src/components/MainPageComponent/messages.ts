import { defineMessages } from 'react-intl';

const messages = defineMessages({
  MainTitle: {
    id: 'Index.MainTitle',
    defaultMessage: 'Welcome to your Nextjs based generator',
  },
  MainPage1: {
    id: 'Index.MainPage1',
    defaultMessage: 'Link to SSG page that prefetches local data (translations)',
  },
  MainPage2: {
    id: 'Index.MainPage2',
    defaultMessage: 'Link to SSG page that prefetches remote and local data at build time.',
  },
  MainPage3: {
    id: 'Index.MainPage3',
    defaultMessage:
      'Link to static page that both prefetches data at build time (translations), and at render time via client-side rendering.',
  },
});

export default messages;
