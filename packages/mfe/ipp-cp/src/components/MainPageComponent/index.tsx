import React from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import Image from 'next/image';
import Link from 'next/link';

import * as S from './styles';
import messages from './messages';

const MainComponent: React.FC<WrappedComponentProps> = ({ intl }) => (
  <S.OuterContainer>
    <S.Logo>
      <Image src="/assets/images/logo.svg" alt="logo" width={100} height={100} />
    </S.Logo>
    <S.H1 data-cy="app-title">{intl.formatMessage(messages.MainTitle)}</S.H1>
    <S.List>
      <S.ListItem>
        <Link href="/page1" passHref>
          <a href="replace">{intl.formatMessage(messages.MainPage1)}</a>
        </Link>
      </S.ListItem>
      <S.ListItem>
        <Link href="/page2" passHref>
          <a href="replace">{intl.formatMessage(messages.MainPage2)}</a>
        </Link>
      </S.ListItem>
      <S.ListItem>
        <Link href="/page3" passHref>
          <a href="replace">{intl.formatMessage(messages.MainPage3)}</a>
        </Link>
      </S.ListItem>
    </S.List>
  </S.OuterContainer>
);

export default injectIntl(MainComponent);
