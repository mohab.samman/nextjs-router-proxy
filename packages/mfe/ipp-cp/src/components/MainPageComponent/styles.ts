import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';

export const OuterContainer = withTheme(styled.div`
  width: 100%;
  height: 1200px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  box-sizing: border-box;
  text-align: center;
  background: url(/assets/images/plp_gradient_bg.svg) no-repeat 50% / cover;
  font-family: Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
`);

export const Logo = withTheme(styled.div`
  position: absolute;
  right: 120px;
  bottom: 10px;
`);

export const H1 = withTheme(styled.h1`
  font-size: 5em;
  font-weight: 600;
  color: #ffffff;
`);

export const List = withTheme(styled.ol`
  color: #fff;
  text-align: left;
`);

export const ListItem = withTheme(styled.li`
  margin: 5px 0;
  > a {
    color: #fff;
  }
`);
