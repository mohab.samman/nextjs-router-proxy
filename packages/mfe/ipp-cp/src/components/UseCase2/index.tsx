import React from 'react';
import Image from 'next/image';
import { injectIntl, WrappedComponentProps } from 'react-intl';
import * as S from './styles';
import messages from './messages';

/**
 *
 * Pure SSG page
 * Data and translations are prefetched
 *
 */
interface IUseCase2 extends WrappedComponentProps {
  userData: {
    id: number;
    email: string;
  }[];
}

const UseCase2: React.FC<IUseCase2> = ({ intl, userData }) => {
  const RemoteDataComponent = () => (
    <S.List>
      <S.ListItem>
        <S.H4>{intl.formatMessage(messages.dataHeading)}</S.H4>
      </S.ListItem>
      {userData.map((item, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <S.ListItem key={i}>
          <S.H4>
            <span>{item.id}</span>
            {item.email}
          </S.H4>
        </S.ListItem>
      ))}
    </S.List>
  );

  return (
    <S.OuterContainer>
      <S.Logo>
        <Image src="/assets/images/logo.svg" alt="logo" width={100} height={100} />
      </S.Logo>
      <S.H1 data-cy="app-title">{intl.formatMessage(messages.title)}</S.H1>
      <S.InlineText data-testid="home-text">
        {intl.formatMessage(messages.description)}
      </S.InlineText>
      {userData && <RemoteDataComponent />}
    </S.OuterContainer>
  );
};

export default injectIntl(UseCase2);
