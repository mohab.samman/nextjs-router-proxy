import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';

export const Wrapper = styled.div`
  text-align: center;
`;

export const OuterContainer = withTheme(styled.div`
  width: 100%;
  height: 1200px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  box-sizing: border-box;
  text-align: center;
  background: url(/assets/images/plp_gradient_bg.svg) no-repeat 50% / cover;
  font-family: Open Sans, Helvetica Neue, Helvetica, Arial, sans-serif;
`);

export const Logo = withTheme(styled.div`
  position: absolute;
  right: 120px;
  bottom: 10px;
`);

export const H1 = withTheme(styled.h1`
  font-size: 5em;
  font-weight: 600;
  color: #ffffff;
`);

export const InlineText = withTheme(styled.span`
  font-size: 2.5em;
  color: #ffffff;
`);

export const H4 = withTheme(styled.h4`
  font-size: 2em;
  font-weight: 400;
  color: #ffffff;

  > span {
    margin-right: 20px;
  }
`);

export const List = withTheme(styled.ul`
  list-style: none;
`);

export const ListItem = withTheme(styled.li`
  font-size: 1em;
  font-weight: 400;
  color: #ffffff;
`);
