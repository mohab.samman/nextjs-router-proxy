import { defineMessages } from 'react-intl';

const messages = defineMessages({
  title: {
    id: 'UseCase2.title',
    defaultMessage: 'I am pure SSG page',
  },
  description: {
    id: 'UseCase2.description',
    defaultMessage: 'Everything is prefetched (translations and remote data)',
  },
  dataHeading: {
    id: 'UseCase2.dataHeading',
    defaultMessage: 'Data is prefetched from a remote api',
  },
});

export default messages;
