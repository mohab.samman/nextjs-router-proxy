const { config } = require('./config');

module.exports = {
  roots: ['<rootDir>/src'],
  verbose: true,
  globals: config.globals,
  testURL: 'http://localhost',
  testMatch: [
    '<rootDir>/src/**/__tests__/**/*.{js,jsx,ts,tsx}',
    '<rootDir>/src/**/*.{spec,test}.{js,jsx,ts,tsx}',
  ],

  testPathIgnorePatterns: ['/node_modules/', '/cypress/', '.cache/Cypress', '/.next/'],
  setupFiles: ['raf/polyfill'],
  setupFilesAfterEnv: ['<rootDir>/jest/setupJest.js'],
  modulePaths: ['<rootDir>/src', '<rootDir>/node_modules'],
  transformIgnorePatterns: ['/node_modules/(?!lodash|@raisin|react-intl)'],
  moduleDirectories: ['node_modules', 'src'],
  moduleNameMapper: {
    '^.+\\.(jpg|jpeg|png|gif|eot|webp|svg|ttf|woff|woff2)$': '<rootDir>/jest/fileMock.js',
    '^jest/(.*)$': '<rootDir>/jest/$1',
  },

  moduleFileExtensions: [
    'web.js',
    'js',
    'web.ts',
    'ts',
    'web.tsx',
    'tsx',
    'json',
    'web.jsx',
    'jsx',
    'node',
  ],

  coverageReporters: ['html', 'text-summary', 'lcov'],
  collectCoverage: true,
  collectCoverageFrom: ['<rootDir>/src/**/*.{js,jsx,ts,tsx}', '!<rootDir>/src/**/*.d.ts'],
  watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
  reporters: ['default', 'jest-junit'], // collects coverage for gitlab
  testResultsProcessor: 'jest-junit',
  watchPathIgnorePatterns: ['node_modules'],
};
