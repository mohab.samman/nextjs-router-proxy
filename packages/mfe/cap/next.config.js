const customConfig = require('./config').config;

module.exports = {
  future: {
    webpack5: true,
  },
  pageExtensions: ['page.tsx'],
  webpack: (config, { webpack }) => {
    config.plugins.push(
      new webpack.DefinePlugin(
        Object.keys(customConfig.globals).reduce(
          (globals, key) => ({ ...globals, [key]: JSON.stringify(customConfig.globals[key]) }),
          {},
        ),
      ),
    );

    return config;
  },
};
