# CAP\_

_Customer Administrator Portal_

## Pre-requisites

_add App pre-requisites_

## Usage

To run/start the project in monorepo root folder:

```
$ yarn start:cap
```

To build the project:

```
$ yarn build:prod
```

To run the tests:

```
$ yarn test
```

To run the test on watch mode:

```
$ yarn test:watch
```

To run eslint on the project.

```
$ yarn lint
```

To see the list the rest of available scripts, check `package.json`

# Getting Started

1. Install project dependencies (if not installed already):

   ```
   $ yarn install
   ```

2. Create a new `.env` file in the root of the project and add the values to make possible the Content Stack request.

   ```
   // .env

    STACK_API_KEY = <Content Stack Key>
    STACK_DELIVERY_TOKEN = <Content Stack Token>
    STACK_ENVIRONMENT = <Content Stack Environment could be "staging | production">
    APP_LOCALE = <de_DE | en_US | en_IE | en_GB | fr_FR | de_AT | es_ES | nl_NL>
    OKTA_USERNAME=<Okta username>
    OKTA_PASSWORD=<Okta password>
    OKTA_KEY=<Okta key from okta profile>
   ```

   Note: Okta keys only required if you need to run cypress automation tests

3. Start the project running the command.

   ```
   $ yarn start
   ```

4. If applicable, add the name of the micro-frontend to corresponding web portal in `portalsConfig.mjs` file located in [`frontend-monorepo` repository](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo). This step will enable you to automatically start the micro-frontend when running the web portal locally. For more info check `fe-monorepo` [README.md](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/-/blob/master/README.md)

## Setup / Run Lighthouse locally.

To run Lighthouse locally, you will first need to install the Lighthouse CI tool globally, by running `npm install -g @lhci/cli@0.7.x`.

Now, you can run `lhci` commands. Run `lhci autorun` command to run Lighthouse on your Micro-Frontend.

This will either output all warnings and errors (if there are any) or a success message in your terminal. It will also generate a report in `.lighthouseci` folder.
