import React, { useState, useEffect, MouseEvent } from 'react';
import { useIntl } from 'react-intl';
import { List, ListItem, Tooltip } from '@material-ui/core';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import InboxIcon from '@material-ui/icons/Inbox';
import { currentSession } from '@raisin/auth';

import LanguageSetting from '../LanguageSetting';
import SessionControls from '../SessionControls';
import messages from './messages';
import * as S from './styles';

const ProfileSettings = ({ isLeftDrawerOpen }: { isLeftDrawerOpen: boolean }) => {
  const intl = useIntl();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [currentUsername, setCurrentUsername] = useState('');

  useEffect(() => {
    const getUser = async () => {
      try {
        const user = await currentSession();

        const username = user.getIdToken().payload.identities[0].userId;

        setCurrentUsername(username);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log('error in fetching current session', error);
      }
    };
    getUser();
  }, []);

  const handleClick = (event: MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'profile-popover' : undefined;

  return (
    <S.ProfileSettings>
      <S.ProfilePopover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        marginThreshold={6}
        anchorOrigin={{
          vertical: -8,
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
      >
        <div style={{ margin: 20 }}>
          <Tooltip title={currentUsername}>
            <S.ProfileHeading>{currentUsername}</S.ProfileHeading>
          </Tooltip>
          <S.ProfileSubHeading>{intl.formatMessage(messages.SubHeading)}</S.ProfileSubHeading>
        </div>
        <S.MidDivider variant="middle" />
        <List component="nav" aria-label="profile options" disablePadding>
          <ListItem button>
            <S.MenuItemIcon>
              <InboxIcon />
            </S.MenuItemIcon>
            <S.MenuItemText primary={intl.formatMessage(messages.Settings)} />
          </ListItem>
          <ListItem button>
            <S.MenuItemIcon>
              <InboxIcon />
            </S.MenuItemIcon>
            <S.MenuItemText
              primary={
                // eslint-disable-next-line react/jsx-wrap-multilines
                <S.SignOutButton
                  data-testid="signOutButton"
                  label={intl.formatMessage(messages.Logout)}
                />
              }
            />
          </ListItem>
        </List>
        <LanguageSetting />
      </S.ProfilePopover>

      {isLeftDrawerOpen ? (
        <S.ProfileButton
          aria-describedby={id}
          onClick={handleClick}
          endIcon={
            open ? (
              <S.ExpandMoreProfileIcon />
            ) : (
              <S.ExpandLessProfileIcon />
              // eslint-disable-next-line react/jsx-indent
            )
          }
        >
          <S.SessionWrapper>
            <Tooltip title={currentUsername || ''}>
              <S.TooltipText>{currentUsername}</S.TooltipText>
            </Tooltip>
            <SessionControls />
          </S.SessionWrapper>
        </S.ProfileButton>
      ) : (
        <S.MoreIconButton onClick={handleClick}>
          <MoreHorizIcon />
        </S.MoreIconButton>
      )}
    </S.ProfileSettings>
  );
};

export default ProfileSettings;
