import { defineMessages } from 'react-intl';

const messages = defineMessages({
  Settings: {
    id: 'Profile.settings.label',
    defaultMessage: 'Settings',
  },
  Logout: {
    id: 'Profile.logout.label',
    defaultMessage: 'Logout',
  },
  Loading: {
    id: 'ProfileSettings.loading.label',
    defaultMessage: 'Loading',
  },
  SubHeading: {
    id: 'ProfileSettings.subheading.label',
    defaultMessage: 'Logged in as Admin',
  },
});

export default messages;
