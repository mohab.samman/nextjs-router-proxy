import styled from 'styled-components';
import {
  Button,
  ListItemIcon,
  Typography,
  ListItemText,
  Divider,
  Popover,
  IconButton,
} from '@material-ui/core';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { SignOutBtn } from '@raisin/auth';

import Colors from '../../styles/themes/colors';
import { fontWeight } from '../../styles/themes/typography';

export const ProfileSettings = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ProfileHeading = styled(Typography)`
  color: ${Colors.black20};
  margin-bottom: 5px;
  display: block;
  width: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

export const ProfilePopover = styled(Popover)`
  & .MuiPopover-paper {
    width: 224px;
    background-color: ${Colors.black};
    border-radius: 8px;
    color: ${Colors.black20};
  }
`;

export const ProfileSubHeading = styled(Typography)`
  color: ${Colors.black20};
  font-size: 12px;
`;

export const MenuItemIcon = styled(ListItemIcon)`
  color: ${Colors.black20};
  min-width: 36px;
`;

export const MenuItemText = styled(ListItemText)`
  color: ${Colors.black20};
`;

export const MidDivider = styled(Divider)`
  border-color: ${Colors.black20};
`;

export const ProfileButton = styled(Button)`
    width: 100%;
    border-radius: 0;
    background-color: ${Colors.black};
    height: 72px;
    text-align: left;
}`;

export const MoreIconButton = styled(IconButton)`
  color: ${Colors.black20};
  min-width: 36px;
`;

export const TooltipText = styled.span`
  display: block;
  width: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

export const SessionWrapper = styled.span`
  width: 100%;
  padding: 10px;
`;

export const ExpandMoreProfileIcon = styled(ExpandMoreIcon)`
  margin-right: 10px;
`;

export const ExpandLessProfileIcon = styled(ExpandLessIcon)`
  margin-right: 10px;
`;

export const SignOutButton = styled(SignOutBtn)`
  padding: 0;
  justify-content: left;
  height: auto;
  font-size: 1rem;
  color: ${Colors.black20};
  font-weight: ${fontWeight.medium};
`;
