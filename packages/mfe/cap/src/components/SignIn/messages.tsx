import { defineMessages } from 'react-intl';

const messages = defineMessages({
  SignInLabel: {
    id: 'SignIn.signInUsingOkta.label',
    defaultMessage: 'Sign In using Okta',
  },
  SessionExpired: {
    id: 'SignIn.timeout.label',
    defaultMessage:
      'Your session has expired and you have been automatically logged out. Please log in again.',
  },
});

export default messages;
