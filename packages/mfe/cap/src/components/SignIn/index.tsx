import React from 'react';
import { Container, Grid } from '@material-ui/core';
import { useIntl } from 'react-intl';
import { SignInBtn } from '@raisin/auth';

import Logo from '../Icons/Logo';
import messages from './messages';
import * as S from './styles';

const SignIn = () => {
  const intl = useIntl();

  return (
    <Container disableGutters>
      <S.StyledContainer container>
        <S.LogoWrapper item data-testid="logo">
          <Logo />
        </S.LogoWrapper>
        <Grid item>
          <SignInBtn label={intl.formatMessage(messages.SignInLabel)} />
        </Grid>
      </S.StyledContainer>
    </Container>
  );
};

export default SignIn;
