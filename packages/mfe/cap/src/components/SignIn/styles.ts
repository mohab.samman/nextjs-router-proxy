import styled from 'styled-components';
import { Grid } from '@material-ui/core';

export const LogoWrapper = styled(Grid)`
  margin-bottom: 24px;
`;

export const StyledContainer = styled(Grid)`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
`;
