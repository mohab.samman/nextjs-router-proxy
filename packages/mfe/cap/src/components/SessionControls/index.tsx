import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';

import { signOut, currentSession } from '@raisin/auth';

import messages from './messages';
import * as S from './styles';

const DELAY = 60 * 60 * 1000; // 60 minutes TODO for dev only
const REFRESH_EVENTS = ['locationchange', 'hashchange', 'click', 'touchstart', 'scroll'];
const COUNTDOWN_KEY = 'countdown';
export const TIMEOUT_ERROR_KEY = 'timeout_error';

const SessionControls = () => {
  const intl = useIntl();
  const [countdown, setCountdown] = useState<number>(0);
  const router = useRouter();

  let tick: number;

  const displayCountdown = () => {
    const isoTimestamp = new Date(countdown).toISOString();

    return countdown > 0
      ? ` ${isoTimestamp.substr(14, 2)}m ${isoTimestamp.substr(17, 2)}s`
      : '--:--';
  };

  const resetCountdown = () => {
    if (typeof window !== 'undefined') {
      const newCountdown = Date.now() + DELAY;
      localStorage.setItem(COUNTDOWN_KEY, newCountdown.toString());
      setCountdown(newCountdown - Date.now());
    }
  };

  const updateCountdown = () => {
    if (typeof window !== 'undefined') {
      const storedTime = localStorage.getItem(COUNTDOWN_KEY);
      let currentCountdown: number = 0;
      if (storedTime !== null) {
        currentCountdown = parseInt(storedTime, 10);
      }

      setCountdown(Number(currentCountdown) - Date.now());

      if (Number(currentCountdown) < Date.now()) {
        localStorage.setItem(TIMEOUT_ERROR_KEY, 'true');
        clearInterval(tick);
        currentSession()
          .then(() => {
            signOut();
          })
          .catch(() => {
            router.push('/');
          });
      }
    }
  };

  useEffect(() => {
    if (typeof window !== 'undefined') {
      resetCountdown();
      tick = window.setInterval(updateCountdown, 300);
    }

    return () => clearInterval(tick);
  }, []);

  useEffect(() => {
    REFRESH_EVENTS.forEach((event) => window.addEventListener(event, resetCountdown));
    return () =>
      REFRESH_EVENTS.forEach((event) => window.removeEventListener(event, resetCountdown));
  }, []);

  return (
    <S.SessionCounter>
      {intl.formatMessage(messages.SessionCounter)}
      {displayCountdown()}
    </S.SessionCounter>
  );
};

export default SessionControls;
