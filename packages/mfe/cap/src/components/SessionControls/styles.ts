import styled from 'styled-components';
import { Typography } from '@material-ui/core';

import Colors from '../../styles/themes/colors';

export const SessionCounter = styled(Typography)`
  color: ${Colors.black20};
  font-size: 12px;
`;
