import React from 'react';
import { screen } from '@testing-library/react';
import { render } from 'utils/test-utils';

import SessionControls from '.';

describe('SessionControls', () => {
  test('renders SessionControls component', () => {
    render(<SessionControls />);

    expect(screen.getByText(/Logout in:/i)).toBeInTheDocument();
  });
});
