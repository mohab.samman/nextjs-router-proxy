import { defineMessages } from 'react-intl';

const messages = defineMessages({
  SessionCounter: {
    id: 'Timeout.countdown.label',
    defaultMessage: 'Logout in:',
  },
});

export default messages;
