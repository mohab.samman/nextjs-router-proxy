import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useRouter } from 'next/router';
import { Divider, List, Tooltip, ListItem, IconButton } from '@material-ui/core';
import {
  Menu as MenuIcon,
  ChevronLeft as ChevronLeftIcon,
  People as PeopleIcon,
} from '@material-ui/icons';

import ProfileSettings from '../ProfileSettings';
import messages from './messages';
import routes from '../../utils/routes';
import Logo from '../Icons/Logo';
import * as S from './styles';

interface IMenu {
  id: string;
  label: string;
  icon: React.ReactNode;
  url: string;
}
interface IMenuItems extends IMenu {
  subitems?: Array<IMenu>;
}
const getMenuItems = (intl: any): Array<IMenuItems> => [
  {
    id: 'customer_management',
    label: intl.formatMessage(messages.CustomerManagement),
    icon: <PeopleIcon />,
    url: routes.customerManagement,
  },
];

const MiniDrawer = ({ children }: { children: React.ReactNode }) => {
  const intl = useIntl();
  const router = useRouter();
  const [open, setOpen] = useState(true);
  const [selectedMenuItem, setSelectedMenuItem] = useState('customer_management');

  const menuItems = getMenuItems(intl);

  const goTo = (item: any) => {
    setSelectedMenuItem(item.id);
    if (item.url) {
      router.push(item.url);
    }
  };

  return (
    <S.Wrapper>
      <S.MiniDrawer variant="permanent" open={open} data-testid="drawer">
        <S.Toolbar>
          {open && <Logo height={32} colored={false} />}

          {open ? (
            <IconButton onClick={() => setOpen(false)}>
              <ChevronLeftIcon color="secondary" />
            </IconButton>
          ) : (
            <S.BurgerIconButton aria-label="open drawer" onClick={() => setOpen(true)} edge="start">
              <MenuIcon color="secondary" />
            </S.BurgerIconButton>
          )}
        </S.Toolbar>
        <Divider />
        <S.MenuWrapper>
          <div>
            {menuItems.map((item: IMenuItems) => (
              <List component="div" key={item.id} disablePadding>
                <List key={item!.id} disablePadding>
                  <ListItem
                    button
                    onClick={() => goTo(item)}
                    selected={selectedMenuItem === item.id}
                  >
                    <S.ListMenuItemIcon>{item.icon}</S.ListMenuItemIcon>

                    {open && (
                      <Tooltip title={item.label}>
                        <S.MenuItemText noWrap>{item.label}</S.MenuItemText>
                      </Tooltip>
                    )}
                  </ListItem>
                </List>
              </List>
            ))}
          </div>
          <div>
            <ProfileSettings isLeftDrawerOpen={open} />
          </div>
        </S.MenuWrapper>
      </S.MiniDrawer>
      <S.Content>{children}</S.Content>
    </S.Wrapper>
  );
};

export default MiniDrawer;
