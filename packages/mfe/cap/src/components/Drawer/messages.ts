import { defineMessages } from 'react-intl';

const messages = defineMessages({
  CustomerManagement: {
    id: 'Drawer.customerManagement.label',
    defaultMessage: 'Customer management',
  },
});

export default messages;
