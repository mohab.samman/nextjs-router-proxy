import styled, { css } from 'styled-components';
import {
  Grid,
  ListItemIcon,
  Typography,
  Drawer,
  IconButton,
  ListItemButton,
} from '@material-ui/core';
import { withTheme } from '@material-ui/styles';

import Colors from '../../styles/themes/colors';

const drawerWidth = '240px';

export const Wrapper = styled.div`
  display: flex;
`;

export const Toolbar = withTheme(styled('div')`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${(props) => props.theme.spacing(0, 2)};
  height: 100px;
  background-color: ${(props) => props.theme.palette.background.toolbar};
`);

export const StyledContainer = styled(Grid)`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const MenuWrapper = styled('div')`
  height: 100vh;
  display: flex;
  margin-top: 24px;
  justify-content: space-between;
  flex-direction: column;
  overflow-x: hidden;
`;

export const ListMenuItemIcon = styled(ListItemIcon)`
  color: ${Colors.black20};
  min-width: 36px;
`;

export const Content = withTheme(styled('main')`
  flex-grow: 1;
  padding: ${(props) => props.theme.spacing(4)};
`);

export const MenuItemText = styled(Typography)`
  color: ${Colors.black20};
`;

const drawerOpen = css`
  width: ${drawerWidth};
  transition: ${({ theme }) =>
    theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    })};
`;

const drawerClose = css`
  width: ${(props) => `calc(${props.theme.spacing(7)} + 1px)`};
  transition: ${({ theme }) =>
    theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    })};
`;

export const MiniDrawer = withTheme(styled(Drawer)`
  .MuiDrawer-root {
    flex-shrink: 0;
    white-space: nowrap;
    box-sizing: border-box;
    overflowx: hidden;
  }

  ${({ open }) => (open ? drawerOpen : drawerClose)}

  .MuiDrawer-paper {
    background-color: ${(props) => props.theme.palette.background.drawer};
    ${({ open }) => (open ? drawerOpen : drawerClose)};
  }
`);

export const BurgerIconButton = styled(IconButton)`
  margin-left: 0px;
`;

export const SubMenuListItem = styled(ListItemButton)`
  padding-left: ${(props) => props.theme.spacing(8)};
`;
