import React from 'react';
import { waitFor, screen } from '@testing-library/react';
import { render } from '../../utils/test-utils';

import MiniDrawer from '.';

describe('<MiniDrawer />', () => {
  it('renders MiniDrawer', async () => {
    render(
      <MiniDrawer>
        <p>children</p>
      </MiniDrawer>,
    );
    const drawer = await waitFor(() => screen.getByTestId('drawer'));
    expect(drawer).toBeInTheDocument();
  });
});
