import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import { SnackbarContent } from '@material-ui/core';

export const SnackbarContentWrapper = withTheme(styled(SnackbarContent)`
  font-size: 20px;
  background-color: ${(props) => {
    const { theme, variant } = props;
    let color;
    switch (variant as string) {
      case 'success':
        color = theme.palette.success.main;
        break;

      case 'error':
        color = theme.palette.error.main;
        break;

      case 'info':
        color = theme.palette.info.main;
        break;

      case 'warning':
        color = theme.palette.warning.main;
        break;

      default:
        color = theme.palette.info.main;
    }
    return color;
  }};

  .MuiSnackbarContent-message {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`);

export const Message = withTheme(styled('div')`
  display: flex;
  align-items: center;
  font-size: 1rem;
`);

export const Icon = withTheme(styled('span')`
  font-size: 20px;
  margin-right: ${(props) => props.theme.spacing(1)};
  margin-top: ${(props) => props.theme.spacing(1)};
`);
