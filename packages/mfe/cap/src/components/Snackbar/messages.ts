import { defineMessages } from 'react-intl';

const messages = defineMessages({
  INVALID_REQUEST_PAYLOAD: {
    id: 'components.StatusMessage.INVALID_REQUEST_PAYLOAD',
    defaultMessage: 'A technical error occurred. The search term might contain invalid characters.',
  },
  RESOURCE_NOT_FOUND: {
    id: 'components.StatusMessage.RESOURCE_NOT_FOUND',
    defaultMessage: 'The requested data was not found',
  },
  UNEXPECTED_ERROR: {
    id: 'components.StatusMessage.UNEXPECTED_ERROR',
    defaultMessage: 'An unexpected error occurred while processing the request',
  },
  UNAUTHORIZED: {
    id: 'components.StatusMessage.UNAUTHORIZED',
    defaultMessage: 'Authorization is required to access this resource.',
  },
  Logout: {
    id: 'Snackbar.signout.label',
    defaultMessage: 'Logout',
  },
});

export default messages;
