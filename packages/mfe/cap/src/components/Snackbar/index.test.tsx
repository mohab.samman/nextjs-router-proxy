import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { render } from 'utils/test-utils';

import Snackbar from '.';

const props = {
  statusCode: 'INVALID_REQUEST_PAYLOAD',
  variant: 'info',
  isOpen: false,
  onClose: () => {},
};

describe('Snackbar', () => {
  test('renders Snackbar component', () => {
    render(<Snackbar {...props} />);
  });

  test('Snackbar is triggered and showed default value for "INVALID_REQUEST_PAYLOAD"', () => {
    render(<Snackbar {...props} isOpen />);
    const snackbarEle = screen.getByText(
      /A technical error occurred. The search term might contain/i,
    );
    expect(snackbarEle).toBeInTheDocument();
  });

  test('Close snackbar', () => {
    const onClose = jest.fn();

    const localProps = {
      ...props,
      isOpen: true,
      onClose,
    };

    render(<Snackbar {...localProps} />);

    const button = screen.getByLabelText('close');
    userEvent.click(button);

    expect(onClose).toHaveBeenCalledTimes(1);
  });
});
