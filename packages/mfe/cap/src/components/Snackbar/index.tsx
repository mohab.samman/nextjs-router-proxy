import React from 'react';
import { useIntl } from 'react-intl';
import { Snackbar as MuiSnackbar, IconButton, Button, SvgIconTypeMap } from '@material-ui/core';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';
import CloseIcon from '@material-ui/icons/Close';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';

import messages from './messages';
import * as S from './styles';

interface ISnackbar {
  isOpen: boolean;
  variant: string;
  statusCode: string;
  message?: string;
  onClose: (val: boolean) => void;
}
interface IVariantIcons {
  [index: string]: OverridableComponent<SvgIconTypeMap<{}, 'svg'>>;
}

const variantIcon: IVariantIcons = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const Snackbar = ({ isOpen, onClose, variant, statusCode, message }: ISnackbar) => {
  const intl = useIntl();
  const Icon = variantIcon[variant];

  const filteredKey =
    Object.keys(messages).find((item: string) => item === statusCode) || 'UNEXPECTED_ERROR';

  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    onClose(false);
  };

  return isOpen ? (
    <MuiSnackbar
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      open={isOpen}
      autoHideDuration={5000}
      onClose={handleClose}
    >
      <S.SnackbarContentWrapper
        variant={variant}
        message={
          // eslint-disable-next-line react/jsx-wrap-multilines
          <>
            <S.Icon>
              <Icon />
            </S.Icon>
            <S.Message data-testid="errorMessage">
              {/* @ts-ignore */}
              {messages[filteredKey] ? intl.formatMessage(messages[filteredKey]) : message}
            </S.Message>
          </>
        }
        action={
          filteredKey === 'UNAUTHORIZED' ? (
            <Button
              color="inherit"
              size="small"
              onClick={() => {
                window.location.href = window.location.origin;
              }}
            >
              {intl.formatMessage(messages.Logout)}
            </Button>
          ) : (
            [
              <IconButton
                key="close"
                aria-label="close"
                color="inherit"
                onClick={() => onClose(false)}
              >
                <CloseIcon />
              </IconButton>,
            ]
          )
        }
      />
    </MuiSnackbar>
  ) : null;
};

export default Snackbar;
