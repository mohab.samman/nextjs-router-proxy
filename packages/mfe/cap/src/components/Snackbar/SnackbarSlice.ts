import { createSlice } from '@reduxjs/toolkit';

type SliceState = { variant: string; isOpen: boolean; data: any }; // TODO fix any

const initialState: SliceState = {
  variant: '',
  isOpen: false,
  data: [],
};

const snackbarSlice = createSlice({
  name: 'snackbar',
  initialState,
  reducers: {
    triggerSnackbar: (state, action) => ({
      ...state,
      variant: action.payload.variant,
      isOpen: true,
      data: action.payload.data,
    }),
    onClose: () => ({
      ...initialState,
    }),
  },
});

export const { triggerSnackbar, onClose } = snackbarSlice.actions;

export default snackbarSlice.reducer;
