import styled from 'styled-components';
import { withTheme } from '@material-ui/styles';
import { alpha } from '@material-ui/core/styles';
import { IconButton, CircularProgress as CircularProgressIcon, InputBase } from '@material-ui/core';

import Colors from '../../styles/themes/colors';

export const Form = withTheme(styled('form')`
  display: flex;

  .MuiTextField-root {
    margin: ${(props) => props.theme.spacing(1)};
    width: 40ch;
  }
`);

export const SearchWrapper = withTheme(styled('div')`
  position: relative;
  background-color: ${(props) => alpha(props.theme.palette.common.white, 0.15)};
  &:hover: {
    background-color: ${(props) => alpha(props.theme.palette.common.white, 0.25)};
  }
  margin-left: 0px;
  margin-right: ${(props) => props.theme.spacing(1)};

  .MuiInputBase-root {
    height: 56px;
    border: none;
  }

  .MuiInputBase-input {
    padding: ${(props) => props.theme.spacing(2)};
    transition: ${(props) => props.theme.transitions.create('width')};
    width: 100%;

    ${(props) => props.theme.breakpoints.up('sm')} {
      width: 40ch;
    }
  }
`);

export const ClearIconButton = withTheme(styled(IconButton)`
  padding: ${(props) => props.theme.spacing(1)};
`);

export const SubmitWrapper = withTheme(styled('div')`
  display: flex;
  align-items: center;
  padding: ${(props) => props.theme.spacing(1)};
`);

export const SubmitButton = styled('div')`
  position: relative;
`;

export const CircularProgress = styled(CircularProgressIcon)`
  color: ${Colors.primaryLight};
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -12px;
  margin-left: -12px;
`;

export const SearchInput = withTheme(styled(InputBase)``);
