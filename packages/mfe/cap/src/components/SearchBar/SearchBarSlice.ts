import { createSlice } from '@reduxjs/toolkit';
import isEmpty from 'lodash/isEmpty';

import { searchCustomerByBac } from '../../api/customer';
import { triggerSnackbar, onClose } from '../Snackbar/SnackbarSlice';

type SliceState = { customers: any; bac_number: string; error: any; isLoading: boolean };

const initialState: SliceState = {
  customers: [],
  bac_number: '',
  error: {},
  isLoading: false,
};
interface Iperson {
  firstName: string;
  lastName: string;
  birthDate: string;
  username: string;
  personId: string;
  contactDetails: IContactDetails;
}
interface IContactDetails {
  email: string;
}
interface ICustomer {
  _embedded: {
    persons: Array<Iperson>;
  };
  id: string;
  status: string;
  distributorId: string;
}

interface IFormattedCustomer {
  bacNumber: string;
  firstName: string;
  lastName: string;
  emailAddress: string;
  birthdate: string;
  status: string;
  distributorId: string;
  username: string;
  personId: string;
}

const formatCustomerData = (customers: ICustomer[]) => {
  let formattedCustomers: Array<IFormattedCustomer> = [];
  if (customers.length) {
    formattedCustomers = customers.map((customer) => {
      // eslint-disable-next-line no-underscore-dangle
      const person = customer._embedded.persons[0];
      return {
        bacNumber: customer.id,
        firstName: person.firstName,
        lastName: person.lastName,
        emailAddress: person.contactDetails.email,
        birthdate: person.birthDate,
        status: customer.status,
        distributorId: customer.distributorId,
        username: person.username,
        personId: person.personId,
      };
    });
  }

  if (isEmpty(formattedCustomers)) {
    return [];
  }

  return formattedCustomers;
};

const searchResult = createSlice({
  name: 'searchList',
  initialState,
  reducers: {
    getSearchResultStart: (state) => ({ ...state, isLoading: true }),

    getSearchResultSuccess: (state, action) => ({
      ...state,
      isLoading: false,
      customers: formatCustomerData([action.payload]),
      error: {},
    }),
    getSearchResultFailed: (state, action) => ({
      ...state,
      isLoading: false,
      customers: [],
      error: {
        statusCode: action.payload.statusCode,
        errorDetails: action.payload.error,
      },
    }),
  },
});

export const {
  getSearchResultStart,
  getSearchResultSuccess,
  getSearchResultFailed,
} = searchResult.actions;

export default searchResult.reducer;

export const getCustomerByBac = (bacnumber: string) => async (
  dispatch: (arg0: { payload: any; type: string }) => void,
) => {
  try {
    dispatch(getSearchResultStart());
    const list = await searchCustomerByBac(bacnumber);
    dispatch(onClose());
    dispatch(getSearchResultSuccess(list));
  } catch (err) {
    dispatch(triggerSnackbar({ variant: 'error', data: err }));
    dispatch(getSearchResultFailed(err));
  }
};
