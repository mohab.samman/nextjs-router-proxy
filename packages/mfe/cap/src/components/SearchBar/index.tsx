import React, { useState, useRef, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { InputAdornment, Button } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import { useDispatch } from 'react-redux';

import { formatBac, validateBac } from '../../utils/bacFormattor';
import { getCustomerByBac } from './SearchBarSlice';
import messages from './messages';
import * as S from './styles';
interface ISearchBar {
  isLoading: boolean;
}

const SearchBar = ({ isLoading }: ISearchBar) => {
  const intl = useIntl();
  const [inputVal, setInputVal] = useState('');
  const [isDisabled, setIsDisabled] = useState(true);
  const dispatch = useDispatch();
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (inputRef && inputRef.current) inputRef.current.focus();
  }, [inputRef]);

  const onInputChange = (val: string) => {
    const newVal = formatBac(val);
    setInputVal(newVal);
    setIsDisabled(validateBac(newVal));
  };

  const handleClearClick = () => {
    setInputVal('');
    setIsDisabled(true);
  };

  const onSearchCustomer = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (inputVal) {
      dispatch(getCustomerByBac(inputVal));
    }
  };

  return (
    <S.Form noValidate autoComplete="off">
      <S.SearchWrapper>
        <S.SearchInput
          margin="none"
          inputRef={inputRef}
          endAdornment={
            // eslint-disable-next-line react/jsx-wrap-multilines
            <InputAdornment position="end">
              <S.ClearIconButton onClick={handleClearClick} disabled={!inputVal.length}>
                <ClearIcon />
              </S.ClearIconButton>

              <S.SubmitWrapper>
                <S.SubmitButton>
                  <Button
                    data-testid="submitButton"
                    variant="contained"
                    size="large"
                    role="button"
                    disabled={isDisabled || isLoading}
                    onClick={onSearchCustomer}
                  >
                    {intl.formatMessage(messages.SearchBarSubmit)}
                  </Button>
                  {isLoading && <S.CircularProgress size={24} />}
                </S.SubmitButton>
              </S.SubmitWrapper>
            </InputAdornment>
          }
          placeholder={intl.formatMessage(messages.SearchBarPlaceholder)}
          value={inputVal}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => onInputChange(e.target.value)}
          inputProps={{
            'aria-label': 'search with bac number',
            'data-testid': 'searchBacNumber',
            role: 'textbox',
          }}
        />
      </S.SearchWrapper>
    </S.Form>
  );
};

export default SearchBar;
