import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { render } from 'utils/test-utils';

import SearchBar from '.';

describe('SearchBar', () => {
  test('renders SearchBar component', () => {
    render(<SearchBar isLoading={false} />);
  });

  test('Correct value in Input box', () => {
    render(<SearchBar isLoading={false} />);
    userEvent.type(
      screen.getByRole('textbox', { name: /search with bac number/i }),
      'BAC111111111111',
    );
    expect(screen.getByRole('textbox', { name: /search with bac number/i })).toHaveValue(
      'BAC_111_111_111_111',
    );
  });

  test('Search Button should be enabled', () => {
    render(<SearchBar isLoading={false} />);
    userEvent.type(
      screen.getByRole('textbox', { name: /search with bac number/i }),
      'BAC111111111111',
    );
    expect(screen.getByRole('button', { name: /Submit/i })).toBeEnabled();
  });

  test('Disabled button', () => {
    render(<SearchBar isLoading={false} />);
    userEvent.type(screen.getByRole('textbox', { name: /search with bac number/i }), 'BAC111');
    expect(screen.getByRole('button', { name: /Submit/i })).toBeDisabled();
  });
});
