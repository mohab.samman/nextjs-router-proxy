import { defineMessages } from 'react-intl';

const messages = defineMessages({
  SearchBarSubmit: {
    id: 'SearchBar.submit.label',
    defaultMessage: 'Submit',
  },
  SearchBarPlaceholder: {
    id: 'SearchBar.input.placeholder',
    defaultMessage: 'Search with BAC Number',
  },
});

export default messages;
