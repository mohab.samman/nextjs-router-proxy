import React, { useContext } from 'react';
import { ToggleButtonGroup } from '@material-ui/core';

import { LocaleContext, LOCALES } from '../../utils/localeContext';
import * as S from './styles';

const LanguageSetting = () => {
  const { language, setLanguage } = useContext(LocaleContext);

  const onClickHandler = (_: React.MouseEvent<HTMLElement>, value: string) => {
    if (value) {
      localStorage.setItem('locale', value);
      setLanguage(value);
    }
  };

  return (
    <S.LanguageSettings>
      <ToggleButtonGroup
        value={language}
        exclusive
        onChange={onClickHandler}
        aria-label="language-settings"
      >
        {LOCALES.map(({ code, label }) => (
          <S.LanguageToggleButton
            key={code}
            value={code}
            aria-label={label}
            selected={language === code}
          >
            {label}
          </S.LanguageToggleButton>
        ))}
      </ToggleButtonGroup>
    </S.LanguageSettings>
  );
};

export default LanguageSetting;
