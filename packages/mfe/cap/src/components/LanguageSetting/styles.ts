import styled from 'styled-components';
import { ToggleButton } from '@material-ui/core';

import Colors from '../../styles/themes/colors';
import { fontWeight } from '../../styles/themes/typography';

export const LanguageSettings = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin: 16px;
`;

export const LanguageToggleButton = styled(ToggleButton)`
  margin-right: 5px;

  &.MuiToggleButton-root {
    padding: 3;
    border: none;
    color: ${Colors.black20};
    border-radius: 0px;
  }

  &.Mui-selected {
    color: ${Colors.primaryRegular};
    font-weight: ${fontWeight.semi_bold};
    border-bottom: 2px solid ${Colors.primaryRegular};
  }

  '&:focus': {
    outline: none;
  }
`;
