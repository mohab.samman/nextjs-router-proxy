import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { render } from 'utils/test-utils';

import LanguageSetting from '.';

describe('LanguageSetting', () => {
  test('LanguageSetting renders "ToggleButtonGroup"', async () => {
    render(<LanguageSetting />);
    expect(screen.getByLabelText(/language-settings/i)).toBeInTheDocument();
  });

  test('button click', async () => {
    render(<LanguageSetting />);
    userEvent.click(screen.getByText('de'));
    expect(screen.getByRole('button', { pressed: true })).toBeInTheDocument();
  });
});
