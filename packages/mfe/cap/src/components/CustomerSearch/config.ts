import { IntlFormatters } from 'react-intl';

import messages from './messages';

const getTableHeaders = (intl: IntlFormatters) => [
  {
    label: intl.formatMessage(messages.BACNumber),
    id: 'bac_number',
  },
  {
    label: intl.formatMessage(messages.FirstName),
    id: 'first_name',
  },
  {
    label: intl.formatMessage(messages.LastName),
    id: 'last_name',
  },
  {
    label: intl.formatMessage(messages.EmailAddress),
    id: 'email_address',
  },
  {
    label: intl.formatMessage(messages.BirthDate),
    id: 'birthdate',
  },
  {
    label: intl.formatMessage(messages.Status),
    id: 'status',
  },
];

export default getTableHeaders;
