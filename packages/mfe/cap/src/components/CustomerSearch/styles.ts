import styled from 'styled-components';
import { Typography, TableRow as MuiTableRow, TableCell as MuiTableCell } from '@material-ui/core';

import Colors from '../../styles/themes/colors';
import { fontWeight } from '../../styles/themes/typography';

export const TableTitle = styled(Typography)`
  margin-bottom: 16px;
  margin-top: 40px;
`;

export const CustomerList = styled('div')`
  border-radius: 5px;
  box-shadow: '0 1px 5px 0 rgba(0, 0, 0, 0.2), 0 3px 1px -2px rgba(0, 0, 0, 0.12), 0 2px 2px 0 rgba(0, 0, 0, 0.14)';
  background-color: ${Colors.white};
`;

export const TableHeadingCell = styled(MuiTableCell)`
  color: ${Colors.black};
  font-weight: ${fontWeight.semi_bold};
  font-size: 0.8rem;
`;

export const TableRow = styled(MuiTableRow)`
  cursor: pointer;
`;

export const TableCell = styled(MuiTableCell)`
  & .MuiTableCell-body {
    color: ${Colors.black};
    fontsize: 1rem;
  }
`;
