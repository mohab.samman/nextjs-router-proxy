import React from 'react';
import { useIntl } from 'react-intl';
import { useSelector } from 'react-redux';
import {
  Typography,
  Table,
  TableBody,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Container,
} from '@material-ui/core';

import { RootState } from '../../store';
import Searchbar from '../SearchBar';
import getTableHeaders from './config';
import messages from './messages';
import * as S from './styles';

interface IHeader {
  label: string;
  id: string;
}
interface ICustomer {
  bacNumber: string;
  firstName: string;
  lastName: string;
  emailAddress: string;
  birthdate: string;
  status: string;
}

const CustomerSearch = () => {
  const intl = useIntl();
  const headers = getTableHeaders(intl);
  const customersData = useSelector((state: RootState) => state.customersData);

  const onRowClick = (customer: ICustomer) => {
    // eslint-disable-next-line no-console
    console.log('nothing happening on it for now', customer);
  };

  const { customers } = customersData;
  const customerCount = customers.length;

  return (
    <div>
      <Container disableGutters maxWidth={false}>
        <Typography variant="h4" style={{ marginBottom: 24 }}>
          {intl.formatMessage(messages.CustomerSearchHeading)}
        </Typography>

        <Searchbar isLoading={customersData.isLoading} />

        {customerCount > 0 && (
          <>
            <S.TableTitle variant="h5">{intl.formatMessage(messages.CustomerList)}</S.TableTitle>

            <S.CustomerList>
              <TableContainer component={Paper}>
                <Table aria-label="customer list">
                  <TableHead>
                    <TableRow>
                      {headers.map((header: IHeader) => (
                        <S.TableHeadingCell
                          data-testid={`table-header-${header.id}`}
                          key={header.id}
                          align="left"
                        >
                          {header.label}
                        </S.TableHeadingCell>
                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody data-testid="table-body">
                    {customers.map((customer: ICustomer) => (
                      <S.TableRow
                        key={customer.bacNumber}
                        hover
                        onClick={() => onRowClick(customer)}
                      >
                        <S.TableCell component="td" scope="row" align="left">
                          {customer.bacNumber}
                        </S.TableCell>
                        <S.TableCell align="left">{customer.firstName}</S.TableCell>
                        <S.TableCell align="left">{customer.lastName}</S.TableCell>
                        <S.TableCell align="left">{customer.emailAddress}</S.TableCell>
                        <S.TableCell align="left">{customer.birthdate}</S.TableCell>
                        <S.TableCell align="left">{customer.status}</S.TableCell>
                      </S.TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </S.CustomerList>
          </>
        )}
      </Container>
    </div>
  );
};

export default CustomerSearch;
