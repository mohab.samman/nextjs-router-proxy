import { defineMessages } from 'react-intl';

const messages = defineMessages({
  CustomerSearchHeading: {
    id: 'CustomerSearch.seachForCustomer.heading',
    defaultMessage: 'Search for a Customer',
  },
  CustomerList: {
    id: 'CustomerSearch.customerList.heading',
    defaultMessage: 'Customer List',
  },
  FirstName: {
    id: 'CustomerSearch.customerList.firstName',
    defaultMessage: 'First name',
  },
  LastName: {
    id: 'CustomerSearch.customerList.lastName',
    defaultMessage: 'Last name',
  },
  BACNumber: {
    id: 'CustomerSearch.customerList.bacNumber',
    defaultMessage: 'BAC number',
  },
  EmailAddress: {
    id: 'CustomerSearch.customerList.emailAddress',
    defaultMessage: 'Email address',
  },
  BirthDate: {
    id: 'CustomerSearch.customerList.birthdate',
    defaultMessage: 'Birth date',
  },
  Status: {
    id: 'CustomerSearch.customerList.status',
    defaultMessage: 'Status',
  },
});

export default messages;
