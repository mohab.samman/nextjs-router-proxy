import { createTheme } from '@material-ui/core/styles';

import breakpoints from './breakpoints';
import palette from './palette';
import typography from './typography';
import components from './components';
import shape from './shape';

const baseTheme = {
  breakpoints,
  palette,
  components,
  shape,
  typography,
};

// @ts-ignore
export default createTheme(baseTheme);
