import Colors from '../colors';

export default {
  styleOverrides: {
    root: {
      borderRadius: 4,
      height: 40,
      color: Colors.black,
      boxShadow:
        '0 1px 3px 0 rgb(0 0 0 / 20%), 0 2px 1px -1px rgb(0 0 0 / 12%), 0 1px 1px 0 rgb(0 0 0 / 14%)',
      backgroundColor: Colors.white,
    },
  },
};
