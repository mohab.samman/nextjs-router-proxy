import Colors from '../colors';

export default {
  styleOverrides: {
    root: {
      '&.MuiListItem-root.Mui-selected': {
        backgroundColor: Colors.primaryRegular,
      },
    },
  },
};
