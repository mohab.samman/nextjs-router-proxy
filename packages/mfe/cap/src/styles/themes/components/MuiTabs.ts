import Colors from '../colors';

export default {
  styleOverrides: {
    root: {
      '& .MuiTabs-indicator': {
        backgroundColor: Colors.primaryRegular,
        height: 3,
      },
    },
  },
};
