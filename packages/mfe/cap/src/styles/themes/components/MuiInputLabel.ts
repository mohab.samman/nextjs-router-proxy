import Colors from '../colors';

export default {
  styleOverrides: {
    root: {
      '&$focused': {
        // increase the specificity for the pseudo class
        color: Colors.black80,
      },
    },
  },
};
