import Colors from '../colors';
export default {
  styleOverrides: {
    root: {
      '&.MuiTableRow-root.MuiTableRow-hover:hover': {
        backgroundColor: Colors.lightGrayishBlue,
      },
    },
  },
};
