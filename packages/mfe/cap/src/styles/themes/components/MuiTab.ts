import Colors from '../colors';
import { fontWeight } from '../typography';

export default {
  styleOverrides: {
    root: {
      fontWeight: fontWeight.normal,
      '&.Mui-selected': {
        fontWeight: fontWeight.semi_bold,
        color: Colors.primaryRegular,
      },
    },
  },
};
