import { fontWeight } from '../typography';
import Colors from '../colors';

export default {
  defaultProps: {
    disableRipple: true,
  },
  styleOverrides: {
    root: {
      fontWeight: fontWeight.semi_bold,
      height: 40,
      color: Colors.black10,
      padding: '8px 16px',
      '&:hover': {
        boxShadow: 'none',
      },
    },
    contained: {
      '&:hover': {
        boxShadow: 'none',
        backgroundColor: Colors.primaryRegular,
      },
    },
    textSecondary: {
      color: Colors.primaryRegular,
      outline: 'none',
      borderRadius: 0,
      padding: '8px 30px 9px',
      '&:hover': {
        boxShadow: 'none',
        backgroundColor: Colors.white,
      },
    },
  },
};
