import MuiButton from './MuiButton';
import MuiTableRow from './MuiTableRow';
import MuiInputBase from './MuiInputBase';
import MuiListItem from './MuiListItem';
import MuiTabs from './MuiTabs';
import MuiTab from './MuiTab';
import MuiInputLabel from './MuiInputLabel';

export default {
  MuiButton,
  MuiTableRow,
  MuiInputBase,
  MuiListItem,
  MuiTabs,
  MuiTab,
  MuiInputLabel,
};
