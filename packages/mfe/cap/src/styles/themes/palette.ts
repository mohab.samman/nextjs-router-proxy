import Colors from './colors';

export default {
  primary: {
    main: Colors.primaryRegular,
    light: Colors.primaryLight,
    dark: Colors.primaryDark,
    contrastText: Colors.black10,
  },
  secondary: {
    main: Colors.secondaryRegular,
    contrastText: Colors.primaryRegular,
  },
  error: {
    main: Colors.red,
  },
  success: {
    main: Colors.green,
  },
  warning: {
    main: Colors.yellow,
  },
  info: {
    main: Colors.black80,
  },
  text: {
    primary: Colors.black80,
    secondary: Colors.black80,
    disabled: Colors.black10,
    default: Colors.black80,
  },
  action: {
    hover: Colors.primaryLight,
  },
  background: {
    drawer: '#30363d', // TODO indhuja to provide color name for this
    appBar: Colors.white,
    toolbar: Colors.black,
    default: Colors.black10,
  },
};
