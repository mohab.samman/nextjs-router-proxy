import Colors from './colors';

const fontFamily = ['Inter', 'arial', 'Roboto', 'Helvetica'].join(',');
export const fontWeight = {
  bold: 700,
  normal: 400,
  medium: 500,
  semi_bold: 600,
};

export default {
  fontFamily,
  htmlFontSize: 16,
  fontSize: 14,
  fontWeightLight: 300,
  fontWeightRegular: 400,
  fontWeightMedium: 500,
  fontWeightBold: 700,
  h4: {
    fontFamily,
    color: Colors.black80,
    fontWeight: fontWeight.bold,
    fontSize: 34,
    lineHeight: 1.235,
  },
  h5: {
    fontFamily,
    fontWeight: fontWeight.bold,
    fontSize: 24,
    lineHeight: 1.334,
  },
  h6: {
    fontFamily,
    fontWeight: fontWeight.medium,
    color: Colors.black80,
    fontSize: '1rem',
    lineHeight: 1.6,
  },
  button: {
    fontFamily,
    fontWeight: fontWeight.semi_bold,
    fontSize: '0.875rem',
    lineHeight: 1.75,
    textTransform: 'none',
  },
};
