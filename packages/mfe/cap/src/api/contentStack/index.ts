import Contentstack from 'contentstack';
import { LOCALES } from 'utils/localeContext';

/**
 * ContentStack
 * Contentstack contains global variables injected by Webpack,
 * the object was defined in the config file in the root directory
 * and contains the stack API Key, the delivery Token and environment
 * to which stack to connect
 */

export interface IContentStackSecrets {
  stackAPIKey: string;
  stackDeliveryToken: string;
  environment: string;
}

declare const contentStack: IContentStackSecrets;

const { stackAPIKey, stackDeliveryToken, environment } = contentStack;

/**
 * ContentStack Authentication
 * ContentStack SDK function to authenticate and
 * connect the the desire stack.
 * The region will always be EU.
 */
const Stack = Contentstack.Stack(
  stackAPIKey,
  stackDeliveryToken,
  environment,
  Contentstack.Region.EU,
);

/**
 * Cache Policy - NETWORK_ELSE_CACHE
 * When the NETWORK_ELSE_CACHE policy is set, the SDK gets data using a network call.
 * However, if the call fails, it retrieves data from cache.
 */
Stack.setCachePolicy(Contentstack.CachePolicy.NETWORK_ELSE_CACHE);

/**
 * If no reference uid (@param entry)  was provided, will return all available entries under that contentType.
 * If a reference uid (@param entry) was provided, will return an object containing the entry.
 */
export const getCMSEntriesForContentType = async (contentType: string, entry?: string) => {
  const { notation } = LOCALES[0];

  try {
    if (entry) {
      const contentTypes = await Stack.ContentType(contentType)
        .Entry(entry)
        .language(notation)
        .fetch();

      return contentTypes.toJSON();
    }

    const response = await Stack.ContentType(contentType)
      .Query()
      .language(notation)
      .toJSON()
      .find();

    return response[0];
  } catch (e) {
    return Promise.reject(e.message);
  }
};
