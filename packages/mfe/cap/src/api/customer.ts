import { getConfigValuesBaseOnRegion } from '../utils/domain';

import createClient from './interceptors';

const { baseURL } = getConfigValuesBaseOnRegion();

const axios = createClient(baseURL);

export const searchCustomerByBac = async (bacnumber: string) => {
  const url = `customers/${bacnumber}?embed=all-persons&embed=reference-accounts`;

  return axios.get(url);
};

export default {
  searchCustomerByBac,
};
