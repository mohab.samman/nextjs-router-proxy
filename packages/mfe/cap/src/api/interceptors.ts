import axios from 'axios';
import { currentSession } from '@raisin/auth';

const HTTP_REQUEST_TIMEOUT = 6000;

const getToken = async () => {
  try {
    const data = await currentSession();
    const jwtToken = data.getIdToken().getJwtToken();

    return `Bearer ${jwtToken}`;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('getToken error', err);
    // TODO logout & redirect to login?
    // if (typeof window !== 'undefined') window.location.href = window.location.origin;
    return '';
  }
};

const createClient = (baseURL: string) => {
  let token = '';
  const api = axios.create({
    baseURL,
    timeout: HTTP_REQUEST_TIMEOUT,
    withCredentials: true,
    headers: {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Access-Control-Allow-Origin': '*',
    },
  });

  getToken().then((data) => {
    token = data;
    api.defaults.headers.Authorization = token;
  });

  api.interceptors.request.use(
    (req) => req,
    (error) => Promise.reject(error),
  );

  api.interceptors.response.use(
    (res) => res.data,
    (error) => {
      let customError = {};

      if (error.response) {
        /*
         * The request was made and the server responded with a
         * status code that falls out of the range of 2xx
         */
        customError = {
          statusCode: error.response.status,
          error: error.response.data,
        };
      } else if (typeof error.response === 'undefined') {
        /*
         * The request was made but no response was received, `error.request`
         * is an instance of XMLHttpRequest in the browser and an instance
         * of http.ClientRequest in Node.js
         */
        customError = {
          statusCode: '403',
          error: {
            code: 'INVALID_REQUEST_PAYLOAD',
            message:
              'A technical error occurred. The search term might contain invalid characters.',
          },
        };
      }

      return Promise.reject(customError);
    },
  );
  return api;
};

export default createClient;
