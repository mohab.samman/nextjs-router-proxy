import React, { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Cookies from 'js-cookie';
import { currentSession, signOut } from '@raisin/auth';
import routes from '../utils/routes';

type User = {};

const AuthContext = React.createContext(
  {} as {
    user: {
      accessToken?: {
        jwtToken?: string;
      };
    };
    logout: ({ redirectLocation }: { redirectLocation: string }) => void;
    isLoading: boolean;
    isAuthenticated: boolean;
    token: string;
  },
);

export const AuthProvider = ({ children }: { children: any }) => {
  const [user, setUser] = useState<User>({});
  const [token, setToken] = useState('');
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);
  const isAuthenticated = !!user;

  const logout = ({ redirectLocation }: { redirectLocation: string }) => {
    signOut();
    setUser({});
    setIsLoading(false);
    Cookies.remove('token');
    router.push(redirectLocation || routes.login);
  };

  useEffect(() => {
    (async () => {
      try {
        const data = await currentSession();
        const jwtToken = data.getIdToken().getJwtToken();
        if (jwtToken) {
          setUser(data);
          setToken(jwtToken);
          router.push(routes.dashboard);
          return data;
        }
        setIsLoading(false);

        return null;
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log('inside catch: ', error);
        setUser({});

        router.push(routes.login);
        return error;
      }
    })();
  }, []);

  useEffect(() => {
    const Component = children.type;

    // If it doesn't require auth, everything's good.
    if (!Component.requiresAuth) return;

    // If we're already authenticated, everything's good.
    if (isAuthenticated) return;

    // If we don't have a token in the cookies, logout
    (async () => {
      const data = await currentSession();

      const jwtToken = data.getIdToken().getJwtToken();
      if (!jwtToken) {
        return logout({ redirectLocation: Component.redirectUnauthenticatedTo });
      }
      setToken(jwtToken);
      setIsLoading(false);

      return null;
    })();

    // If we're not loading give the try to authenticate with the given token.
  }, [isLoading, isAuthenticated, children.type.requiresAuth]);

  return (
    <AuthContext.Provider
      value={{
        user,
        logout,
        isLoading,
        isAuthenticated: !!user,
        token,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
