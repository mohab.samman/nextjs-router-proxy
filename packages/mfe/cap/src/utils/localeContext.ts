import { createContext } from 'react';

export const LOCALES = [
  {
    code: 'en_US',
    label: 'en',
    notation: 'en-us',
  },
  {
    code: 'de_DE',
    label: 'de',
    notation: 'de-de',
  },
];

const supportedLanguage = LOCALES.map(({ code }) => code);

const getLanguage = () => {
  if (typeof window !== 'undefined') {
    const localStorageLocale = localStorage.getItem('locale');
    if (localStorageLocale && supportedLanguage.indexOf(localStorageLocale) >= 0) {
      return localStorageLocale;
    }

    const browserLanguage = navigator.language.replace('-', '_'); // en-GB => en_GB,
    if (supportedLanguage.indexOf(browserLanguage) > 1) {
      return browserLanguage;
    }
  }

  return supportedLanguage[0];
};

export type LocaleContextType = {
  language: string;
  setLanguage: (lang: string) => void;
};

export const Locale = {
  language: getLanguage(),
  setLanguage: () => {},
};

export const LocaleContext = createContext<LocaleContextType>(Locale);

export const getShortLocale = (locale: string) => locale?.substr(0, 2);
