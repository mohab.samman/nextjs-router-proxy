import React from 'react';
import { create } from 'jss';
import { ThemeProvider } from '@material-ui/core/styles';
import { StylesProvider, jssPreset } from '@material-ui/styles';
import { CssBaseline } from '@material-ui/core';
import StyledEngineProvider from '@material-ui/core/StyledEngineProvider';

import theme from '../styles/themes';

const jss = create({ plugins: [...jssPreset().plugins] });

const StyledTheme = ({ children }: { children: React.ReactNode }) => (
  <ThemeProvider theme={theme}>
    <StyledEngineProvider injectFirst>
      <StylesProvider jss={jss}>
        <CssBaseline />
        {children}
      </StylesProvider>
    </StyledEngineProvider>
  </ThemeProvider>
);

export default StyledTheme;
