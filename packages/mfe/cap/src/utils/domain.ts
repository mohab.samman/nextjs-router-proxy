// global variables injected with config/index.ts
declare const apiBaseUrlDEU: string;
declare const apiBaseUrlGBR: string;

const configurations = {
  deu: {
    baseURL: apiBaseUrlDEU,
  },
  gbr: {
    baseURL: apiBaseUrlGBR,
  },
};

export const getConfigValuesBaseOnRegion = () => {
  if (typeof window !== 'undefined') {
    const { hostname } = window.location;
    // As the url domains have the same structure this line will always work
    const region = hostname.includes('co.uk') ? 'gbr' : 'deu';
    return configurations[region] ?? configurations.deu;
  }

  return configurations['deu'];
};

export default {
  getConfigValuesBaseOnRegion,
};
