import React, { FC, ReactElement } from 'react';
import { render, RenderOptions } from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';

import getMessages from './getMessages';
import { store } from '../store';

import StyledTheme from './styledTheme';
import { LocaleContext, Locale, getShortLocale } from './localeContext';

// eslint-disable-next-line react/prop-types
const Providers: FC = ({ children }) => {
  let messages = {};

  const fetchMsg = async () => {
    try {
      messages = await getMessages(Locale.language);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log('failed in fetching translations: ', error);
    }
  };
  fetchMsg();

  return (
    <Provider store={store}>
      <LocaleContext.Provider value={{ language: Locale.language, setLanguage: () => {} }}>
        <IntlProvider
          messages={messages}
          locale={getShortLocale(Locale.language)}
          defaultLocale={getShortLocale(Locale.language)}
        >
          <StyledTheme>{children}</StyledTheme>
        </IntlProvider>
      </LocaleContext.Provider>
    </Provider>
  );
};

const customRender = (ui: ReactElement, options?: Omit<RenderOptions, 'wrapper'>) =>
  render(ui, { wrapper: Providers, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
