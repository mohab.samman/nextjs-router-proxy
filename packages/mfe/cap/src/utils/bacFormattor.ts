const BAC_NUMBER_LENGTH = 19;

export const formatBac = (bacNumber: string) =>
  bacNumber
    .split('')
    .filter((item) => item !== '_')
    .map((item, i, arr) => {
      if (i + 1 < arr.length && (i + 1) % 3 === 0) {
        return `${item}_`;
      }
      return item;
    })
    .join('');

export const validateBac = (bacNumber: string) => {
  if (bacNumber.length !== BAC_NUMBER_LENGTH) {
    return true;
  }

  return false;
};
