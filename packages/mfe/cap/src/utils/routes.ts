const routes = {
  login: '/login',
  dashboard: '/dashboard',
  customerManagement: '/dashboard/customer-management',
};

export default routes;
