import React, { useState, useEffect } from 'react';
import { GetStaticProps, NextPage } from 'next';
import { useDispatch, useSelector } from 'react-redux';

import { IntlProvider, MessageFormatElement } from 'react-intl';
import { Container } from '@material-ui/core';

import getMessages from '../../utils/getMessages';
import Drawer from '../../components/Drawer';
import CustomerSearch from '../../components/CustomerSearch';
import Snackbar from '../../components/Snackbar';
import { onClose } from '../../components/Snackbar/SnackbarSlice';
import { LocaleContext, Locale, getShortLocale } from '../../utils/localeContext';
import { RootState } from '../../store';

interface IDashboard {
  translations: Record<string, string> | Record<string, MessageFormatElement[]>;
  locale: string;
}

const Dashboard: NextPage<IDashboard> = ({ translations, locale }) => {
  const [language, setLanguage] = useState(locale);
  const [messages, setMessages] = useState(translations);
  const snackbar = useSelector((state: RootState) => state.snackbar);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchMsg = async () => {
      const dashboardMessages = await getMessages(language);

      setMessages(dashboardMessages);
    };
    fetchMsg();
  }, [language]);

  return (
    <LocaleContext.Provider value={{ language, setLanguage }}>
      <IntlProvider
        messages={messages}
        locale={getShortLocale(language)}
        defaultLocale={getShortLocale(Locale.language)}
      >
        <Drawer>
          <Container disableGutters maxWidth={false}>
            <CustomerSearch />
          </Container>
        </Drawer>
        <Snackbar
          isOpen={snackbar.isOpen}
          onClose={() => dispatch(onClose())}
          statusCode={snackbar.data?.error?.code}
          variant={snackbar.variant}
          message={snackbar?.data?.error?.message || ''}
        />
      </IntlProvider>
    </LocaleContext.Provider>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const locale = Locale.language;
  const translations = await getMessages(locale);

  return {
    props: { translations, locale },
  };
};
// @ts-ignore
Dashboard.requiresAuth = true;
// @ts-ignore
Dashboard.redirectUnauthenticated = '/login';

export default Dashboard;
