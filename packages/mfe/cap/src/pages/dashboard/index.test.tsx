import React from 'react';
import { render } from 'utils/test-utils';

import { Locale } from '../../utils/localeContext';
import getMessages from '../../utils/getMessages';
import Dashboard from './index.page';

const fetchMsg = async (locale: string) => {
  const dashboardMessages = await getMessages(locale);

  return dashboardMessages;
};

describe('<Dashboard />', () => {
  it('renders Dashboard ', async () => {
    const messages = await fetchMsg(Locale.language);

    render(<Dashboard translations={messages} locale={Locale.language} />);
  });
});
