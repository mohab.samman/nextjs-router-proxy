import React from 'react';
import Head from 'next/head';
import { Provider } from 'react-redux';
import { NextPage } from 'next';
import { AppProps } from 'next/dist/next-server/lib/router/router';

import StyledTheme from '../utils/styledTheme';
import '@fontsource/open-sans';
import { AuthProvider } from '../contexts/auth';
import { store } from '../store';

type CustomPage = NextPage & {
  requiresAuth?: boolean;
  redirectUnauthenticatedTo?: string;
};
interface CustomAppProps extends Omit<AppProps, 'Component'> {
  Component: CustomPage;
}

function App({ Component, pageProps }: CustomAppProps) {
  return (
    <>
      <Head>
        <title>CAP</title>
        <meta name="description" content="Customer Administrator Portal" />
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
      <AuthProvider>
        <Provider store={store}>
          <StyledTheme>
            <Component {...pageProps} />
          </StyledTheme>
        </Provider>
      </AuthProvider>
    </>
  );
}

export default App;
