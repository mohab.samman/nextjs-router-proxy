import styled from 'styled-components';
import { Grid } from '@material-ui/core';

export const LogoWrapper = styled(Grid)`
  marginbottom: 24;
`;

export const StyledContainer = styled(Grid)`
  height: '100vh';
  display: 'flex';
  flexdirection: 'column';
  alignitems: 'center';
  justifycontent: 'center';
  width: '100%';
`;
