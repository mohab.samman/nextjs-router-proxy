import React, { useState, useEffect } from 'react';
import { GetStaticProps, NextPage } from 'next';
import { IntlProvider, MessageFormatElement } from 'react-intl';
import getMessages from '../../utils/getMessages';
import { LocaleContext, Locale, getShortLocale } from '../../utils/localeContext';
import SignIn from '../../components/SignIn';
interface ILogin {
  translations: Record<string, string> | Record<string, MessageFormatElement[]>;
  locale: string;
}
const Login: NextPage<ILogin> = ({ translations, locale }) => {
  const [language, setLanguage] = useState(locale);
  const [messages, setMessages] = useState(translations);

  useEffect(() => {
    const fetchMsg = async () => {
      const loginMessages = await getMessages(language);

      setMessages(loginMessages);
    };
    fetchMsg();
  }, [language]);

  return (
    <LocaleContext.Provider value={{ language, setLanguage }}>
      <IntlProvider
        messages={messages}
        locale={getShortLocale(language)}
        defaultLocale={getShortLocale(Locale.language)}
      >
        <SignIn />
      </IntlProvider>
    </LocaleContext.Provider>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const locale = Locale.language;
  const translations = await getMessages(locale);

  return {
    props: { translations, locale },
  };
};

export default Login;
