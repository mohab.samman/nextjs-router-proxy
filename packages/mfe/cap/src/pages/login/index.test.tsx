import React from 'react';
import { render, waitFor, screen } from 'utils/test-utils';

import Login from './index.page';
import { Locale } from '../../utils/localeContext';
import getMessages from '../../utils/getMessages';

const fetchMsg = async (locale: string) => {
  try {
    const dashboardMessages = await getMessages(locale);

    return dashboardMessages;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('error in login tests', err);
    return {};
  }
};

describe('<Login />', () => {
  it('renders Login ', async () => {
    const messages = await fetchMsg(Locale.language);

    render(<Login translations={messages} locale={Locale.language} />);
    const signInButton = await waitFor(() => screen.getByTestId('signInButton'));
    const logo = await waitFor(() => screen.getByTestId('logo'));
    expect(signInButton).toBeInTheDocument();
    expect(logo).toBeInTheDocument();
  });
});
