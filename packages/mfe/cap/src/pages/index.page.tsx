import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

import { useAuth } from '../contexts/auth';

const Home = () => {
  const { user, isLoading } = useAuth();
  const router = useRouter();

  useEffect(() => {
    router.prefetch('/dashboard');
  }, []);

  useEffect(() => {
    if (user?.accessToken?.jwtToken) {
      router.push('/dashboard');
    } else {
      router.push('/login');
    }
  }, [user]);

  return <>{isLoading && <div>Loading...</div>}</>;
};

export default Home;
