# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.4.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cap@0.3.0...@raisin/cap@0.4.0) (2021-07-23)


### Features

* **ContentStack:** fixed broken language notation ([46d57ca](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/46d57ca765db025cceb69f36327dc2882a508960))
* **Customer:** removed commented code ([cc41c03](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cc41c0330cbfdfc75d7984b59c7be34cb6bb5cf1))
* **Customers:** fixed interface convention ([4c38266](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4c38266a948d086dc5c6cbc49e042986a1ddf332))
* **Customers:** fixed interface convention ([2f24c86](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2f24c86156cbc81f998ae239781767daf833a795))
* **CustomerSearch:** fixed type errors ([8f520bf](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8f520bfef6ac4aab9aedcb1fb3b361689134a2ba))
* **CustomerSearch:** updated translations and fixed types ([2002d4a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2002d4a35d9f1f85ac520fdede2375b3a2d5baa2))
* **Pipeline:** fixed multiple types issue for build step ([cb8293a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cb8293a61995ea1e660f28da50511bfb267524a9))
* **Pipeline:** removed exclusion regex in next config ([2ea4e77](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2ea4e77cb75078e60771ced486ab97d4f1872b5f))
* **Search:** fixed formatted customer data ([863f7e7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/863f7e7395e19131be25ebbc9960ebc74c923e3a))
* **Search:** fixed locale context types ([0c559bc](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0c559bc51f13edfcef02e6afca1f3cfec2c8efb2))
* **SearchCustomer:** search customer by bac number ([7019dea](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7019dea276c8da90bf3d8626e0553b918b2136c6))
* **Styles:** cleaned commented code and fixed theme ([0f92fef](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0f92fefd9fb066f36a4d68f9eacb381265394906))
* **Tests:** fixed unit tests ([12fb1e0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/12fb1e02d71d48ded94a6d1cf3b9ca53909a5c75))
* **Tests:** fixing tests and jest setup ([8715c7f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8715c7f0143c942eda9720271ab91af4401d32c9))
* **Tests:** renamed page extensions to fix unit test cases in page directory ([bde291e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/bde291ea397e62bd57b80c6b497d6753f4c42a29))





# [0.3.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cap@0.2.0...@raisin/cap@0.3.0) (2021-07-14)


### Features

* **Cypress:** fixed env variable ([dd1d812](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/dd1d812bfbda3436ce6de08cf81ca1f9727c2395))





# [0.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/compare/@raisin/cap@2.30.0...@raisin/cap@0.2.0) (2021-07-09)


### Features

* **Auth:** auth feature implementation ([34f6ad0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/34f6ad06d001ecf43dee6db2bfd40449d151399f))
* **Auth:** bumped auth version ([f5c81e8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f5c81e8c142f124176e9b978e1aedbf2b2829f45))
* **Auth:** fixed protected routes issue ([5c0132e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5c0132e371e5ea633b65537883b2f4ba4532d45c))
* **Auth:** IIFE function for auth ([1b143c3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/1b143c30600bca64c50534622710beb019e0cbbc))
* **Auth:** resolved conflicts ([41f60e7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/41f60e7d62afa30b2c8be91c16faa0fbd7d4492c))
* **CAP:** added mocked test ([3ed4d29](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3ed4d29ad20d267c05362fa765dadb19cf25ae95))
* **CAP:** fixed lint issue ([e39bad9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e39bad96db14ceea2fb19c9bfab1f79413208ce6))
* **CAP:** prefetch dashboard ([3d08017](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3d08017c61e973f5611e37e8d6264c8296f1e395))
* **CAP:** setting up auth and cap together ([5275ba1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5275ba13ad0d895479c2fd1cf0711ed082915846))
* **CAP:** theme setup MUI for CAP ([87a1722](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/87a172286e81967922bb6ac64e7399d863cc937b))
* **Cleanup:** cleanup and fixed ts errors ([e4ab569](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/e4ab569aa23bd3886bbc02c7cd05d324930439d6))
* **Coflicts:** resovled conflicts ([d7b148d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d7b148d9d7248a3a6f18cf8cbefced9f2cb7b072))
* **Conflicts:** resolved conflicts ([81be796](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/81be79684c1b09106c0f96defbf61e9ef7062310))
* **Cypress:** added console log ([9d1d65c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/9d1d65c0fcd01d2dc4a21ee9cfca0087c5051193))
* **Cypress:** downgraded node version to 14.x for pipeline ([6563cfd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/6563cfdb122ef36f19edf1c19574fb932b2ddebb))
* **Cypress:** fixed cypress test setup for nextjs ([b513520](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/b5135203a02a386625d8c1142b8452eaa12d7038))
* **Cypress:** js to ts files ([52f9f78](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/52f9f78fb29d78c7991eb0316bad66576af31854))
* **Cypress:** rebased branch with latest ([7fd8982](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/7fd898266893273f9abcb2f5393c4ca5c1ef6238))
* **Cypress:** resolved conflicts ([3695e61](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/3695e61dcbabb284e6e018996ff5b573dcbf5fa4))
* **Cypress:** skipped integration test step in pipeline ([644ff70](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/644ff702016911d3996a4570e7d4c66454d6f9b8))
* **Cypress:** temporary fix for cypress baseUrl ([279a7c0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/279a7c005832f2772082d9cbdbaaad7bf3bdc1e4))
* **Dashboard:** test case ([08805d0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/08805d031427d2076be5b495bd8933c4f68cb139))
* **Drawer:** left drawer implementation in nextjs ([0226d70](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/0226d7097c08d4e8aa4dc1b1430dc92d53e4c200))
* **Improvements:** cleaned changelog and updated readme file for cap ([252efd3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/252efd35efcd242520fb2b166022f2af988cd7d6))
* **Improvements:** fixed many minor improvements ([5fcfb1f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/5fcfb1fad007a0500633e0b4775512249a0b2f83))
* **Jest:** clear jest cache ([237fa2b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/237fa2b0ad2170d71185e997e4d01386ed229226))
* **Jest:** pipeline breaking for npm modules auth ([a4a1fc7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/a4a1fc7590261a5042807ff09aa16d0608fb8b08))
* **Login:** working login flow ([fbd7126](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/fbd7126d18a05967f4767db2268482f07e3aab56))
* **NodeVersion:** improvements for node version and readme ([00489ff](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/00489ff1d5595fc291d95c48053c6a4342616759))
* **Profile:** fixed styles for user profile ([f0588dd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f0588ddfdb5b998a1dbee95e98b59d83661e872f))
* **Profile:** translations for profile ([f376d0b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/f376d0bc4e23b46aa59e2230aafad362c7c3cfce))
* **Profile:** user profile ([cf73fb4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/cf73fb40becd0e2284396adbbe5104deaedb9399))
* **Routes:** introduced routes config file ([2ef4ea2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2ef4ea2a980920110afae62222ae756a64fbd573))
* **Session:** implemented session timer in user profile ([8ad59dd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/8ad59dd1f1ac83ca96fc9bfd6d2a85bed3edcfa1))
* **Session:** mocked test setup ([06c6e11](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/06c6e11c47d0eafe10f013b7177dbf71f391a406))
* **Test:** fixed unit test cases for react-intl ([4d23c85](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/4d23c85ea48c25e43811ad21cd0c7349e6bd4a99))
* **Test:** jss styled in setup ([18404d8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/18404d8bd229ec76c584b00f49976a27e711d1b2))
* **Translations:** added translations for CAP ([2338fb7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/2338fb7301d1adcf4cd94f38feb85fe20b94b18d))
* **Translations:** react intl for dashbaord ([457cc2b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/457cc2ba8c573fd20f583d9b58ce1f5de420faa1))
* **Typescropt:** added types for child components ([d591df9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/commit/d591df904643574514b5f86b1dfd2b8f7635a3dd))





# [2.30.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.29.0...@raisin/cap@2.30.0) (2021-06-30)


### Features

* **Cypress:** added CI stage to execute cypress tests ([9dbb40c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9dbb40cbac48fb1ce958bdba1d75289e40111ac4))





# [2.29.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.28.0...@raisin/cap@2.29.0) (2021-06-18)


### Features

* **Cypress:** Customer details tests ([7e0727d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/7e0727d7090c622fb1ec082d6b70e9239d39519c))





# [2.28.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.27.0...@raisin/cap@2.28.0) (2021-06-14)


### Features

* **Cypress:** Automate customer search ([44c3ab5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/44c3ab5ffc8a60576b5fe9c6cf4f4fdd2c3b96ae))





# [2.27.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.26.0...@raisin/cap@2.27.0) (2021-06-09)


### Features

* **Pipeline:** retrigger pipeline ([5e749cb](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/5e749cb7ee566d21db5edc8923a2a49376bbdc22))





# [2.26.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.25.0...@raisin/cap@2.26.0) (2021-06-09)


### Features

* **Lint:** fixed lint issues ([11d12a2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/11d12a296f96679ec3100912cfe18528a1fc0ca5))
* **Lint:** removed unused default props ([a90ab88](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a90ab88fae30652c104c24b51c5c8420162a61de))
* **UnderConstruction:** added CAP under construction view ([aedf826](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/aedf8269d8f1eb417b8d92c1280968046b13c002))





# [2.25.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.24.0...@raisin/cap@2.25.0) (2021-06-04)


### Features

* **Cypress:** okta authentication setup for Cypress tests ([050fb45](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/050fb454a7ce33216eb1e77fe1833067011f9458))





# [2.24.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.23.0...@raisin/cap@2.24.0) (2021-06-03)


### Features

* **Gitlab and Amplify:** Add dynamic import to the libraries build scripts ([9e4417d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9e4417df5dc283cc916460e05000723197c41fb8))





# [2.23.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.22.0...@raisin/cap@2.23.0) (2021-06-02)


### Features

* **Copy:** fixed copy bac text and tab selection ([bb98e4c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/bb98e4c5a7c954df7b82da9b37bd93dcf384588f))
* **Copy:** implemented copy bac feature ([5c579c4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/5c579c4b3c6aa5b74707f2129e1214e6d3ff84cd))
* **Translations:** added translation for Bac copy feature ([a9bb826](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a9bb82618cf56d82db2acb489c6f2f3676fa2fd6))
* **UX:** aria-label changed ([51d466f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/51d466f7f439309916bf76346ff173eb809217e2))
* **UX:** ux enhancements ([2845b82](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/2845b82be2dad063f5a8777d77e48953a6bad9b7))





# [2.22.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.21.0...@raisin/cap@2.22.0) (2021-06-01)


### Features

* **Customer:** removed data-testid ([aa49ec2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/aa49ec2e4151e902f73ccd586c19f1b3a9d8ae33))
* **RightPanel:** added status customer number and distrubution partner on right panel ([929e578](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/929e578321b2c423cae0414c5c55ccfda418ac4f))
* **RightPanel:** fixed right panel collapse style issue ([c90d855](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c90d8552fb93769aa8d5472d5209b699516cbf96))
* **TextField:** changed Typography to Input ReadOnly component ([df0b19f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/df0b19fe5e6edfd2f355c4e68102a5ad72e88f21))
* **Translations:** added translations for DE ([b0de489](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b0de4891b055374a4a3f4108d04da4ac97698bd2))





# [2.21.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.20.0...@raisin/cap@2.21.0) (2021-05-26)


### Features

* **ScrollableTabs:** poc for scrollable tabs in cap customers ([263db4a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/263db4acee793f5fc3a666ea9724f0fe820cc76f))
* **ScrollTab:** code cleanup ([26df32d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/26df32dfb5a9ae3f881e1ebb3ec2a29e081379bf))
* **Style:** fixed input label focus style ([e790925](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e790925716fa962cdd5b39f2dfdddc3a53b26006))





# [2.20.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.19.0...@raisin/cap@2.20.0) (2021-05-17)


### Features

* **Refactor:** cleaned up unused styled-components for cap ([f592c8c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f592c8c1420d1bb6825074e162deaab9a3f9fee6))





# [2.19.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.18.0...@raisin/cap@2.19.0) (2021-05-12)


### Features

* **Auth:** fixed auth signout to button component ([2f29c25](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/2f29c2505dda3941fe8dbfa17daa144a24842bf8))
* **Breadcrumbs:** implemented breadcrumbs with typography ([e1445e1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e1445e1142fe709fdaaaad61ef0a34615379d2c2))
* **Cleanup:** cleaned unused variables and styles ([fa4d47b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fa4d47b4ff7361501597d1140aa48322aadeb991))
* **Customers:** fixed customer MFE with tabs and vertical navigation ([61ce532](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/61ce53282957b26f3b62b929e75ab25b0ba78803))
* **Customers:** fixed snake_case to camelCase ([5fb5e49](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/5fb5e49569a4b71b00c37274815e60d4800e50ad))
* **Enhancements:** addressed code review comments ([e77d55e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e77d55e4e23273bbae9cc042a9288742345fb927))
* **Fonts:** added Inter as a google font and fixed style issues on list items drawer ([b71829c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b71829c1ed53dc7f005d7db13dc585bf549205d1))
* **Layout:** fixed broken routes and selectedMenuItem implementation ([e77d061](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e77d061c5241acb50ba5b0fc5512976cc45dac5f))
* **Layout:** fixed user profile styles and logout ([7c9bfd9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/7c9bfd90abf19b8df1f110ef8aeaa13fd656d40e))
* **Layout:** implement lefet panel layout and login screen ([1078ea9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1078ea98dea952f3e0a2d1ec2c89a1c06b93f424))
* **Layout:** implement left panel layout navigation ([193b658](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/193b6583355c538fe936ec0becbf1163d71c7aae))
* **LeftDrawer:** fixed selected left drawer item ([0d6bd61](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/0d6bd61f6f92b1b2cbf7b564da1c6957e432ab46))
* **Logo:** colored and black-white logo provision ([68a9b12](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/68a9b120dd9e51de097ecb5196886874fd8c26f4))
* **Navigation:** fixed drawer hamburger menu and toolbar ([4cb52bd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4cb52bd564c0874824c469479b85cc51975be6c1))
* **Navigation:** fixed nested routes and theme pallete ([3b70470](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3b7047069f9785935f39e1f7449a2e474558abb5))
* **Prettier:** fixed code formatting and prettier errors ([e006624](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e006624e3f8d76483955389304c34accb54c6bff))
* **Review:** fixed color variables and prettified newrelic script reverted ([9cf584f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9cf584faa0d4e3eb96f236ac7ff6803847d9b5cf))
* **Style:** fixed alignment issue on left drawer ([855f66a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/855f66a3744fbb0bdee201d76e5dc07af03a90b9))
* **Tests:** fixed broken test ([5f9149b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/5f9149b16c3ec7c5658e511048b8899e845d114d))
* **Tests:** fixed tests by skipping it for now and fixed eslint issues ([d0304a1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d0304a14074ffdba443479e53221241aed72a5fd))
* **Theme:** fixed font weights ([371b1c2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/371b1c2e0e87df8aa0c0c1b4578ce11e66873c3a))
* **Tooltip:** tooltip for user profile ([b2d4b9d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b2d4b9d9b2b9220de7316aaee68c0ac6a1f24e49))
* **Translations:** added translations keys for CAP portal ([9874ae4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9874ae48cd62aadf88b654e416aeef1ef6245fa5))
* **Translations:** DE translations fixed for cap and customers ([fa39ade](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fa39adeee8ee7eebc846d2f91e1e0cb2a35ee2fe))
* **Translations:** fixed messages for customer list ([5829ca6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/5829ca62ab64524268830504f24f30b2fc28f296))
* **Translations:** shared translations from customers mfe to cap portal ([79257f0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/79257f0cdce555e78b4556b012cb1a4f25aeb66d))
* **Translations:** updated translations with transifex ([7949797](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/79497976ee7462e160b0c273867ce1db6b1c73ea))
* **UnitTests:** skipped and commented some tests for now will need rewrite ([3a64b34](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3a64b3486fe835ed912a27fc11074ec948fe2f50))
* **UX:** auto focused input search ([1916247](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1916247a0e289a5352b63fa03b0d135aa5398b0b))
* **UX:** fixed ux review comments for search customers ([ce2530b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/ce2530bf0b185dd6a7e0263ad0fbe7577c7614c9))
* **UX:** fixed ux review comments on left panel and search bar ([e3575fb](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e3575fbfc0ca115fd84eed178ef9fed789323e14))
* **UX:** more fixes related to elavation and border radius ([21b2767](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/21b27675a77e9dad955fc386c1a679cc9e677648))
* **UXReview:** fixed ux review comments ([651e737](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/651e737dea57ea81d72b3efea15789676d60f9d9))
* **UXReview:** improved us suggestions for customer search ([fa7f84c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fa7f84c82f6007efd61b0414bd571a43639cd18f))





# [2.18.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.17.1...@raisin/cap@2.18.0) (2021-05-12)


### Features

* **footer:** Create new component footer with contact details, social media and content links components ([d75ca82](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d75ca8260f334551804fb0603cb35a35efcf4622))





## [2.17.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.17.0...@raisin/cap@2.17.1) (2021-04-26)


### Bug Fixes

* typo ([30853f2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/30853f20196e170b57187227ccf2fe6ccc1038b9))





# [2.17.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.16.0...@raisin/cap@2.17.0) (2021-04-26)


### Features

* **Interceptor:** updated interceptor to redirect user to login on invalid jwt ([f5dc624](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f5dc624e133dfe70839b626b0bbfdb82fc9bb906))
* **Interceptor:** updated interceptor to redirect user to login on invalid jwt ([caa2419](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/caa241936619111934decadf8dd1288c0d8f0ef4))
* **Translations:** added translations for 401 error returned by cas server ([c3d91a4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c3d91a4358afb345f172dbb1757978c25590a5e7))
* **Unauthotized:** logout user on 401 ([fcbc5a9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fcbc5a93a90def3102986bf78d8a6bdc2b744879))





# [2.16.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.15.6...@raisin/cap@2.16.0) (2021-04-21)


### Features

* **Transifex:** updated parent project to transidex resources ([fba6679](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fba66792f013dfd50f0971a3ff95b644204b7a4b))
* **Transifex:** updated translations for cap mfe ([1d86552](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1d865521af50ff1e418e11d0ee1632d9935f855e))
* **Translations:** fixed typo ([adc5fa0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/adc5fa078820cd4f187dd8081a92654b266f9583))





## [2.15.6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.15.5...@raisin/cap@2.15.6) (2021-03-31)

**Note:** Version bump only for package @raisin/cap





## [2.15.5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.15.4...@raisin/cap@2.15.5) (2021-03-31)

**Note:** Version bump only for package @raisin/cap





## [2.15.4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.15.3...@raisin/cap@2.15.4) (2021-03-31)

**Note:** Version bump only for package @raisin/cap





## [2.15.3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.15.2...@raisin/cap@2.15.3) (2021-03-30)


### Bug Fixes

* **utils:** Fix logic about how to know if it is GBR or DEU based on the domain ([4ac627d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4ac627d84fa52c7955c2de7aa0e698f895c3beca))





## [2.15.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.15.1...@raisin/cap@2.15.2) (2021-03-29)

**Note:** Version bump only for package @raisin/cap





## [2.15.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.15.0...@raisin/cap@2.15.1) (2021-03-26)

**Note:** Version bump only for package @raisin/cap





# [2.15.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.14.0...@raisin/cap@2.15.0) (2021-03-26)


### Features

* **CAS:** Change endpoints for production envs ([f67ce8f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f67ce8fc802209c48cd501e3bf5e748bf64f9011))





# [2.14.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.13.1...@raisin/cap@2.14.0) (2021-03-26)


### Features

* **CAP:** Modifying staging pool id and dev namespace ([c6080ae](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c6080aef3846b3861717eda8ea9cd84347326350))
* **CAP:** Removing JSON.Stringify() and updating webpack accordingly ([e697931](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e6979318cf329ab1d23e9320b4a2daaca360236c))





## [2.13.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.13.0...@raisin/cap@2.13.1) (2021-03-25)

**Note:** Version bump only for package @raisin/cap





# [2.13.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.12.5...@raisin/cap@2.13.0) (2021-03-25)


### Features

* **New Relic:** Fix typo ([5625858](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/56258580a87a4074a137e1a4f88225bbbc89a43b))
* **NewRelic:** Add newrelic snippet ([cf7d40d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/cf7d40d258628776f7b43f04c38ea47ef9d13b19))
* **NewRelic:** Refactor to use function instead of class ([19c4c33](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/19c4c330a0918bee89fea68071ebd5dc2a916eec))
* **NewRelic:** Refactor to use function instead of class ([8e17627](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/8e176274ce5072b490c879e7d62f4ed99a56df16))





## [2.12.5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.12.4...@raisin/cap@2.12.5) (2021-03-25)

**Note:** Version bump only for package @raisin/cap





## [2.12.4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.12.3...@raisin/cap@2.12.4) (2021-03-25)

**Note:** Version bump only for package @raisin/cap





## [2.12.3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.12.2...@raisin/cap@2.12.3) (2021-03-25)

**Note:** Version bump only for package @raisin/cap





## [2.12.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.12.1...@raisin/cap@2.12.2) (2021-03-22)

**Note:** Version bump only for package @raisin/cap





## [2.12.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.12.0...@raisin/cap@2.12.1) (2021-03-18)

**Note:** Version bump only for package @raisin/cap





# [2.12.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.11.0...@raisin/cap@2.12.0) (2021-03-17)


### Features

* **CD Pipeline:** Dummy change to have the pipeline triggered ([a5066c0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a5066c004a613208b320b2dd90c6443174c55324))





# [2.11.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.10.1...@raisin/cap@2.11.0) (2021-03-11)


### Features

* **customers/cap:** Add staging token for api test on CORS issue and preview to MR ([9a65be1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9a65be1a783333dc3a0be67757e236384e831b82))
* **Docs:** updated text ([71f397c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/71f397cde191fd2b73177a25331735c07e1c4d53))





## [2.10.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.10.0...@raisin/cap@2.10.1) (2021-03-03)

**Note:** Version bump only for package @raisin/cap





# [2.10.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.9.0...@raisin/cap@2.10.0) (2021-03-03)


### Features

* **Logo:** added accessibility for logo ([8aa545b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/8aa545be5ded78d7c03e56ec1e8e35b1c336c894))
* **Logo:** added height for logo ([4fe3cd4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4fe3cd44af8a74da70df8c015d53e55c028d3934))
* **Logo:** added raisin logo for cap ([f458463](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f458463fadaae332bfe8334799a873722ab42ff6))
* **Logo:** customized width and height ([c2ac333](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c2ac333dd3651aa52cd01284bcf84c50e6ab8fde))





# [2.9.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.8.1...@raisin/cap@2.9.0) (2021-03-02)


### Bug Fixes

* **cap/palette:** Avoid error color override ([c4651e9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c4651e97089437855e572bd05b66120af52b4665))
* **cap/SessionControls:** Remove event listeners ([8ef5457](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/8ef5457b6239db29ff9ad7aa506e873c7cad3ee6))


### Features

* **cap/SessionControls:** Create timeout component to handle logout on inactivity ([a1f79d4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a1f79d4dc61681af69948f96da90c17f6d60eebb))





## [2.8.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.8.0...@raisin/cap@2.8.1) (2021-03-02)

**Note:** Version bump only for package @raisin/cap





# [2.8.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.7.0...@raisin/cap@2.8.0) (2021-03-01)


### Features

* **auth:** Add config file for mf, update webpack to work with this config file and removing .env file and references ([4c38561](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4c38561eff9360421cf56928e1e94cad1f46c59f))





# 2.7.0 (2021-02-25)


### Features

* **CAP:** renamed cap host to cap and fixed yarn script to start only cap packages ([f7bedc7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f7bedc73d333cd969744d560ee4320d1ba681cbf))





## [2.6.4](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.6.3...@raisin/cap@2.6.4) (2021-02-18)

**Note:** Version bump only for package @raisin/cap

## [2.6.3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.6.2...@raisin/cap@2.6.3) (2021-02-18)

**Note:** Version bump only for package @raisin/cap

## [2.6.2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.6.1...@raisin/cap@2.6.2) (2021-02-18)

**Note:** Version bump only for package @raisin/cap

## [2.6.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.6.0...@raisin/cap@2.6.1) (2021-02-16)

**Note:** Version bump only for package @raisin/cap

# [2.6.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.5.0...@raisin/cap@2.6.0) (2021-02-15)

### Bug Fixes

- **host:** Fix function name ([c58aa6c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c58aa6c9a2f64b3897d25e4e0a1bd57804aba858))

### Features

- **host:** Allow injecting reducers into store ([6694284](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/66942844e2d6be37b8eba66a376768c161742a24))

# [2.5.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.4.0...@raisin/cap@2.5.0) (2021-02-12)

### Features

- **Minor:** Remove fake commit ([01aa759](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/01aa75927be4b197687c7e17f5b29bc1fa2bce0c))

# [2.4.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.3.0...@raisin/cap@2.4.0) (2021-02-12)

### Features

- **host:** Adding msg ([3375678](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3375678e5897407a09c14e4a5f00d1e37f25b6d4))
- **host:** Change demo ([6262bdc](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/6262bdcbcecac79ea07f386b749b6f49ba3ba9b7))
- **host:** Change message ([83f59c6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/83f59c6414f1f239514002cd0a50562f6b460306))
- **host:** Demo host ([de7f4d1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/de7f4d144fd215dce791b435f25e00b6b552bb2b))
- **host:** Test ([e4e5d6e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e4e5d6e2c75a218feb95ff9e1aa23a98e84ffa5b))

# [2.3.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.2.0...@raisin/cap@2.3.0) (2021-02-12)

### Features

- **Colors:** fixed icons labels alignments ([3e890b2](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3e890b2d1e0844f6e2812a8d6b04a541c0bfafe8))
- **Colors:** merged latest master and fixed colors ([dbe365d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/dbe365da20c6b30255c29d6c3912cb03a217dddc))
- **MaterialUI:** fixed input component ([1db6f93](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1db6f935a4bb40f5c1dba03589b6a624c64f678a))
- **Minor:** fake commit ([39665c5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/39665c5b56ee8139060515e8dad92c4138c2a3ce))
- **Theme:** fixed theme configuration for Button and removed cap-ui-lib usage from MF ([8b1f0d5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/8b1f0d53ae3c6cc8eeca3c2ef9ba327824cdfbd2))

# [2.2.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.1.1...@raisin/cap@2.2.0) (2021-02-11)

### Bug Fixes

- **Locale:** Fix bug on lang switch ([f747c0d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f747c0d31efdb81face2fa1d09d677796aed59ce))

### Features

- **Layout:** Create Layout component and wrap Dashboard and CustomerData with it ([17bd393](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/17bd393ec3546581b52fb76bb6f6801c5d174aa1))

## [2.1.1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.1.0...@raisin/cap@2.1.1) (2021-02-09)

**Note:** Version bump only for package @raisin/cap

# [2.1.0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/compare/@raisin/cap@2.0.0...@raisin/cap@2.1.0) (2021-02-09)

### Features

- **materialui:** fixed lint issue ([385e383](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/385e38315620802bc4029602535e56f4463bb980))
- **materialUI:** fixed eslint errors ([0295b97](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/0295b976db6068e9f6f6753c982e7180de86fd67))
- **materialUI:** fixed maxwidth for containers ([5ed213f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/5ed213f46015221645e962cb00d4f1c5437e03e1))
- **materialUI:** rebased with master ([6162334](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/6162334db7435feb181c16d02651ac51d92c7424))
- **MaterialUi:** fixed color themes ([bfb0d37](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/bfb0d378cdc9acf66ca67a6e9dc4a6a3641d759d))
- **MaterialUI:** added size for spinner progress ([7c76714](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/7c76714fda824ca6569857bef80b6bc97346c060))
- **MaterialUI:** fixed lint issues ([abf31c0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/abf31c05d36945362ea1080f68b5b6e4b23232e0))
- **MaterialUI:** fixed shared context theme issue ([99f4dca](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/99f4dca1dccb58073c272f3feb196aeee10763ed))
- **MaterialUI:** fixed snackbar close action ([cf93d88](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/cf93d884370f00c7b87ae9ed9b3cd27103f70104))
- **MaterialUI:** fixed styling text issues ([d299299](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d299299d8811b85bc492afe2a313939815228d06))
- **MaterialUI:** fixed table & tab component on dashboard ([dda8be0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/dda8be07aae93d0936be456d39691f031445443c))
- **MaterialUI:** temp tabs introduced ([077e25b](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/077e25b74a64fc30ff6f104b05806b1767ab3832))

# 2.0.0 (2021-02-08)

### Bug Fixes

- **App:** reverting dotenv-webpack ([07a07b8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/07a07b8649e88111bb9bdee1ed5ac3e6ed5125c0))
- **Ci Pipeline:** Add url to the repository in package.json ([f0f7ac5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/f0f7ac5f3e7177f467017f36f864f2cad8e94a25))
- **Ci Pipeline:** Create NPM packages for all submodules ([512a802](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/512a802203a221e1ee5a418a42bbd4e9363bf31f))
- **husky:** config adapted to monorepo setup using version 5.0.6 of husky ([d1e5c58](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d1e5c588583cc57e6d24e88a36d4f479aff72db6))
- **husky config:** fixed double quotes to single quotes to make eslint stop complaining ([48cb60f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/48cb60fd4994d212b3696e1b62cbcc1a05f3bab9))
- **huskyconfig:** added proper commitizen config to husky for creating commit messages ([fd2cd71](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/fd2cd712befb279420ae336f9bf098d81e0a3f6c))

### Features

- **App:** Adding dotenv-webpack package and fixing unit test cases ([08471bd](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/08471bdd8577d77c0c310bcfc2f131457c9fdbff))
- **App:** adding Provider in test-utils for Host app ([0ebfa5e](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/0ebfa5ef6256d86b09f1aeeafe93289ec63fbdfc))
- **capfixes:** fixed config ([1208107](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/1208107ed65e194f0205b6ea2dc7af0dc8e08626))
- **capfixes:** fixed translations for cap search results page ([56ff3c8](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/56ff3c88b24561ae6e7cf18d30a0a992095d79d0))
- **capfixes:** minor change to retrigger build ([80cb187](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/80cb187eb0bfe409e15618222f8cea9298aca5ba))
- **capfixes:** moved reusable components to ui-lib ([d211477](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d21147791055d44774ebc82da6dac5adb059151e))
- **capfixes:** updated ui-lib version ([570de08](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/570de08b5579eaa0d936d87235c5a101a676bf51))
- **CD:** Integrate CAP with Amplify for Staging, Onboarding and Production ([171d0f3](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/171d0f3463853be52ba6cbfdbc37d45131df83f3))
- **csstokens:** Added css tokens based on current theme available ([a2c2294](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a2c22947d44e59325aefc6ceaac6da5441b18747))
- **CustomerData:** Add a new mf for CustomerData ([e6d5688](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/e6d5688f7dbb10202f85467ea538364805a047e6))
- **CustomerData:** Add route to the customer retail page ([a5a6a3a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/a5a6a3a62351f5cda39f2676bddf4a0d0f5d4d09))
- **Cypress:** Adding cypress for CAP ([4f2a9df](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/4f2a9df71abdc1c7b083bc46d01b9a8d19e5df23))
- **Cypress:** Adding review comments ([b863b09](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b863b09cc3383829a93e30bdec58dcfedd138b61))
- **Cypress:** Ignoring report files ([204d383](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/204d383eea10985c8f04a4cbcec2410eaf45bead))
- **Internationalisation:** Internationalisation setup for auth and host ([53f29ef](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/53f29ef177507dae40b28a399984d87014151761))
- **Notification:** Adding Notification component ([70870a1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/70870a175557ce2e387cad60ffaf97b81243d6d6))
- **Notification:** Adding notifications ([87fffc9](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/87fffc9d3db4644b2a075ff4d3e380bf379e6d33))
- **Notification:** Adding review comments ([dd84bfe](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/dd84bfe740dba553ad9a3e12d61ab7bc11b1cbb6))
- **Notification:** Modifying Notification logic ([73beb57](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/73beb573d1e4d11a9a2813ee98a7c3b8aa2538f9))
- **Notification:** Modifying Notification logic ([51820ea](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/51820ea7cc2cb3606787c6017ee93838fe51fd87))
- **precise-commits:** ignored json files for now ([d6505d5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/d6505d59f3fd66e6edc342048e161ea01a2be796))
- **Search:** Adding animation on Search component ([1227503](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/122750372be8915861613830aa24f84d040cb04a))
- **Search:** adding BAC formatter ([104da1f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/104da1fd86a11b6793e802e76ba9dc167709ac00))
- **Search:** Adding final api url ([41d073a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/41d073a1a119e6e0f2dcfe8fba77075770b4fce8))
- **Search:** adding interceptors for axios ([87275fa](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/87275fa50cca2ddeb3439c0a2c1a87ae6918de6e))
- **Search:** adding redux toolkit and adding logic to implement search api ([b318076](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b3180769567995c223cd6f00f7d8a7e498f4eec8))
- **Search:** Adding review changes ([b596273](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b596273ac01c13e580f7acc264dd1fcf73f6cb92))
- **Search:** Adding review changes ([53ad072](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/53ad072de64d7038b1f3cf74fffe43df5537f2ac))
- **Search:** Adding UI changes ([706d6e1](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/706d6e1f74c6101b8c83f1b41ed785d879ce1808))
- **searchresult:** cleaned up commented code ([2d414da](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/2d414da437344c8a99c4a265075c6030437518f2))
- **searchresult:** consuming search api and interceptors ([4390720](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/43907205cba654338b4537b52c9da3417203b1d4))
- **searchresult:** fixed result not found case to consume searched bac_number ([86fc7c7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/86fc7c7aa28f4ae6b92ab12e6da4272a7f6ad003))
- **searchresult:** implemented mocked tabs and table and spinner components ([b720677](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b7206779b679113e81ef6e7dfa858cd1866b0d88))
- **searchresult:** lint issues ([ef971e5](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/ef971e5fc4a100c4805da6c9112e15b905df9a6d))
- **searchresult:** merged with search-implementation ([9b66f1d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9b66f1d93ce3309ce609b0caf91270620e5928ab))
- **searchresult:** merged with search-implementation ([0a79d45](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/0a79d4559fa59c78ab01e0d1e4e4a6dc28bc7069))
- **StatusMessage:** Renaming notifications to statusmessage ([8c8fe6a](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/8c8fe6a91672a45e4406436577ed610577e2b715))
- **StatusMessage:** Renaming translation keys ([3579f4f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3579f4f9c1aa6f7da06c351bfd1ab005abe8c124))
- **StatusMessage:** Renaming translation keys ([3b9fd20](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/3b9fd2063aad95508d89fd887d1fa745f16476f4))
- **StatusMessage:** Renaming translation keys ([b55087d](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/b55087df19a44fc8fb71c6e3aa933f926b801c39))
- **theme:** camelCase css tokens ([44e0137](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/44e0137038982a963034ab6434824d5aa8c1fd9e))
- **theme:** updated css tokens for tab ([7dfffa6](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/7dfffa632947a3fa26dd0476b3260b5926c1bc6c))
- **translations:** welcome screen translations ([aa94cba](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/aa94cba97cc4879b74e0d17c0a68f98fdbeb52b9))
- **translations:** welcome screen translations ([caf985f](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/caf985f72f58e6988339536372d28ec887358dfa))
- **UserCard:** Add UserCard and UserInformation components ([48c140c](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/48c140c433b4ad802b798671dee4b13cedc0af17))
- **utils/customer:** Add user data formater function for customer retails component ([9efece7](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/9efece71d2bdc93bbcac4b9238c0c038a8626a58))
- **validations:** Added button disabled for search bar ([7360ae0](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/7360ae03569f34283c765f3c404b3663f31d3f2a))
- **validations:** cleaned up theme ([c48c809](https://gitlab.com/raisin-global/raisin-gmbh/frontend/cap/cap/commit/c48c809f5b6ac99920583c291212f540bd188e16))

### BREAKING CHANGES

- **husky:** husky uses scripts from .husky directory now
