import CustomerManagementPage from './CustomerManagementPage';

class CustomerSearchPage {
  visit() {
    cy.visitCap('/');

    return this;
  }

  getSearchBacNumber() {
    return cy.get('[data-testid="searchBacNumber"]');
  }

  getClearIcon() {
    return cy.get('[data-testid="ClearIcon"]');
  }

  searchCustomerByBacNumber(bacNumber) {
    this.getSearchBacNumber().clear().type(bacNumber);
    this.clickSubmitButton();

    return this;
  }

  getSubmitButton() {
    return cy.get('[data-testid="submitButton"]');
  }

  clickSubmitButton() {
    this.getSubmitButton().click();

    return this;
  }

  getCustomerListHeaders() {
    return cy.get('[data-testid=table-header-bac_number]');
  }

  getCustomerListValues() {
    return cy.get('[data-testid="table-body"] td.MuiTableCell-root');
  }

  clickCustomerFromResults() {
    this.getCustomerListValues().eq(1).click();

    return new CustomerManagementPage();
  }

  getErrorMessage() {
    return cy.get('[data-testid="errorMessage"]');
  }
}

export default CustomerSearchPage;
