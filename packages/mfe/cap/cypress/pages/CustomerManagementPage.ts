class CustomerManagementPage {
  getCustomerDetails() {
    return cy.get('[data-testid="customer_details"]');
  }

  getCustomerLogin() {
    return cy.get('[data-testid="login"]');
  }

  getCustomerAddress() {
    return cy.get('[data-testid="address"]');
  }
}

export default CustomerManagementPage;
