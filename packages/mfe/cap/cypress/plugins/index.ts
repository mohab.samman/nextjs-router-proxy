/* eslint-disable no-unused-vars */
/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

import aws from 'aws-sdk';
import cypressOtp from 'cypress-otp';
require('dotenv').config();

/**
 * @type {Cypress.PluginConfig}
 */

async function oktaCredentials() {
  const secretId = 'okta/test-user';
  const client = new aws.SecretsManager({ region: 'eu-central-1' });
  const secret = await client.getSecretValue({ SecretId: secretId }).promise();
  return JSON.parse(JSON.stringify(secret.SecretString));
}

module.exports = async (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  on('task', {
    generateOTP: cypressOtp,
  });
  if (process.env.ci === 'false') {
    const okta = {
      username: process.env.OKTA_USERNAME,
      password: process.env.OKTA_PASSWORD,
      key: process.env.OKTA_KEY,
    };
    // todo override for local cypress tests
    const baseUrl = 'http://localhost:3002';
    return { ...config, okta, baseUrl };
  }
  const credentials = await oktaCredentials();
  const secrets = JSON.parse(credentials);
  const okta = {
    username: secrets.okta_username,
    password: secrets.okta_password,
    key: secrets.okta_key,
  };
  return { ...config, okta };
};
