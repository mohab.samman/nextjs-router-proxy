// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('oktaSignIn', (username, password, key) => {
  cy.task('generateOTP', key).then((token) => {
    cy.visit('/');
    cy.get('[data-testid="signInButton"]').click();
    cy.get('#okta-signin-username').type(username);
    cy.get('#okta-signin-password').type(password);
    cy.get('#okta-signin-submit').click();
    cy.get('#input10').type(token);
    cy.get('[data-type="save"]').click();
  });
});

Cypress.Commands.add('visitCap', () => {
  cy.visit('/');
  cy.get('body').then(($body) => {
    if ($body.find('[data-testid="signInButton"]').length) {
      cy.get('[data-testid="signInButton"]').click();
    }
  });
});
