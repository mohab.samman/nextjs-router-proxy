/**
 * Application Variables
 * These variables are used to run the project locally and in the GitLab pipeline.
 */

const { APP_LOCALE, STACK_API_KEY, STACK_DELIVERY_TOKEN, STACK_ENVIRONMENT } = process.env;

const NAME = 'cap';
const MFE_NAME = 'cap';
const APP_PORT = '3002';
const APP_HOST = 'localhost';
const ENVIRONMENT = 'local';
const LOCALE = APP_LOCALE || 'en-US';

/**
 * Environments
 * These are the environments where the application runs:
 * For each environment there are different values, urls, endpoints and domains.
 * Staging, Onboarding and Production env configs are necessary to deploy the project with the GitLab pipeline.
 * Development env config is used only to run the project locally.
 */
const environments = {
  local: {
    publicPath: '/',

    apiBaseUrl: {
      DEU: 'https://internal-api.dev-weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.dev-raisin.co.uk/cas/v1/',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  development: {
    publicPath: `https://mfe-${MFE_NAME}.amplifyapp.com/`,

    apiBaseUrl: {
      DEU: 'https://internal-api.dev-weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.dev-raisin.co.uk/cas/v1/',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  staging: {
    publicPath: `https://mfe-${MFE_NAME}.testraisin.com/`,

    apiBaseUrl: {
      DEU: 'https://internal-api.testweltsparen.de/cas/v1/',
      GBR: 'https://internal-api.testraisin.co.uk/cas/v1/',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  onboarding: {
    publicPath: `https://mfe-${MFE_NAME}.onboarding-raisin.com/`,

    apiBaseUrl: {
      DEU: 'https://internal-api.onboarding-weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.onboarding-raisin.co.uk/cas/v1/',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
  production: {
    publicPath: `https://mfe-${MFE_NAME}.raisin.com/`,

    apiBaseUrl: {
      DEU: 'https://internal-api.weltsparen.de/cas/v1/',
      GBR: 'https://internal-api.raisin.co.uk/cas/v1/',
    },

    contentStack: {
      stackAPIKey: STACK_API_KEY,
      stackDeliveryToken: STACK_DELIVERY_TOKEN,
      environment: STACK_ENVIRONMENT,
    },
  },
};

/**
 * Configuration
 * The config object is used internally for the project.
 * The object has the  following section:
 * name: µFE name
 * port: On what port should the project run locally
 * host: URL for the application, localhost for development,
 * globals: Object used for injecting global values into the application.
 */
const config = {
  name: NAME,
  port: APP_PORT,
  host: APP_HOST,

  globals: {
    contentStack: environments[ENVIRONMENT].contentStack,
    locale: LOCALE,
    apiBaseUrlGBR: environments[ENVIRONMENT].apiBaseUrl.GBR,
    apiBaseUrlDEU: environments[ENVIRONMENT].apiBaseUrl.DEU,
  },
  publicPath: environments[ENVIRONMENT].publicPath,
};

module.exports.config = config;
