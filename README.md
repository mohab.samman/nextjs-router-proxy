# Frontend Monorepo

This repository contains all the Raisin Web Portals and their subsequent microfrontends (µFEs) as well as all the UI Libraries and Specific components NPM packages. The µFEs are set up with [Next.js](https://nextjs.org/)

### Prerequisites

- run `nvm install node` to install latest node version (you'll need [nvm](https://github.com/nvm-sh/nvm/blob/master/README.md) for this)
- run `nvm use node` to use it
- run `npm install -g yarn@1.22.10`
- run `yarn global add husky@5.0.6`

### Getting started

- clone git repo with `git@gitlab.com:raisin-global/raisin-gmbh/frontend/frontend-monorepo.git`
- run `yarn` to install packages dependencies
- run `yarn start:<portal-name>` (e.g: run `yarn start:cap`) and you will get the web portal started with all its µFEs running
- alternatively, you can navigate manually to an `mfe` and start it using its `yarn start` command 

# Microfrontends

In this section you can find relevant info on µFEs inside the fe-monorepo

### MFEPorts.json

This file helps us keep an evidence of local development ports used by each µFE. The file is consumed by the `next-mf-generator` during a µFE generation to identify which is the next port a µFE should have when running locally, and updated with the new µFE name and port at the same time.

## Microfrontends creation

To generate a new µFE, use our [next-mf-generator](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-tools/next-mf-generator-cli) tool. All information on how to do that is provided in the project [README.md](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-tools/next-mf-generator-cli/-/blob/master/README.md). After the generation is complete, return to this document and continue with the section below

### Making sure everything works correctly

1. Navigate to `portalsConfig.js` at the root of the `fe-monorepo`. Define your µFE name and then add it to the array of the belonging portal. Like this, you can run your new µFE together with your portal locally by using the portal `yarn start` command. e.g `yarn run start:cap`

> **Info** If the µFE you generated is a portal, you should as well export from the same file a portal identifier name followed by an array with the belonging µFEs (e.g.`cp: [MF_CUSTOMER_PORTAL]`). Then in `package.json` you should define a new start command under `scripts`. This would be the same as for the other portals, but passing as an argument your portal identifier name instead.

2. If you did not install dependencies during the µFE generation, you should install them by navigating to the new µFE and running `yarn install`. Or just run `yarn install` at the root level of the `fe-monorepo`. Yarn workspaces will take care of installing all missing dependencies in all µFEs.
3. Also from the root of the monorepo, start your µFE using the start command associated with your portal/µFE. Alternatively, you can also start it directly from your new µFE folder as a standalone by running `yarn start`

### Steps to successfully integrate a µFE into a portal

In order to successfully set up and integrate your new µFE, there are a few steps left:

1.  Defining **CODEOWNERS**  
    You will need to define the CODEOWNERS responsible for the µFE you just created.
    To set up, navigate locally to the [CODEOWNERS](https://gitlab.com/raisin-global/raisin-gmbh/frontend/frontend-monorepo/-/blob/master/CODEOWNERS) file present at the root of this project. Declare the path to the µFE you just created and tag the responsible contributors.
    CODEOWNERS.
    e.g.`packages/mfe/cap @vishal.rajole`
    > **Info**: This step is required to provide ownership over each µFE and will require the code owners to approve all MR opened for that µFE.
2.  **Integrate** your µFE into the Web Portal  
    **WIP**: will be reworked once [Next.js MultiZones](https://nextjs.org/docs/advanced-features/multi-zones) features is clarified and used in one of the portals 
    > **Info**: This step will enable amplify to identify and run all µFE belonging to a portal when deployed

## Portals

List of portals with links to their Wiki Space

- [CAP - Customer Admin Portal](https://raisin-jira.atlassian.net/wiki/spaces/RXTS/pages/1440317613/CAP+Customer+Administrator+Portal)
- [CP - Customer Portal](https://raisin-jira.atlassian.net/wiki/spaces/OA/pages/1099923558/Customer+Portal)

## How to create and/or update resources on Transifex for µFEs

This information is available in the Readme.md file of your µFE. You can also find additional information on [translations and Transifex in our Wiki](https://raisin-jira.atlassian.net/wiki/spaces/OA/pages/1914765670/Translations)

## Dependencies

Each team is free to update the dependencies in the µFEs they own as they wish, with one caveat: `webpack` version needs to be synced across all µFEs and libs inside this monorepo. We are using `yarn workspaces` which hoists shared dependencies at the root level. Webpack has an issue with this when it comes to linking files in its internals. You can read more about it [here](https://raisin-jira.atlassian.net/browse/OA-6701)

Currently, the webpack version shared across all µFEs/libs is: `5.23.0`.

## Read/References
- To read more about the Raisin Monorepo setup check out [Monorepo setup](https://raisin-jira.atlassian.net/wiki/spaces/OA/pages/1914962279/Monorepo)

- For the entire documentation regarding the Web Portals Framework, have a look at [Raisin Frontend Framework](https://raisin-jira.atlassian.net/wiki/spaces/OA/pages/1533313404/WIP+Raisin+Frontend+Framework+RFF)

- [Monorepo Decision](https://raisin-jira.atlassian.net/wiki/spaces/ENGPEOP/pages/755564662/Decision+Monorepo+for+New+Raisin+Frontend+Code)

- [Frontend Decisions](https://raisin-jira.atlassian.net/wiki/spaces/ENGPEOP/pages/96927972/Frontend%2BDecisions)

- [Deployment Flow](https://miro.com/app/board/o9J_lXqOVJ0=/)

- [Domain Conventions](https://raisin-jira.atlassian.net/wiki/spaces/CLOUDAUTO/pages/1424490636/Domain+convention+proposal)

- babel config is setup at project root due to monorepo setup [babel rootmode config for Monorepo](https://babeljs.io/docs/en/options#rootmode)

- [Babel monorepo](https://babeljs.io/docs/en/config-files#monorepos)

## Troubleshooting

Nothing here yet :)
