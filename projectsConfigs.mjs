import fs from "fs";
import path from "path";
const PROJECT_DIRECTORY = "frontend-monorepo";

/*
 * This script could be either running in the root of the monorepo
 * or inside a lib / mfe. That's why the ROOT_DIRECTORY needs to be dynamic
 */

const ROOT_DIRECTORY =
  process.cwd().split("/").pop() === PROJECT_DIRECTORY
    ? process.cwd()
    : path.resolve(process.cwd(), "../../../");

const libsFolder = path.resolve(ROOT_DIRECTORY, "packages/lib");

const MF_CAP = "cap";
const MF_CUSTOMER_PORTAL = "customer-portal";
const MF_IPP_CP = "ipp-cp";

const libs = [];

fs.readdirSync(libsFolder).forEach((fileName) => libs.push(fileName));

export default {
  cap: [MF_CAP],
  cp: [MF_CUSTOMER_PORTAL],
  'ipp-cp': [MF_IPP_CP],
  libs,
};
