module.exports = {
	presets: [['@babel/preset-env', { modules: false }], '@babel/preset-react'],
	// enables us to create custom .babelrc configs for µFE
	babelrcRoots: ['.', 'packages/*'],
	plugins: [
		'react-hot-loader/babel',
		'@babel/plugin-proposal-class-properties',
		'@babel/plugin-syntax-dynamic-import',
		[
			'babel-plugin-styled-components',
			{
				displayName: true,
				minify: false,
				transpileTemplateLiterals: false,
				pure: true,
			},
		],
	],

	env: {
		test: {
			plugins: ['@babel/plugin-transform-modules-commonjs', 'dynamic-import-node'],
		},
	},
};
